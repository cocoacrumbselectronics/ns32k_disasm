use clap::value_t;
use clap::{App, Arg};

use std::fs::metadata;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use ns32k_disasm::Ns32kDisassembler;

fn load_binary_file_in_vector(path: &PathBuf) -> std::result::Result<Vec<u8>, std::io::Error> {
    let mut f = File::open(&path)?; //.expect("no file found");
    let metadata = metadata(&path)?; //.expect("unable to read metadata");
    let mut buffer = vec![0; metadata.len() as usize];
    f.read(&mut buffer)?; //.expect("buffer overflow");

    Ok(buffer)
} /* end load_binary_file_in_vector */

fn main() -> std::result::Result<(), std::io::Error> {
    let matches = App::new("NS32000 disassembler")
        .version("0.1")
        .author("Cocoacrumbs <cocoacrumbs@telenet.be>")
        .about("Disassembles a binary into NS32000 mnenomics.")
        .arg(
            Arg::with_name("disassembleFrom")
                .short("d")
                .long("disassembleFrom")
                .help("Specifies from which offset in the file to start the disassembly.")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("length")
                .short("l")
                .long("length")
                .help("Specifies the number of bytes to disassemble.")
                .takes_value(true),
        )
        .arg(Arg::with_name("INPUT").help("Sets the input file to use.").required(true).index(1))
        .get_matches();

    // Calling .unwrap() is safe here because "INPUT" is required.
    let filename = PathBuf::from(matches.value_of("INPUT").unwrap());
    println!("Using input file: {:?}", filename);

    let start_address: usize = value_t!(matches.value_of("disassembleFrom"), usize).unwrap_or(0);
    println!("start address: {:?}", start_address);

    let bytes = load_binary_file_in_vector(&filename)?;

    let mut my_disassembler = Ns32kDisassembler::new(&bytes, start_address);

    while my_disassembler.get_program_counter() < bytes.len() {
        // for _elem in 0..=1 {
        let (address_string, hex_string, instruction_string) = my_disassembler.disassemble_next_instruction()?;
        my_disassembler.print_instruction_to_screen(address_string, hex_string, instruction_string);
    } /* end while */
    Ok(())
} /* end main */
