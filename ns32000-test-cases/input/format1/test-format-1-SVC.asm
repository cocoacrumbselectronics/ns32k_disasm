.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: SVC
#
# Syntax:   SVC
#
#           !      SVC      !
#           +---------------+
#           !1 1 1 0 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The SVC instruction activates the Supervisor Call Trap (SVC). The Supervisor
# Call Trap passes program execution control to the SVC service procedure. The
# return address pushed onto the Interrupt Stack is the address of the SVC instruc-
# tion itself.
#
    svc
#
end:
