.psize 0, 143   # suppress form feed, 143 columns
#
#
# Test case for FORMAT 2 instructions
#
# Instruction under test: ADDQi
#
# Syntax:   ADDQi   src     dest
#                   quick   gen
#                           rmw.i
#
#           !  dest   !  src  !     ADDQi   !
#           +---------+-------+---------+---+
#           !   gen   ! quick !0 0 0 1 1! i !
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# The ADDQi instruction adds the src and dest operands and places the result in the
# dest operand location. Before the addition is performed, src is sign-extended to
# the length of dest.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
# ADD quick BYTE 
#
#
# Register
#
    addqb   0, r0
    addqb   -8, r0
    addqb   1, r1
    addqb   -7, r1
    addqb   2, r2
    addqb   -6, r2
    addqb   3, r3
    addqb   -5, r3
    addqb   4, r4
    addqb   -4, r4
    addqb   5, r5
    addqb   -3, r5
    addqb   6, r6
    addqb   -2, r6
    addqb   7, r7
    addqb   -1, r7
#
# Register relative
#
    addqb   7, 63(r0)
    addqb   -8, -64(r0)
    addqb   7, 8191(r0)
    addqb   -8, -8192(r0)
    addqb   7, 536870911(r0)
    addqb   -8, -536870912(r0)
    #
    addqb   7, 63(r1)
    addqb   -8, -64(r1)
    addqb   7, 8191(r1)
    addqb   -8, -8192(r1)
    addqb   7, 536870911(r1)
    addqb   -8, -536870912(r1)
    #
    addqb   7, 63(r2)
    addqb   -8, -64(r2)
    addqb   7, 8191(r2)
    addqb   -8, -8192(r2)
    addqb   7, 536870911(r2)
    addqb   -8, -536870912(r2)
    #
    addqb   7, 63(r3)
    addqb   -8, -64(r3)
    addqb   7, 8191(r3)
    addqb   -8, -8192(r3)
    addqb   7, 536870911(r3)
    addqb   -8, -536870912(r3)
    #
    addqb   7, 63(r4)
    addqb   -8, -64(r4)
    addqb   7, 8191(r4)
    addqb   -8, -8192(r4)
    addqb   7, 536870911(r4)
    addqb   -8, -536870912(r4)
    #
    addqb   7, 63(r5)
    addqb   -8, -64(r5)
    addqb   7, 8191(r5)
    addqb   -8, -8192(r5)
    addqb   7, 536870911(r5)
    addqb   -8, -536870912(r5)
    #
    addqb   7, 63(r6)
    addqb   -8, -64(r6)
    addqb   7, 8191(r6)
    addqb   -8, -8192(r6)
    addqb   7, 536870911(r6)
    addqb   -8, -536870912(r6)
    #
    addqb   7, 63(r7)
    addqb   -8, -64(r7)
    addqb   7, 8191(r7)
    addqb   -8, -8192(r7)
    addqb   7, 536870911(r7)
    addqb   -8, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    addqb   7, -64(63(fp))
    addqb   -8, -64(63(fp))
    addqb   7, 63(-64(fp))
    addqb   -8, 63(-64(fp))
    #
    addqb   7, -8192(63(fp))
    addqb   -8, -64(8191(fp))
    addqb   7, 63(-8192(fp))
    addqb   -8, 8191(-64(fp))
    #
    addqb   7, -8192(8191(fp))
    addqb   -8, -8192(8191(fp))
    addqb   7, 8191(-8192(fp))
    addqb   -8, 8191(-8192(fp))
    #
    addqb   7, -536870912(8191(fp))
    addqb   -8, -8192(536870911(fp))
    addqb   7, 8191(-536870912(fp))
    addqb   -8, 536870911(-8192(fp))
    #
    addqb   7, 536870911(-536870912(fp))
    addqb   -8, 536870911(-536870912(fp))
    addqb   7, -536870912(536870911(fp))
    addqb   -8, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    addqb   7, -64(63(sp))
    addqb   -8, -64(63(sp))
    addqb   7, 63(-64(sp))
    addqb   -8, 63(-64(sp))
    #
    addqb   7, -8192(63(sp))
    addqb   -8, -64(8191(sp))
    addqb   7, 63(-8192(sp))
    addqb   -8, 8191(-64(sp))
    #
    addqb   7, -8192(8191(sp))
    addqb   -8, -8192(8191(sp))
    addqb   7, 8191(-8192(sp))
    addqb   -8, 8191(-8192(sp))
    #
    addqb   7, -536870912(8191(sp))
    addqb   -8, -8192(536870911(sp))
    addqb   7, 8191(-536870912(sp))
    addqb   -8, 536870911(-8192(sp))
    #
    addqb   7, 536870911(-536870912(sp))
    addqb   -8, 536870911(-536870912(sp))
    addqb   7, -536870912(536870911(sp))
    addqb   -8, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    addqb   7, -64(63(sb))
    addqb   -8, -64(63(sb))
    addqb   7, 63(-64(sb))
    addqb   -8, 63(-64(sb))
    #
    addqb   7, -8192(63(sb))
    addqb   -8, -64(8191(sb))
    addqb   7, 63(-8192(sb))
    addqb   -8, 8191(-64(sb))
    #
    addqb   7, -8192(8191(sb))
    addqb   -8, -8192(8191(sb))
    addqb   7, 8191(-8192(sb))
    addqb   -8, 8191(-8192(sb))
    #
    addqb   7, -536870912(8191(sb))
    addqb   -8, -8192(536870911(sb))
    addqb   7, 8191(-536870912(sb))
    addqb   -8, 536870911(-8192(sb))
    #
    addqb   7, 536870911(-536870912(sb))
    addqb   -8, 536870911(-536870912(sb))
    addqb   7, -536870912(536870911(sb))
    addqb   -8, -536870912(536870911(sb))

#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    addqb	7, @-64
    addqb	-8, @-64
    addqb	7, @63
    addqb	-8, @63
    addqb	7, @-65
    addqb	-8, @-65
    addqb	7, @64
    addqb	-8, @64
    #
    addqb	7, @-8192
    addqb	-8, @-8192
    addqb	7, @8191
    addqb	-8, @8191
    addqb	7, @-8193
    addqb	-8, @-8193
    addqb	7, @8192
    addqb	-8, @8192
    #
    addqb	7, @-536870912
    addqb	-8, @536870911
#
# External
#
    addqb	7, ext(-64) + 63
    addqb	-8, ext(-64) + 63
    addqb	7, ext(63) + -64
    addqb	-8, ext(63) + -64
    #
    addqb	7, ext(-65) + 63
    addqb	-8, ext(-65) + 63
    addqb	7, ext(63) + -65
    addqb	-8, ext(63) + -65
    addqb	7, ext(-64) + 64
    addqb	-8, ext(-64) + 64
    addqb	7, ext(64) + -64
    addqb	-8, ext(64) + -64
    #
    addqb	7, ext(-8192) + 8191
    addqb	-8, ext(-8192) + 8191
    addqb	7, ext(8191) + -8192
    addqb	-8, ext(8191) + -8192
    #
    addqb	7, ext(-536870912) + 8191
    addqb	-8, ext(-536870912) + 8191
    addqb	7, ext(8191) + -536870912
    addqb	-8, ext(8191) + -536870912
    addqb	7, ext(-8192) + 536870911
    addqb	-8, ext(-8192) + 536870911
    addqb	7, ext(536870911) + -8192
    addqb	-8, ext(536870911) + -8192
    #
    addqb	7, ext(-536870912) + 536870911
    addqb	-8, ext(-536870912) + 536870911
    addqb	7, ext(536870911) + -536870912
    addqb	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    addqb	7, tos
    addqb	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    addqb	7, -64(fp)
    addqb	-8, -64(fp)
    addqb	7, 63(fp)
    addqb	-8, 63(fp)
    #
    addqb	7, -8192(fp)
    addqb	-8, -8192(fp)
    addqb	7, 8191(fp)
    addqb	-8, 8191(fp)
    #
    addqb	7, -536870912(fp)
    addqb	-8, -536870912(fp)
    addqb	7, 536870911(fp)
    addqb	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    addqb	7, -64(sp)
    addqb	-8, -64(sp)
    addqb	7, 63(sp)
    addqb	-8, 63(sp)
    #
    addqb	7, -8192(sp)
    addqb	-8, -8192(sp)
    addqb	7, 8191(sp)
    addqb	-8, 8191(sp)
    #
    addqb	7, -536870912(sp)
    addqb	-8, -536870912(sp)
    addqb	7, 536870911(sp)
    addqb	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    addqb	7, -64(sb)
    addqb	-8, -64(sb)
    addqb	7, 63(sb)
    addqb	-8, 63(sb)
    #
    addqb	7, -8192(sb)
    addqb	-8, -8192(sb)
    addqb	7, 8191(sb)
    addqb	-8, 8191(sb)
    #
    addqb	7, -536870912(sb)
    addqb	-8, -536870912(sb)
    addqb	7, 536870911(sb)
    addqb	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    addqb	7, -64(pc)
    addqb	-8, -64(pc)
    addqb	7, 63(pc)
    addqb	-8, 63(pc)
    #
    addqb	7, -8192(pc)
    addqb	-8, -8192(pc)
    addqb	7, 8191(pc)
    addqb	-8, 8191(pc)
    #
    addqb	7, -536870912(pc)
    addqb	-8, -536870912(pc)
    addqb	7, 536870911(pc)
    addqb	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    addqb   7, r7[r0:b]
    addqb   -8, r6[r1:b]
    #
    # Register Relative
    addqb   7, 63(r5)[r2:b]
    addqb   -8, 63(r4)[r3:b]
    addqb   7, 8191(r3)[r4:b]
    addqb   -8, 8191(r2)[r5:b]
    addqb   7, 536870911(r1)[r6:b]
    addqb   -8, 536870911(r0)[r7:b]
    #
    # Memory Relative
    addqb   7, -64(63(fp))[r0:b]
    addqb   -8, -64(63(fp))[r1:b]
    addqb   7, -8192(8191(fp))[r2:b]
    addqb   -8, -8192(8191(fp))[r3:b]
    addqb   7, -536870912(536870911(fp))[r4:b]
    addqb   -8, -536870912(536870911(fp))[r5:b]
    addqb   7, -64(63(sp))[r7:b]
    addqb   -8, -64(63(sp))[r6:b]
    addqb   7, -8192(8191(sp))[r5:b]
    addqb   -8, -8192(8191(sp))[r4:b]
    addqb   7, -536870912(536870911(sp))[r3:b]
    addqb   -8, -536870912(536870911(sp))[r2:b]
    addqb   7, -64(63(sb))[r0:b]
    addqb   -8, -64(63(sb))[r1:b]
    addqb   7, -8192(8191(sb))[r2:b]
    addqb   -8, -8192(8191(sb))[r3:b]
    addqb   7, -536870912(536870911(sb))[r4:b]
    addqb   -8, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    addqb   7, @63[r0:b]
    addqb   -8, @63[r1:b]
    addqb   7, @8191[r2:b]
    addqb   -8, @8191[r3:b]
    addqb   7, @536870911[r4:b]
    addqb   -8, @536870911[r5:b]
    #
    # External
    addqb   7, ext(-64) + 63[r7:b]
    addqb   -8, ext(-64) + 63[r6:b]
    addqb   7, ext(-8192) + 8191[r5:b]
    addqb   -8, ext(-8192) + 8191[r4:b]
    addqb   7, ext(-536870912) + 536870911[r3:b]
    addqb   -8, ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    addqb   7, tos[r0:b]
    addqb   -8, tos[r7:b]
    #
    # Memory Space
    addqb   7, 63(fp)[r0:b]
    addqb   -8, 63(fp)[r0:b]
    addqb   7, 8191(fp)[r0:b]
    addqb   -8, 8191(fp)[r0:b]
    addqb   7, 536870911(fp)[r0:b]
    addqb   -8, 536870911(fp)[r0:b]
    addqb   7, 63(sp)[r0:b]
    addqb   -8, 63(sp)[r0:b]
    addqb   7, 8191(sp)[r0:b]
    addqb   -8, 8191(sp)[r0:b]
    addqb   7, 536870911(sp)[r0:b]
    addqb   -8, 536870911(sp)[r0:b]
    addqb   7, 63(sb)[r0:b]
    addqb   -8, 63(sb)[r0:b]
    addqb   7, 8191(sb)[r0:b]
    addqb   -8, 8191(sb)[r0:b]
    addqb   7, 536870911(sb)[r0:b]
    addqb   -8, 536870911(sb)[r0:b]
    addqb   7, 63(pc)[r0:b]
    addqb   -8, 63(pc)[r0:b]
    addqb   7, 8191(pc)[r0:b]
    addqb   -8, 8191(pc)[r0:b]
    addqb   7, 536870911(pc)[r0:b]
    addqb   -8, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    addqb   7, r1[r0:w]
    addqb   -8, r1[r0:w]
    #
    # Register Relative
    addqb   7, 63(r1)[r0:w]
    addqb   -8, 63(r1)[r0:w]
    addqb   7, 8191(r1)[r0:w]
    addqb   -8, 8191(r1)[r0:w]
    addqb   7, 536870911(r1)[r0:w]
    addqb   -8, 536870911(r1)[r0:w]
    #
    # Memory Relative
    addqb   7, -64(63(fp))[r0:w]
    addqb   -8, -64(63(fp))[r1:w]
    addqb   7, -8192(8191(fp))[r2:w]
    addqb   -8, -8192(8191(fp))[r3:w]
    addqb   7, -536870912(536870911(fp))[r4:w]
    addqb   -8, -536870912(536870911(fp))[r5:w]
    addqb   7, -64(63(sp))[r7:w]
    addqb   -8, -64(63(sp))[r6:w]
    addqb   7, -8192(8191(sp))[r5:w]
    addqb   -8, -8192(8191(sp))[r4:w]
    addqb   7, -536870912(536870911(sp))[r3:w]
    addqb   -8, -536870912(536870911(sp))[r2:w]
    addqb   7, -64(63(sb))[r0:w]
    addqb   -8, -64(63(sb))[r1:w]
    addqb   7, -8192(8191(sb))[r2:w]
    addqb   -8, -8192(8191(sb))[r3:w]
    addqb   7, -536870912(536870911(sb))[r4:w]
    addqb   -8, -536870912(536870911(sb))[r5:w]
    #
    # Absolute
    addqb   7, @63[r0:w]
    addqb   -8, @63[r0:w]
    addqb   7, @8191[r0:w]
    addqb   -8, @8191[r0:w]
    addqb   7, @536870911[r0:w]
    addqb   -8, @536870911[r0:w]
    #
    # External
    addqb   7, ext(-64) + 63[r0:w]
    addqb   -8, ext(-64) + 63[r0:w]
    addqb   7, ext(-8192) + 8191[r0:w]
    addqb   -8, ext(-8192) + 8191[r0:w]
    addqb   7, ext(-536870912) + 536870911[r0:w]
    addqb   -8, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    addqb   7, tos[r0:w]
    addqb   -8, tos[r0:w]
    #
    # Memory Space
    addqb   7, 63(fp)[r0:w]
    addqb   -8, 63(fp)[r0:w]
    addqb   7, 8191(fp)[r0:w]
    addqb   -8, 8191(fp)[r0:w]
    addqb   7, 536870911(fp)[r0:w]
    addqb   -8, 536870911(fp)[r0:w]
    addqb   7, 63(sp)[r0:w]
    addqb   -8, 63(sp)[r0:w]
    addqb   7, 8191(sp)[r0:w]
    addqb   -8, 8191(sp)[r0:w]
    addqb   7, 536870911(sp)[r0:w]
    addqb   -8, 536870911(sp)[r0:w]
    addqb   7, 63(sb)[r0:w]
    addqb   -8, 63(sb)[r0:w]
    addqb   7, 8191(sb)[r0:w]
    addqb   -8, 8191(sb)[r0:w]
    addqb   7, 536870911(sb)[r0:w]
    addqb   -8, 536870911(sb)[r0:w]
    addqb   7, 63(pc)[r0:w]
    addqb   -8, 63(pc)[r0:w]
    addqb   7, 8191(pc)[r0:w]
    addqb   -8, 8191(pc)[r0:w]
    addqb   7, 536870911(pc)[r0:w]
    addqb   -8, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    addqb   7, r1[r0:d]
    addqb   -8, r1[r0:d]
    #
    # Register Relative
    addqb   7, 63(r1)[r0:d]
    addqb   -8, 63(r1)[r0:d]
    addqb   7, 8191(r1)[r0:d]
    addqb   -8, 8191(r1)[r0:d]
    addqb   7, 536870911(r1)[r0:d]
    addqb   -8, 536870911(r1)[r0:d]
    #
    # Memory Relative
    addqb   7, -64(63(fp))[r0:d]
    addqb   -8, -64(63(fp))[r1:d]
    addqb   7, -8192(8191(fp))[r2:d]
    addqb   -8, -8192(8191(fp))[r3:d]
    addqb   7, -536870912(536870911(fp))[r4:d]
    addqb   -8, -536870912(536870911(fp))[r5:d]
    addqb   7, -64(63(sp))[r7:d]
    addqb   -8, -64(63(sp))[r6:d]
    addqb   7, -8192(8191(sp))[r5:d]
    addqb   -8, -8192(8191(sp))[r4:d]
    addqb   7, -536870912(536870911(sp))[r3:d]
    addqb   -8, -536870912(536870911(sp))[r2:d]
    addqb   7, -64(63(sb))[r0:d]
    addqb   -8, -64(63(sb))[r1:d]
    addqb   7, -8192(8191(sb))[r2:d]
    addqb   -8, -8192(8191(sb))[r3:d]
    addqb   7, -536870912(536870911(sb))[r4:d]
    addqb   -8, -536870912(536870911(sb))[r5:d]
    #
    # Absolute
    addqb   7, @63[r0:d]
    addqb   -8, @63[r0:d]
    addqb   7, @8191[r0:d]
    addqb   -8, @8191[r0:d]
    addqb   7, @536870911[r0:d]
    addqb   -8, @536870911[r0:d]
    #
    # External
    addqb   7, ext(-64) + 63[r0:d]
    addqb   -8, ext(-64) + 63[r0:d]
    addqb   7, ext(-8192) + 8191[r0:d]
    addqb   -8, ext(-8192) + 8191[r0:d]
    addqb   7, ext(-536870912) + 536870911[r0:d]
    addqb   -8, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    addqb   7, tos[r0:d]
    addqb   -8, tos[r0:d]
    #
    # Memory Space
    addqb   7, 63(fp)[r0:d]
    addqb   -8, 63(fp)[r0:d]
    addqb   7, 8191(fp)[r0:d]
    addqb   -8, 8191(fp)[r0:d]
    addqb   7, 536870911(fp)[r0:d]
    addqb   -8, 536870911(fp)[r0:d]
    addqb   7, 63(sp)[r0:d]
    addqb   -8, 63(sp)[r0:d]
    addqb   7, 8191(sp)[r0:d]
    addqb   -8, 8191(sp)[r0:d]
    addqb   7, 536870911(sp)[r0:d]
    addqb   -8, 536870911(sp)[r0:d]
    addqb   7, 63(sb)[r0:d]
    addqb   -8, 63(sb)[r0:d]
    addqb   7, 8191(sb)[r0:d]
    addqb   -8, 8191(sb)[r0:d]
    addqb   7, 536870911(sb)[r0:d]
    addqb   -8, 536870911(sb)[r0:d]
    addqb   7, 63(pc)[r0:d]
    addqb   -8, 63(pc)[r0:d]
    addqb   7, 8191(pc)[r0:d]
    addqb   -8, 8191(pc)[r0:d]
    addqb   7, 536870911(pc)[r0:d]
    addqb   -8, 536870911(pc)[r0:d]
#
# ADD quick WORD
#
#
# Register
#
    addqw   7, r0
    addqw   -8, r0
    addqw   7, r1
    addqw   -8, r1
    addqw   7, r2
    addqw   -8, r2
    addqw   7, r3
    addqw   -8, r3
    addqw   7, r4
    addqw   -8, r4
    addqw   7, r5
    addqw   -8, r5
    addqw   7, r6
    addqw   -8, r6
    addqw   7, r7
    addqw   -8, r7
#
# Register relative
#
    addqw   7, 63(r0)
    addqw   -8, -64(r0)
    addqw   7, 8191(r0)
    addqw   -8, -8192(r0)
    addqw   7, 536870911(r0)
    addqw   -8, -536870912(r0)
    #
    addqw   7, 63(r1)
    addqw   -8, -64(r1)
    addqw   7, 8191(r1)
    addqw   -8, -8192(r1)
    addqw   7, 536870911(r1)
    addqw   -8, -536870912(r1)
    #
    addqw   7, 63(r2)
    addqw   -8, -64(r2)
    addqw   7, 8191(r2)
    addqw   -8, -8192(r2)
    addqw   7, 536870911(r2)
    addqw   -8, -536870912(r2)
    #
    addqw   7, 63(r3)
    addqw   -8, -64(r3)
    addqw   7, 8191(r3)
    addqw   -8, -8192(r3)
    addqw   7, 536870911(r3)
    addqw   -8, -536870912(r3)
    #
    addqw   7, 63(r4)
    addqw   -8, -64(r4)
    addqw   7, 8191(r4)
    addqw   -8, -8192(r4)
    addqw   7, 536870911(r4)
    addqw   -8, -536870912(r4)
    #
    addqw   7, 63(r5)
    addqw   -8, -64(r5)
    addqw   7, 8191(r5)
    addqw   -8, -8192(r5)
    addqw   7, 536870911(r5)
    addqw   -8, -536870912(r5)
    #
    addqw   7, 63(r6)
    addqw   -8, -64(r6)
    addqw   7, 8191(r6)
    addqw   -8, -8192(r6)
    addqw   7, 536870911(r6)
    addqw   -8, -536870912(r6)
    #
    addqw   7, 63(r7)
    addqw   -8, -64(r7)
    addqw   7, 8191(r7)
    addqw   -8, -8192(r7)
    addqw   7, 536870911(r7)
    addqw   -8, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    addqw   7, -64(63(fp))
    addqw   -8, -64(63(fp))
    addqw   7, 63(-64(fp))
    addqw   -8, 63(-64(fp))
    #
    addqw   7, -8192(63(fp))
    addqw   -8, -64(8191(fp))
    addqw   7, 63(-8192(fp))
    addqw   -8, 8191(-64(fp))
    #
    addqw   7, -8192(8191(fp))
    addqw   -8, -8192(8191(fp))
    addqw   7, 8191(-8192(fp))
    addqw   -8, 8191(-8192(fp))
    #
    addqw   7, -536870912(8191(fp))
    addqw   -8, -8192(536870911(fp))
    addqw   7, 8191(-536870912(fp))
    addqw   -8, 536870911(-8192(fp))
    #
    addqw   7, 536870911(-536870912(fp))
    addqw   -8, 536870911(-536870912(fp))
    addqw   7, -536870912(536870911(fp))
    addqw   -8, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    addqw   7, -64(63(sp))
    addqw   -8, -64(63(sp))
    addqw   7, 63(-64(sp))
    addqw   -8, 63(-64(sp))
    #
    addqw   7, -8192(63(sp))
    addqw   -8, -64(8191(sp))
    addqw   7, 63(-8192(sp))
    addqw   -8, 8191(-64(sp))
    #
    addqw   7, -8192(8191(sp))
    addqw   -8, -8192(8191(sp))
    addqw   7, 8191(-8192(sp))
    addqw   -8, 8191(-8192(sp))
    #
    addqw   7, -536870912(8191(sp))
    addqw   -8, -8192(536870911(sp))
    addqw   7, 8191(-536870912(sp))
    addqw   -8, 536870911(-8192(sp))
    #
    addqw   7, 536870911(-536870912(sp))
    addqw   -8, 536870911(-536870912(sp))
    addqw   7, -536870912(536870911(sp))
    addqw   -8, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    addqw   7, -64(63(sb))
    addqw   -8, -64(63(sb))
    addqw   7, 63(-64(sb))
    addqw   -8, 63(-64(sb))
    #
    addqw   7, -8192(63(sb))
    addqw   -8, -64(8191(sb))
    addqw   7, 63(-8192(sb))
    addqw   -8, 8191(-64(sb))
    #
    addqw   7, -8192(8191(sb))
    addqw   -8, -8192(8191(sb))
    addqw   7, 8191(-8192(sb))
    addqw   -8, 8191(-8192(sb))
    #
    addqw   7, -536870912(8191(sb))
    addqw   -8, -8192(536870911(sb))
    addqw   7, 8191(-536870912(sb))
    addqw   -8, 536870911(-8192(sb))
    #
    addqw   7, 536870911(-536870912(sb))
    addqw   -8, 536870911(-536870912(sb))
    addqw   7, -536870912(536870911(sb))
    addqw   -8, -536870912(536870911(sb))

#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    addqw	7, @-64
    addqw	-8, @-64
    addqw	7, @63
    addqw	-8, @63
    addqw	7, @-65
    addqw	-8, @-65
    addqw	7, @64
    addqw	-8, @64
    #
    addqw	7, @-8192
    addqw	-8, @-8192
    addqw	7, @8191
    addqw	-8, @8191
    addqw	7, @-8193
    addqw	-8, @-8193
    addqw	7, @8192
    addqw	-8, @8192
    #
    addqw	7, @-536870912
    addqw	-8, @536870911
#
# External
#
    addqw	7, ext(-64) + 63
    addqw	-8, ext(-64) + 63
    addqw	7, ext(63) + -64
    addqw	-8, ext(63) + -64
    #
    addqw	7, ext(-65) + 63
    addqw	-8, ext(-65) + 63
    addqw	7, ext(63) + -65
    addqw	-8, ext(63) + -65
    addqw	7, ext(-64) + 64
    addqw	-8, ext(-64) + 64
    addqw	7, ext(64) + -64
    addqw	-8, ext(64) + -64
    #
    addqw	7, ext(-8192) + 8191
    addqw	-8, ext(-8192) + 8191
    addqw	7, ext(8191) + -8192
    addqw	-8, ext(8191) + -8192
    #
    addqw	7, ext(-536870912) + 8191
    addqw	-8, ext(-536870912) + 8191
    addqw	7, ext(8191) + -536870912
    addqw	-8, ext(8191) + -536870912
    addqw	7, ext(-8192) + 536870911
    addqw	-8, ext(-8192) + 536870911
    addqw	7, ext(536870911) + -8192
    addqw	-8, ext(536870911) + -8192
    #
    addqw	7, ext(-536870912) + 536870911
    addqw	-8, ext(-536870912) + 536870911
    addqw	7, ext(536870911) + -536870912
    addqw	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    addqw	7, tos
    addqw	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    addqw	7, -64(fp)
    addqw	-8, -64(fp)
    addqw	7, 63(fp)
    addqw	-8, 63(fp)
    #
    addqw	7, -8192(fp)
    addqw	-8, -8192(fp)
    addqw	7, 8191(fp)
    addqw	-8, 8191(fp)
    #
    addqw	7, -536870912(fp)
    addqw	-8, -536870912(fp)
    addqw	7, 536870911(fp)
    addqw	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    addqw	7, -64(sp)
    addqw	-8, -64(sp)
    addqw	7, 63(sp)
    addqw	-8, 63(sp)
    #
    addqw	7, -8192(sp)
    addqw	-8, -8192(sp)
    addqw	7, 8191(sp)
    addqw	-8, 8191(sp)
    #
    addqw	7, -536870912(sp)
    addqw	-8, -536870912(sp)
    addqw	7, 536870911(sp)
    addqw	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    addqw	7, -64(sb)
    addqw	-8, -64(sb)
    addqw	7, 63(sb)
    addqw	-8, 63(sb)
    #
    addqw	7, -8192(sb)
    addqw	-8, -8192(sb)
    addqw	7, 8191(sb)
    addqw	-8, 8191(sb)
    #
    addqw	7, -536870912(sb)
    addqw	-8, -536870912(sb)
    addqw	7, 536870911(sb)
    addqw	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    addqw	7, -64(pc)
    addqw	-8, -64(pc)
    addqw	7, 63(pc)
    addqw	-8, 63(pc)
    #
    addqw	7, -8192(pc)
    addqw	-8, -8192(pc)
    addqw	7, 8191(pc)
    addqw	-8, 8191(pc)
    #
    addqw	7, -536870912(pc)
    addqw	-8, -536870912(pc)
    addqw	7, 536870911(pc)
    addqw	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    addqw   7, r7[r0:b]
    addqw   -8, r6[r1:b]
    #
    # Register Relative
    addqw   7, 63(r5)[r2:b]
    addqw   -8, 63(r4)[r3:b]
    addqw   7, 8191(r3)[r4:b]
    addqw   -8, 8191(r2)[r5:b]
    addqw   7, 536870911(r1)[r6:b]
    addqw   -8, 536870911(r0)[r7:b]
    #
    # Memory Relative
    addqw   7, -64(63(fp))[r0:b]
    addqw   -8, -64(63(fp))[r1:b]
    addqw   7, -8192(8191(fp))[r2:b]
    addqw   -8, -8192(8191(fp))[r3:b]
    addqw   7, -536870912(536870911(fp))[r4:b]
    addqw   -8, -536870912(536870911(fp))[r5:b]
    addqw   7, -64(63(sp))[r7:b]
    addqw   -8, -64(63(sp))[r6:b]
    addqw   7, -8192(8191(sp))[r5:b]
    addqw   -8, -8192(8191(sp))[r4:b]
    addqw   7, -536870912(536870911(sp))[r3:b]
    addqw   -8, -536870912(536870911(sp))[r2:b]
    addqw   7, -64(63(sb))[r0:b]
    addqw   -8, -64(63(sb))[r1:b]
    addqw   7, -8192(8191(sb))[r2:b]
    addqw   -8, -8192(8191(sb))[r3:b]
    addqw   7, -536870912(536870911(sb))[r4:b]
    addqw   -8, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    addqw   7, @63[r0:b]
    addqw   -8, @63[r0:b]
    addqw   7, @8191[r0:b]
    addqw   -8, @8191[r0:b]
    addqw   7, @536870911[r0:b]
    addqw   -8, @536870911[r0:b]
    #
    # External
    addqw   7, ext(-64) + 63[r0:b]
    addqw   -8, ext(-64) + 63[r0:b]
    addqw   7, ext(-8192) + 8191[r0:b]
    addqw   -8, ext(-8192) + 8191[r0:b]
    addqw   7, ext(-536870912) + 536870911[r0:b]
    addqw   -8, ext(-536870912) + 536870911[r0:b]
    #
    # Top Of Stack
    addqw   7, tos[r0:b]
    addqw   -8, tos[r0:b]
    #
    # Memory Space
    addqw   7, 63(fp)[r0:b]
    addqw   -8, 63(fp)[r0:b]
    addqw   7, 8191(fp)[r0:b]
    addqw   -8, 8191(fp)[r0:b]
    addqw   7, 536870911(fp)[r0:b]
    addqw   -8, 536870911(fp)[r0:b]
    addqw   7, 63(sp)[r0:b]
    addqw   -8, 63(sp)[r0:b]
    addqw   7, 8191(sp)[r0:b]
    addqw   -8, 8191(sp)[r0:b]
    addqw   7, 536870911(sp)[r0:b]
    addqw   -8, 536870911(sp)[r0:b]
    addqw   7, 63(sb)[r0:b]
    addqw   -8, 63(sb)[r0:b]
    addqw   7, 8191(sb)[r0:b]
    addqw   -8, 8191(sb)[r0:b]
    addqw   7, 536870911(sb)[r0:b]
    addqw   -8, 536870911(sb)[r0:b]
    addqw   7, 63(pc)[r0:b]
    addqw   -8, 63(pc)[r0:b]
    addqw   7, 8191(pc)[r0:b]
    addqw   -8, 8191(pc)[r0:b]
    addqw   7, 536870911(pc)[r0:b]
    addqw   -8, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    addqw   7, r1[r0:w]
    addqw   -8, r1[r0:w]
    #
    # Register Relative
    addqw   7, 63(r1)[r0:w]
    addqw   -8, 63(r1)[r0:w]
    addqw   7, 8191(r1)[r0:w]
    addqw   -8, 8191(r1)[r0:w]
    addqw   7, 536870911(r1)[r0:w]
    addqw   -8, 536870911(r1)[r0:w]
    #
    # Memory Relative
    addqw   7, -64(63(fp))[r0:w]
    addqw   -8, -64(63(fp))[r0:w]
    addqw   7, -8192(8191(fp))[r0:w]
    addqw   -8, -8192(8191(fp))[r0:w]
    addqw   7, -536870912(536870911(fp))[r0:w]
    addqw   -8, -536870912(536870911(fp))[r0:w]
    #
    # Absolute
    addqw   7, @63[r0:w]
    addqw   -8, @63[r0:w]
    addqw   7, @8191[r0:w]
    addqw   -8, @8191[r0:w]
    addqw   7, @536870911[r0:w]
    addqw   -8, @536870911[r0:w]
    #
    # External
    addqw   7, ext(-64) + 63[r0:w]
    addqw   -8, ext(-64) + 63[r0:w]
    addqw   7, ext(-8192) + 8191[r0:w]
    addqw   -8, ext(-8192) + 8191[r0:w]
    addqw   7, ext(-536870912) + 536870911[r0:w]
    addqw   -8, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    addqw   7, tos[r0:w]
    addqw   -8, tos[r0:w]
    #
    # Memory Space
    addqw   7, 63(fp)[r0:w]
    addqw   -8, 63(fp)[r0:w]
    addqw   7, 8191(fp)[r0:w]
    addqw   -8, 8191(fp)[r0:w]
    addqw   7, 536870911(fp)[r0:w]
    addqw   -8, 536870911(fp)[r0:w]
    addqw   7, 63(sp)[r0:w]
    addqw   -8, 63(sp)[r0:w]
    addqw   7, 8191(sp)[r0:w]
    addqw   -8, 8191(sp)[r0:w]
    addqw   7, 536870911(sp)[r0:w]
    addqw   -8, 536870911(sp)[r0:w]
    addqw   7, 63(sb)[r0:w]
    addqw   -8, 63(sb)[r0:w]
    addqw   7, 8191(sb)[r0:w]
    addqw   -8, 8191(sb)[r0:w]
    addqw   7, 536870911(sb)[r0:w]
    addqw   -8, 536870911(sb)[r0:w]
    addqw   7, 63(pc)[r0:w]
    addqw   -8, 63(pc)[r0:w]
    addqw   7, 8191(pc)[r0:w]
    addqw   -8, 8191(pc)[r0:w]
    addqw   7, 536870911(pc)[r0:w]
    addqw   -8, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    addqw   7, r1[r0:d]
    addqw   -8, r1[r0:d]
    #
    # Register Relative
    addqw   7, 63(r1)[r0:d]
    addqw   -8, 63(r1)[r0:d]
    addqw   7, 8191(r1)[r0:d]
    addqw   -8, 8191(r1)[r0:d]
    addqw   7, 536870911(r1)[r0:d]
    addqw   -8, 536870911(r1)[r0:d]
    #
    # Memory Relative
    addqw   7, -64(63(fp))[r0:d]
    addqw   -8, -64(63(fp))[r0:d]
    addqw   7, -8192(8191(fp))[r0:d]
    addqw   -8, -8192(8191(fp))[r0:d]
    addqw   7, -536870912(536870911(fp))[r0:d]
    addqw   -8, -536870912(536870911(fp))[r0:d]
    #
    # Absolute
    addqw   7, @63[r0:d]
    addqw   -8, @63[r0:d]
    addqw   7, @8191[r0:d]
    addqw   -8, @8191[r0:d]
    addqw   7, @536870911[r0:d]
    addqw   -8, @536870911[r0:d]
    #
    # External
    addqw   7, ext(-64) + 63[r0:d]
    addqw   -8, ext(-64) + 63[r0:d]
    addqw   7, ext(-8192) + 8191[r0:d]
    addqw   -8, ext(-8192) + 8191[r0:d]
    addqw   7, ext(-536870912) + 536870911[r0:d]
    addqw   -8, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    addqw   7, tos[r0:d]
    addqw   -8, tos[r0:d]
    #
    # Memory Space
    addqw   7, 63(fp)[r0:d]
    addqw   -8, 63(fp)[r0:d]
    addqw   7, 8191(fp)[r0:d]
    addqw   -8, 8191(fp)[r0:d]
    addqw   7, 536870911(fp)[r0:d]
    addqw   -8, 536870911(fp)[r0:d]
    addqw   7, 63(sp)[r0:d]
    addqw   -8, 63(sp)[r0:d]
    addqw   7, 8191(sp)[r0:d]
    addqw   -8, 8191(sp)[r0:d]
    addqw   7, 536870911(sp)[r0:d]
    addqw   -8, 536870911(sp)[r0:d]
    addqw   7, 63(sb)[r0:d]
    addqw   -8, 63(sb)[r0:d]
    addqw   7, 8191(sb)[r0:d]
    addqw   -8, 8191(sb)[r0:d]
    addqw   7, 536870911(sb)[r0:d]
    addqw   -8, 536870911(sb)[r0:d]
    addqw   7, 63(pc)[r0:d]
    addqw   -8, 63(pc)[r0:d]
    addqw   7, 8191(pc)[r0:d]
    addqw   -8, 8191(pc)[r0:d]
    addqw   7, 536870911(pc)[r0:d]
    addqw   -8, 536870911(pc)[r0:d]

#
# ADD quick DOUBLE WORD
#
#
# Register
#
    addqd   7, r0
    addqd   -8, r0
    addqd   7, r1
    addqd   -8, r1
    addqd   7, r2
    addqd   -8, r2
    addqd   7, r3
    addqd   -8, r3
    addqd   7, r4
    addqd   -8, r4
    addqd   7, r5
    addqd   -8, r5
    addqd   7, r6
    addqd   -8, r6
    addqd   7, r7
    addqd   -8, r7
#
# Register relative
#
    addqd   7, 63(r0)
    addqd   -8, -64(r0)
    addqd   7, 8191(r0)
    addqd   -8, -8192(r0)
    addqd   7, 536870911(r0)
    addqd   -8, -536870912(r0)
    #
    addqd   7, 63(r1)
    addqd   -8, -64(r1)
    addqd   7, 8191(r1)
    addqd   -8, -8192(r1)
    addqd   7, 536870911(r1)
    addqd   -8, -536870912(r1)
    #
    addqd   7, 63(r2)
    addqd   -8, -64(r2)
    addqd   7, 8191(r2)
    addqd   -8, -8192(r2)
    addqd   7, 536870911(r2)
    addqd   -8, -536870912(r2)
    #
    addqd   7, 63(r3)
    addqd   -8, -64(r3)
    addqd   7, 8191(r3)
    addqd   -8, -8192(r3)
    addqd   7, 536870911(r3)
    addqd   -8, -536870912(r3)
    #
    addqd   7, 63(r4)
    addqd   -8, -64(r4)
    addqd   7, 8191(r4)
    addqd   -8, -8192(r4)
    addqd   7, 536870911(r4)
    addqd   -8, -536870912(r4)
    #
    addqd   7, 63(r5)
    addqd   -8, -64(r5)
    addqd   7, 8191(r5)
    addqd   -8, -8192(r5)
    addqd   7, 536870911(r5)
    addqd   -8, -536870912(r5)
    #
    addqd   7, 63(r6)
    addqd   -8, -64(r6)
    addqd   7, 8191(r6)
    addqd   -8, -8192(r6)
    addqd   7, 536870911(r6)
    addqd   -8, -536870912(r6)
    #
    addqd   7, 63(r7)
    addqd   -8, -64(r7)
    addqd   7, 8191(r7)
    addqd   -8, -8192(r7)
    addqd   7, 536870911(r7)
    addqd   -8, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    addqd   7, -64(63(fp))
    addqd   -8, -64(63(fp))
    addqd   7, 63(-64(fp))
    addqd   -8, 63(-64(fp))
    #
    addqd   7, -8192(63(fp))
    addqd   -8, -64(8191(fp))
    addqd   7, 63(-8192(fp))
    addqd   -8, 8191(-64(fp))
    #
    addqd   7, -8192(8191(fp))
    addqd   -8, -8192(8191(fp))
    addqd   7, 8191(-8192(fp))
    addqd   -8, 8191(-8192(fp))
    #
    addqd   7, -536870912(8191(fp))
    addqd   -8, -8192(536870911(fp))
    addqd   7, 8191(-536870912(fp))
    addqd   -8, 536870911(-8192(fp))
    #
    addqd   7, 536870911(-536870912(fp))
    addqd   -8, 536870911(-536870912(fp))
    addqd   7, -536870912(536870911(fp))
    addqd   -8, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    addqd   7, -64(63(sp))
    addqd   -8, -64(63(sp))
    addqd   7, 63(-64(sp))
    addqd   -8, 63(-64(sp))
    #
    addqd   7, -8192(63(sp))
    addqd   -8, -64(8191(sp))
    addqd   7, 63(-8192(sp))
    addqd   -8, 8191(-64(sp))
    #
    addqd   7, -8192(8191(sp))
    addqd   -8, -8192(8191(sp))
    addqd   7, 8191(-8192(sp))
    addqd   -8, 8191(-8192(sp))
    #
    addqd   7, -536870912(8191(sp))
    addqd   -8, -8192(536870911(sp))
    addqd   7, 8191(-536870912(sp))
    addqd   -8, 536870911(-8192(sp))
    #
    addqd   7, 536870911(-536870912(sp))
    addqd   -8, 536870911(-536870912(sp))
    addqd   7, -536870912(536870911(sp))
    addqd   -8, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    addqd   7, -64(63(sb))
    addqd   -8, -64(63(sb))
    addqd   7, 63(-64(sb))
    addqd   -8, 63(-64(sb))
    #
    addqd   7, -8192(63(sb))
    addqd   -8, -64(8191(sb))
    addqd   7, 63(-8192(sb))
    addqd   -8, 8191(-64(sb))
    #
    addqd   7, -8192(8191(sb))
    addqd   -8, -8192(8191(sb))
    addqd   7, 8191(-8192(sb))
    addqd   -8, 8191(-8192(sb))
    #
    addqd   7, -536870912(8191(sb))
    addqd   -8, -8192(536870911(sb))
    addqd   7, 8191(-536870912(sb))
    addqd   -8, 536870911(-8192(sb))
    #
    addqd   7, 536870911(-536870912(sb))
    addqd   -8, 536870911(-536870912(sb))
    addqd   7, -536870912(536870911(sb))
    addqd   -8, -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    addqd	7, @-64
    addqd	-8, @-64
    addqd	7, @63
    addqd	-8, @63
    addqd	7, @-65
    addqd	-8, @-65
    addqd	7, @64
    addqd	-8, @64
    #
    addqd	7, @-8192
    addqd	-8, @-8192
    addqd	7, @8191
    addqd	-8, @8191
    addqd	7, @-8193
    addqd	-8, @-8193
    addqd	7, @8192
    addqd	-8, @8192
    #
    addqd	7, @-536870912
    addqd	-8, @536870911
#
# External
#
    addqd	7, ext(-64) + 63
    addqd	-8, ext(-64) + 63
    addqd	7, ext(63) + -64
    addqd	-8, ext(63) + -64
    #
    addqd	7, ext(-65) + 63
    addqd	-8, ext(-65) + 63
    addqd	7, ext(63) + -65
    addqd	-8, ext(63) + -65
    addqd	7, ext(-64) + 64
    addqd	-8, ext(-64) + 64
    addqd	7, ext(64) + -64
    addqd	-8, ext(64) + -64
    #
    addqd	7, ext(-8192) + 8191
    addqd	-8, ext(-8192) + 8191
    addqd	7, ext(8191) + -8192
    addqd	-8, ext(8191) + -8192
    #
    addqd	7, ext(-536870912) + 8191
    addqd	-8, ext(-536870912) + 8191
    addqd	7, ext(8191) + -536870912
    addqd	-8, ext(8191) + -536870912
    addqd	7, ext(-8192) + 536870911
    addqd	-8, ext(-8192) + 536870911
    addqd	7, ext(536870911) + -8192
    addqd	-8, ext(536870911) + -8192
    #
    addqd	7, ext(-536870912) + 536870911
    addqd	-8, ext(-536870912) + 536870911
    addqd	7, ext(536870911) + -536870912
    addqd	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    addqd	7, tos
    addqd	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    addqd	7, -64(fp)
    addqd	-8, -64(fp)
    addqd	7, 63(fp)
    addqd	-8, 63(fp)
    #
    addqd	7, -8192(fp)
    addqd	-8, -8192(fp)
    addqd	7, 8191(fp)
    addqd	-8, 8191(fp)
    #
    addqd	7, -536870912(fp)
    addqd	-8, -536870912(fp)
    addqd	7, 536870911(fp)
    addqd	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    addqd	7, -64(sp)
    addqd	-8, -64(sp)
    addqd	7, 63(sp)
    addqd	-8, 63(sp)
    #
    addqd	7, -8192(sp)
    addqd	-8, -8192(sp)
    addqd	7, 8191(sp)
    addqd	-8, 8191(sp)
    #
    addqd	7, -536870912(sp)
    addqd	-8, -536870912(sp)
    addqd	7, 536870911(sp)
    addqd	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    addqd	7, -64(sb)
    addqd	-8, -64(sb)
    addqd	7, 63(sb)
    addqd	-8, 63(sb)
    #
    addqd	7, -8192(sb)
    addqd	-8, -8192(sb)
    addqd	7, 8191(sb)
    addqd	-8, 8191(sb)
    #
    addqd	7, -536870912(sb)
    addqd	-8, -536870912(sb)
    addqd	7, 536870911(sb)
    addqd	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    addqd	7, -64(pc)
    addqd	-8, -64(pc)
    addqd	7, 63(pc)
    addqd	-8, 63(pc)
    #
    addqd	7, -8192(pc)
    addqd	-8, -8192(pc)
    addqd	7, 8191(pc)
    addqd	-8, 8191(pc)
    #
    addqd	7, -536870912(pc)
    addqd	-8, -536870912(pc)
    addqd	7, 536870911(pc)
    addqd	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    addqd   7, r7[r0:b]
    addqd   -8, r6[r1:b]
    #
    # Register Relative
    addqd   7, 63(r5)[r2:b]
    addqd   -8, 63(r4)[r3:b]
    addqd   7, 8191(r3)[r4:b]
    addqd   -8, 8191(r2)[r5:b]
    addqd   7, 536870911(r1)[r6:b]
    addqd   -8, 536870911(r0)[r7:b]
    #
    # Memory Relative
    addqd   7, -64(63(fp))[r0:b]
    addqd   -8, -64(63(fp))[r0:b]
    addqd   7, -8192(8191(fp))[r0:b]
    addqd   -8, -8192(8191(fp))[r0:b]
    addqd   7, -536870912(536870911(fp))[r0:b]
    addqd   -8, -536870912(536870911(fp))[r0:b]
    #
    # Absolute
    addqd   7, @63[r0:b]
    addqd   -8, @63[r0:b]
    addqd   7, @8191[r0:b]
    addqd   -8, @8191[r0:b]
    addqd   7, @536870911[r0:b]
    addqd   -8, @536870911[r0:b]
    #
    # External
    addqd   7, ext(-64) + 63[r0:b]
    addqd   -8, ext(-64) + 63[r0:b]
    addqd   7, ext(-8192) + 8191[r0:b]
    addqd   -8, ext(-8192) + 8191[r0:b]
    addqd   7, ext(-536870912) + 536870911[r0:b]
    addqd   -8, ext(-536870912) + 536870911[r0:b]
    #
    # Top Of Stack
    addqd   7, tos[r0:b]
    addqd   -8, tos[r0:b]
    #
    # Memory Space
    addqd   7, 63(fp)[r0:b]
    addqd   -8, 63(fp)[r0:b]
    addqd   7, 8191(fp)[r0:b]
    addqd   -8, 8191(fp)[r0:b]
    addqd   7, 536870911(fp)[r0:b]
    addqd   -8, 536870911(fp)[r0:b]
    addqd   7, 63(sp)[r0:b]
    addqd   -8, 63(sp)[r0:b]
    addqd   7, 8191(sp)[r0:b]
    addqd   -8, 8191(sp)[r0:b]
    addqd   7, 536870911(sp)[r0:b]
    addqd   -8, 536870911(sp)[r0:b]
    addqd   7, 63(sb)[r0:b]
    addqd   -8, 63(sb)[r0:b]
    addqd   7, 8191(sb)[r0:b]
    addqd   -8, 8191(sb)[r0:b]
    addqd   7, 536870911(sb)[r0:b]
    addqd   -8, 536870911(sb)[r0:b]
    addqd   7, 63(pc)[r0:b]
    addqd   -8, 63(pc)[r0:b]
    addqd   7, 8191(pc)[r0:b]
    addqd   -8, 8191(pc)[r0:b]
    addqd   7, 536870911(pc)[r0:b]
    addqd   -8, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    addqd   7, r1[r0:w]
    addqd   -8, r1[r0:w]
    #
    # Register Relative
    addqd   7, 63(r1)[r0:w]
    addqd   -8, 63(r1)[r0:w]
    addqd   7, 8191(r1)[r0:w]
    addqd   -8, 8191(r1)[r0:w]
    addqd   7, 536870911(r1)[r0:w]
    addqd   -8, 536870911(r1)[r0:w]
    #
    # Memory Relative
    addqd   7, -64(63(fp))[r0:w]
    addqd   -8, -64(63(fp))[r0:w]
    addqd   7, -8192(8191(fp))[r0:w]
    addqd   -8, -8192(8191(fp))[r0:w]
    addqd   7, -536870912(536870911(fp))[r0:w]
    addqd   -8, -536870912(536870911(fp))[r0:w]
    #
    # Absolute
    addqd   7, @63[r0:w]
    addqd   -8, @63[r0:w]
    addqd   7, @8191[r0:w]
    addqd   -8, @8191[r0:w]
    addqd   7, @536870911[r0:w]
    addqd   -8, @536870911[r0:w]
    #
    # External
    addqd   7, ext(-64) + 63[r0:w]
    addqd   -8, ext(-64) + 63[r0:w]
    addqd   7, ext(-8192) + 8191[r0:w]
    addqd   -8, ext(-8192) + 8191[r0:w]
    addqd   7, ext(-536870912) + 536870911[r0:w]
    addqd   -8, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    addqd   7, tos[r0:w]
    addqd   -8, tos[r0:w]
    #
    # Memory Space
    addqd   7, 63(fp)[r0:w]
    addqd   -8, 63(fp)[r0:w]
    addqd   7, 8191(fp)[r0:w]
    addqd   -8, 8191(fp)[r0:w]
    addqd   7, 536870911(fp)[r0:w]
    addqd   -8, 536870911(fp)[r0:w]
    addqd   7, 63(sp)[r0:w]
    addqd   -8, 63(sp)[r0:w]
    addqd   7, 8191(sp)[r0:w]
    addqd   -8, 8191(sp)[r0:w]
    addqd   7, 536870911(sp)[r0:w]
    addqd   -8, 536870911(sp)[r0:w]
    addqd   7, 63(sb)[r0:w]
    addqd   -8, 63(sb)[r0:w]
    addqd   7, 8191(sb)[r0:w]
    addqd   -8, 8191(sb)[r0:w]
    addqd   7, 536870911(sb)[r0:w]
    addqd   -8, 536870911(sb)[r0:w]
    addqd   7, 63(pc)[r0:w]
    addqd   -8, 63(pc)[r0:w]
    addqd   7, 8191(pc)[r0:w]
    addqd   -8, 8191(pc)[r0:w]
    addqd   7, 536870911(pc)[r0:w]
    addqd   -8, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    addqd   7, r1[r0:d]
    addqd   -8, r1[r0:d]
    #
    # Register Relative
    addqd   7, 63(r1)[r0:d]
    addqd   -8, 63(r1)[r0:d]
    addqd   7, 8191(r1)[r0:d]
    addqd   -8, 8191(r1)[r0:d]
    addqd   7, 536870911(r1)[r0:d]
    addqd   -8, 536870911(r1)[r0:d]
    #
    # Memory Relative
    addqd   7, -64(63(fp))[r0:d]
    addqd   -8, -64(63(fp))[r0:d]
    addqd   7, -8192(8191(fp))[r0:d]
    addqd   -8, -8192(8191(fp))[r0:d]
    addqd   7, -536870912(536870911(fp))[r0:d]
    addqd   -8, -536870912(536870911(fp))[r0:d]
    #
    # Absolute
    addqd   7, @63[r0:d]
    addqd   -8, @63[r0:d]
    addqd   7, @8191[r0:d]
    addqd   -8, @8191[r0:d]
    addqd   7, @536870911[r0:d]
    addqd   -8, @536870911[r0:d]
    #
    # External
    addqd   7, ext(-64) + 63[r0:d]
    addqd   -8, ext(-64) + 63[r0:d]
    addqd   7, ext(-8192) + 8191[r0:d]
    addqd   -8, ext(-8192) + 8191[r0:d]
    addqd   7, ext(-536870912) + 536870911[r0:d]
    addqd   -8, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    addqd   7, tos[r0:d]
    addqd   -8, tos[r0:d]
    #
    # Memory Space
    addqd   7, 63(fp)[r0:d]
    addqd   -8, 63(fp)[r0:d]
    addqd   7, 8191(fp)[r0:d]
    addqd   -8, 8191(fp)[r0:d]
    addqd   7, 536870911(fp)[r0:d]
    addqd   -8, 536870911(fp)[r0:d]
    addqd   7, 63(sp)[r0:d]
    addqd   -8, 63(sp)[r0:d]
    addqd   7, 8191(sp)[r0:d]
    addqd   -8, 8191(sp)[r0:d]
    addqd   7, 536870911(sp)[r0:d]
    addqd   -8, 536870911(sp)[r0:d]
    addqd   7, 63(sb)[r0:d]
    addqd   -8, 63(sb)[r0:d]
    addqd   7, 8191(sb)[r0:d]
    addqd   -8, 8191(sb)[r0:d]
    addqd   7, 536870911(sb)[r0:d]
    addqd   -8, 536870911(sb)[r0:d]
    addqd   7, 63(pc)[r0:d]
    addqd   -8, 63(pc)[r0:d]
    addqd   7, 8191(pc)[r0:d]
    addqd   -8, 8191(pc)[r0:d]
    addqd   7, 536870911(pc)[r0:d]
    addqd   -8, 536870911(pc)[r0:d]
