.psize 0, 143   # suppress form feed, 143 columns
#
#
# Test case for FORMAT 2 instructions
#
# Instruction under test: MOVQi
#
# Syntax:   movqi   src     dest
#                   quick   gen
#                           write.i
#
#           !  dest   !  src  !     MOVQi   !
#           +---------+-------+---------+---+
#           !   gen   ! quick !1 0 1 1 1! i !
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# The MOVQi instruction copies the src operand to the dest operand
# Before the copy operation, src is sign-extended to the length of dest.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
# ADD quick BYTE 
#
#
# Register
#
    movqb   0, r0
    movqb   -8, r0
    movqb   1, r1
    movqb   -7, r1
    movqb   2, r2
    movqb   -6, r2
    movqb   3, r3
    movqb   -5, r3
    movqb   4, r4
    movqb   -4, r4
    movqb   5, r5
    movqb   -3, r5
    movqb   6, r6
    movqb   -2, r6
    movqb   7, r7
    movqb   -1, r7
#
# Register relative
#
    movqb   7, 63(r0)
    movqb   -8, -64(r0)
    movqb   7, 8191(r0)
    movqb   -8, -8192(r0)
    movqb   7, 536870911(r0)
    movqb   -8, -536870912(r0)
    #
    movqb   7, 63(r1)
    movqb   -8, -64(r1)
    movqb   7, 8191(r1)
    movqb   -8, -8192(r1)
    movqb   7, 536870911(r1)
    movqb   -8, -536870912(r1)
    #
    movqb   7, 63(r2)
    movqb   -8, -64(r2)
    movqb   7, 8191(r2)
    movqb   -8, -8192(r2)
    movqb   7, 536870911(r2)
    movqb   -8, -536870912(r2)
    #
    movqb   7, 63(r3)
    movqb   -8, -64(r3)
    movqb   7, 8191(r3)
    movqb   -8, -8192(r3)
    movqb   7, 536870911(r3)
    movqb   -8, -536870912(r3)
    #
    movqb   7, 63(r4)
    movqb   -8, -64(r4)
    movqb   7, 8191(r4)
    movqb   -8, -8192(r4)
    movqb   7, 536870911(r4)
    movqb   -8, -536870912(r4)
    #
    movqb   7, 63(r5)
    movqb   -8, -64(r5)
    movqb   7, 8191(r5)
    movqb   -8, -8192(r5)
    movqb   7, 536870911(r5)
    movqb   -8, -536870912(r5)
    #
    movqb   7, 63(r6)
    movqb   -8, -64(r6)
    movqb   7, 8191(r6)
    movqb   -8, -8192(r6)
    movqb   7, 536870911(r6)
    movqb   -8, -536870912(r6)
    #
    movqb   7, 63(r7)
    movqb   -8, -64(r7)
    movqb   7, 8191(r7)
    movqb   -8, -8192(r7)
    movqb   7, 536870911(r7)
    movqb   -8, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    movqb   7, -64(63(fp))
    movqb   -8, -64(63(fp))
    movqb   7, 63(-64(fp))
    movqb   -8, 63(-64(fp))
    #
    movqb   7, -8192(63(fp))
    movqb   -8, -64(8191(fp))
    movqb   7, 63(-8192(fp))
    movqb   -8, 8191(-64(fp))
    #
    movqb   7, -8192(8191(fp))
    movqb   -8, -8192(8191(fp))
    movqb   7, 8191(-8192(fp))
    movqb   -8, 8191(-8192(fp))
    #
    movqb   7, -536870912(8191(fp))
    movqb   -8, -8192(536870911(fp))
    movqb   7, 8191(-536870912(fp))
    movqb   -8, 536870911(-8192(fp))
    #
    movqb   7, 536870911(-536870912(fp))
    movqb   -8, 536870911(-536870912(fp))
    movqb   7, -536870912(536870911(fp))
    movqb   -8, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    movqb   7, -64(63(sp))
    movqb   -8, -64(63(sp))
    movqb   7, 63(-64(sp))
    movqb   -8, 63(-64(sp))
    #
    movqb   7, -8192(63(sp))
    movqb   -8, -64(8191(sp))
    movqb   7, 63(-8192(sp))
    movqb   -8, 8191(-64(sp))
    #
    movqb   7, -8192(8191(sp))
    movqb   -8, -8192(8191(sp))
    movqb   7, 8191(-8192(sp))
    movqb   -8, 8191(-8192(sp))
    #
    movqb   7, -536870912(8191(sp))
    movqb   -8, -8192(536870911(sp))
    movqb   7, 8191(-536870912(sp))
    movqb   -8, 536870911(-8192(sp))
    #
    movqb   7, 536870911(-536870912(sp))
    movqb   -8, 536870911(-536870912(sp))
    movqb   7, -536870912(536870911(sp))
    movqb   -8, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    movqb   7, -64(63(sb))
    movqb   -8, -64(63(sb))
    movqb   7, 63(-64(sb))
    movqb   -8, 63(-64(sb))
    #
    movqb   7, -8192(63(sb))
    movqb   -8, -64(8191(sb))
    movqb   7, 63(-8192(sb))
    movqb   -8, 8191(-64(sb))
    #
    movqb   7, -8192(8191(sb))
    movqb   -8, -8192(8191(sb))
    movqb   7, 8191(-8192(sb))
    movqb   -8, 8191(-8192(sb))
    #
    movqb   7, -536870912(8191(sb))
    movqb   -8, -8192(536870911(sb))
    movqb   7, 8191(-536870912(sb))
    movqb   -8, 536870911(-8192(sb))
    #
    movqb   7, 536870911(-536870912(sb))
    movqb   -8, 536870911(-536870912(sb))
    movqb   7, -536870912(536870911(sb))
    movqb   -8, -536870912(536870911(sb))

#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    movqb	7, @-64
    movqb	-8, @-64
    movqb	7, @63
    movqb	-8, @63
    movqb	7, @-65
    movqb	-8, @-65
    movqb	7, @64
    movqb	-8, @64
    #
    movqb	7, @-8192
    movqb	-8, @-8192
    movqb	7, @8191
    movqb	-8, @8191
    movqb	7, @-8193
    movqb	-8, @-8193
    movqb	7, @8192
    movqb	-8, @8192
    #
    movqb	7, @-536870912
    movqb	-8, @536870911
#
# External
#
    movqb	7, ext(-64) + 63
    movqb	-8, ext(-64) + 63
    movqb	7, ext(63) + -64
    movqb	-8, ext(63) + -64
    #
    movqb	7, ext(-65) + 63
    movqb	-8, ext(-65) + 63
    movqb	7, ext(63) + -65
    movqb	-8, ext(63) + -65
    movqb	7, ext(-64) + 64
    movqb	-8, ext(-64) + 64
    movqb	7, ext(64) + -64
    movqb	-8, ext(64) + -64
    #
    movqb	7, ext(-8192) + 8191
    movqb	-8, ext(-8192) + 8191
    movqb	7, ext(8191) + -8192
    movqb	-8, ext(8191) + -8192
    #
    movqb	7, ext(-536870912) + 8191
    movqb	-8, ext(-536870912) + 8191
    movqb	7, ext(8191) + -536870912
    movqb	-8, ext(8191) + -536870912
    movqb	7, ext(-8192) + 536870911
    movqb	-8, ext(-8192) + 536870911
    movqb	7, ext(536870911) + -8192
    movqb	-8, ext(536870911) + -8192
    #
    movqb	7, ext(-536870912) + 536870911
    movqb	-8, ext(-536870912) + 536870911
    movqb	7, ext(536870911) + -536870912
    movqb	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    movqb	7, tos
    movqb	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    movqb	7, -64(fp)
    movqb	-8, -64(fp)
    movqb	7, 63(fp)
    movqb	-8, 63(fp)
    #
    movqb	7, -8192(fp)
    movqb	-8, -8192(fp)
    movqb	7, 8191(fp)
    movqb	-8, 8191(fp)
    #
    movqb	7, -536870912(fp)
    movqb	-8, -536870912(fp)
    movqb	7, 536870911(fp)
    movqb	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    movqb	7, -64(sp)
    movqb	-8, -64(sp)
    movqb	7, 63(sp)
    movqb	-8, 63(sp)
    #
    movqb	7, -8192(sp)
    movqb	-8, -8192(sp)
    movqb	7, 8191(sp)
    movqb	-8, 8191(sp)
    #
    movqb	7, -536870912(sp)
    movqb	-8, -536870912(sp)
    movqb	7, 536870911(sp)
    movqb	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    movqb	7, -64(sb)
    movqb	-8, -64(sb)
    movqb	7, 63(sb)
    movqb	-8, 63(sb)
    #
    movqb	7, -8192(sb)
    movqb	-8, -8192(sb)
    movqb	7, 8191(sb)
    movqb	-8, 8191(sb)
    #
    movqb	7, -536870912(sb)
    movqb	-8, -536870912(sb)
    movqb	7, 536870911(sb)
    movqb	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    movqb	7, -64(pc)
    movqb	-8, -64(pc)
    movqb	7, 63(pc)
    movqb	-8, 63(pc)
    #
    movqb	7, -8192(pc)
    movqb	-8, -8192(pc)
    movqb	7, 8191(pc)
    movqb	-8, 8191(pc)
    #
    movqb	7, -536870912(pc)
    movqb	-8, -536870912(pc)
    movqb	7, 536870911(pc)
    movqb	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    movqb   7, r7[r0:b]
    movqb   -8, r6[r1:b]
    #
    # Register Relative
    movqb   7, 63(r5)[r2:b]
    movqb   -8, 63(r4)[r3:b]
    movqb   7, 8191(r3)[r4:b]
    movqb   -8, 8191(r2)[r5:b]
    movqb   7, 536870911(r1)[r6:b]
    movqb   -8, 536870911(r0)[r7:b]
    #
    # Memory Relative
    movqb   7, -64(63(fp))[r0:b]
    movqb   -8, -64(63(fp))[r1:b]
    movqb   7, -8192(8191(fp))[r2:b]
    movqb   -8, -8192(8191(fp))[r3:b]
    movqb   7, -536870912(536870911(fp))[r4:b]
    movqb   -8, -536870912(536870911(fp))[r5:b]
    movqb   7, -64(63(sp))[r7:b]
    movqb   -8, -64(63(sp))[r6:b]
    movqb   7, -8192(8191(sp))[r5:b]
    movqb   -8, -8192(8191(sp))[r4:b]
    movqb   7, -536870912(536870911(sp))[r3:b]
    movqb   -8, -536870912(536870911(sp))[r2:b]
    movqb   7, -64(63(sb))[r0:b]
    movqb   -8, -64(63(sb))[r1:b]
    movqb   7, -8192(8191(sb))[r2:b]
    movqb   -8, -8192(8191(sb))[r3:b]
    movqb   7, -536870912(536870911(sb))[r4:b]
    movqb   -8, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    movqb   7, @63[r0:b]
    movqb   -8, @63[r1:b]
    movqb   7, @8191[r2:b]
    movqb   -8, @8191[r3:b]
    movqb   7, @536870911[r4:b]
    movqb   -8, @536870911[r5:b]
    #
    # External
    movqb   7, ext(-64) + 63[r7:b]
    movqb   -8, ext(-64) + 63[r6:b]
    movqb   7, ext(-8192) + 8191[r5:b]
    movqb   -8, ext(-8192) + 8191[r4:b]
    movqb   7, ext(-536870912) + 536870911[r3:b]
    movqb   -8, ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    movqb   7, tos[r0:b]
    movqb   -8, tos[r7:b]
    #
    # Memory Space
    movqb   7, 63(fp)[r0:b]
    movqb   -8, 63(fp)[r0:b]
    movqb   7, 8191(fp)[r0:b]
    movqb   -8, 8191(fp)[r0:b]
    movqb   7, 536870911(fp)[r0:b]
    movqb   -8, 536870911(fp)[r0:b]
    movqb   7, 63(sp)[r0:b]
    movqb   -8, 63(sp)[r0:b]
    movqb   7, 8191(sp)[r0:b]
    movqb   -8, 8191(sp)[r0:b]
    movqb   7, 536870911(sp)[r0:b]
    movqb   -8, 536870911(sp)[r0:b]
    movqb   7, 63(sb)[r0:b]
    movqb   -8, 63(sb)[r0:b]
    movqb   7, 8191(sb)[r0:b]
    movqb   -8, 8191(sb)[r0:b]
    movqb   7, 536870911(sb)[r0:b]
    movqb   -8, 536870911(sb)[r0:b]
    movqb   7, 63(pc)[r0:b]
    movqb   -8, 63(pc)[r0:b]
    movqb   7, 8191(pc)[r0:b]
    movqb   -8, 8191(pc)[r0:b]
    movqb   7, 536870911(pc)[r0:b]
    movqb   -8, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    movqb   7, r1[r0:w]
    movqb   -8, r1[r0:w]
    #
    # Register Relative
    movqb   7, 63(r1)[r0:w]
    movqb   -8, 63(r1)[r0:w]
    movqb   7, 8191(r1)[r0:w]
    movqb   -8, 8191(r1)[r0:w]
    movqb   7, 536870911(r1)[r0:w]
    movqb   -8, 536870911(r1)[r0:w]
    #
    # Memory Relative
    movqb   7, -64(63(fp))[r0:w]
    movqb   -8, -64(63(fp))[r1:w]
    movqb   7, -8192(8191(fp))[r2:w]
    movqb   -8, -8192(8191(fp))[r3:w]
    movqb   7, -536870912(536870911(fp))[r4:w]
    movqb   -8, -536870912(536870911(fp))[r5:w]
    movqb   7, -64(63(sp))[r7:w]
    movqb   -8, -64(63(sp))[r6:w]
    movqb   7, -8192(8191(sp))[r5:w]
    movqb   -8, -8192(8191(sp))[r4:w]
    movqb   7, -536870912(536870911(sp))[r3:w]
    movqb   -8, -536870912(536870911(sp))[r2:w]
    movqb   7, -64(63(sb))[r0:w]
    movqb   -8, -64(63(sb))[r1:w]
    movqb   7, -8192(8191(sb))[r2:w]
    movqb   -8, -8192(8191(sb))[r3:w]
    movqb   7, -536870912(536870911(sb))[r4:w]
    movqb   -8, -536870912(536870911(sb))[r5:w]
    #
    # Absolute
    movqb   7, @63[r0:w]
    movqb   -8, @63[r0:w]
    movqb   7, @8191[r0:w]
    movqb   -8, @8191[r0:w]
    movqb   7, @536870911[r0:w]
    movqb   -8, @536870911[r0:w]
    #
    # External
    movqb   7, ext(-64) + 63[r0:w]
    movqb   -8, ext(-64) + 63[r0:w]
    movqb   7, ext(-8192) + 8191[r0:w]
    movqb   -8, ext(-8192) + 8191[r0:w]
    movqb   7, ext(-536870912) + 536870911[r0:w]
    movqb   -8, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    movqb   7, tos[r0:w]
    movqb   -8, tos[r0:w]
    #
    # Memory Space
    movqb   7, 63(fp)[r0:w]
    movqb   -8, 63(fp)[r0:w]
    movqb   7, 8191(fp)[r0:w]
    movqb   -8, 8191(fp)[r0:w]
    movqb   7, 536870911(fp)[r0:w]
    movqb   -8, 536870911(fp)[r0:w]
    movqb   7, 63(sp)[r0:w]
    movqb   -8, 63(sp)[r0:w]
    movqb   7, 8191(sp)[r0:w]
    movqb   -8, 8191(sp)[r0:w]
    movqb   7, 536870911(sp)[r0:w]
    movqb   -8, 536870911(sp)[r0:w]
    movqb   7, 63(sb)[r0:w]
    movqb   -8, 63(sb)[r0:w]
    movqb   7, 8191(sb)[r0:w]
    movqb   -8, 8191(sb)[r0:w]
    movqb   7, 536870911(sb)[r0:w]
    movqb   -8, 536870911(sb)[r0:w]
    movqb   7, 63(pc)[r0:w]
    movqb   -8, 63(pc)[r0:w]
    movqb   7, 8191(pc)[r0:w]
    movqb   -8, 8191(pc)[r0:w]
    movqb   7, 536870911(pc)[r0:w]
    movqb   -8, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    movqb   7, r1[r0:d]
    movqb   -8, r1[r0:d]
    #
    # Register Relative
    movqb   7, 63(r1)[r0:d]
    movqb   -8, 63(r1)[r0:d]
    movqb   7, 8191(r1)[r0:d]
    movqb   -8, 8191(r1)[r0:d]
    movqb   7, 536870911(r1)[r0:d]
    movqb   -8, 536870911(r1)[r0:d]
    #
    # Memory Relative
    movqb   7, -64(63(fp))[r0:d]
    movqb   -8, -64(63(fp))[r1:d]
    movqb   7, -8192(8191(fp))[r2:d]
    movqb   -8, -8192(8191(fp))[r3:d]
    movqb   7, -536870912(536870911(fp))[r4:d]
    movqb   -8, -536870912(536870911(fp))[r5:d]
    movqb   7, -64(63(sp))[r7:d]
    movqb   -8, -64(63(sp))[r6:d]
    movqb   7, -8192(8191(sp))[r5:d]
    movqb   -8, -8192(8191(sp))[r4:d]
    movqb   7, -536870912(536870911(sp))[r3:d]
    movqb   -8, -536870912(536870911(sp))[r2:d]
    movqb   7, -64(63(sb))[r0:d]
    movqb   -8, -64(63(sb))[r1:d]
    movqb   7, -8192(8191(sb))[r2:d]
    movqb   -8, -8192(8191(sb))[r3:d]
    movqb   7, -536870912(536870911(sb))[r4:d]
    movqb   -8, -536870912(536870911(sb))[r5:d]
    #
    # Absolute
    movqb   7, @63[r0:d]
    movqb   -8, @63[r0:d]
    movqb   7, @8191[r0:d]
    movqb   -8, @8191[r0:d]
    movqb   7, @536870911[r0:d]
    movqb   -8, @536870911[r0:d]
    #
    # External
    movqb   7, ext(-64) + 63[r0:d]
    movqb   -8, ext(-64) + 63[r0:d]
    movqb   7, ext(-8192) + 8191[r0:d]
    movqb   -8, ext(-8192) + 8191[r0:d]
    movqb   7, ext(-536870912) + 536870911[r0:d]
    movqb   -8, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    movqb   7, tos[r0:d]
    movqb   -8, tos[r0:d]
    #
    # Memory Space
    movqb   7, 63(fp)[r0:d]
    movqb   -8, 63(fp)[r0:d]
    movqb   7, 8191(fp)[r0:d]
    movqb   -8, 8191(fp)[r0:d]
    movqb   7, 536870911(fp)[r0:d]
    movqb   -8, 536870911(fp)[r0:d]
    movqb   7, 63(sp)[r0:d]
    movqb   -8, 63(sp)[r0:d]
    movqb   7, 8191(sp)[r0:d]
    movqb   -8, 8191(sp)[r0:d]
    movqb   7, 536870911(sp)[r0:d]
    movqb   -8, 536870911(sp)[r0:d]
    movqb   7, 63(sb)[r0:d]
    movqb   -8, 63(sb)[r0:d]
    movqb   7, 8191(sb)[r0:d]
    movqb   -8, 8191(sb)[r0:d]
    movqb   7, 536870911(sb)[r0:d]
    movqb   -8, 536870911(sb)[r0:d]
    movqb   7, 63(pc)[r0:d]
    movqb   -8, 63(pc)[r0:d]
    movqb   7, 8191(pc)[r0:d]
    movqb   -8, 8191(pc)[r0:d]
    movqb   7, 536870911(pc)[r0:d]
    movqb   -8, 536870911(pc)[r0:d]
#
# ADD quick WORD
#
#
# Register
#
    movqw   7, r0
    movqw   -8, r0
    movqw   7, r1
    movqw   -8, r1
    movqw   7, r2
    movqw   -8, r2
    movqw   7, r3
    movqw   -8, r3
    movqw   7, r4
    movqw   -8, r4
    movqw   7, r5
    movqw   -8, r5
    movqw   7, r6
    movqw   -8, r6
    movqw   7, r7
    movqw   -8, r7
#
# Register relative
#
    movqw   7, 63(r0)
    movqw   -8, -64(r0)
    movqw   7, 8191(r0)
    movqw   -8, -8192(r0)
    movqw   7, 536870911(r0)
    movqw   -8, -536870912(r0)
    #
    movqw   7, 63(r1)
    movqw   -8, -64(r1)
    movqw   7, 8191(r1)
    movqw   -8, -8192(r1)
    movqw   7, 536870911(r1)
    movqw   -8, -536870912(r1)
    #
    movqw   7, 63(r2)
    movqw   -8, -64(r2)
    movqw   7, 8191(r2)
    movqw   -8, -8192(r2)
    movqw   7, 536870911(r2)
    movqw   -8, -536870912(r2)
    #
    movqw   7, 63(r3)
    movqw   -8, -64(r3)
    movqw   7, 8191(r3)
    movqw   -8, -8192(r3)
    movqw   7, 536870911(r3)
    movqw   -8, -536870912(r3)
    #
    movqw   7, 63(r4)
    movqw   -8, -64(r4)
    movqw   7, 8191(r4)
    movqw   -8, -8192(r4)
    movqw   7, 536870911(r4)
    movqw   -8, -536870912(r4)
    #
    movqw   7, 63(r5)
    movqw   -8, -64(r5)
    movqw   7, 8191(r5)
    movqw   -8, -8192(r5)
    movqw   7, 536870911(r5)
    movqw   -8, -536870912(r5)
    #
    movqw   7, 63(r6)
    movqw   -8, -64(r6)
    movqw   7, 8191(r6)
    movqw   -8, -8192(r6)
    movqw   7, 536870911(r6)
    movqw   -8, -536870912(r6)
    #
    movqw   7, 63(r7)
    movqw   -8, -64(r7)
    movqw   7, 8191(r7)
    movqw   -8, -8192(r7)
    movqw   7, 536870911(r7)
    movqw   -8, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    movqw   7, -64(63(fp))
    movqw   -8, -64(63(fp))
    movqw   7, 63(-64(fp))
    movqw   -8, 63(-64(fp))
    #
    movqw   7, -8192(63(fp))
    movqw   -8, -64(8191(fp))
    movqw   7, 63(-8192(fp))
    movqw   -8, 8191(-64(fp))
    #
    movqw   7, -8192(8191(fp))
    movqw   -8, -8192(8191(fp))
    movqw   7, 8191(-8192(fp))
    movqw   -8, 8191(-8192(fp))
    #
    movqw   7, -536870912(8191(fp))
    movqw   -8, -8192(536870911(fp))
    movqw   7, 8191(-536870912(fp))
    movqw   -8, 536870911(-8192(fp))
    #
    movqw   7, 536870911(-536870912(fp))
    movqw   -8, 536870911(-536870912(fp))
    movqw   7, -536870912(536870911(fp))
    movqw   -8, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    movqw   7, -64(63(sp))
    movqw   -8, -64(63(sp))
    movqw   7, 63(-64(sp))
    movqw   -8, 63(-64(sp))
    #
    movqw   7, -8192(63(sp))
    movqw   -8, -64(8191(sp))
    movqw   7, 63(-8192(sp))
    movqw   -8, 8191(-64(sp))
    #
    movqw   7, -8192(8191(sp))
    movqw   -8, -8192(8191(sp))
    movqw   7, 8191(-8192(sp))
    movqw   -8, 8191(-8192(sp))
    #
    movqw   7, -536870912(8191(sp))
    movqw   -8, -8192(536870911(sp))
    movqw   7, 8191(-536870912(sp))
    movqw   -8, 536870911(-8192(sp))
    #
    movqw   7, 536870911(-536870912(sp))
    movqw   -8, 536870911(-536870912(sp))
    movqw   7, -536870912(536870911(sp))
    movqw   -8, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    movqw   7, -64(63(sb))
    movqw   -8, -64(63(sb))
    movqw   7, 63(-64(sb))
    movqw   -8, 63(-64(sb))
    #
    movqw   7, -8192(63(sb))
    movqw   -8, -64(8191(sb))
    movqw   7, 63(-8192(sb))
    movqw   -8, 8191(-64(sb))
    #
    movqw   7, -8192(8191(sb))
    movqw   -8, -8192(8191(sb))
    movqw   7, 8191(-8192(sb))
    movqw   -8, 8191(-8192(sb))
    #
    movqw   7, -536870912(8191(sb))
    movqw   -8, -8192(536870911(sb))
    movqw   7, 8191(-536870912(sb))
    movqw   -8, 536870911(-8192(sb))
    #
    movqw   7, 536870911(-536870912(sb))
    movqw   -8, 536870911(-536870912(sb))
    movqw   7, -536870912(536870911(sb))
    movqw   -8, -536870912(536870911(sb))

#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    movqw	7, @-64
    movqw	-8, @-64
    movqw	7, @63
    movqw	-8, @63
    movqw	7, @-65
    movqw	-8, @-65
    movqw	7, @64
    movqw	-8, @64
    #
    movqw	7, @-8192
    movqw	-8, @-8192
    movqw	7, @8191
    movqw	-8, @8191
    movqw	7, @-8193
    movqw	-8, @-8193
    movqw	7, @8192
    movqw	-8, @8192
    #
    movqw	7, @-536870912
    movqw	-8, @536870911
#
# External
#
    movqw	7, ext(-64) + 63
    movqw	-8, ext(-64) + 63
    movqw	7, ext(63) + -64
    movqw	-8, ext(63) + -64
    #
    movqw	7, ext(-65) + 63
    movqw	-8, ext(-65) + 63
    movqw	7, ext(63) + -65
    movqw	-8, ext(63) + -65
    movqw	7, ext(-64) + 64
    movqw	-8, ext(-64) + 64
    movqw	7, ext(64) + -64
    movqw	-8, ext(64) + -64
    #
    movqw	7, ext(-8192) + 8191
    movqw	-8, ext(-8192) + 8191
    movqw	7, ext(8191) + -8192
    movqw	-8, ext(8191) + -8192
    #
    movqw	7, ext(-536870912) + 8191
    movqw	-8, ext(-536870912) + 8191
    movqw	7, ext(8191) + -536870912
    movqw	-8, ext(8191) + -536870912
    movqw	7, ext(-8192) + 536870911
    movqw	-8, ext(-8192) + 536870911
    movqw	7, ext(536870911) + -8192
    movqw	-8, ext(536870911) + -8192
    #
    movqw	7, ext(-536870912) + 536870911
    movqw	-8, ext(-536870912) + 536870911
    movqw	7, ext(536870911) + -536870912
    movqw	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    movqw	7, tos
    movqw	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    movqw	7, -64(fp)
    movqw	-8, -64(fp)
    movqw	7, 63(fp)
    movqw	-8, 63(fp)
    #
    movqw	7, -8192(fp)
    movqw	-8, -8192(fp)
    movqw	7, 8191(fp)
    movqw	-8, 8191(fp)
    #
    movqw	7, -536870912(fp)
    movqw	-8, -536870912(fp)
    movqw	7, 536870911(fp)
    movqw	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    movqw	7, -64(sp)
    movqw	-8, -64(sp)
    movqw	7, 63(sp)
    movqw	-8, 63(sp)
    #
    movqw	7, -8192(sp)
    movqw	-8, -8192(sp)
    movqw	7, 8191(sp)
    movqw	-8, 8191(sp)
    #
    movqw	7, -536870912(sp)
    movqw	-8, -536870912(sp)
    movqw	7, 536870911(sp)
    movqw	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    movqw	7, -64(sb)
    movqw	-8, -64(sb)
    movqw	7, 63(sb)
    movqw	-8, 63(sb)
    #
    movqw	7, -8192(sb)
    movqw	-8, -8192(sb)
    movqw	7, 8191(sb)
    movqw	-8, 8191(sb)
    #
    movqw	7, -536870912(sb)
    movqw	-8, -536870912(sb)
    movqw	7, 536870911(sb)
    movqw	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    movqw	7, -64(pc)
    movqw	-8, -64(pc)
    movqw	7, 63(pc)
    movqw	-8, 63(pc)
    #
    movqw	7, -8192(pc)
    movqw	-8, -8192(pc)
    movqw	7, 8191(pc)
    movqw	-8, 8191(pc)
    #
    movqw	7, -536870912(pc)
    movqw	-8, -536870912(pc)
    movqw	7, 536870911(pc)
    movqw	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    movqw   7, r7[r0:b]
    movqw   -8, r6[r1:b]
    #
    # Register Relative
    movqw   7, 63(r5)[r2:b]
    movqw   -8, 63(r4)[r3:b]
    movqw   7, 8191(r3)[r4:b]
    movqw   -8, 8191(r2)[r5:b]
    movqw   7, 536870911(r1)[r6:b]
    movqw   -8, 536870911(r0)[r7:b]
    #
    # Memory Relative
    movqw   7, -64(63(fp))[r0:b]
    movqw   -8, -64(63(fp))[r1:b]
    movqw   7, -8192(8191(fp))[r2:b]
    movqw   -8, -8192(8191(fp))[r3:b]
    movqw   7, -536870912(536870911(fp))[r4:b]
    movqw   -8, -536870912(536870911(fp))[r5:b]
    movqw   7, -64(63(sp))[r7:b]
    movqw   -8, -64(63(sp))[r6:b]
    movqw   7, -8192(8191(sp))[r5:b]
    movqw   -8, -8192(8191(sp))[r4:b]
    movqw   7, -536870912(536870911(sp))[r3:b]
    movqw   -8, -536870912(536870911(sp))[r2:b]
    movqw   7, -64(63(sb))[r0:b]
    movqw   -8, -64(63(sb))[r1:b]
    movqw   7, -8192(8191(sb))[r2:b]
    movqw   -8, -8192(8191(sb))[r3:b]
    movqw   7, -536870912(536870911(sb))[r4:b]
    movqw   -8, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    movqw   7, @63[r0:b]
    movqw   -8, @63[r0:b]
    movqw   7, @8191[r0:b]
    movqw   -8, @8191[r0:b]
    movqw   7, @536870911[r0:b]
    movqw   -8, @536870911[r0:b]
    #
    # External
    movqw   7, ext(-64) + 63[r0:b]
    movqw   -8, ext(-64) + 63[r0:b]
    movqw   7, ext(-8192) + 8191[r0:b]
    movqw   -8, ext(-8192) + 8191[r0:b]
    movqw   7, ext(-536870912) + 536870911[r0:b]
    movqw   -8, ext(-536870912) + 536870911[r0:b]
    #
    # Top Of Stack
    movqw   7, tos[r0:b]
    movqw   -8, tos[r0:b]
    #
    # Memory Space
    movqw   7, 63(fp)[r0:b]
    movqw   -8, 63(fp)[r0:b]
    movqw   7, 8191(fp)[r0:b]
    movqw   -8, 8191(fp)[r0:b]
    movqw   7, 536870911(fp)[r0:b]
    movqw   -8, 536870911(fp)[r0:b]
    movqw   7, 63(sp)[r0:b]
    movqw   -8, 63(sp)[r0:b]
    movqw   7, 8191(sp)[r0:b]
    movqw   -8, 8191(sp)[r0:b]
    movqw   7, 536870911(sp)[r0:b]
    movqw   -8, 536870911(sp)[r0:b]
    movqw   7, 63(sb)[r0:b]
    movqw   -8, 63(sb)[r0:b]
    movqw   7, 8191(sb)[r0:b]
    movqw   -8, 8191(sb)[r0:b]
    movqw   7, 536870911(sb)[r0:b]
    movqw   -8, 536870911(sb)[r0:b]
    movqw   7, 63(pc)[r0:b]
    movqw   -8, 63(pc)[r0:b]
    movqw   7, 8191(pc)[r0:b]
    movqw   -8, 8191(pc)[r0:b]
    movqw   7, 536870911(pc)[r0:b]
    movqw   -8, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    movqw   7, r1[r0:w]
    movqw   -8, r1[r0:w]
    #
    # Register Relative
    movqw   7, 63(r1)[r0:w]
    movqw   -8, 63(r1)[r0:w]
    movqw   7, 8191(r1)[r0:w]
    movqw   -8, 8191(r1)[r0:w]
    movqw   7, 536870911(r1)[r0:w]
    movqw   -8, 536870911(r1)[r0:w]
    #
    # Memory Relative
    movqw   7, -64(63(fp))[r0:w]
    movqw   -8, -64(63(fp))[r0:w]
    movqw   7, -8192(8191(fp))[r0:w]
    movqw   -8, -8192(8191(fp))[r0:w]
    movqw   7, -536870912(536870911(fp))[r0:w]
    movqw   -8, -536870912(536870911(fp))[r0:w]
    #
    # Absolute
    movqw   7, @63[r0:w]
    movqw   -8, @63[r0:w]
    movqw   7, @8191[r0:w]
    movqw   -8, @8191[r0:w]
    movqw   7, @536870911[r0:w]
    movqw   -8, @536870911[r0:w]
    #
    # External
    movqw   7, ext(-64) + 63[r0:w]
    movqw   -8, ext(-64) + 63[r0:w]
    movqw   7, ext(-8192) + 8191[r0:w]
    movqw   -8, ext(-8192) + 8191[r0:w]
    movqw   7, ext(-536870912) + 536870911[r0:w]
    movqw   -8, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    movqw   7, tos[r0:w]
    movqw   -8, tos[r0:w]
    #
    # Memory Space
    movqw   7, 63(fp)[r0:w]
    movqw   -8, 63(fp)[r0:w]
    movqw   7, 8191(fp)[r0:w]
    movqw   -8, 8191(fp)[r0:w]
    movqw   7, 536870911(fp)[r0:w]
    movqw   -8, 536870911(fp)[r0:w]
    movqw   7, 63(sp)[r0:w]
    movqw   -8, 63(sp)[r0:w]
    movqw   7, 8191(sp)[r0:w]
    movqw   -8, 8191(sp)[r0:w]
    movqw   7, 536870911(sp)[r0:w]
    movqw   -8, 536870911(sp)[r0:w]
    movqw   7, 63(sb)[r0:w]
    movqw   -8, 63(sb)[r0:w]
    movqw   7, 8191(sb)[r0:w]
    movqw   -8, 8191(sb)[r0:w]
    movqw   7, 536870911(sb)[r0:w]
    movqw   -8, 536870911(sb)[r0:w]
    movqw   7, 63(pc)[r0:w]
    movqw   -8, 63(pc)[r0:w]
    movqw   7, 8191(pc)[r0:w]
    movqw   -8, 8191(pc)[r0:w]
    movqw   7, 536870911(pc)[r0:w]
    movqw   -8, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    movqw   7, r1[r0:d]
    movqw   -8, r1[r0:d]
    #
    # Register Relative
    movqw   7, 63(r1)[r0:d]
    movqw   -8, 63(r1)[r0:d]
    movqw   7, 8191(r1)[r0:d]
    movqw   -8, 8191(r1)[r0:d]
    movqw   7, 536870911(r1)[r0:d]
    movqw   -8, 536870911(r1)[r0:d]
    #
    # Memory Relative
    movqw   7, -64(63(fp))[r0:d]
    movqw   -8, -64(63(fp))[r0:d]
    movqw   7, -8192(8191(fp))[r0:d]
    movqw   -8, -8192(8191(fp))[r0:d]
    movqw   7, -536870912(536870911(fp))[r0:d]
    movqw   -8, -536870912(536870911(fp))[r0:d]
    #
    # Absolute
    movqw   7, @63[r0:d]
    movqw   -8, @63[r0:d]
    movqw   7, @8191[r0:d]
    movqw   -8, @8191[r0:d]
    movqw   7, @536870911[r0:d]
    movqw   -8, @536870911[r0:d]
    #
    # External
    movqw   7, ext(-64) + 63[r0:d]
    movqw   -8, ext(-64) + 63[r0:d]
    movqw   7, ext(-8192) + 8191[r0:d]
    movqw   -8, ext(-8192) + 8191[r0:d]
    movqw   7, ext(-536870912) + 536870911[r0:d]
    movqw   -8, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    movqw   7, tos[r0:d]
    movqw   -8, tos[r0:d]
    #
    # Memory Space
    movqw   7, 63(fp)[r0:d]
    movqw   -8, 63(fp)[r0:d]
    movqw   7, 8191(fp)[r0:d]
    movqw   -8, 8191(fp)[r0:d]
    movqw   7, 536870911(fp)[r0:d]
    movqw   -8, 536870911(fp)[r0:d]
    movqw   7, 63(sp)[r0:d]
    movqw   -8, 63(sp)[r0:d]
    movqw   7, 8191(sp)[r0:d]
    movqw   -8, 8191(sp)[r0:d]
    movqw   7, 536870911(sp)[r0:d]
    movqw   -8, 536870911(sp)[r0:d]
    movqw   7, 63(sb)[r0:d]
    movqw   -8, 63(sb)[r0:d]
    movqw   7, 8191(sb)[r0:d]
    movqw   -8, 8191(sb)[r0:d]
    movqw   7, 536870911(sb)[r0:d]
    movqw   -8, 536870911(sb)[r0:d]
    movqw   7, 63(pc)[r0:d]
    movqw   -8, 63(pc)[r0:d]
    movqw   7, 8191(pc)[r0:d]
    movqw   -8, 8191(pc)[r0:d]
    movqw   7, 536870911(pc)[r0:d]
    movqw   -8, 536870911(pc)[r0:d]

#
# ADD quick DOUBLE WORD
#
#
# Register
#
    movqd   7, r0
    movqd   -8, r0
    movqd   7, r1
    movqd   -8, r1
    movqd   7, r2
    movqd   -8, r2
    movqd   7, r3
    movqd   -8, r3
    movqd   7, r4
    movqd   -8, r4
    movqd   7, r5
    movqd   -8, r5
    movqd   7, r6
    movqd   -8, r6
    movqd   7, r7
    movqd   -8, r7
#
# Register relative
#
    movqd   7, 63(r0)
    movqd   -8, -64(r0)
    movqd   7, 8191(r0)
    movqd   -8, -8192(r0)
    movqd   7, 536870911(r0)
    movqd   -8, -536870912(r0)
    #
    movqd   7, 63(r1)
    movqd   -8, -64(r1)
    movqd   7, 8191(r1)
    movqd   -8, -8192(r1)
    movqd   7, 536870911(r1)
    movqd   -8, -536870912(r1)
    #
    movqd   7, 63(r2)
    movqd   -8, -64(r2)
    movqd   7, 8191(r2)
    movqd   -8, -8192(r2)
    movqd   7, 536870911(r2)
    movqd   -8, -536870912(r2)
    #
    movqd   7, 63(r3)
    movqd   -8, -64(r3)
    movqd   7, 8191(r3)
    movqd   -8, -8192(r3)
    movqd   7, 536870911(r3)
    movqd   -8, -536870912(r3)
    #
    movqd   7, 63(r4)
    movqd   -8, -64(r4)
    movqd   7, 8191(r4)
    movqd   -8, -8192(r4)
    movqd   7, 536870911(r4)
    movqd   -8, -536870912(r4)
    #
    movqd   7, 63(r5)
    movqd   -8, -64(r5)
    movqd   7, 8191(r5)
    movqd   -8, -8192(r5)
    movqd   7, 536870911(r5)
    movqd   -8, -536870912(r5)
    #
    movqd   7, 63(r6)
    movqd   -8, -64(r6)
    movqd   7, 8191(r6)
    movqd   -8, -8192(r6)
    movqd   7, 536870911(r6)
    movqd   -8, -536870912(r6)
    #
    movqd   7, 63(r7)
    movqd   -8, -64(r7)
    movqd   7, 8191(r7)
    movqd   -8, -8192(r7)
    movqd   7, 536870911(r7)
    movqd   -8, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    movqd   7, -64(63(fp))
    movqd   -8, -64(63(fp))
    movqd   7, 63(-64(fp))
    movqd   -8, 63(-64(fp))
    #
    movqd   7, -8192(63(fp))
    movqd   -8, -64(8191(fp))
    movqd   7, 63(-8192(fp))
    movqd   -8, 8191(-64(fp))
    #
    movqd   7, -8192(8191(fp))
    movqd   -8, -8192(8191(fp))
    movqd   7, 8191(-8192(fp))
    movqd   -8, 8191(-8192(fp))
    #
    movqd   7, -536870912(8191(fp))
    movqd   -8, -8192(536870911(fp))
    movqd   7, 8191(-536870912(fp))
    movqd   -8, 536870911(-8192(fp))
    #
    movqd   7, 536870911(-536870912(fp))
    movqd   -8, 536870911(-536870912(fp))
    movqd   7, -536870912(536870911(fp))
    movqd   -8, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    movqd   7, -64(63(sp))
    movqd   -8, -64(63(sp))
    movqd   7, 63(-64(sp))
    movqd   -8, 63(-64(sp))
    #
    movqd   7, -8192(63(sp))
    movqd   -8, -64(8191(sp))
    movqd   7, 63(-8192(sp))
    movqd   -8, 8191(-64(sp))
    #
    movqd   7, -8192(8191(sp))
    movqd   -8, -8192(8191(sp))
    movqd   7, 8191(-8192(sp))
    movqd   -8, 8191(-8192(sp))
    #
    movqd   7, -536870912(8191(sp))
    movqd   -8, -8192(536870911(sp))
    movqd   7, 8191(-536870912(sp))
    movqd   -8, 536870911(-8192(sp))
    #
    movqd   7, 536870911(-536870912(sp))
    movqd   -8, 536870911(-536870912(sp))
    movqd   7, -536870912(536870911(sp))
    movqd   -8, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    movqd   7, -64(63(sb))
    movqd   -8, -64(63(sb))
    movqd   7, 63(-64(sb))
    movqd   -8, 63(-64(sb))
    #
    movqd   7, -8192(63(sb))
    movqd   -8, -64(8191(sb))
    movqd   7, 63(-8192(sb))
    movqd   -8, 8191(-64(sb))
    #
    movqd   7, -8192(8191(sb))
    movqd   -8, -8192(8191(sb))
    movqd   7, 8191(-8192(sb))
    movqd   -8, 8191(-8192(sb))
    #
    movqd   7, -536870912(8191(sb))
    movqd   -8, -8192(536870911(sb))
    movqd   7, 8191(-536870912(sb))
    movqd   -8, 536870911(-8192(sb))
    #
    movqd   7, 536870911(-536870912(sb))
    movqd   -8, 536870911(-536870912(sb))
    movqd   7, -536870912(536870911(sb))
    movqd   -8, -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    movqd	7, @-64
    movqd	-8, @-64
    movqd	7, @63
    movqd	-8, @63
    movqd	7, @-65
    movqd	-8, @-65
    movqd	7, @64
    movqd	-8, @64
    #
    movqd	7, @-8192
    movqd	-8, @-8192
    movqd	7, @8191
    movqd	-8, @8191
    movqd	7, @-8193
    movqd	-8, @-8193
    movqd	7, @8192
    movqd	-8, @8192
    #
    movqd	7, @-536870912
    movqd	-8, @536870911
#
# External
#
    movqd	7, ext(-64) + 63
    movqd	-8, ext(-64) + 63
    movqd	7, ext(63) + -64
    movqd	-8, ext(63) + -64
    #
    movqd	7, ext(-65) + 63
    movqd	-8, ext(-65) + 63
    movqd	7, ext(63) + -65
    movqd	-8, ext(63) + -65
    movqd	7, ext(-64) + 64
    movqd	-8, ext(-64) + 64
    movqd	7, ext(64) + -64
    movqd	-8, ext(64) + -64
    #
    movqd	7, ext(-8192) + 8191
    movqd	-8, ext(-8192) + 8191
    movqd	7, ext(8191) + -8192
    movqd	-8, ext(8191) + -8192
    #
    movqd	7, ext(-536870912) + 8191
    movqd	-8, ext(-536870912) + 8191
    movqd	7, ext(8191) + -536870912
    movqd	-8, ext(8191) + -536870912
    movqd	7, ext(-8192) + 536870911
    movqd	-8, ext(-8192) + 536870911
    movqd	7, ext(536870911) + -8192
    movqd	-8, ext(536870911) + -8192
    #
    movqd	7, ext(-536870912) + 536870911
    movqd	-8, ext(-536870912) + 536870911
    movqd	7, ext(536870911) + -536870912
    movqd	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    movqd	7, tos
    movqd	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    movqd	7, -64(fp)
    movqd	-8, -64(fp)
    movqd	7, 63(fp)
    movqd	-8, 63(fp)
    #
    movqd	7, -8192(fp)
    movqd	-8, -8192(fp)
    movqd	7, 8191(fp)
    movqd	-8, 8191(fp)
    #
    movqd	7, -536870912(fp)
    movqd	-8, -536870912(fp)
    movqd	7, 536870911(fp)
    movqd	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    movqd	7, -64(sp)
    movqd	-8, -64(sp)
    movqd	7, 63(sp)
    movqd	-8, 63(sp)
    #
    movqd	7, -8192(sp)
    movqd	-8, -8192(sp)
    movqd	7, 8191(sp)
    movqd	-8, 8191(sp)
    #
    movqd	7, -536870912(sp)
    movqd	-8, -536870912(sp)
    movqd	7, 536870911(sp)
    movqd	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    movqd	7, -64(sb)
    movqd	-8, -64(sb)
    movqd	7, 63(sb)
    movqd	-8, 63(sb)
    #
    movqd	7, -8192(sb)
    movqd	-8, -8192(sb)
    movqd	7, 8191(sb)
    movqd	-8, 8191(sb)
    #
    movqd	7, -536870912(sb)
    movqd	-8, -536870912(sb)
    movqd	7, 536870911(sb)
    movqd	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    movqd	7, -64(pc)
    movqd	-8, -64(pc)
    movqd	7, 63(pc)
    movqd	-8, 63(pc)
    #
    movqd	7, -8192(pc)
    movqd	-8, -8192(pc)
    movqd	7, 8191(pc)
    movqd	-8, 8191(pc)
    #
    movqd	7, -536870912(pc)
    movqd	-8, -536870912(pc)
    movqd	7, 536870911(pc)
    movqd	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    movqd   7, r7[r0:b]
    movqd   -8, r6[r1:b]
    #
    # Register Relative
    movqd   7, 63(r5)[r2:b]
    movqd   -8, 63(r4)[r3:b]
    movqd   7, 8191(r3)[r4:b]
    movqd   -8, 8191(r2)[r5:b]
    movqd   7, 536870911(r1)[r6:b]
    movqd   -8, 536870911(r0)[r7:b]
    #
    # Memory Relative
    movqd   7, -64(63(fp))[r0:b]
    movqd   -8, -64(63(fp))[r0:b]
    movqd   7, -8192(8191(fp))[r0:b]
    movqd   -8, -8192(8191(fp))[r0:b]
    movqd   7, -536870912(536870911(fp))[r0:b]
    movqd   -8, -536870912(536870911(fp))[r0:b]
    #
    # Absolute
    movqd   7, @63[r0:b]
    movqd   -8, @63[r0:b]
    movqd   7, @8191[r0:b]
    movqd   -8, @8191[r0:b]
    movqd   7, @536870911[r0:b]
    movqd   -8, @536870911[r0:b]
    #
    # External
    movqd   7, ext(-64) + 63[r0:b]
    movqd   -8, ext(-64) + 63[r0:b]
    movqd   7, ext(-8192) + 8191[r0:b]
    movqd   -8, ext(-8192) + 8191[r0:b]
    movqd   7, ext(-536870912) + 536870911[r0:b]
    movqd   -8, ext(-536870912) + 536870911[r0:b]
    #
    # Top Of Stack
    movqd   7, tos[r0:b]
    movqd   -8, tos[r0:b]
    #
    # Memory Space
    movqd   7, 63(fp)[r0:b]
    movqd   -8, 63(fp)[r0:b]
    movqd   7, 8191(fp)[r0:b]
    movqd   -8, 8191(fp)[r0:b]
    movqd   7, 536870911(fp)[r0:b]
    movqd   -8, 536870911(fp)[r0:b]
    movqd   7, 63(sp)[r0:b]
    movqd   -8, 63(sp)[r0:b]
    movqd   7, 8191(sp)[r0:b]
    movqd   -8, 8191(sp)[r0:b]
    movqd   7, 536870911(sp)[r0:b]
    movqd   -8, 536870911(sp)[r0:b]
    movqd   7, 63(sb)[r0:b]
    movqd   -8, 63(sb)[r0:b]
    movqd   7, 8191(sb)[r0:b]
    movqd   -8, 8191(sb)[r0:b]
    movqd   7, 536870911(sb)[r0:b]
    movqd   -8, 536870911(sb)[r0:b]
    movqd   7, 63(pc)[r0:b]
    movqd   -8, 63(pc)[r0:b]
    movqd   7, 8191(pc)[r0:b]
    movqd   -8, 8191(pc)[r0:b]
    movqd   7, 536870911(pc)[r0:b]
    movqd   -8, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    movqd   7, r1[r0:w]
    movqd   -8, r1[r0:w]
    #
    # Register Relative
    movqd   7, 63(r1)[r0:w]
    movqd   -8, 63(r1)[r0:w]
    movqd   7, 8191(r1)[r0:w]
    movqd   -8, 8191(r1)[r0:w]
    movqd   7, 536870911(r1)[r0:w]
    movqd   -8, 536870911(r1)[r0:w]
    #
    # Memory Relative
    movqd   7, -64(63(fp))[r0:w]
    movqd   -8, -64(63(fp))[r0:w]
    movqd   7, -8192(8191(fp))[r0:w]
    movqd   -8, -8192(8191(fp))[r0:w]
    movqd   7, -536870912(536870911(fp))[r0:w]
    movqd   -8, -536870912(536870911(fp))[r0:w]
    #
    # Absolute
    movqd   7, @63[r0:w]
    movqd   -8, @63[r0:w]
    movqd   7, @8191[r0:w]
    movqd   -8, @8191[r0:w]
    movqd   7, @536870911[r0:w]
    movqd   -8, @536870911[r0:w]
    #
    # External
    movqd   7, ext(-64) + 63[r0:w]
    movqd   -8, ext(-64) + 63[r0:w]
    movqd   7, ext(-8192) + 8191[r0:w]
    movqd   -8, ext(-8192) + 8191[r0:w]
    movqd   7, ext(-536870912) + 536870911[r0:w]
    movqd   -8, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    movqd   7, tos[r0:w]
    movqd   -8, tos[r0:w]
    #
    # Memory Space
    movqd   7, 63(fp)[r0:w]
    movqd   -8, 63(fp)[r0:w]
    movqd   7, 8191(fp)[r0:w]
    movqd   -8, 8191(fp)[r0:w]
    movqd   7, 536870911(fp)[r0:w]
    movqd   -8, 536870911(fp)[r0:w]
    movqd   7, 63(sp)[r0:w]
    movqd   -8, 63(sp)[r0:w]
    movqd   7, 8191(sp)[r0:w]
    movqd   -8, 8191(sp)[r0:w]
    movqd   7, 536870911(sp)[r0:w]
    movqd   -8, 536870911(sp)[r0:w]
    movqd   7, 63(sb)[r0:w]
    movqd   -8, 63(sb)[r0:w]
    movqd   7, 8191(sb)[r0:w]
    movqd   -8, 8191(sb)[r0:w]
    movqd   7, 536870911(sb)[r0:w]
    movqd   -8, 536870911(sb)[r0:w]
    movqd   7, 63(pc)[r0:w]
    movqd   -8, 63(pc)[r0:w]
    movqd   7, 8191(pc)[r0:w]
    movqd   -8, 8191(pc)[r0:w]
    movqd   7, 536870911(pc)[r0:w]
    movqd   -8, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    movqd   7, r1[r0:d]
    movqd   -8, r1[r0:d]
    #
    # Register Relative
    movqd   7, 63(r1)[r0:d]
    movqd   -8, 63(r1)[r0:d]
    movqd   7, 8191(r1)[r0:d]
    movqd   -8, 8191(r1)[r0:d]
    movqd   7, 536870911(r1)[r0:d]
    movqd   -8, 536870911(r1)[r0:d]
    #
    # Memory Relative
    movqd   7, -64(63(fp))[r0:d]
    movqd   -8, -64(63(fp))[r0:d]
    movqd   7, -8192(8191(fp))[r0:d]
    movqd   -8, -8192(8191(fp))[r0:d]
    movqd   7, -536870912(536870911(fp))[r0:d]
    movqd   -8, -536870912(536870911(fp))[r0:d]
    #
    # Absolute
    movqd   7, @63[r0:d]
    movqd   -8, @63[r0:d]
    movqd   7, @8191[r0:d]
    movqd   -8, @8191[r0:d]
    movqd   7, @536870911[r0:d]
    movqd   -8, @536870911[r0:d]
    #
    # External
    movqd   7, ext(-64) + 63[r0:d]
    movqd   -8, ext(-64) + 63[r0:d]
    movqd   7, ext(-8192) + 8191[r0:d]
    movqd   -8, ext(-8192) + 8191[r0:d]
    movqd   7, ext(-536870912) + 536870911[r0:d]
    movqd   -8, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    movqd   7, tos[r0:d]
    movqd   -8, tos[r0:d]
    #
    # Memory Space
    movqd   7, 63(fp)[r0:d]
    movqd   -8, 63(fp)[r0:d]
    movqd   7, 8191(fp)[r0:d]
    movqd   -8, 8191(fp)[r0:d]
    movqd   7, 536870911(fp)[r0:d]
    movqd   -8, 536870911(fp)[r0:d]
    movqd   7, 63(sp)[r0:d]
    movqd   -8, 63(sp)[r0:d]
    movqd   7, 8191(sp)[r0:d]
    movqd   -8, 8191(sp)[r0:d]
    movqd   7, 536870911(sp)[r0:d]
    movqd   -8, 536870911(sp)[r0:d]
    movqd   7, 63(sb)[r0:d]
    movqd   -8, 63(sb)[r0:d]
    movqd   7, 8191(sb)[r0:d]
    movqd   -8, 8191(sb)[r0:d]
    movqd   7, 536870911(sb)[r0:d]
    movqd   -8, 536870911(sb)[r0:d]
    movqd   7, 63(pc)[r0:d]
    movqd   -8, 63(pc)[r0:d]
    movqd   7, 8191(pc)[r0:d]
    movqd   -8, 8191(pc)[r0:d]
    movqd   7, 536870911(pc)[r0:d]
    movqd   -8, 536870911(pc)[r0:d]
