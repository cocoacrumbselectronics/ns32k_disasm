mkdir -p ./output/format0/bin
mkdir -p ./output/format0/lst
mkdir -p ./output/format0/out

mkdir -p ./output/format1/bin
mkdir -p ./output/format1/lst
mkdir -p ./output/format1/out

mkdir -p ./output/format2/bin
mkdir -p ./output/format2/lst
mkdir -p ./output/format2/out

mkdir -p ./output/format3/bin
mkdir -p ./output/format3/lst
mkdir -p ./output/format3/out

mkdir -p ./output/format5/bin
mkdir -p ./output/format5/lst
mkdir -p ./output/format5/out

mkdir -p ./output/format6/bin
mkdir -p ./output/format6/lst
mkdir -p ./output/format6/out

mkdir -p ./output/format7/bin
mkdir -p ./output/format7/lst
mkdir -p ./output/format7/out

mkdir -p ./output/format8/bin
mkdir -p ./output/format8/lst
mkdir -p ./output/format8/out

mkdir -p ./output/format9/bin
mkdir -p ./output/format9/lst
mkdir -p ./output/format9/out

# No format 10

mkdir -p ./output/format11/bin
mkdir -p ./output/format11/lst
mkdir -p ./output/format11/out

# No format 12

# No format 13

mkdir -p ./output/format14/bin
mkdir -p ./output/format14/lst
mkdir -p ./output/format14/out


# Format 0 test cases:
# ====================
#   BR (Unconditional Branch)
./ns32k-as -aghls=./output/format0/lst/test-format-0-Bcond.lst \
           --listing-lhs-width 7 \
           -o ./output/format0/out/test-format-0-Bcond.out \
           ./input/format0/test-format-0-Bcond.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format0/out/test-format-0-Bcond.out \
                ./output/format0/bin/test-format-0-Bcond.bin

# Format 0 test cases:
# ====================
#   BSR (Branch To Subroutine)
./ns32k-as -aghls=./output/format1/lst/test-format-1-BSR.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-BSR.out \
           ./input/format1/test-format-1-BSR.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-BSR.out \
                ./output/format1/bin/test-format-1-BSR.bin
#   RET (Return from Subroutine)
./ns32k-as -aghls=./output/format1/lst/test-format-1-RET.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-RET.out \
           ./input/format1/test-format-1-RET.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-RET.out \
                ./output/format1/bin/test-format-1-RET.bin
#   CXP (Call External Procedure)
./ns32k-as -aghls=./output/format1/lst/test-format-1-CXP.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-CXP.out \
           ./input/format1/test-format-1-CXP.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-CXP.out \
                ./output/format1/bin/test-format-1-CXP.bin
#   RXP (Return from External Procedure)
./ns32k-as -aghls=./output/format1/lst/test-format-1-RXP.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-RXP.out \
           ./input/format1/test-format-1-RXP.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-RXP.out \
                ./output/format1/bin/test-format-1-RXP.bin
#   RETT (Return from Trap)
./ns32k-as -aghls=./output/format1/lst/test-format-1-RETT.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-RETT.out \
           ./input/format1/test-format-1-RETT.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-RETT.out \
                ./output/format1/bin/test-format-1-RETT.bin
#   RETI (Return from Interrupt)
./ns32k-as -aghls=./output/format1/lst/test-format-1-RETI.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-RETI.out \
           ./input/format1/test-format-1-RETI.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-RETI.out \
                ./output/format1/bin/test-format-1-RETI.bin
#   SAVE (Save General Purpose Registers)
./ns32k-as -aghls=./output/format1/lst/test-format-1-SAVE.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-SAVE.out \
           ./input/format1/test-format-1-SAVE.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-SAVE.out \
                ./output/format1/bin/test-format-1-SAVE.bin
#   RESTORE (Restore General Purpose Registers)
./ns32k-as -aghls=./output/format1/lst/test-format-1-RESTORE.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-RESTORE.out \
           ./input/format1/test-format-1-RESTORE.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-RESTORE.out \
                ./output/format1/bin/test-format-1-RESTORE.bin
#   ENTER (Enter New Procedure Context)
./ns32k-as -aghls=./output/format1/lst/test-format-1-ENTER.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-ENTER.out \
           ./input/format1/test-format-1-ENTER.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-ENTER.out \
                ./output/format1/bin/test-format-1-ENTER.bin
#   EXIT (Exit Procedure Context)
./ns32k-as -aghls=./output/format1/lst/test-format-1-EXIT.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-EXIT.out \
           ./input/format1/test-format-1-EXIT.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-EXIT.out \
                ./output/format1/bin/test-format-1-EXIT.bin
#   NOP (No Operation)
./ns32k-as -aghls=./output/format1/lst/test-format-1-NOP.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-NOP.out \
           ./input/format1/test-format-1-NOP.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-NOP.out \
                ./output/format1/bin/test-format-1-NOP.bin
#   WAIT (Wait)
./ns32k-as -aghls=./output/format1/lst/test-format-1-WAIT.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-WAIT.out \
           ./input/format1/test-format-1-WAIT.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-WAIT.out \
                ./output/format1/bin/test-format-1-WAIT.bin
#   DIA (Diagnose)
./ns32k-as -aghls=./output/format1/lst/test-format-1-DIA.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-DIA.out \
           ./input/format1/test-format-1-DIA.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-DIA.out \
                ./output/format1/bin/test-format-1-DIA.bin
#   FLAG (Trap on Flag)
./ns32k-as -aghls=./output/format1/lst/test-format-1-FLAG.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-FLAG.out \
           ./input/format1/test-format-1-FLAG.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-FLAG.out \
                ./output/format1/bin/test-format-1-FLAG.bin
#   SVC (Supervisor Call)
./ns32k-as -aghls=./output/format1/lst/test-format-1-SVC.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-SVC.out \
           ./input/format1/test-format-1-SVC.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-SVC.out \
                ./output/format1/bin/test-format-1-SVC.bin
#   BPT (Breakpoint Trap)
./ns32k-as -aghls=./output/format1/lst/test-format-1-BPT.lst \
           --listing-lhs-width 7 \
           -o ./output/format1/out/test-format-1-BPT.out \
           ./input/format1/test-format-1-BPT.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format1/out/test-format-1-BPT.out \
                ./output/format1/bin/test-format-1-BPT.bin


# Format 2 test cases:
# ====================
#   ADDQi (Add Quick Integer)
./ns32k-as -aghls=./output/format2/lst/test-format-2-ADDQi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format2/out/test-format-2-ADDQi.out \
           ./input/format2/test-format-2-ADDQi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format2/out/test-format-2-ADDQi.out \
                ./output/format2/bin/test-format-2-ADDQi.bin

#   CMPQi (Compare Quick Integer)
./ns32k-as -aghls=./output/format2/lst/test-format-2-CMPQi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format2/out/test-format-2-CMPQi.out \
           ./input/format2/test-format-2-CMPQi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format2/out/test-format-2-CMPQi.out \
                ./output/format2/bin/test-format-2-CMPQi.bin

#   SPRi (Store Processor Register)
./ns32k-as -aghls=./output/format2/lst/test-format-2-SPRi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format2/out/test-format-2-SPRi.out \
           ./input/format2/test-format-2-SPRi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format2/out/test-format-2-SPRi.out \
                ./output/format2/bin/test-format-2-SPRi.bin

#   Scondi (Save Condition as Boolean)
./ns32k-as -aghls=./output/format2/lst/test-format-2-Scondi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format2/out/test-format-2-Scondi.out \
           ./input/format2/test-format-2-Scondi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format2/out/test-format-2-Scondi.out \
                ./output/format2/bin/test-format-2-Scondi.bin

#   ACBi (Add, Compare and Branch)
./ns32k-as -aghls=./output/format2/lst/test-format-2-ACBi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format2/out/test-format-2-ACBi.out \
           ./input/format2/test-format-2-ACBi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format2/out/test-format-2-ACBi.out \
                ./output/format2/bin/test-format-2-ACBi.bin

#   MOVQi (Move Quick Integer)
./ns32k-as -aghls=./output/format2/lst/test-format-2-MOVQi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format2/out/test-format-2-MOVQi.out \
           ./input/format2/test-format-2-MOVQi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format2/out/test-format-2-MOVQi.out \
                ./output/format2/bin/test-format-2-MOVQi.bin

#   LPRi (Load Processor Register)
./ns32k-as -aghls=./output/format2/lst/test-format-2-LPRi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format2/out/test-format-2-LPRi.out \
           ./input/format2/test-format-2-LPRi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format2/out/test-format-2-LPRi.out \
                ./output/format2/bin/test-format-2-LPRi.bin


# Format 3 test cases:
# ====================
#   CXPD (Call External Procedure with Descriptor)
./ns32k-as -aghls=./output/format3/lst/test-format-3-CXPD.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format3/out/test-format-3-CXPD.out \
           ./input/format3/test-format-3-CXPD.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format3/out/test-format-3-CXPD.out \
                ./output/format3/bin/test-format-3-CXPD.bin

#   BICPSR (Bit Clear in PSR)
./ns32k-as -aghls=./output/format3/lst/test-format-3-BICPSR.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format3/out/test-format-3-BICPSR.out \
           ./input/format3/test-format-3-BICPSR.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format3/out/test-format-3-BICPSR.out \
                ./output/format3/bin/test-format-3-BICPSR.bin

#   JUMP (Jump)
./ns32k-as -aghls=./output/format3/lst/test-format-3-JUMP.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format3/out/test-format-3-JUMP.out \
           ./input/format3/test-format-3-JUMP.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format3/out/test-format-3-JUMP.out \
                ./output/format3/bin/test-format-3-JUMP.bin

#   BISPSR (Bit Set in PSR)
./ns32k-as -aghls=./output/format3/lst/test-format-3-BISPSR.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format3/out/test-format-3-BISPSR.out \
           ./input/format3/test-format-3-BISPSR.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format3/out/test-format-3-BISPSR.out \
                ./output/format3/bin/test-format-3-BISPSR.bin

#   ADJSPi (Adjust Stack Pointer)
./ns32k-as -aghls=./output/format3/lst/test-format-3-ADJSPi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format3/out/test-format-3-ADJSPi.out \
           ./input/format3/test-format-3-ADJSPi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format3/out/test-format-3-ADJSPi.out \
                ./output/format3/bin/test-format-3-ADJSPi.bin

#   JSR (Jump to Subroutine)
./ns32k-as -aghls=./output/format3/lst/test-format-3-JSR.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format3/out/test-format-3-JSR.out \
           ./input/format3/test-format-3-JSR.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format3/out/test-format-3-JSR.out \
                ./output/format3/bin/test-format-3-JSR.bin

#   CASE (Case Branch)
./ns32k-as -aghls=./output/format3/lst/test-format-3-CASEi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format3/out/test-format-3-CASEi.out \
           ./input/format3/test-format-3-CASEi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format3/out/test-format-3-CASEi.out \
                ./output/format3/bin/test-format-3-CASEi.bin


# Format 5 test cases:
# ====================
#   MOVS (Move String)
./ns32k-as -aghls=./output/format5/lst/test-format-5-MOVS.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format5/out/test-format-5-MOVS.out \
           ./input/format5/test-format-5-MOVS.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format5/out/test-format-5-MOVS.out \
                ./output/format5/bin/test-format-5-MOVS.bin

#   CMPS (Compare Strings)
./ns32k-as -aghls=./output/format5/lst/test-format-5-CMPS.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format5/out/test-format-5-CMPS.out \
           ./input/format5/test-format-5-CMPS.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format5/out/test-format-5-CMPS.out \
                ./output/format5/bin/test-format-5-CMPS.bin

#   SETCFG (Compare Strings)
./ns32k-as -aghls=./output/format5/lst/test-format-5-SETCFG.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format5/out/test-format-5-SETCFG.out \
           ./input/format5/test-format-5-SETCFG.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format5/out/test-format-5-SETCFG.out \
                ./output/format5/bin/test-format-5-SETCFG.bin

#   SKPS (Skip String)
./ns32k-as -aghls=./output/format5/lst/test-format-5-SKPS.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format5/out/test-format-5-SKPS.out \
           ./input/format5/test-format-5-SKPS.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format5/out/test-format-5-SKPS.out \
                ./output/format5/bin/test-format-5-SKPS.bin


# Format 6 test cases:
# ====================
#   ROTi (Move String)
./ns32k-as -aghls=./output/format6/lst/test-format-6-ROTi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format6/out/test-format-6-ROTi.out \
           ./input/format6/test-format-6-ROTi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format6/out/test-format-6-ROTi.out \
                ./output/format6/bin/test-format-6-ROTi.bin


# Format 7 test cases:
# ====================
#   MOVMi (Move Multiple)
./ns32k-as -aghls=./output/format7/lst/test-format-7-MOVMi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format7/out/test-format-7-MOVMi.out \
           ./input/format7/test-format-7-MOVMi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format7/out/test-format-7-MOVMi.out \
                ./output/format7/bin/test-format-7-MOVMi.bin


# Format 8 test cases:
# ====================
#   EXTi (Extract Field)
./ns32k-as -aghls=./output/format8/lst/test-format-8-EXTi.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format8/out/test-format-8-EXTi.out \
           ./input/format8/test-format-8-EXTi.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format8/out/test-format-8-EXTi.out \
                ./output/format8/bin/test-format-8-EXTi.bin


# Format 9 test cases:
# ====================
#   MOVif (Move Converting Integer to Floating Point)
./ns32k-as -aghls=./output/format9/lst/test-format-9-MOVif.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format9/out/test-format-9-MOVif.out \
           ./input/format9/test-format-9-MOVif.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format9/out/test-format-9-MOVif.out \
                ./output/format9/bin/test-format-9-MOVif.bin


# No format 10


# Format 11 test cases:
# ====================
#   ADDf (Add Floating)
./ns32k-as -aghls=./output/format11/lst/test-format-11-ADDf.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format11/out/test-format-11-ADDf.out \
           ./input/format11/test-format-11-ADDf.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format11/out/test-format-11-ADDf.out \
                ./output/format11/bin/test-format-11-ADDf.bin


# No format 12


# No format 13


# Format 14 test cases:
# ====================
#   RDVAL (Validate Address for Reading)
./ns32k-as -aghls=./output/format14/lst/test-format-14-RDVAL.lst \
           --listing-lhs-width 7 --listing-cont-lines 3 \
           -o ./output/format14/out/test-format-14-RDVAL.out \
           ./input/format14/test-format-14-RDVAL.asm
./ns32k-objcopy -O binary \
                -j .text \
                ./output/format14/out/test-format-14-RDVAL.out \
                ./output/format14/bin/test-format-14-RDVAL.bin

