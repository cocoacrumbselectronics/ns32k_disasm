.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: RETI
#
# Syntax:   RETI
#
#           !     RETI      !
#           +---------------+
#           !0 1 0 1 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The RETI instruction returns control from an interrupt service procedure to the
# program during which the interrupt was accepted, and informs any interrupt
# control circuitry present in the system that this is being done.
#
    reti
#
end:
