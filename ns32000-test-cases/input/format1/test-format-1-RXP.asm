.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: RXP
#
# Syntax:   RXP     constant
#                   disp
#
#           !      RXP      !
#           +---------------+
#           !0 0 1 1 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The RXP instruction returns control from an externally-called procedure
# and removes any procedure parameters from the stack.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
    rxp     63
    rxp     -64
    rxp     8191
    rxp     -8192
    rxp     536870911
    rxp     -536870912
#
    rxp     5
    rxp     -18
    rxp     4025
    rxp     -125
    rxp     5987621
    rxp     -24869248
#
end:
