.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 3 instructions
#
# Instruction under test: JUMP
#
# Syntax:   JUMP    dest
#                   gen
#                   addr
#
#           !  dest   !        jump          !
#           +---------+---------------------+
#           !   gen   !1 1 0 0 1 1 1 1 1 1 1!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# The jump instruction jumps to the procedure at the address specified by dest after
# saving the return address on the stack. The return address is the address of the
# next sequential instruction.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
# Register
#
    jump   r0
    jump   r1
    jump   r2
    jump   r3
    jump   r4
    jump   r5
    jump   r6
    jump   r7
#
# Register relative
#
    jump   63(r0)
    jump   -64(r0)
    jump   8191(r0)
    jump   -8192(r0)
    jump   536870911(r0)
    jump   -536870912(r0)
    #
    jump   63(r1)
    jump   -64(r1)
    jump   8191(r1)
    jump   -8192(r1)
    jump   536870911(r1)
    jump   -536870912(r1)
    #
    jump   63(r2)
    jump   -64(r2)
    jump   8191(r2)
    jump   -8192(r2)
    jump   536870911(r2)
    jump   -536870912(r2)
    #
    jump   63(r3)
    jump   -64(r3)
    jump   8191(r3)
    jump   -8192(r3)
    jump   536870911(r3)
    jump   -536870912(r3)
    #
    jump   63(r4)
    jump   -64(r4)
    jump   8191(r4)
    jump   -8192(r4)
    jump   536870911(r4)
    jump   -536870912(r4)
    #
    jump   63(r5)
    jump   -64(r5)
    jump   8191(r5)
    jump   -8192(r5)
    jump   536870911(r5)
    jump   -536870912(r5)
    #
    jump   63(r6)
    jump   -64(r6)
    jump   8191(r6)
    jump   -8192(r6)
    jump   536870911(r6)
    jump   -536870912(r6)
    #
    jump   63(r7)
    jump   -64(r7)
    jump   8191(r7)
    jump   -8192(r7)
    jump   536870911(r7)
    jump   -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    jump   -64(63(fp))
    jump   63(-64(fp))
    jump   -8192(63(fp))
    jump   -64(8191(fp))
    jump   63(-8192(fp))
    jump   8191(-64(fp))
    jump   -8192(8191(fp))
    jump   8191(-8192(fp))
    jump   -536870912(8191(fp))
    jump   -8192(536870911(fp))
    jump   8191(-536870912(fp))
    jump   536870911(-8192(fp))
    jump   536870911(-536870912(fp))
    jump   -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    jump   -64(63(sp))
    jump   63(-64(sp))
    jump   -8192(63(sp))
    jump   -64(8191(sp))
    jump   63(-8192(sp))
    jump   8191(-64(sp))
    jump   -8192(8191(sp))
    jump   8191(-8192(sp))
    jump   -536870912(8191(sp))
    jump   -8192(536870911(sp))
    jump   8191(-536870912(sp))
    jump   536870911(-8192(sp))
    jump   536870911(-536870912(sp))
    jump   -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    jump   -64(63(sb))
    jump   63(-64(sb))
    jump   -8192(63(sb))
    jump   -64(8191(sb))
    jump   63(-8192(sb))
    jump   8191(-64(sb))
    jump   -8192(8191(sb))
    jump   8191(-8192(sb))
    jump   -536870912(8191(sb))
    jump   -8192(536870911(sb))
    jump   8191(-536870912(sb))
    jump   536870911(-8192(sb))
    jump   536870911(-536870912(sb))
    jump   -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    jump	@-64
    jump	@63
    jump	@-8192
    jump	@8191
    jump	@-536870912
    jump	@536870911
#
# External
#
    jump	ext(-64) + 63
    jump	ext(63) + -64
    jump	ext(-8192) + 8191
    jump	ext(8191) + -8192
    jump	ext(-536870912) + 8191
    jump	ext(8191) + -536870912
    jump	ext(-8192) + 536870911
    jump	ext(536870911) + -8192
    jump	ext(-536870912) + 536870911
    jump	ext(536870911) + -536870912
#
# Top Of Stack
#
    jump	tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    jump	-64(fp)
    jump	63(fp)
    jump	-8192(fp)
    jump	8191(fp)
    jump	-536870912(fp)
    jump	536870911(fp)
    #
    # Stack Pointer (sp)
    #
    jump	-64(sp)
    jump	63(sp)
    jump	-8192(sp)
    jump	8191(sp)
    jump	-536870912(sp)
    jump	536870911(sp)
    #
    # Static Memory (sb)
    #
    jump	-64(sb)
    jump	63(sb)
    jump	-8192(sb)
    jump	8191(sb)
    jump	-536870912(sb)
    jump	536870911(sb)
    #
    # Program Counter (pc)
    #
    jump	-64(pc)
    jump	63(pc)
    jump	-8192(pc)
    jump	8191(pc)
    jump	-536870912(pc)
    jump	536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    jump   r7[r0:b]
    #
    # Register Relative
    jump   63(r5)[r2:b]
    jump   8191(r3)[r4:b]
    jump   536870911(r1)[r6:b]
    #
    # Memory Relative
    jump   -64(63(fp))[r0:b]
    jump   -8192(8191(fp))[r2:b]
    jump   -536870912(536870911(fp))[r4:b]
    jump   -64(63(sp))[r7:b]
    jump   -8192(8191(sp))[r5:b]
    jump   -536870912(536870911(sp))[r3:b]
    jump   -64(63(sb))[r0:b]
    jump   -8192(8191(sb))[r2:b]
    jump   -536870912(536870911(sb))[r4:b]
    #
    # Absolute
    jump   @63[r0:b]
    jump   @8191[r2:b]
    jump   @536870911[r4:b]
    #
    # External
    jump   ext(-64) + 63[r7:b]
    jump   ext(-8192) + 8191[r5:b]
    jump   ext(-536870912) + 536870911[r3:b]
    #
    # Top Of Stack
    jump   tos[r0:b]
    #
    # Memory Space
    jump   63(fp)[r0:b]
    jump   8191(fp)[r0:b]
    jump   536870911(fp)[r0:b]
    jump   63(sp)[r0:b]
    jump   8191(sp)[r0:b]
    jump   536870911(sp)[r0:b]
    jump   63(sb)[r0:b]
    jump   8191(sb)[r0:b]
    jump   536870911(sb)[r0:b]
    jump   63(pc)[r0:b]
    jump   8191(pc)[r0:b]
    jump   536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    jump   r1[r0:w]
    #
    # Register Relative
    jump   63(r1)[r0:w]
    jump   8191(r1)[r0:w]
    jump   536870911(r1)[r0:w]
    #
    # Memory Relative
    jump   -64(63(fp))[r0:w]
    jump   -8192(8191(fp))[r2:w]
    jump   -536870912(536870911(fp))[r4:w]
    jump   -64(63(sp))[r7:w]
    jump   -8192(8191(sp))[r5:w]
    jump   -536870912(536870911(sp))[r3:w]
    jump   -64(63(sb))[r0:w]
    jump   -8192(8191(sb))[r2:w]
    jump   -536870912(536870911(sb))[r4:w]
    #
    # Absolute
    jump   @63[r0:w]
    jump   @8191[r0:w]
    jump   @536870911[r0:w]
    #
    # External
    jump   ext(-64) + 63[r0:w]
    jump   ext(-8192) + 8191[r0:w]
    jump   ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    jump   tos[r0:w]
    #
    # Memory Space
    jump   63(fp)[r0:w]
    jump   8191(fp)[r0:w]
    jump   536870911(fp)[r0:w]
    jump   63(sp)[r0:w]
    jump   8191(sp)[r0:w]
    jump   536870911(sp)[r0:w]
    jump   63(sb)[r0:w]
    jump   8191(sb)[r0:w]
    jump   536870911(sb)[r0:w]
    jump   63(pc)[r0:w]
    jump   8191(pc)[r0:w]
    jump   536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    jump   r1[r0:d]
    #
    # Register Relative
    jump   63(r1)[r0:d]
    jump   8191(r1)[r0:d]
    jump   536870911(r1)[r0:d]
    #
    # Memory Relative
    jump   -64(63(fp))[r0:d]
    jump   -8192(8191(fp))[r2:d]
    jump   -536870912(536870911(fp))[r4:d]
    jump   -64(63(sp))[r7:d]
    jump   -8192(8191(sp))[r5:d]
    jump   -536870912(536870911(sp))[r3:d]
    jump   -64(63(sb))[r0:d]
    jump   -8192(8191(sb))[r2:d]
    jump   -536870912(536870911(sb))[r4:d]
    #
    # Absolute
    jump   @63[r0:d]
    jump   @8191[r0:d]
    jump   @536870911[r0:d]
    #
    # External
    jump   ext(-64) + 63[r0:d]
    jump   ext(-8192) + 8191[r0:d]
    jump   ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    jump   tos[r0:d]
    #
    # Memory Space
    jump   63(fp)[r0:d]
    jump   8191(fp)[r0:d]
    jump   536870911(fp)[r0:d]
    jump   63(sp)[r0:d]
    jump   8191(sp)[r0:d]
    jump   536870911(sp)[r0:d]
    jump   63(sb)[r0:d]
    jump   8191(sb)[r0:d]
    jump   536870911(sb)[r0:d]
    jump   63(pc)[r0:d]
    jump   8191(pc)[r0:d]
    jump   536870911(pc)[r0:d]
#
end:
