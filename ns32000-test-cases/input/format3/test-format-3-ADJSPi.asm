.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 3 instructions
#
# Instruction under test: ADJSP
#
# Syntax:   ADJSP   src
#                   gen
#                   read.i
#
#           !  desc   !       ADJSPi        !
#           +---------+---------------------+
#           !   gen   !1 0 1 0 1 1 1 1 1! i !
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# The ADJSPi instruction adjusts the value of the current stack pointer by subtrac-
# ting the src operand from it. This has the effect of lengthening the stack by
# the number of bytes given in the src operand if positive, and shortening it if
# src is negative. The S flag in the PSR determines whether the current stack
# pointer register is SP0 or SP1. Regardless of the length of the src operand, the
# entire stack pointer is modified. The src operand is interpreted as a signed
# integer, and is sign-extended to 32 bits before the subtraction is performed.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
# Register
#
    adjspb   r0
    adjspw   r1
    adjspd   r2
    adjspb   r3
    adjspw   r4
    adjspd   r5
    adjspb   r6
    adjspw   r7
#
# Register relative
#
    adjspd   63(r0)
    adjspb   -64(r0)
    adjspw   8191(r0)
    adjspd   -8192(r0)
    adjspb   536870911(r0)
    adjspw   -536870912(r0)
    #
    adjspd   63(r1)
    adjspb   -64(r1)
    adjspw   8191(r1)
    adjspd   -8192(r1)
    adjspb   536870911(r1)
    adjspw   -536870912(r1)
    #
    adjspd   63(r2)
    adjspb   -64(r2)
    adjspw   8191(r2)
    adjspd   -8192(r2)
    adjspb   536870911(r2)
    adjspw   -536870912(r2)
    #
    adjspd   63(r3)
    adjspb   -64(r3)
    adjspw   8191(r3)
    adjspd   -8192(r3)
    adjspb   536870911(r3)
    adjspw   -536870912(r3)
    #
    adjspd   63(r4)
    adjspb   -64(r4)
    adjspw   8191(r4)
    adjspd   -8192(r4)
    adjspb   536870911(r4)
    adjspw   -536870912(r4)
    #
    adjspd   63(r5)
    adjspb   -64(r5)
    adjspw   8191(r5)
    adjspd   -8192(r5)
    adjspb   536870911(r5)
    adjspw   -536870912(r5)
    #
    adjspd   63(r6)
    adjspb   -64(r6)
    adjspw   8191(r6)
    adjspd   -8192(r6)
    adjspb   536870911(r6)
    adjspw   -536870912(r6)
    #
    adjspd   63(r7)
    adjspb   -64(r7)
    adjspw   8191(r7)
    adjspd   -8192(r7)
    adjspb   536870911(r7)
    adjspw   -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    adjspd   -64(63(fp))
    adjspb   63(-64(fp))
    adjspw   -8192(63(fp))
    adjspd   -64(8191(fp))
    adjspb   63(-8192(fp))
    adjspw   8191(-64(fp))
    adjspd   -8192(8191(fp))
    adjspb   8191(-8192(fp))
    adjspw   -536870912(8191(fp))
    adjspd   -8192(536870911(fp))
    adjspb   8191(-536870912(fp))
    adjspw   536870911(-8192(fp))
    adjspd   536870911(-536870912(fp))
    adjspb   -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    adjspw   -64(63(sp))
    adjspd   63(-64(sp))
    adjspb   -8192(63(sp))
    adjspw   -64(8191(sp))
    adjspd   63(-8192(sp))
    adjspb   8191(-64(sp))
    adjspw   -8192(8191(sp))
    adjspd   8191(-8192(sp))
    adjspb   -536870912(8191(sp))
    adjspw   -8192(536870911(sp))
    adjspd   8191(-536870912(sp))
    adjspb   536870911(-8192(sp))
    adjspw   536870911(-536870912(sp))
    adjspd   -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    adjspb   -64(63(sb))
    adjspw   63(-64(sb))
    adjspd   -8192(63(sb))
    adjspb   -64(8191(sb))
    adjspw   63(-8192(sb))
    adjspd   8191(-64(sb))
    adjspb   -8192(8191(sb))
    adjspw   8191(-8192(sb))
    adjspd   -536870912(8191(sb))
    adjspb   -8192(536870911(sb))
    adjspw   8191(-536870912(sb))
    adjspd   536870911(-8192(sb))
    adjspb   536870911(-536870912(sb))
    adjspw   -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    adjspd	@-64
    adjspb	@63
    adjspw	@-8192
    adjspd	@8191
    adjspb	@-536870912
    adjspw	@536870911
#
# External
#
    adjspd	ext(-64) + 63
    adjspb	ext(63) + -64
    adjspw	ext(-8192) + 8191
    adjspd	ext(8191) + -8192
    adjspb	ext(-536870912) + 8191
    adjspw	ext(8191) + -536870912
    adjspd	ext(-8192) + 536870911
    adjspb	ext(536870911) + -8192
    adjspw	ext(-536870912) + 536870911
    adjspd	ext(536870911) + -536870912
#
# Top Of Stack
#
    adjspb	tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    adjspw	-64(fp)
    adjspd	63(fp)
    adjspb	-8192(fp)
    adjspw	8191(fp)
    adjspd	-536870912(fp)
    adjspb	536870911(fp)
    #
    # Stack Pointer (sp)
    #
    adjspw	-64(sp)
    adjspd	63(sp)
    adjspb	-8192(sp)
    adjspw	8191(sp)
    adjspd	-536870912(sp)
    adjspb	536870911(sp)
    #
    # Static Memory (sb)
    #
    adjspw	-64(sb)
    adjspd	63(sb)
    adjspb	-8192(sb)
    adjspw	8191(sb)
    adjspd	-536870912(sb)
    adjspb	536870911(sb)
    #
    # Program Counter (pc)
    #
    adjspw	-64(pc)
    adjspd	63(pc)
    adjspb	-8192(pc)
    adjspw	8191(pc)
    adjspd	-536870912(pc)
    adjspb	536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    adjspw   r7[r0:b]
    #
    # Register Relative
    adjspd   63(r5)[r2:b]
    adjspb   8191(r3)[r4:b]
    adjspw   536870911(r1)[r6:b]
    #
    # Memory Relative
    adjspd   -64(63(fp))[r0:b]
    adjspb   -8192(8191(fp))[r2:b]
    adjspw   -536870912(536870911(fp))[r4:b]
    adjspd   -64(63(sp))[r7:b]
    adjspb   -8192(8191(sp))[r5:b]
    adjspw   -536870912(536870911(sp))[r3:b]
    adjspd   -64(63(sb))[r0:b]
    adjspb   -8192(8191(sb))[r2:b]
    adjspw   -536870912(536870911(sb))[r4:b]
    #
    # Absolute
    adjspd   @63[r0:b]
    adjspb   @8191[r2:b]
    adjspw   @536870911[r4:b]
    #
    # External
    adjspd   ext(-64) + 63[r7:b]
    adjspb   ext(-8192) + 8191[r5:b]
    adjspw   ext(-536870912) + 536870911[r3:b]
    #
    # Top Of Stack
    adjspd   tos[r0:b]
    #
    # Memory Space
    adjspb   63(fp)[r0:b]
    adjspw   8191(fp)[r0:b]
    adjspd   536870911(fp)[r0:b]
    adjspb   63(sp)[r0:b]
    adjspw   8191(sp)[r0:b]
    adjspd   536870911(sp)[r0:b]
    adjspb   63(sb)[r0:b]
    adjspw   8191(sb)[r0:b]
    adjspd   536870911(sb)[r0:b]
    adjspb   63(pc)[r0:b]
    adjspw   8191(pc)[r0:b]
    adjspd   536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    adjspb  r1[r0:w]
    #
    # Register Relative
    adjspw   63(r1)[r0:w]
    adjspd   8191(r1)[r0:w]
    adjspb   536870911(r1)[r0:w]
    #
    # Memory Relative
    adjspw   -64(63(fp))[r0:w]
    adjspd   -8192(8191(fp))[r2:w]
    adjspb   -536870912(536870911(fp))[r4:w]
    adjspw   -64(63(sp))[r7:w]
    adjspd   -8192(8191(sp))[r5:w]
    adjspb   -536870912(536870911(sp))[r3:w]
    adjspw   -64(63(sb))[r0:w]
    adjspd   -8192(8191(sb))[r2:w]
    adjspb   -536870912(536870911(sb))[r4:w]
    #
    # Absolute
    adjspw   @63[r0:w]
    adjspd   @8191[r0:w]
    adjspb   @536870911[r0:w]
    #
    # External
    adjspw   ext(-64) + 63[r0:w]
    adjspd   ext(-8192) + 8191[r0:w]
    adjspb   ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    adjspw   tos[r0:w]
    #
    # Memory Space
    adjspd   63(fp)[r0:w]
    adjspb   8191(fp)[r0:w]
    adjspw   536870911(fp)[r0:w]
    adjspd   63(sp)[r0:w]
    adjspb   8191(sp)[r0:w]
    adjspw   536870911(sp)[r0:w]
    adjspd   63(sb)[r0:w]
    adjspb   8191(sb)[r0:w]
    adjspw   536870911(sb)[r0:w]
    adjspd   63(pc)[r0:w]
    adjspb   8191(pc)[r0:w]
    adjspw   536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    adjspd   r1[r0:d]
    #
    # Register Relative
    adjspb   63(r1)[r0:d]
    adjspw   8191(r1)[r0:d]
    adjspd   536870911(r1)[r0:d]
    #
    # Memory Relative
    adjspb   -64(63(fp))[r0:d]
    adjspw   -8192(8191(fp))[r2:d]
    adjspd   -536870912(536870911(fp))[r4:d]
    adjspb   -64(63(sp))[r7:d]
    adjspw   -8192(8191(sp))[r5:d]
    adjspd   -536870912(536870911(sp))[r3:d]
    adjspb   -64(63(sb))[r0:d]
    adjspw   -8192(8191(sb))[r2:d]
    adjspd   -536870912(536870911(sb))[r4:d]
    #
    # Absolute
    adjspb   @63[r0:d]
    adjspw   @8191[r0:d]
    adjspd   @536870911[r0:d]
    #
    # External
    adjspb   ext(-64) + 63[r0:d]
    adjspw   ext(-8192) + 8191[r0:d]
    adjspd   ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    adjspb   tos[r0:d]
    #
    # Memory Space
    adjspw   63(fp)[r0:d]
    adjspd   8191(fp)[r0:d]
    adjspb   536870911(fp)[r0:d]
    adjspw   63(sp)[r0:d]
    adjspd   8191(sp)[r0:d]
    adjspb   536870911(sp)[r0:d]
    adjspw   63(sb)[r0:d]
    adjspd   8191(sb)[r0:d]
    adjspb   536870911(sb)[r0:d]
    adjspw   63(pc)[r0:d]
    adjspd   8191(pc)[r0:d]
    adjspb   536870911(pc)[r0:d]
#
end:
