.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: ENTER
#
# Syntax:   ENTER   reglist, constant
#                   imm      disp
#
#           !     ENTER     !
#           +---------------+
#           !1 0 0 0 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The ENTER instruction creates a "Frame" on the current stack for use by a proce-
# dure. A Frame is a block of memory on the stack that provides local storage for
# the current procedure. The constant operand specifies the number of bytes to be
# reserved on the stack for local data storage. The Frame Pointer (FP) register is
# saved and then set up as a pointer from which frame information can be located.
#
    enter   [r0], 63
    enter   [r0], -64
    enter   [r0], 8191
    enter   [r0], -8192
    enter   [r0], 536870911
    enter   [r0], -536870912
    enter   [r1], 63
    enter   [r2], -64
    enter   [r3], 8191
    enter   [r4], -8192
    enter   [r5], 536870911
    enter   [r6], -536870912
    enter   [r7], 63
#
    enter   [r0, r1], -64
    enter   [r0, r1, r2], 8191
    enter   [r0, r1, r2, r3], -8192
    enter   [r0, r1, r2, r3, r4], 536870911
    enter   [r0, r1, r2, r3, r4, r5], -536870912
    enter   [r0, r1, r2, r3, r4, r5, r6], 63
    enter   [r0, r1, r2, r3, r4, r5, r6, r7], -64
#
end:
