.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 2 instructions
#
# Instruction under test: Scondi
#
# Syntax:   Scondi  dest
#                   gen
#                   write.i
#
#           !  dest   !  cond !    Scondi   !
#           +---------+-------+---------+---+
#           !   gen   ! short !0 1 1 1 1! i !
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# The Scondi instruction sets the dest operand to the integer value "1" if the
# specified condition is true, and to "0" if false. These are the Boolean values
# "True" and "False", respectively.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
    seqb    63
    seqb    -64
    seqb    8191
    seqb    -8192
    seqb    536870911
    seqb    -536870912
    #
    seqw    63
    seqw    -64
    seqw    8191
    seqw    -8192
    seqw    536870911
    seqw    -536870912
    #
    seqd    63
    seqd    -64
    seqd    8191
    seqd    -8192
    seqd    536870911
    seqd    -536870912
#
#   Not Equal               NE      Z flag clear        0001
#
    sneb    63
    sneb    -64
    sneb    8191
    sneb    -8192
    sneb    536870911
    sneb    -536870912
    #
    snew    63
    snew    -64
    snew    8191
    snew    -8192
    snew    536870911
    snew    -536870912
    #
    sned    63
    sned    -64
    sned    8191
    sned    -8192
    sned    536870911
    sned    -536870912
#
#   Carry Set               CS      C flag set          0010
#
    scsb    63
    scsb    -64
    scsb    8191
    scsb    -8192
    scsb    536870911
    scsb    -536870912
    #
    scsw    63
    scsw    -64
    scsw    8191
    scsw    -8192
    scsw    536870911
    scsw    -536870912
    #
    scsd    63
    scsd    -64
    scsd    8191
    scsd    -8192
    scsd    536870911
    scsd    -536870912
#
#   Carry Clear             CC      C flag clear        0011
#
    sccb    63
    sccb    -64
    sccb    8191
    sccb    -8192
    sccb    536870911
    sccb    -536870912
    #
    sccw    63
    sccw    -64
    sccw    8191
    sccw    -8192
    sccw    536870911
    sccw    -536870912
    #
    sccd    63
    sccd    -64
    sccd    8191
    sccd    -8192
    sccd    536870911
    sccd    -536870912
#
#   Higher                  HI      L flag set          0100
#
    shib    63
    shib    -64
    shib    8191
    shib    -8192
    shib    536870911
    shib    -536870912
    #
    shiw    63
    shiw    -64
    shiw    8191
    shiw    -8192
    shiw    536870911
    shiw    -536870912
    #
    shid    63
    shid    -64
    shid    8191
    shid    -8192
    shid    536870911
    shid    -536870912
#
#   Lower or Same           LS      L flag clear        0101
#
    slsb    63
    slsb    -64
    slsb    8191
    slsb    -8192
    slsb    536870911
    slsb    -536870912
    #
    slsw    63
    slsw    -64
    slsw    8191
    slsw    -8192
    slsw    536870911
    slsw    -536870912
    #
    slsd    63
    slsd    -64
    slsd    8191
    slsd    -8192
    slsd    536870911
    slsd    -536870912
#
#   Greater Than            GT      N flag set          0110
#
    sgtb    63
    sgtb    -64
    sgtb    8191
    sgtb    -8192
    sgtb    536870911
    sgtb    -536870912
    #
    sgtw    63
    sgtw    -64
    sgtw    8191
    sgtw    -8192
    sgtw    536870911
    sgtw    -536870912
    #
    sgtd    63
    sgtd    -64
    sgtd    8191
    sgtd    -8192
    sgtd    536870911
    sgtd    -536870912
#
#   Less Than or Equal      LE      N flag clear        0111
#
    sleb    63
    sleb    -64
    sleb    8191
    sleb    -8192
    sleb    536870911
    sleb    -536870912
    #
    slew    63
    slew    -64
    slew    8191
    slew    -8192
    slew    536870911
    slew    -536870912
    #
    sled    63
    sled    -64
    sled    8191
    sled    -8192
    sled    536870911
    sled    -536870912
#
#   Flag Set                FS      F flag set          1000
#
    sfsb    63
    sfsb    -64
    sfsb    8191
    sfsb    -8192
    sfsb    536870911
    sfsb    -536870912
    #
    sfsw    63
    sfsw    -64
    sfsw    8191
    sfsw    -8192
    sfsw    536870911
    sfsw    -536870912
    #
    sfsd    63
    sfsd    -64
    sfsd    8191
    sfsd    -8192
    sfsd    536870911
    sfsd    -536870912
#
#   Flag Clear              FC      F flag clear        1001
#
    sfcb    63
    sfcb    -64
    sfcb    8191
    sfcb    -8192
    sfcb    536870911
    sfcb    -536870912
    #
    sfcw    63
    sfcw    -64
    sfcw    8191
    sfcw    -8192
    sfcw    536870911
    sfcw    -536870912
    #
    sfcd    63
    sfcd    -64
    sfcd    8191
    sfcd    -8192
    sfcd    536870911
    sfcd    -536870912
#
#   Lower                   LO      Z and L flags clear 1010
#
    slob    63
    slob    -64
    slob    8191
    slob    -8192
    slob    536870911
    slob    -536870912
    #
    slow    63
    slow    -64
    slow    8191
    slow    -8192
    slow    536870911
    slow    -536870912
    #
    slod    63
    slod    -64
    slod    8191
    slod    -8192
    slod    536870911
    slod    -536870912
#
#   Higher or Same          HS      Z or L flags set    1011
#
    shsb    63
    shsb    -64
    shsb    8191
    shsb    -8192
    shsb    536870911
    shsb    -536870912
    shsb    63
    shsb    -64
    shsb    8191
    shsb    -8192
    shsb    536870911
    shsb    -536870912
    shsb    63
    shsb    -64
    shsb    8191
    shsb    -8192
    shsb    536870911
    shsb    -536870912
#
#   Less Than               LT      Z and N flags clear 1100
#
    sltb    63
    sltb    -64
    sltb    8191
    sltb    -8192
    sltb    536870911
    sltb    -536870912
    #
    sltw    63
    sltw    -64
    sltw    8191
    sltw    -8192
    sltw    536870911
    sltw    -536870912
    #
    sltd    63
    sltd    -64
    sltd    8191
    sltd    -8192
    sltd    536870911
    sltd    -536870912
#
#   Greater Than or Equal   GE      Z or N flags set    1101
#
    sgeb    63
    sgeb    -64
    sgeb    8191
    sgeb    -8192
    sgeb    536870911
    sgeb    -536870912
    #
    sgew    63
    sgew    -64
    sgew    8191
    sgew    -8192
    sgew    536870911
    sgew    -536870912
    #
    sged    63
    sged    -64
    sged    8191
    sged    -8192
    sged    536870911
    sged    -536870912
#
end:
