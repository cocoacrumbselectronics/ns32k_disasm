.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: WAIT
#
# Syntax:   WAIT
#
#           !     WAIT      !
#           +---------------+
#           !1 0 1 1 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The WAIT instruction suspends program execution until an interrupt occurs. An
# interrupt restores program execution by passing it to an interrupt service
# procedure. When the WAIT instruction is interrupted, the return address saved is
# the address of the instruction following the WAIT instruction.
#
    wait
#
end:
