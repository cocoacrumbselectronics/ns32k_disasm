use std::fs::metadata;
use std::fs::File;
use std::io::Read;
use std::io::{self, BufRead};
use std::path::Path;

/* ************************************************************************* */

use ns32k_disasm::Ns32kDisassembler;

/* ************************************************************************* */

fn load_binary_file_in_vector<P>(path: P) -> std::result::Result<Vec<u8>, std::io::Error>
where
    P: AsRef<Path>,
{
    let mut f = File::open(&path)?; //.expect("no file found");
    let metadata = metadata(&path)?; //.expect("unable to read metadata");
    let mut buffer = vec![0; metadata.len() as usize];
    f.read(&mut buffer)?; //.expect("buffer overflow");

    Ok(buffer)
} /* end load_binary_file_in_vector */

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
} /* read_lines */

/* ************************************************************************* */

fn remove_whitespace(s: &str) -> String {
    s.split_whitespace().collect()
}

fn valid_lst_line(lst_line: &String) -> bool {
    // println!("valid_lst_line -> {}", lst_line);
    let split: Vec<&str> = lst_line.split_ascii_whitespace().collect();
    if split.len() > 2 {
        if let Ok(_my_int) = split[0].parse::<i32>() {
            if let Ok(_my_int) = i64::from_str_radix(split[1], 16) {
                return true;
            } /* end if */
        } /* end if */
    } /* end if */

    return false;
} /* end valid_lst_line */

fn process_lst_line(lst_line: &String) -> (String, String, String) {
    let split: Vec<&str> = lst_line.split_ascii_whitespace().collect();

    // When we arrive here, we should have a valid line containing a disassembled instruction. E.g.:
    //  130 014c 8C83E000 0000DFFF FFFF     	    addqb   7, 536870911(-536870912(fp))
    let address_string = split[1];
    let hex_bytes_start = lst_line.find(split[2]).unwrap();
    let hex_bytes_stop = lst_line.find("\t").unwrap();
    let hex_string_slice = &lst_line[hex_bytes_start..hex_bytes_stop];
    let hex_string = remove_whitespace(hex_string_slice);

    let mnenomic_slice = &lst_line[hex_bytes_stop + 1..];
    let mnenomic_string = remove_whitespace(mnenomic_slice);

    return (address_string.to_string(), hex_string.to_string(), mnenomic_string.to_string());
} /* end process_lst_line */

/* ************************************************************************* */

fn compare_addresses(lst_address_str: &String, bin_address_str: &String) -> bool {
    let mut lst_address_string = String::from(lst_address_str);

    while lst_address_string.len() < bin_address_str.len() {
        lst_address_string = format!("0{}", lst_address_string);
    } /* end while */

    return (bin_address_str.to_lowercase()).eq(&(lst_address_string.to_lowercase()));
} /* end compare_addresses */

fn compare_hex_bytes(lst_hex_bytes_str: &String, bin_hex_bytes_str: &String) -> bool {
    let mut lst_hex_bytes_string = String::from(lst_hex_bytes_str);
    let mut bin_hex_bytes_string = String::from(bin_hex_bytes_str);

    lst_hex_bytes_string = remove_whitespace(lst_hex_bytes_string.as_str());
    bin_hex_bytes_string = remove_whitespace(bin_hex_bytes_string.as_str());

    return (bin_hex_bytes_string.to_lowercase()).eq(&(lst_hex_bytes_string.to_lowercase()));
} /* end compare_hex_bytes */

fn compare_instructions(lst_instruction_str: &String, bin_instruction_str: &String) -> bool {
    let mut lst_instruction_string = String::from(lst_instruction_str);
    let mut bin_instruction_string = String::from(bin_instruction_str);

    lst_instruction_string = remove_whitespace(lst_instruction_string.as_str());
    bin_instruction_string = remove_whitespace(bin_instruction_string.as_str());

    return (bin_instruction_string.to_lowercase()).eq(&(lst_instruction_string.to_lowercase()));
} /* end compare_hex_bytes */

/* ************************************************************************* */

#[test]
fn test_format_0_bcond() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format0/bin/test-format-0-Bcond.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format0/lst/test-format-0-Bcond.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_0_bcond */

/* ************************************************************************* */

#[test]
fn test_format_1_bsr() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-BSR.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-BSR.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_bsr */

/* *** */

#[test]
fn test_format_1_ret() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-RET.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-RET.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_ret */

/* *** */

#[test]
fn test_format_1_cxp() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-CXP.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-CXP.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_cxp */

/* *** */

#[test]
fn test_format_1_rxp() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-RXP.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-RXP.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_rxp */

/* *** */

#[test]
fn test_format_1_rett() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-RETT.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-RETT.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_rett */

/* *** */

#[test]
fn test_format_1_reti() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-RETI.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-RETI.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_reti */

/* *** */

#[test]
fn test_format_1_save() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-SAVE.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-SAVE.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_save */

/* *** */

#[test]
fn test_format_1_restore() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-RESTORE.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-RESTORE.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_restore */

/* *** */

#[test]
fn test_format_1_enter() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-ENTER.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-ENTER.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_enter */

/* *** */

#[test]
fn test_format_1_exit() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-EXIT.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-EXIT.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_exit */

/* *** */

#[test]
fn test_format_1_nop() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-NOP.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-NOP.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_nop */

/* *** */

#[test]
fn test_format_1_wait() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-WAIT.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-WAIT.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_wait */

/* *** */

#[test]
fn test_format_1_dia() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-DIA.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-DIA.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_dia */

/* *** */

#[test]
fn test_format_1_flag() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-FLAG.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-FLAG.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_flag */

/* *** */

#[test]
fn test_format_1_svc() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-SVC.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-SVC.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_svc */

/* *** */

#[test]
fn test_format_1_bpt() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format1/bin/test-format-1-BPT.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format1/lst/test-format-1-BPT.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_1_bpt */

/* ************************************************************************* */

#[test]
fn test_format_2_addqi() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format2/bin/test-format-2-ADDQi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format2/lst/test-format-2-ADDQi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_2_addqi */

/* *** */

#[test]
fn test_format_2_cmpqi() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format2/bin/test-format-2-CMPQi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format2/lst/test-format-2-CMPQi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_2_cmpqi */

/* *** */

#[test]
fn test_format_2_spri() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format2/bin/test-format-2-SPRi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format2/lst/test-format-2-SPRi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_2_spri */

/* *** */

#[test]
fn test_format_2_scondi() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format2/bin/test-format-2-Scondi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format2/lst/test-format-2-Scondi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_2_scondi */

/* *** */

#[test]
fn test_format_2_acbi() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format2/bin/test-format-2-ACBi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format2/lst/test-format-2-ACBi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_2_acbi */

/* *** */

#[test]
fn test_format_2_movqi() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format2/bin/test-format-2-MOVQi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format2/lst/test-format-2-MOVQi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_2_movqi */

/* *** */

#[test]
fn test_format_2_lpri() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format2/bin/test-format-2-LPRi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format2/lst/test-format-2-LPRi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_2_lpri */

/* ************************************************************************* */

#[test]
fn test_format_3_cxpd() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format3/bin/test-format-3-CXPD.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format3/lst/test-format-3-CXPD.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_3_cxpd */

/* *** */

#[test]
fn test_format_3_bicpsr() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format3/bin/test-format-3-BICPSR.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format3/lst/test-format-3-BICPSR.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_3_bicpsr */

/* *** */

#[test]
fn test_format_3_jump() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format3/bin/test-format-3-JUMP.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format3/lst/test-format-3-JUMP.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_3_jump */

/* *** */

#[test]
fn test_format_3_bispsr() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format3/bin/test-format-3-BISPSR.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format3/lst/test-format-3-BISPSR.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_3_bispsr */

/* *** */

#[test]
fn test_format_3_adjsp() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format3/bin/test-format-3-ADJSPi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format3/lst/test-format-3-ADJSPi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_3_adjsp */

/* *** */

#[test]
fn test_format_3_jsr() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format3/bin/test-format-3-JSR.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format3/lst/test-format-3-JSR.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_3_jsr */

/* *** */

#[test]
fn test_format_3_case() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format3/bin/test-format-3-CASEi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format3/lst/test-format-3-CASEi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_3_case */

/* ************************************************************************* */

#[test]
fn test_format_5_movs() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format5/bin/test-format-5-MOVS.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format5/lst/test-format-5-MOVS.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_5_movs */

/* *** */

#[test]
fn test_format_5_cmps() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format5/bin/test-format-5-CMPS.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format5/lst/test-format-5-CMPS.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_5_cmps */

/* *** */

#[test]
fn test_format_5_setcfg() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format5/bin/test-format-5-SETCFG.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format5/lst/test-format-5-SETCFG.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_5_setcfg */

/* *** */

#[test]
fn test_format_5_skps() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format5/bin/test-format-5-SKPS.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format5/lst/test-format-5-SKPS.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_5_skps */

/* ************************************************************************* */

#[test]
fn test_format_6_roti() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format6/bin/test-format-6-ROTi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format6/lst/test-format-6-ROTi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_6_roti */

/* ************************************************************************* */

#[test]
fn test_format_7_movmi() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format7/bin/test-format-7-MOVMi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format7/lst/test-format-7-MOVMi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_7_movmi */

/* ************************************************************************* */

#[test]
fn test_format_8_exti() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format8/bin/test-format-8-EXTi.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format8/lst/test-format-8-EXTi.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_8_exti */

/* ************************************************************************* */

#[test]
fn test_format_9_movif() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format9/bin/test-format-9-MOVif.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format9/lst/test-format-9-MOVif.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_9_movif */

/* ************************************************************************* */

#[test]
fn test_format_11_addf() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format11/bin/test-format-11-ADDf.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format11/lst/test-format-11-ADDf.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_11_addf */

/* ************************************************************************* */

#[test]
fn test_format_15_rdval() -> Result<(), std::io::Error> {
    let bin_filename = String::from("./ns32000-test-cases/output/format14/bin/test-format-14-RDVAL.bin");
    let lst_filename = String::from("./ns32000-test-cases/output/format14/lst/test-format-14-RDVAL.lst");

    let bytes = load_binary_file_in_vector(&bin_filename)?;
    let mut my_disassembler = Ns32kDisassembler::new(&bytes, 0);

    let lines = read_lines(&lst_filename)?;
    for line in lines {
        if let Ok(lst_line) = line {
            if valid_lst_line(&lst_line) == false {
                continue;
            }
            let (lst_address_str, lst_hex_str, lst_instruction_str) = process_lst_line(&lst_line);
            let (bin_address_str, bin_hex_str, bin_instruction_str) = my_disassembler.disassemble_next_instruction()?;
            assert!(
                compare_addresses(&lst_address_str, &bin_address_str),
                "{}: {} != {}",
                lst_address_str,
                lst_address_str,
                bin_address_str
            );
            assert!(
                compare_hex_bytes(&lst_hex_str, &bin_hex_str),
                "{}: {} != {}",
                lst_address_str,
                lst_hex_str,
                bin_hex_str
            );
            assert!(
                compare_instructions(&lst_instruction_str, &bin_instruction_str),
                "{}: {} != {}",
                lst_address_str,
                lst_instruction_str,
                bin_instruction_str
            );
        } /* end if */
    } /* end for */

    Ok(())
} /* end test_format_15_rdval */
