.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 7 instructions
#
# Instruction under test: MOVM
#
# Syntax.   MOVMi   block1, block2, length
# `                 gen     gen     disp
#                   addr    addr
#
#           !  block1 !  block2 !           MOVMi           !
#           +---------+---------+-------+---+---------------+
#           !   gen   !  gen    !0 0 0 0! i !1 1 0 0 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
#
# The MOVMi instruction copies the contents of block1 to block2. The instruction
# copies consecutive integers from block1 to consecutive integer locations in
# block2.
#
# Regression test cases:
#
    movmb   -8192(8191(fp))[r3:b], -8192(8191(fp))[r2:b], 256
    movmb   -8192(8191(fp))[r2:b], -8192(8191(fp))[r3:b], 256
    movmb   -536870912(536870911(fp))[r3:b], -536870912(536870911(fp))[r2:b], 256
    movmb   -536870912(536870911(fp))[r2:b], -536870912(536870911(fp))[r3:b], 256
    movmb   -8192(r0), -8192(8191(fp))[r3:b], 256
    movmb   -8192(8191(fp))[r3:b], -8192(r0), 256
    movmb   -536870912(r0), -536870912(536870911(fp))[r3:b], 256
    movmb   -536870912(536870911(fp))[r3:b], -536870912(r0), 256
#
# Register
#
    movmb r7, r0, 2
    movmd r6, r1, 2
    movmw r5, r2, 2
    movmb r4, r3, 2
    movmd r3, r4, 2
    movmw r2, r5, 2
    movmb r1, r6, 2
    movmd r0, r7, 2
#
# Register relative
#
    movmb -64(r7), r0, 2
    movmd 63(r6), r1, 2
    movmw 8191(r5), r2, 2
    movmb -8192(r4), r3, 2
    movmd -536870912(r3), r4, 2
    movmw 536870911(r2), r5, 2
    movmb -64(r1), r6, 2
    movmd 63(r0), r7, 2
#
    movmb r7, -64(r7), 2
    movmd r6, 63(r6), 2
    movmw r5, 8191(r5), 2
    movmb r4, -8192(r4), 2
    movmd r3, -536870912(r3), 2
    movmw r2, 536870911(r2), 2
    movmb r1, -64(r1), 2
    movmd r0, 63(r0), 2
#
    movmb 63(r0), -64(r7), 2
    movmd -64(r1), 63(r6), 2
    movmw 536870911(r2), 8191(r5), 2
    movmb -536870912(r3), -8192(r4), 2
    movmd -8192(r4), -536870912(r3), 2
    movmw 8191(r5), 536870911(r2), 2
    movmb 63(r6), -64(r1), 2
    movmd -64(r7), 63(r0), 2
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    movmb   r0, -64(63(fp)), 2
    movmd   r1, -64(63(fp)), 2
    movmw   r2, 63(-64(fp)), 2
    movmb   r3, 63(-64(fp)), 2
    #
    movmd   r0, -8192(63(fp)), 4
    movmw   r1, -64(8191(fp)), 4
    movmb   r2, 63(-8192(fp)), 4
    movmd   r3, 8191(-64(fp)), 4
    #
    movmw   r4, -8192(8191(fp)), 127
    movmb   r5, -8192(8191(fp)), 127
    movmd   r6, 8191(-8192(fp)), 127
    movmw   r7, 8191(-8192(fp)), 127
    #
    movmb   63(r0), -536870912(8191(fp)), 8192
    movmd   -64(r1), -8192(536870911(fp)), 8192
    movmw   536870911(r2), 8191(-536870912(fp)), 8192
    movmb   -536870912(r3), 536870911(-8192(fp)), 8192
    #
    movmd   -8192(r4), 536870911(-536870912(fp)), 100000
    movmw   8191(r5), 536870911(-536870912(fp)), 100000
    movmb   63(r6), -536870912(536870911(fp)), 100000
    movmd   -64(r7), -536870912(536870911(fp)), 100000
    #
    # Stack Pointer (sp)
    #
    movmb   r0, -64(63(sp)), 0
    movmw   r1, -64(63(sp)), 0
    movmd   r2, 63(-64(sp)), 0
    movmb   r3, 63(-64(sp)), 0
    #
    movmw   -8192(8191(fp)), -8192(63(sp)), 2
    movmd   -8192(8191(fp)), -64(8191(sp)), 2
    movmb   8191(-8192(fp)), 63(-8192(sp)), 2
    movmw   8191(-8192(fp)), 8191(-64(sp)), 2
    #
    movmd   -8192(8191(sp)), -8192(8191(fp)), 4
    movmb   -8192(8191(sp)), -8192(8191(fp)), 4
    movmw   8191(-8192(sp)), 8191(-8192(fp)), 4
    movmd   8191(-8192(sp)), 8191(-8192(fp)), 4
    #
    movmb   r4, -536870912(8191(sp)), 8192
    movmw   r5, -8192(536870911(sp)), 8192
    movmd   r6, 8191(-536870912(sp)), 8192
    movmb   r7, 536870911(-8192(sp)), 8192
    #
    movmw   63(r0), 536870911(-536870912(sp)), 100000
    movmd   -64(r1), 536870911(-536870912(sp)), 100000
    movmb   536870911(r2), -536870912(536870911(sp)), 100000
    movmw   -536870912(r3), -536870912(536870911(sp)), 100000
    #
    # Static Memory (sb)
    #
    movmb   r0, -64(63(sb)), 0
    movmw   r1, -64(63(sb)), 0
    movmd   r2, 63(-64(sb)), 0
    movmb   r3, 63(-64(sb)), 0
    #
    movmw   -8192(8191(fp)), -8192(63(sb)), 2
    movmd   -8192(8191(fp)), -64(8191(sb)), 2
    movmb   8191(-8192(fp)), 63(-8192(sb)), 2
    movmw   8191(-8192(fp)), 8191(-64(sb)), 2
    #
    movmd   -8192(8191(sp)), -8192(8191(sb)), 4
    movmb   -8192(8191(sp)), -8192(8191(sb)), 4
    movmw   8191(-8192(sp)), 8191(-8192(sb)), 4
    movmd   8191(-8192(sp)), 8191(-8192(sb)), 4
    #
    movmb   r4, -536870912(8191(sb)), 8192
    movmw   r5, -8192(536870911(sb)), 8192
    movmd   r6, 8191(-536870912(sb)), 8192
    movmb   r7, 536870911(-8192(sb)), 8192
    #
    movmw   63(r0), 536870911(-536870912(sb)), 100000
    movmd   -64(r1), 536870911(-536870912(sb)), 100000
    movmb   536870911(r2), -536870912(536870911(sb)), 100000
    movmw   -536870912(r3), -536870912(536870911(sb)), 100000
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    movmb	r0, @-64, 0
    movmw	r1, @-64, 1
    movmd	r2, @63, 2
    movmb	r3, @63, 3
    movmw	7(r4), @-65, 4
    movmd	-8(r5), @-65, 4
    movmb	7(r6), @64, 4
    movmw	-8(r7), @64, 4
    #
    movmd	-8192(63(fp)), @-8192, 8
    movmb	@-8192, -8192(536870911(sp)), 16
    movmw	@8191, r0, 32
    movmd	@8191, -64(r1), 64
    movmb	@-8193, -8192(63(sb)), 128
    movmw	@-8193, -536870912(-8192(fp)), 256
    movmd	@8192, r2, 512
    movmb	@8192, -1(r4), 1024
    #
    movmw	r5, @-536870912, 2048
    movmd	r6, @536870911, 100000
#
#
# External
#
    movmb	ext(-64) + 63, r0, 0
    movmw	ext(-64) + 63, r1, 1
    movmd	ext(63) + -64, r2, 2
    movmb	ext(63) + -64, r3, 3
    #
    movmw	ext(-65) + 63, 7(r4), 4
    movmd	ext(-65) + 63, -8(r5), 8
    movmb	ext(63) + -65, 7(r6), 16
    movmw	ext(63) + -65, -8(r7), 32
    movmd	7(r4), ext(-64) + 64, 64
    movmb	-8(r5), ext(-64) + 64, 128
    movmw	7(r6), ext(64) + -64, 512
    movmd	-8(r7), ext(64) + -64, 1024
    #
    movmb	@8191, ext(-8192) + 8191, 2048
    movmw	@-8192, ext(-8192) + 8191, 4096
    movmd	@536870911, ext(8191) + -8192, 8192
    movmb	@-536870912, ext(8191) + -8192, 16384
    #
    movmw	ext(-536870912) + 8191, -8192(8191(sp)), 32768
    movmd	ext(-536870912) + 8191, -8192(8191(sp)), 65536
    movmb	ext(8191) + -536870912, 8191(-8192(sp)), 131072
    movmw	ext(8191) + -536870912, 8191(-8192(sp)), 256144
    movmd	ext(-8192) + 536870911, @8191, 0
    movmb	ext(-8192) + 536870911, @-8192, 1
    movmw	ext(536870911) + -8192, ext(64) + -64, 2
    movmd	ext(536870911) + -8192, ext(8191) + -8192, 4
    #
    movmb	-64(63(fp)), ext(-536870912) + 536870911, 8
    movmw	-64(63(fp)), ext(-536870912) + 536870911, 16 
    movmd	ext(536870911) + -536870912, 63(-64(fp)), 32
    movmb	ext(536870911) + -536870912, 63(-64(fp)), 64
#
# Top Of Stack
#
    movmb	r0, tos, 128
    movmw	tos, 7(r1), 256
    movmd	@-8, tos, 512
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    movmb	r0, -64(fp), 0
    movmw	-64(fp), r1, 1
    movmd	7(r2), 63(fp), 2 
    movmb	63(fp), -8(r3), 3
    #
    movmw	63(fp), -8192(fp), 4
    movmd	-8192(fp), 63(fp), 8
    movmb	-64(fp), 8191(fp), 16
    movmw	8191(fp), -64(fp), 32
    #
    movmd	-64(63(fp)), -536870912(fp), 64
    movmb	ext(-536870912) + 536870911, -536870912(fp), 128
    movmw	536870911(fp), @-8192, 256
    movmd	536870911(fp), -8192(8191(sp)), 512
    #
    # Stack Pointer (sp)
    #
    movmb	r0, -64(sp), 0
    movmw	-64(sp), r1, 1
    movmd	7(r2), 63(sp), 2 
    movmb	63(sp), -8(r3), 3
    #
    movmw	63(sp), -8192(sp), 4
    movmd	-8192(sp), 63(fp), 8
    movmb	-64(sp), 8191(sp), 16
    movmw	8191(fp), -64(sp), 32
    #
    movmd	-64(63(sp)), -536870912(sp), 64
    movmb	ext(-536870912) + 536870911, -536870912(sp), 128
    movmw	536870911(sp), @-8192, 256
    movmd	536870911(fp), -8192(8191(sp)), 512
    #
    # Static Memory (sb)
    #
    movmb	r0, -64(sb), 0
    movmw	-64(sb), r1, 1
    movmd	7(r2), 63(sb), 2 
    movmb	63(sb), -8(r3), 3
    #
    movmw	63(fp), -8192(sb), 4
    movmd	-8192(sb), 63(sb), 8
    movmb	-64(sb), 8191(sp), 16
    movmw	8191(sb), -64(sp), 32
    #
    movmd	-64(63(fp)), -536870912(fp), 64
    movmb	ext(-536870912) + 536870911, -536870912(fp), 128
    movmw	536870911(fp), @-8192, 256
    movmd	536870911(fp), -8192(8191(sp)), 512
    #
    # Program Counter (pc)
    #
    movmb	r0, -64(pc), 0
    movmw	-64(pc), r1, 1
    movmd	7(r2), 63(pc), 2 
    movmb	63(pc), -8(r3), 3
    #
    movmw	63(pc), -8192(fp), 4
    movmd	-8192(fp), 63(pc), 8
    movmb	-64(sp), 8191(pc), 16
    movmw	8191(pc), -64(pc), 32
    #
    movmd	-64(63(fp)), -536870912(pc), 64
    movmb	ext(-536870912) + 536870911, -536870912(pc), 128
    movmw	536870911(pc), @-8192, 256
    movmd	536870911(pc), -8192(8191(sp)), 512
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    movmb   r0, r7[r0:b], 0
    movmb   r1, r6[r1:b], 0
    #
    # Register Relative
    movmb   7(r2), 63(r5)[r2:b], 1
    movmb   -8(r3), 63(r4)[r3:b], 2
    movmb   8191(r3)[r4:b], -8192(fp), 4
    movmb   8191(r2)[r5:b], @-8192, 8
    movmb   ext(-536870912) + 536870911, 536870911(r1)[r6:b], 16
    movmb   -64(pc), 536870911(r0)[r7:b], 32
    #
    # Memory Relative
    movmb   r7, -64(63(fp))[r0:b], 64
    movmb   -64(63(fp))[r1:b], -8(r6), 128 
    movmb   -8192(8191(fp))[r3:b], -8192(8191(fp))[r2:b], 256
    movmb   @-8, -8192(8191(fp))[r3:b], 512
    movmb   -536870912(536870911(fp))[r4:b], ext(8191) + -536870912, 1024
    movmb   -536870912(536870911(fp))[r4:b], -536870912(536870911(fp))[r5:b], 2048
    movmb   ext(8191) + -536870912, -64(63(sp))[r7:b], 4096
    movmb   -64(63(sp))[r6:b], -64(r5), 8192
    movmb   -64(63(sp))[r6:b], -8192(8191(sp))[r5:b], 16384
    movmb   -8192(8191(sp))[r4:b], -64(pc), 32768
    movmb   -536870912(536870911(sp))[r3:b], 63(r5)[r2:b], 65536 
    movmb   -536870912(536870911(sp))[r3:b], -536870912(536870911(sp))[r2:b], 131072
    movmb   7(r4), -64(63(sb))[r0:b], 262144
    movmb   -64(63(sb))[r1:b], 8192(r5)[r2:b], 0
    movmb   -64(63(sb))[r1:b], -8192(8191(sb))[r2:b], 1
    movmb   -8(r1), -8192(8191(sb))[r3:b], 3
    movmb   -536870912(536870911(sb))[r4:b], -8192(sp), 9
    movmb   -536870912(536870911(sb))[r4:b], -536870912(536870911(sb))[r5:b], 27
    #
    # Absolute
    movmb   7(r2), @63[r0:b], 63
    movmb   @63[r1:b], -8(r1), 1819
    movmb   @63[r1:b], @8191[r2:b], 8191
    movmb   -8192(sp), @8191[r3:b], 16384
    movmb   @536870911[r4:b], -8192(pc), 2
    movmb   @536870911[r4:b], @536870911[r5:b], 4
    #
    # External
    movmb   7(r2), ext(-64) + 63[r7:b], 6
    movmb   ext(-64) + 63[r6:b], -8(r1), 7
    movmb   ext(-64) + 63[r6:b], ext(-8192) + 8191[r5:b], 8
    movmb   -8192(sp), ext(-8192) + 8191[r4:b], 9
    movmb   ext(-536870912) + 536870911[r3:b], -8192(fp), 10
    movmb   ext(-536870912) + 536870911[r3:b], ext(-536870912) + 536870911[r2:b], 11
    #
    # Top Of Stack
    movmb   ext(-536870912) + 536870911[r2:b], tos[r0:b], 13
    movmb   7(r1), tos[r0:b], 13
    movmb   tos[r7:b], @63[r0:b], 14
    #
    # Memory Space
    movmb   7(r7), 63(fp)[r0:b], 15
    movmb   63(fp)[r0:b], @63[r0:b], 16
    movmb   63(fp)[r0:b], 8191(fp)[r0:b], 17
    movmb   -8(r1), 8191(fp)[r0:b], 18
    movmb   8191(fp)[r0:b], 536870911(fp)[r0:b], 19
    movmb   @63[r0:b], 536870911(fp)[r0:b], 20
    movmb   @63[r0:b], 63(sp)[r0:b], 21
    movmb   63(sp)[r0:b], @63[r0:b], 22
    movmb   63(sp)[r0:b], 8191(sp)[r0:b], 23
    movmb   -8(r6), 8191(sp)[r0:b], 24
    movmb   536870911(sp)[r0:b], ext(-536870912) + 536870911[r3:b], 25
    movmb   536870911(sp)[r0:b], 536870911(sp)[r0:b], 26
    movmb   7(r1), 63(sb)[r0:b], 27
    movmb   63(sb)[r0:b], r3, 28
    movmb   63(sb)[r0:b], 8191(sb)[r0:b], 29
    movmb   8191(sb)[r0:b], @63[r0:b], 30
    movmb   536870911(sb)[r0:b], ext(-536870912) + 536870911[r3:b], 31
    movmb   536870911(sb)[r0:b], 536870911(sb)[r0:b], 32
    movmb   7(r6), 63(pc)[r0:b], 32
    movmb   63(pc)[r0:b], @63[r0:b], 33
    movmb   63(pc)[r0:b], 8191(pc)[r0:b], 34
    movmb   -8(r1), 8191(pc)[r0:b], 35
    movmb   536870911(pc)[r0:b], ext(-536870912) + 536870911[r3:b], 36
    movmb   536870911(pc)[r0:b], 536870911(pc)[r0:b], 37
#
end:
