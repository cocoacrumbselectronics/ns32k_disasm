.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 11 instructions
#
# Instruction under test: ADDf
#
# Syntax.   ADDf    src,    dest
# `                 gen     gen
#                   read.f  rmw.f
#
#           !   src   !   dest  !           ADDf            !
#           +---------+---------+---------+-+---------------+
#           !   gen   !   gen   !0 0 0 0 0!f!1 0 1 1 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
#
# The ADDf instruction adds the src and dest operands and places the result in
# the dest operand location. Results for normalized and zero operands are given
# in the table below. The symbols "m" and "n" represent any non-zero normalized
# numbers. The symbols "+z" and "-z" represent positive zero and negative zero,
# respectively.
#
# Register
#
    addf   r7, r0
    addl   r6, r1
    addf   r5, r2
    addl   r4, r3
    addf   r3, r4
    addl   r2, r5
    addf   r1, r6
    addl   r0, r7
#
# Register relative
#
    addf   -64(r7), r0
    addl   63(r6), r1
    addf   8191(r5), r2
    addl   -8192(r4), r3
    addf   -536870912(r3), r4
    addl   536870911(r2), r5
    addf   -64(r1), r6
    addl   63(r0), r7
#
    addf   r7, -64(r7)
    addl   r6, 63(r6)
    addf   r5, 8191(r5)
    addl   r4, -8192(r4)
    addf   r3, -536870912(r3)
    addl   r2, 536870911(r2)
    addf   r1, -64(r1)
    addl   r0, 63(r0)
#
    addf   63(r0), -64(r7)
    addl   -64(r1), 63(r6)
    addf   536870911(r2), 8191(r5)
    addl   -536870912(r3), -8192(r4)
    addf   -8192(r4), -536870912(r3)
    addl    8191(r5), 536870911(r2)
    addf   63(r6), -64(r1)
    addl   -64(r7), 63(r0)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    addf   r0, -64(63(fp))
    addl   r1, -64(63(fp))
    addf   r2, 63(-64(fp))
    addl   r3, 63(-64(fp))
    #
    addf   r0, -8192(63(fp))
    addl   r1, -64(8191(fp))
    addf   r2, 63(-8192(fp))
    addl   r3, 8191(-64(fp))
    #
    addf   r4, -8192(8191(fp))
    addl   r5, -8192(8191(fp))
    addf   r6, 8191(-8192(fp))
    addl   r7, 8191(-8192(fp))
    #
    addf   63(r0), -536870912(8191(fp))
    addl   -64(r1), -8192(536870911(fp))
    addf   536870911(r2), 8191(-536870912(fp))
    addl   -536870912(r3), 536870911(-8192(fp))
    #
    addf   -8192(r4), 536870911(-536870912(fp))
    addl   8191(r5), 536870911(-536870912(fp))
    addf   63(r6), -536870912(536870911(fp))
    addl   -64(r7), -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    addf   r0, -64(63(sp))
    addl   r1, -64(63(sp))
    addf   r2, 63(-64(sp))
    addl   r3, 63(-64(sp))
    #
    addf   -8192(8191(fp)), -8192(63(sp))
    addl   -8192(8191(fp)), -64(8191(sp))
    addf   8191(-8192(fp)), 63(-8192(sp))
    addl   8191(-8192(fp)), 8191(-64(sp))
    #
    addf   -8192(8191(sp)), -8192(8191(fp))
    addl   -8192(8191(sp)), -8192(8191(fp))
    addf   8191(-8192(sp)), 8191(-8192(fp))
    addl   8191(-8192(sp)), 8191(-8192(fp))
    #
    addf   r4, -536870912(8191(sp))
    addl   r5, -8192(536870911(sp))
    addf   r6, 8191(-536870912(sp))
    addl   r7, 536870911(-8192(sp))
    #
    addf   63(r0), 536870911(-536870912(sp))
    addl   -64(r1), 536870911(-536870912(sp))
    addf   536870911(r2), -536870912(536870911(sp))
    addl   -536870912(r3), -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    addf   r0, -64(63(sb))
    addl   r1, -64(63(sb))
    addf   r2, 63(-64(sb))
    addl   r3, 63(-64(sb))
    #
    addf   -8192(8191(fp)), -8192(63(sb))
    addl   -8192(8191(fp)), -64(8191(sb))
    addf   8191(-8192(fp)), 63(-8192(sb))
    addl   8191(-8192(fp)), 8191(-64(sb))
    #
    addf   -8192(8191(sp)), -8192(8191(sb))
    addl   -8192(8191(sp)), -8192(8191(sb))
    addf   8191(-8192(sp)), 8191(-8192(sb))
    addl   8191(-8192(sp)), 8191(-8192(sb))
    #
    addf   r4, -536870912(8191(sb))
    addl   r5, -8192(536870911(sb))
    addf   r6, 8191(-536870912(sb))
    addl   r7, 536870911(-8192(sb))
    #
    addf   63(r0), 536870911(-536870912(sb))
    addl   -64(r1), 536870911(-536870912(sb))
    addf   536870911(r2), -536870912(536870911(sb))
    addl   -536870912(r3), -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    addf	r0, @-64
    addl	r1, @-64
    addf	r2, @63
    addl	r3, @63
    addf	7(r4), @-65
    addl	-8(r5), @-65
    addf	7(r6), @64
    addl	-8(r7), @64
    #
    addf	-8192(63(fp)), @-8192
    addl	@-8192, -8192(536870911(sp))
    addf	@8191, r0
    addl	@8191, -64(r1)
    addf	@-8193, -8192(63(sb))
    addl	@-8193, -536870912(-8192(fp))
    addf	@8192, r2
    addl	@8192, -1(r4)
    #
    addf	r5, @-536870912
    addl	r6, @536870911
#
#
# External
#
    addf	ext(-64) + 63, r0
    addl	ext(-64) + 63, r1
    addf	ext(63) + -64, r2
    addl	ext(63) + -64, r3
    #
    addf	ext(-65) + 63, 7(r4)
    addl	ext(-65) + 63, -8(r5)
    addf	ext(63) + -65, 7(r6)
    addl	ext(63) + -65, -8(r7)
    addf	7(r4), ext(-64) + 64
    addl	-8(r5), ext(-64) + 64
    addf	7(r6), ext(64) + -64
    addl	-8(r7), ext(64) + -64
    #
    addf	@8191, ext(-8192) + 8191
    addl	@-8192, ext(-8192) + 81916
    addf	@536870911, ext(8191) + -8192
    addl	@-536870912, ext(8191) + -8192
    #
    addf	ext(-536870912) + 8191, -8192(8191(sp))
    addl	ext(-536870912) + 8191, -8192(8191(sp))
    addf	ext(8191) + -536870912, 8191(-8192(sp))
    addl	ext(8191) + -536870912, 8191(-8192(sp))
    addf	ext(-8192) + 536870911, @8191
    addl	ext(-8192) + 536870911, @-8192
    addf	ext(536870911) + -8192, ext(64) + -64
    addl	ext(536870911) + -8192, ext(8191) + -8192
    #
    addf	-64(63(fp)), ext(-536870912) + 536870911
    addl	-64(63(fp)), ext(-536870912) + 536870911
    addf	ext(536870911) + -536870912, 63(-64(fp))
    addl	ext(536870911) + -536870912, 63(-64(fp))
#
# Top Of Stack
#
    addf	r0, tos
    addl	tos, 7(r1)
    addf	@-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    addl	r0, -64(fp)
    addf	-64(fp), r1
    addl	7(r2), 63(fp) 
    addf	63(fp), -8(r3)
    #
    addl	63(fp), -8192(fp)
    addf	-8192(fp), 63(fp)
    addl	-64(fp), 8191(fp)
    addf	8191(fp), -64(fp)
    #
    addl	-64(63(fp)), -536870912(fp)
    addf	ext(-536870912) + 536870911, -536870912(fp)
    addl	536870911(fp), @-8192
    addf	536870911(fp), -8192(8191(sp))
    #
    # Stack Pointer (sp)
    #
    addl	r0, -64(sp)
    addf	-64(sp), r1
    addl	7(r2), 63(sp) 
    addf	63(sp), -8(r3)
    #
    addl	63(sp), -8192(sp)
    addf	-8192(sp), 63(fp)
    addl	-64(sp), 8191(sp)
    addf	8191(fp), -64(sp)
    #
    addl	-64(63(sp)), -536870912(sp)
    addf	ext(-536870912) + 536870911, -536870912(sp)
    addl	536870911(sp), @-8192
    addf	536870911(fp), -8192(8191(sp))
    #
    # Static Memory (sb)
    #
    addl	r0, -64(sb)
    addf	-64(sb), r1
    addl	7(r2), 63(sb)
    addf	63(sb), -8(r3)
    #
    addl	63(fp), -8192(sb)
    addf	-8192(sb), 63(sb)
    addl	-64(sb), 8191(sp)
    addf	8191(sb), -64(sp)
    #
    addl	-64(63(fp)), -536870912(fp)
    addf	ext(-536870912) + 536870911, -536870912(fp)
    addl	536870911(fp), @-8192
    addf	536870911(fp), -8192(8191(sp))
    #
    # Program Counter (pc)
    #
    addl	r0, -64(pc)
    addf	-64(pc), r1
    addl	7(r2), 63(pc) 
    addf	63(pc), -8(r3)
    #
    addl	63(pc), -8192(fp)
    addf	-8192(fp), 63(pc)
    addl	-64(sp), 8191(pc)
    addf	8191(pc), -64(pc)
    #
    addl	-64(63(fp)), -536870912(pc)
    addf	ext(-536870912) + 536870911, -536870912(pc)
    addl	536870911(pc), @-8192
    addf	536870911(pc), -8192(8191(sp))
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    addl   r0, r7[r0:b]
    addf   r1, r6[r1:b]
    #
    # Register Relative
    addl   7(r2), 63(r5)[r2:b]
    addf   -8(r3), 63(r4)[r3:b]
    addl   8191(r3)[r4:b], -8192(fp)
    addf   8191(r2)[r5:b], @-8192
    addl   ext(-536870912) + 536870911, 536870911(r1)[r6:b]
    addf   -64(pc), 536870911(r0)[r7:b]
    #
    # Memory Relative
    addl   r7, -64(63(fp))[r0:b]
    addf   -64(63(fp))[r1:b], -8(r6)
    addl   -8192(8191(fp))[r3:b], -8192(8191(fp))[r2:b]
    addf   @-8, -8192(8191(fp))[r3:b]
    addl   -536870912(536870911(fp))[r4:b], ext(8191) + -536870912
    addf   -536870912(536870911(fp))[r4:b], -536870912(536870911(fp))[r5:b]
    addl   ext(8191) + -536870912, -64(63(sp))[r7:b]
    addf   -64(63(sp))[r6:b], -64(r5)
    addl   -64(63(sp))[r6:b], -8192(8191(sp))[r5:b]
    addf   -8192(8191(sp))[r4:b], -64(pc)
    addl   -536870912(536870911(sp))[r3:b], 63(r5)[r2:b]
    addf   -536870912(536870911(sp))[r3:b], -536870912(536870911(sp))[r2:b]
    addl   7(r4), -64(63(sb))[r0:b]
    addf   -64(63(sb))[r1:b], 8192(r5)[r2:b]
    addl   -64(63(sb))[r1:b], -8192(8191(sb))[r2:b]
    addf   -8(r1), -8192(8191(sb))[r3:b]
    addl   -536870912(536870911(sb))[r4:b], -8192(sp)
    addf   -536870912(536870911(sb))[r4:b], -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    addl   7(r2), @63[r0:b]
    addf   @63[r1:b], -8(r1)
    addl   @63[r1:b], @8191[r2:b]
    addf   -8192(sp), @8191[r3:b]
    addl   @536870911[r4:b], -8192(pc)
    addf   @536870911[r4:b], @536870911[r5:b]
    #
    # External
    addl   7(r2), ext(-64) + 63[r7:b]
    addf   ext(-64) + 63[r6:b], -8(r1)
    addl   ext(-64) + 63[r6:b], ext(-8192) + 8191[r5:b]
    addf   -8192(sp), ext(-8192) + 8191[r4:b]
    addl   ext(-536870912) + 536870911[r3:b], -8192(fp)
    addf   ext(-536870912) + 536870911[r3:b], ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    addl   ext(-536870912) + 536870911[r2:b], tos[r0:b]
    addf   7(r1), tos[r0:b]
    addl   tos[r7:b], @63[r0:b]
    #
    # Memory Space
    addf   7(r7), 63(fp)[r0:b]
    addl   63(fp)[r0:b], @63[r0:b]
    addf   63(fp)[r0:b], 8191(fp)[r0:b]
    addl   -8(r1), 8191(fp)[r0:b]
    addf   8191(fp)[r0:b], 536870911(fp)[r0:b]
    addl   @63[r0:b], 536870911(fp)[r0:b]
    addf   @63[r0:b], 63(sp)[r0:b]
    addl   63(sp)[r0:b], @63[r0:b]
    addf   63(sp)[r0:b], 8191(sp)[r0:b]
    addl   -8(r6), 8191(sp)[r0:b]
    addf   536870911(sp)[r0:b], ext(-536870912) + 536870911[r3:b]
    addl   536870911(sp)[r0:b], 536870911(sp)[r0:b]
    addf   7(r1), 63(sb)[r0:b]
    addl   63(sb)[r0:b], r3
    addf   63(sb)[r0:b], 8191(sb)[r0:b]
    addl   8191(sb)[r0:b], @63[r0:b]
    addf   536870911(sb)[r0:b], ext(-536870912) + 536870911[r3:b]
    addl   536870911(sb)[r0:b], 536870911(sb)[r0:b]
    addf   7(r6), 63(pc)[r0:b]
    addl   63(pc)[r0:b], @63[r0:b]
    addf   63(pc)[r0:b], 8191(pc)[r0:b]
    addl   -8(r1), 8191(pc)[r0:b]
    addf   536870911(pc)[r0:b], ext(-536870912) + 536870911[r3:b]
    addl   536870911(pc)[r0:b], 536870911(pc)[r0:b]
#
end:
