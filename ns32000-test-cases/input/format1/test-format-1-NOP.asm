.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: NOP
#
# Syntax:   NOP
#
#           !      NOP      !
#           +---------------+
#           !1 0 1 0 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The NOP instruction passes control to the next sequential instruction. No opera-
# tion is performed.
#
    nop
#
end:
