.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: RESTORE
#
# Syntax:   RESTORE reglist
#                   imm
#
#           !    RESTORE    !
#           +---------------+
#           !0 1 1 1 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The RESTORE registers instruction restores from the current stack the General
# Purpose registers specified by reglist.
#
    restore [r0]
    restore [r1]
    restore [r2]
    restore [r3]
    restore [r4]
    restore [r5]
    restore [r6]
    restore [r7]
#
    restore [r0, r1]
    restore [r0, r1, r2]
    restore [r0, r1, r2, r3]
    restore [r0, r1, r2, r3, r4]
    restore [r0, r1, r2, r3, r4, r5]
    restore [r0, r1, r2, r3, r4, r5, r6]
    restore [r0, r1, r2, r3, r4, r5, r6, r7]
#
end:
