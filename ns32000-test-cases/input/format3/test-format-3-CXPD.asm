.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 3 instructions
#
# Instruction under test: CXPD
#
# Syntax:   CXPD    dest
#                   gen
#                   addr
#
#           !  desc   !         CXPD        !
#           +---------+---------------------+
#           !   gen   !0 0 0 0 1 1 1 1 1 1 1!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# The CXPD instruction calls the external procedure specified by the desc
# (descriptor) operand. The descriptor is a 32-bit value.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
# Register
#
    cxpd   r0
    cxpd   r1
    cxpd   r2
    cxpd   r3
    cxpd   r4
    cxpd   r5
    cxpd   r6
    cxpd   r7
#
# Register relative
#
    cxpd   63(r0)
    cxpd   -64(r0)
    cxpd   8191(r0)
    cxpd   -8192(r0)
    cxpd   536870911(r0)
    cxpd   -536870912(r0)
    #
    cxpd   63(r1)
    cxpd   -64(r1)
    cxpd   8191(r1)
    cxpd   -8192(r1)
    cxpd   536870911(r1)
    cxpd   -536870912(r1)
    #
    cxpd   63(r2)
    cxpd   -64(r2)
    cxpd   8191(r2)
    cxpd   -8192(r2)
    cxpd   536870911(r2)
    cxpd   -536870912(r2)
    #
    cxpd   63(r3)
    cxpd   -64(r3)
    cxpd   8191(r3)
    cxpd   -8192(r3)
    cxpd   536870911(r3)
    cxpd   -536870912(r3)
    #
    cxpd   63(r4)
    cxpd   -64(r4)
    cxpd   8191(r4)
    cxpd   -8192(r4)
    cxpd   536870911(r4)
    cxpd   -536870912(r4)
    #
    cxpd   63(r5)
    cxpd   -64(r5)
    cxpd   8191(r5)
    cxpd   -8192(r5)
    cxpd   536870911(r5)
    cxpd   -536870912(r5)
    #
    cxpd   63(r6)
    cxpd   -64(r6)
    cxpd   8191(r6)
    cxpd   -8192(r6)
    cxpd   536870911(r6)
    cxpd   -536870912(r6)
    #
    cxpd   63(r7)
    cxpd   -64(r7)
    cxpd   8191(r7)
    cxpd   -8192(r7)
    cxpd   536870911(r7)
    cxpd   -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    cxpd   -64(63(fp))
    cxpd   63(-64(fp))
    cxpd   -8192(63(fp))
    cxpd   -64(8191(fp))
    cxpd   63(-8192(fp))
    cxpd   8191(-64(fp))
    cxpd   -8192(8191(fp))
    cxpd   8191(-8192(fp))
    cxpd   -536870912(8191(fp))
    cxpd   -8192(536870911(fp))
    cxpd   8191(-536870912(fp))
    cxpd   536870911(-8192(fp))
    cxpd   536870911(-536870912(fp))
    cxpd   -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    cxpd   -64(63(sp))
    cxpd   63(-64(sp))
    cxpd   -8192(63(sp))
    cxpd   -64(8191(sp))
    cxpd   63(-8192(sp))
    cxpd   8191(-64(sp))
    cxpd   -8192(8191(sp))
    cxpd   8191(-8192(sp))
    cxpd   -536870912(8191(sp))
    cxpd   -8192(536870911(sp))
    cxpd   8191(-536870912(sp))
    cxpd   536870911(-8192(sp))
    cxpd   536870911(-536870912(sp))
    cxpd   -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    cxpd   -64(63(sb))
    cxpd   63(-64(sb))
    cxpd   -8192(63(sb))
    cxpd   -64(8191(sb))
    cxpd   63(-8192(sb))
    cxpd   8191(-64(sb))
    cxpd   -8192(8191(sb))
    cxpd   8191(-8192(sb))
    cxpd   -536870912(8191(sb))
    cxpd   -8192(536870911(sb))
    cxpd   8191(-536870912(sb))
    cxpd   536870911(-8192(sb))
    cxpd   536870911(-536870912(sb))
    cxpd   -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    cxpd	@-64
    cxpd	@63
    cxpd	@-8192
    cxpd	@8191
    cxpd	@-536870912
    cxpd	@536870911
#
# External
#
    cxpd	ext(-64) + 63
    cxpd	ext(63) + -64
    cxpd	ext(-8192) + 8191
    cxpd	ext(8191) + -8192
    cxpd	ext(-536870912) + 8191
    cxpd	ext(8191) + -536870912
    cxpd	ext(-8192) + 536870911
    cxpd	ext(536870911) + -8192
    cxpd	ext(-536870912) + 536870911
    cxpd	ext(536870911) + -536870912
#
# Top Of Stack
#
    cxpd	tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    cxpd	-64(fp)
    cxpd	63(fp)
    cxpd	-8192(fp)
    cxpd	8191(fp)
    cxpd	-536870912(fp)
    cxpd	536870911(fp)
    #
    # Stack Pointer (sp)
    #
    cxpd	-64(sp)
    cxpd	63(sp)
    cxpd	-8192(sp)
    cxpd	8191(sp)
    cxpd	-536870912(sp)
    cxpd	536870911(sp)
    #
    # Static Memory (sb)
    #
    cxpd	-64(sb)
    cxpd	63(sb)
    cxpd	-8192(sb)
    cxpd	8191(sb)
    cxpd	-536870912(sb)
    cxpd	536870911(sb)
    #
    # Program Counter (pc)
    #
    cxpd	-64(pc)
    cxpd	63(pc)
    cxpd	-8192(pc)
    cxpd	8191(pc)
    cxpd	-536870912(pc)
    cxpd	536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    cxpd   r7[r0:b]
    #
    # Register Relative
    cxpd   63(r5)[r2:b]
    cxpd   8191(r3)[r4:b]
    cxpd   536870911(r1)[r6:b]
    #
    # Memory Relative
    cxpd   -64(63(fp))[r0:b]
    cxpd   -8192(8191(fp))[r2:b]
    cxpd   -536870912(536870911(fp))[r4:b]
    cxpd   -64(63(sp))[r7:b]
    cxpd   -8192(8191(sp))[r5:b]
    cxpd   -536870912(536870911(sp))[r3:b]
    cxpd   -64(63(sb))[r0:b]
    cxpd   -8192(8191(sb))[r2:b]
    cxpd   -536870912(536870911(sb))[r4:b]
    #
    # Absolute
    cxpd   @63[r0:b]
    cxpd   @8191[r2:b]
    cxpd   @536870911[r4:b]
    #
    # External
    cxpd   ext(-64) + 63[r7:b]
    cxpd   ext(-8192) + 8191[r5:b]
    cxpd   ext(-536870912) + 536870911[r3:b]
    #
    # Top Of Stack
    cxpd   tos[r0:b]
    #
    # Memory Space
    cxpd   63(fp)[r0:b]
    cxpd   8191(fp)[r0:b]
    cxpd   536870911(fp)[r0:b]
    cxpd   63(sp)[r0:b]
    cxpd   8191(sp)[r0:b]
    cxpd   536870911(sp)[r0:b]
    cxpd   63(sb)[r0:b]
    cxpd   8191(sb)[r0:b]
    cxpd   536870911(sb)[r0:b]
    cxpd   63(pc)[r0:b]
    cxpd   8191(pc)[r0:b]
    cxpd   536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    cxpd   r1[r0:w]
    #
    # Register Relative
    cxpd   63(r1)[r0:w]
    cxpd   8191(r1)[r0:w]
    cxpd   536870911(r1)[r0:w]
    #
    # Memory Relative
    cxpd   -64(63(fp))[r0:w]
    cxpd   -8192(8191(fp))[r2:w]
    cxpd   -536870912(536870911(fp))[r4:w]
    cxpd   -64(63(sp))[r7:w]
    cxpd   -8192(8191(sp))[r5:w]
    cxpd   -536870912(536870911(sp))[r3:w]
    cxpd   -64(63(sb))[r0:w]
    cxpd   -8192(8191(sb))[r2:w]
    cxpd   -536870912(536870911(sb))[r4:w]
    #
    # Absolute
    cxpd   @63[r0:w]
    cxpd   @8191[r0:w]
    cxpd   @536870911[r0:w]
    #
    # External
    cxpd   ext(-64) + 63[r0:w]
    cxpd   ext(-8192) + 8191[r0:w]
    cxpd   ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    cxpd   tos[r0:w]
    #
    # Memory Space
    cxpd   63(fp)[r0:w]
    cxpd   8191(fp)[r0:w]
    cxpd   536870911(fp)[r0:w]
    cxpd   63(sp)[r0:w]
    cxpd   8191(sp)[r0:w]
    cxpd   536870911(sp)[r0:w]
    cxpd   63(sb)[r0:w]
    cxpd   8191(sb)[r0:w]
    cxpd   536870911(sb)[r0:w]
    cxpd   63(pc)[r0:w]
    cxpd   8191(pc)[r0:w]
    cxpd   536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    cxpd   r1[r0:d]
    #
    # Register Relative
    cxpd   63(r1)[r0:d]
    cxpd   8191(r1)[r0:d]
    cxpd   536870911(r1)[r0:d]
    #
    # Memory Relative
    cxpd   -64(63(fp))[r0:d]
    cxpd   -8192(8191(fp))[r2:d]
    cxpd   -536870912(536870911(fp))[r4:d]
    cxpd   -64(63(sp))[r7:d]
    cxpd   -8192(8191(sp))[r5:d]
    cxpd   -536870912(536870911(sp))[r3:d]
    cxpd   -64(63(sb))[r0:d]
    cxpd   -8192(8191(sb))[r2:d]
    cxpd   -536870912(536870911(sb))[r4:d]
    #
    # Absolute
    cxpd   @63[r0:d]
    cxpd   @8191[r0:d]
    cxpd   @536870911[r0:d]
    #
    # External
    cxpd   ext(-64) + 63[r0:d]
    cxpd   ext(-8192) + 8191[r0:d]
    cxpd   ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    cxpd   tos[r0:d]
    #
    # Memory Space
    cxpd   63(fp)[r0:d]
    cxpd   8191(fp)[r0:d]
    cxpd   536870911(fp)[r0:d]
    cxpd   63(sp)[r0:d]
    cxpd   8191(sp)[r0:d]
    cxpd   536870911(sp)[r0:d]
    cxpd   63(sb)[r0:d]
    cxpd   8191(sb)[r0:d]
    cxpd   536870911(sb)[r0:d]
    cxpd   63(pc)[r0:d]
    cxpd   8191(pc)[r0:d]
    cxpd   536870911(pc)[r0:d]
#
end:
