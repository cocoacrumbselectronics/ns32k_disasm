.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 6 instructions
#
# Instruction under test: ROTi
#
# Syntax.   ROTi    count,  dest
# `                 gen     gen
#                   read.B  rmw.i
#
#           !  count  !   dest  !           ROTi            !
#           +---------+---------+-------+---+---------------+
#           !   gen   !   gen   !0 0 0 0! i !0 1 0 0 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
#
# The ROTi instruction performs a rotation shift on the dest operand in the manner
# specified by the count operand. The sign of count determines the direction of
# the shift. The absolute value of count gives the number of bit positions to
# shift the dest operand.
#
# Register
#
    rotb   0, r0
    rotb   -8, r0
    rotb   1, r1
    rotb   -7, r1
    rotb   2, r2
    rotb   -6, r2
    rotb   3, r3
    rotb   -5, r3
    rotb   4, r4
    rotb   -4, r4
    rotb   5, r5
    rotb   -3, r5
    rotb   6, r6
    rotb   -2, r6
    rotb   7, r7
    rotb   -1, r7
#
# Register relative
#
    rotb   7, 63(r0)
    rotb   -8, -64(r0)
    rotb   7, 8191(r0)
    rotb   -8, -8192(r0)
    rotb   7, 536870911(r0)
    rotb   -8, -536870912(r0)
    #
    rotb   7, 63(r1)
    rotb   -8, -64(r1)
    rotb   7, 8191(r1)
    rotb   -8, -8192(r1)
    rotb   7, 536870911(r1)
    rotb   -8, -536870912(r1)
    #
    rotb   7, 63(r2)
    rotb   -8, -64(r2)
    rotb   7, 8191(r2)
    rotb   -8, -8192(r2)
    rotb   7, 536870911(r2)
    rotb   -8, -536870912(r2)
    #
    rotb   7, 63(r3)
    rotb   -8, -64(r3)
    rotb   7, 8191(r3)
    rotb   -8, -8192(r3)
    rotb   7, 536870911(r3)
    rotb   -8, -536870912(r3)
    #
    rotb   7, 63(r4)
    rotb   -8, -64(r4)
    rotb   7, 8191(r4)
    rotb   -8, -8192(r4)
    rotb   7, 536870911(r4)
    rotb   -8, -536870912(r4)
    #
    rotb   7, 63(r5)
    rotb   -8, -64(r5)
    rotb   7, 8191(r5)
    rotb   -8, -8192(r5)
    rotb   7, 536870911(r5)
    rotb   -8, -536870912(r5)
    #
    rotb   7, 63(r6)
    rotb   -8, -64(r6)
    rotb   7, 8191(r6)
    rotb   -8, -8192(r6)
    rotb   7, 536870911(r6)
    rotb   -8, -536870912(r6)
    #
    rotb   7, 63(r7)
    rotb   -8, -64(r7)
    rotb   7, 8191(r7)
    rotb   -8, -8192(r7)
    rotb   7, 536870911(r7)
    rotb   -8, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    rotb   7, -64(63(fp))
    rotb   -8, -64(63(fp))
    rotb   7, 63(-64(fp))
    rotb   -8, 63(-64(fp))
    #
    rotb   7, -8192(63(fp))
    rotb   -8, -64(8191(fp))
    rotb   7, 63(-8192(fp))
    rotb   -8, 8191(-64(fp))
    #
    rotb   7, -8192(8191(fp))
    rotb   -8, -8192(8191(fp))
    rotb   7, 8191(-8192(fp))
    rotb   -8, 8191(-8192(fp))
    #
    rotb   7, -536870912(8191(fp))
    rotb   -8, -8192(536870911(fp))
    rotb   7, 8191(-536870912(fp))
    rotb   -8, 536870911(-8192(fp))
    #
    rotb   7, 536870911(-536870912(fp))
    rotb   -8, 536870911(-536870912(fp))
    rotb   7, -536870912(536870911(fp))
    rotb   -8, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    rotb   7, -64(63(sp))
    rotb   -8, -64(63(sp))
    rotb   7, 63(-64(sp))
    rotb   -8, 63(-64(sp))
    #
    rotb   7, -8192(63(sp))
    rotb   -8, -64(8191(sp))
    rotb   7, 63(-8192(sp))
    rotb   -8, 8191(-64(sp))
    #
    rotb   7, -8192(8191(sp))
    rotb   -8, -8192(8191(sp))
    rotb   7, 8191(-8192(sp))
    rotb   -8, 8191(-8192(sp))
    #
    rotb   7, -536870912(8191(sp))
    rotb   -8, -8192(536870911(sp))
    rotb   7, 8191(-536870912(sp))
    rotb   -8, 536870911(-8192(sp))
    #
    rotb   7, 536870911(-536870912(sp))
    rotb   -8, 536870911(-536870912(sp))
    rotb   7, -536870912(536870911(sp))
    rotb   -8, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    rotb   7, -64(63(sb))
    rotb   -8, -64(63(sb))
    rotb   7, 63(-64(sb))
    rotb   -8, 63(-64(sb))
    #
    rotb   7, -8192(63(sb))
    rotb   -8, -64(8191(sb))
    rotb   7, 63(-8192(sb))
    rotb   -8, 8191(-64(sb))
    #
    rotb   7, -8192(8191(sb))
    rotb   -8, -8192(8191(sb))
    rotb   7, 8191(-8192(sb))
    rotb   -8, 8191(-8192(sb))
    #
    rotb   7, -536870912(8191(sb))
    rotb   -8, -8192(536870911(sb))
    rotb   7, 8191(-536870912(sb))
    rotb   -8, 536870911(-8192(sb))
    #
    rotb   7, 536870911(-536870912(sb))
    rotb   -8, 536870911(-536870912(sb))
    rotb   7, -536870912(536870911(sb))
    rotb   -8, -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    rotb	7, @-64
    rotb	-8, @-64
    rotb	7, @63
    rotb	-8, @63
    rotb	7, @-65
    rotb	-8, @-65
    rotb	7, @64
    rotb	-8, @64
    #
    rotb	7, @-8192
    rotb	-8, @-8192
    rotb	7, @8191
    rotb	-8, @8191
    rotb	7, @-8193
    rotb	-8, @-8193
    rotb	7, @8192
    rotb	-8, @8192
    #
    rotb	7, @-536870912
    rotb	-8, @536870911
#
# External
#
    rotb	7, ext(-64) + 63
    rotb	-8, ext(-64) + 63
    rotb	7, ext(63) + -64
    rotb	-8, ext(63) + -64
    #
    rotb	7, ext(-65) + 63
    rotb	-8, ext(-65) + 63
    rotb	7, ext(63) + -65
    rotb	-8, ext(63) + -65
    rotb	7, ext(-64) + 64
    rotb	-8, ext(-64) + 64
    rotb	7, ext(64) + -64
    rotb	-8, ext(64) + -64
    #
    rotb	7, ext(-8192) + 8191
    rotb	-8, ext(-8192) + 8191
    rotb	7, ext(8191) + -8192
    rotb	-8, ext(8191) + -8192
    #
    rotb	7, ext(-536870912) + 8191
    rotb	-8, ext(-536870912) + 8191
    rotb	7, ext(8191) + -536870912
    rotb	-8, ext(8191) + -536870912
    rotb	7, ext(-8192) + 536870911
    rotb	-8, ext(-8192) + 536870911
    rotb	7, ext(536870911) + -8192
    rotb	-8, ext(536870911) + -8192
    #
    rotb	7, ext(-536870912) + 536870911
    rotb	-8, ext(-536870912) + 536870911
    rotb	7, ext(536870911) + -536870912
    rotb	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    rotb	7, tos
    rotb	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    rotb	7, -64(fp)
    rotb	-8, -64(fp)
    rotb	7, 63(fp)
    rotb	-8, 63(fp)
    #
    rotb	7, -8192(fp)
    rotb	-8, -8192(fp)
    rotb	7, 8191(fp)
    rotb	-8, 8191(fp)
    #
    rotb	7, -536870912(fp)
    rotb	-8, -536870912(fp)
    rotb	7, 536870911(fp)
    rotb	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    rotb	7, -64(sp)
    rotb	-8, -64(sp)
    rotb	7, 63(sp)
    rotb	-8, 63(sp)
    #
    rotb	7, -8192(sp)
    rotb	-8, -8192(sp)
    rotb	7, 8191(sp)
    rotb	-8, 8191(sp)
    #
    rotb	7, -536870912(sp)
    rotb	-8, -536870912(sp)
    rotb	7, 536870911(sp)
    rotb	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    rotb	7, -64(sb)
    rotb	-8, -64(sb)
    rotb	7, 63(sb)
    rotb	-8, 63(sb)
    #
    rotb	7, -8192(sb)
    rotb	-8, -8192(sb)
    rotb	7, 8191(sb)
    rotb	-8, 8191(sb)
    #
    rotb	7, -536870912(sb)
    rotb	-8, -536870912(sb)
    rotb	7, 536870911(sb)
    rotb	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    rotb	7, -64(pc)
    rotb	-8, -64(pc)
    rotb	7, 63(pc)
    rotb	-8, 63(pc)
    #
    rotb	7, -8192(pc)
    rotb	-8, -8192(pc)
    rotb	7, 8191(pc)
    rotb	-8, 8191(pc)
    #
    rotb	7, -536870912(pc)
    rotb	-8, -536870912(pc)
    rotb	7, 536870911(pc)
    rotb	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    rotb   7, r7[r0:b]
    rotb   -8, r6[r1:b]
    #
    # Register Relative
    rotb   7, 63(r5)[r2:b]
    rotb   -8, 63(r4)[r3:b]
    rotb   7, 8191(r3)[r4:b]
    rotb   -8, 8191(r2)[r5:b]
    rotb   7, 536870911(r1)[r6:b]
    rotb   -8, 536870911(r0)[r7:b]
    #
    # Memory Relative
    rotb   7, -64(63(fp))[r0:b]
    rotb   -8, -64(63(fp))[r1:b]
    rotb   7, -8192(8191(fp))[r2:b]
    rotb   -8, -8192(8191(fp))[r3:b]
    rotb   7, -536870912(536870911(fp))[r4:b]
    rotb   -8, -536870912(536870911(fp))[r5:b]
    rotb   7, -64(63(sp))[r7:b]
    rotb   -8, -64(63(sp))[r6:b]
    rotb   7, -8192(8191(sp))[r5:b]
    rotb   -8, -8192(8191(sp))[r4:b]
    rotb   7, -536870912(536870911(sp))[r3:b]
    rotb   -8, -536870912(536870911(sp))[r2:b]
    rotb   7, -64(63(sb))[r0:b]
    rotb   -8, -64(63(sb))[r1:b]
    rotb   7, -8192(8191(sb))[r2:b]
    rotb   -8, -8192(8191(sb))[r3:b]
    rotb   7, -536870912(536870911(sb))[r4:b]
    rotb   -8, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    rotb   7, @63[r0:b]
    rotb   -8, @63[r1:b]
    rotb   7, @8191[r2:b]
    rotb   -8, @8191[r3:b]
    rotb   7, @536870911[r4:b]
    rotb   -8, @536870911[r5:b]
    #
    # External
    rotb   7, ext(-64) + 63[r7:b]
    rotb   -8, ext(-64) + 63[r6:b]
    rotb   7, ext(-8192) + 8191[r5:b]
    rotb   -8, ext(-8192) + 8191[r4:b]
    rotb   7, ext(-536870912) + 536870911[r3:b]
    rotb   -8, ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    rotb   7, tos[r0:b]
    rotb   -8, tos[r7:b]
    #
    # Memory Space
    rotb   7, 63(fp)[r0:b]
    rotb   -8, 63(fp)[r0:b]
    rotb   7, 8191(fp)[r0:b]
    rotb   -8, 8191(fp)[r0:b]
    rotb   7, 536870911(fp)[r0:b]
    rotb   -8, 536870911(fp)[r0:b]
    rotb   7, 63(sp)[r0:b]
    rotb   -8, 63(sp)[r0:b]
    rotb   7, 8191(sp)[r0:b]
    rotb   -8, 8191(sp)[r0:b]
    rotb   7, 536870911(sp)[r0:b]
    rotb   -8, 536870911(sp)[r0:b]
    rotb   7, 63(sb)[r0:b]
    rotb   -8, 63(sb)[r0:b]
    rotb   7, 8191(sb)[r0:b]
    rotb   -8, 8191(sb)[r0:b]
    rotb   7, 536870911(sb)[r0:b]
    rotb   -8, 536870911(sb)[r0:b]
    rotb   7, 63(pc)[r0:b]
    rotb   -8, 63(pc)[r0:b]
    rotb   7, 8191(pc)[r0:b]
    rotb   -8, 8191(pc)[r0:b]
    rotb   7, 536870911(pc)[r0:b]
    rotb   -8, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    rotb   7, r1[r0:w]
    rotb   -8, r1[r0:w]
    #
    # Register Relative
    rotb   7, 63(r1)[r0:w]
    rotb   -8, 63(r1)[r0:w]
    rotb   7, 8191(r1)[r0:w]
    rotb   -8, 8191(r1)[r0:w]
    rotb   7, 536870911(r1)[r0:w]
    rotb   -8, 536870911(r1)[r0:w]
    #
    # Memory Relative
    rotb   7, -64(63(fp))[r0:w]
    rotb   -8, -64(63(fp))[r1:w]
    rotb   7, -8192(8191(fp))[r2:w]
    rotb   -8, -8192(8191(fp))[r3:w]
    rotb   7, -536870912(536870911(fp))[r4:w]
    rotb   -8, -536870912(536870911(fp))[r5:w]
    rotb   7, -64(63(sp))[r7:w]
    rotb   -8, -64(63(sp))[r6:w]
    rotb   7, -8192(8191(sp))[r5:w]
    rotb   -8, -8192(8191(sp))[r4:w]
    rotb   7, -536870912(536870911(sp))[r3:w]
    rotb   -8, -536870912(536870911(sp))[r2:w]
    rotb   7, -64(63(sb))[r0:w]
    rotb   -8, -64(63(sb))[r1:w]
    rotb   7, -8192(8191(sb))[r2:w]
    rotb   -8, -8192(8191(sb))[r3:w]
    rotb   7, -536870912(536870911(sb))[r4:w]
    rotb   -8, -536870912(536870911(sb))[r5:w]
    #
    # Absolute
    rotb   7, @63[r0:w]
    rotb   -8, @63[r0:w]
    rotb   7, @8191[r0:w]
    rotb   -8, @8191[r0:w]
    rotb   7, @536870911[r0:w]
    rotb   -8, @536870911[r0:w]
    #
    # External
    rotb   7, ext(-64) + 63[r0:w]
    rotb   -8, ext(-64) + 63[r0:w]
    rotb   7, ext(-8192) + 8191[r0:w]
    rotb   -8, ext(-8192) + 8191[r0:w]
    rotb   7, ext(-536870912) + 536870911[r0:w]
    rotb   -8, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    rotb   7, tos[r0:w]
    rotb   -8, tos[r0:w]
    #
    # Memory Space
    rotb   7, 63(fp)[r0:w]
    rotb   -8, 63(fp)[r0:w]
    rotb   7, 8191(fp)[r0:w]
    rotb   -8, 8191(fp)[r0:w]
    rotb   7, 536870911(fp)[r0:w]
    rotb   -8, 536870911(fp)[r0:w]
    rotb   7, 63(sp)[r0:w]
    rotb   -8, 63(sp)[r0:w]
    rotb   7, 8191(sp)[r0:w]
    rotb   -8, 8191(sp)[r0:w]
    rotb   7, 536870911(sp)[r0:w]
    rotb   -8, 536870911(sp)[r0:w]
    rotb   7, 63(sb)[r0:w]
    rotb   -8, 63(sb)[r0:w]
    rotb   7, 8191(sb)[r0:w]
    rotb   -8, 8191(sb)[r0:w]
    rotb   7, 536870911(sb)[r0:w]
    rotb   -8, 536870911(sb)[r0:w]
    rotb   7, 63(pc)[r0:w]
    rotb   -8, 63(pc)[r0:w]
    rotb   7, 8191(pc)[r0:w]
    rotb   -8, 8191(pc)[r0:w]
    rotb   7, 536870911(pc)[r0:w]
    rotb   -8, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    rotb   7, r1[r0:d]
    rotb   -8, r1[r0:d]
    #
    # Register Relative
    rotb   7, 63(r1)[r0:d]
    rotb   -8, 63(r1)[r0:d]
    rotb   7, 8191(r1)[r0:d]
    rotb   -8, 8191(r1)[r0:d]
    rotb   7, 536870911(r1)[r0:d]
    rotb   -8, 536870911(r1)[r0:d]
    #
    # Memory Relative
    rotb   7, -64(63(fp))[r0:d]
    rotb   -8, -64(63(fp))[r1:d]
    rotb   7, -8192(8191(fp))[r2:d]
    rotb   -8, -8192(8191(fp))[r3:d]
    rotb   7, -536870912(536870911(fp))[r4:d]
    rotb   -8, -536870912(536870911(fp))[r5:d]
    rotb   7, -64(63(sp))[r7:d]
    rotb   -8, -64(63(sp))[r6:d]
    rotb   7, -8192(8191(sp))[r5:d]
    rotb   -8, -8192(8191(sp))[r4:d]
    rotb   7, -536870912(536870911(sp))[r3:d]
    rotb   -8, -536870912(536870911(sp))[r2:d]
    rotb   7, -64(63(sb))[r0:d]
    rotb   -8, -64(63(sb))[r1:d]
    rotb   7, -8192(8191(sb))[r2:d]
    rotb   -8, -8192(8191(sb))[r3:d]
    rotb   7, -536870912(536870911(sb))[r4:d]
    rotb   -8, -536870912(536870911(sb))[r5:d]
    #
    # Absolute
    rotb   7, @63[r0:d]
    rotb   -8, @63[r0:d]
    rotb   7, @8191[r0:d]
    rotb   -8, @8191[r0:d]
    rotb   7, @536870911[r0:d]
    rotb   -8, @536870911[r0:d]
    #
    # External
    rotb   7, ext(-64) + 63[r0:d]
    rotb   -8, ext(-64) + 63[r0:d]
    rotb   7, ext(-8192) + 8191[r0:d]
    rotb   -8, ext(-8192) + 8191[r0:d]
    rotb   7, ext(-536870912) + 536870911[r0:d]
    rotb   -8, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    rotb   7, tos[r0:d]
    rotb   -8, tos[r0:d]
    #
    # Memory Space
    rotb   7, 63(fp)[r0:d]
    rotb   -8, 63(fp)[r0:d]
    rotb   7, 8191(fp)[r0:d]
    rotb   -8, 8191(fp)[r0:d]
    rotb   7, 536870911(fp)[r0:d]
    rotb   -8, 536870911(fp)[r0:d]
    rotb   7, 63(sp)[r0:d]
    rotb   -8, 63(sp)[r0:d]
    rotb   7, 8191(sp)[r0:d]
    rotb   -8, 8191(sp)[r0:d]
    rotb   7, 536870911(sp)[r0:d]
    rotb   -8, 536870911(sp)[r0:d]
    rotb   7, 63(sb)[r0:d]
    rotb   -8, 63(sb)[r0:d]
    rotb   7, 8191(sb)[r0:d]
    rotb   -8, 8191(sb)[r0:d]
    rotb   7, 536870911(sb)[r0:d]
    rotb   -8, 536870911(sb)[r0:d]
    rotb   7, 63(pc)[r0:d]
    rotb   -8, 63(pc)[r0:d]
    rotb   7, 8191(pc)[r0:d]
    rotb   -8, 8191(pc)[r0:d]
    rotb   7, 536870911(pc)[r0:d]
    rotb   -8, 536870911(pc)[r0:d]
#
# WORD
#
#
# Register
#
    rotw   15, r0
    rotw   -16, r0
    rotw   15, r1
    rotw   -16, r1
    rotw   15, r2
    rotw   -16, r2
    rotw   15, r3
    rotw   -16, r3
    rotw   15, r4
    rotw   -16, r4
    rotw   15, r5
    rotw   -16, r5
    rotw   15, r6
    rotw   -16, r6
    rotw   15, r7
    rotw   -16, r7
#
# Register relative
#
    rotw   15, 63(r0)
    rotw   -16, -64(r0)
    rotw   15, 8191(r0)
    rotw   -16, -8192(r0)
    rotw   15, 536870911(r0)
    rotw   -16, -536870912(r0)
    #
    rotw   15, 63(r1)
    rotw   -16, -64(r1)
    rotw   15, 8191(r1)
    rotw   -16, -8192(r1)
    rotw   15, 536870911(r1)
    rotw   -16, -536870912(r1)
    #
    rotw   15, 63(r2)
    rotw   -16, -64(r2)
    rotw   15, 8191(r2)
    rotw   -16, -8192(r2)
    rotw   15, 536870911(r2)
    rotw   -16, -536870912(r2)
    #
    rotw   15, 63(r3)
    rotw   -16, -64(r3)
    rotw   15, 8191(r3)
    rotw   -16, -8192(r3)
    rotw   15, 536870911(r3)
    rotw   -16, -536870912(r3)
    #
    rotw   15, 63(r4)
    rotw   -16, -64(r4)
    rotw   15, 8191(r4)
    rotw   -16, -8192(r4)
    rotw   15, 536870911(r4)
    rotw   -16, -536870912(r4)
    #
    rotw   15, 63(r5)
    rotw   -16, -64(r5)
    rotw   15, 8191(r5)
    rotw   -16, -8192(r5)
    rotw   15, 536870911(r5)
    rotw   -16, -536870912(r5)
    #
    rotw   15, 63(r6)
    rotw   -16, -64(r6)
    rotw   15, 8191(r6)
    rotw   -16, -8192(r6)
    rotw   15, 536870911(r6)
    rotw   -16, -536870912(r6)
    #
    rotw   15, 63(r7)
    rotw   -16, -64(r7)
    rotw   15, 8191(r7)
    rotw   -16, -8192(r7)
    rotw   15, 536870911(r7)
    rotw   -16, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    rotw   15, -64(63(fp))
    rotw   -16, -64(63(fp))
    rotw   15, 63(-64(fp))
    rotw   -16, 63(-64(fp))
    #
    rotw   15, -8192(63(fp))
    rotw   -16, -64(8191(fp))
    rotw   15, 63(-8192(fp))
    rotw   -16, 8191(-64(fp))
    #
    rotw   15, -8192(8191(fp))
    rotw   -16, -8192(8191(fp))
    rotw   15, 8191(-8192(fp))
    rotw   -16, 8191(-8192(fp))
    #
    rotw   15, -536870912(8191(fp))
    rotw   -16, -8192(536870911(fp))
    rotw   15, 8191(-536870912(fp))
    rotw   -16, 536870911(-8192(fp))
    #
    rotw   15, 536870911(-536870912(fp))
    rotw   -16, 536870911(-536870912(fp))
    rotw   15, -536870912(536870911(fp))
    rotw   -16, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    rotw   15, -64(63(sp))
    rotw   -16, -64(63(sp))
    rotw   15, 63(-64(sp))
    rotw   -16, 63(-64(sp))
    #
    rotw   15, -8192(63(sp))
    rotw   -16, -64(8191(sp))
    rotw   15, 63(-8192(sp))
    rotw   -16, 8191(-64(sp))
    #
    rotw   15, -8192(8191(sp))
    rotw   -16, -8192(8191(sp))
    rotw   15, 8191(-8192(sp))
    rotw   -16, 8191(-8192(sp))
    #
    rotw   15, -536870912(8191(sp))
    rotw   -16, -8192(536870911(sp))
    rotw   15, 8191(-536870912(sp))
    rotw   -16, 536870911(-8192(sp))
    #
    rotw   15, 536870911(-536870912(sp))
    rotw   -16, 536870911(-536870912(sp))
    rotw   15, -536870912(536870911(sp))
    rotw   -16, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    rotw   15, -64(63(sb))
    rotw   -16, -64(63(sb))
    rotw   15, 63(-64(sb))
    rotw   -16, 63(-64(sb))
    #
    rotw   15, -8192(63(sb))
    rotw   -16, -64(8191(sb))
    rotw   15, 63(-8192(sb))
    rotw   -16, 8191(-64(sb))
    #
    rotw   15, -8192(8191(sb))
    rotw   -16, -8192(8191(sb))
    rotw   15, 8191(-8192(sb))
    rotw   -16, 8191(-8192(sb))
    #
    rotw   15, -536870912(8191(sb))
    rotw   -16, -8192(536870911(sb))
    rotw   15, 8191(-536870912(sb))
    rotw   -16, 536870911(-8192(sb))
    #
    rotw   15, 536870911(-536870912(sb))
    rotw   -16, 536870911(-536870912(sb))
    rotw   15, -536870912(536870911(sb))
    rotw   -16, -536870912(536870911(sb))

#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    rotw	7, @-64
    rotw	-8, @-64
    rotw	7, @63
    rotw	-8, @63
    rotw	7, @-65
    rotw	-8, @-65
    rotw	7, @64
    rotw	-8, @64
    #
    rotw	7, @-8192
    rotw	-8, @-8192
    rotw	7, @8191
    rotw	-8, @8191
    rotw	7, @-8193
    rotw	-8, @-8193
    rotw	7, @8192
    rotw	-8, @8192
    #
    rotw	7, @-536870912
    rotw	-8, @536870911
#
# External
#
    rotw	7, ext(-64) + 63
    rotw	-8, ext(-64) + 63
    rotw	7, ext(63) + -64
    rotw	-8, ext(63) + -64
    #
    rotw	7, ext(-65) + 63
    rotw	-8, ext(-65) + 63
    rotw	7, ext(63) + -65
    rotw	-8, ext(63) + -65
    rotw	7, ext(-64) + 64
    rotw	-8, ext(-64) + 64
    rotw	7, ext(64) + -64
    rotw	-8, ext(64) + -64
    #
    rotw	7, ext(-8192) + 8191
    rotw	-8, ext(-8192) + 8191
    rotw	7, ext(8191) + -8192
    rotw	-8, ext(8191) + -8192
    #
    rotw	7, ext(-536870912) + 8191
    rotw	-8, ext(-536870912) + 8191
    rotw	7, ext(8191) + -536870912
    rotw	-8, ext(8191) + -536870912
    rotw	7, ext(-8192) + 536870911
    rotw	-8, ext(-8192) + 536870911
    rotw	7, ext(536870911) + -8192
    rotw	-8, ext(536870911) + -8192
    #
    rotw	7, ext(-536870912) + 536870911
    rotw	-8, ext(-536870912) + 536870911
    rotw	7, ext(536870911) + -536870912
    rotw	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    rotw	7, tos
    rotw	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    rotw	7, -64(fp)
    rotw	-8, -64(fp)
    rotw	7, 63(fp)
    rotw	-8, 63(fp)
    #
    rotw	7, -8192(fp)
    rotw	-8, -8192(fp)
    rotw	7, 8191(fp)
    rotw	-8, 8191(fp)
    #
    rotw	7, -536870912(fp)
    rotw	-8, -536870912(fp)
    rotw	7, 536870911(fp)
    rotw	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    rotw	7, -64(sp)
    rotw	-8, -64(sp)
    rotw	7, 63(sp)
    rotw	-8, 63(sp)
    #
    rotw	7, -8192(sp)
    rotw	-8, -8192(sp)
    rotw	7, 8191(sp)
    rotw	-8, 8191(sp)
    #
    rotw	7, -536870912(sp)
    rotw	-8, -536870912(sp)
    rotw	7, 536870911(sp)
    rotw	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    rotw	7, -64(sb)
    rotw	-8, -64(sb)
    rotw	7, 63(sb)
    rotw	-8, 63(sb)
    #
    rotw	7, -8192(sb)
    rotw	-8, -8192(sb)
    rotw	7, 8191(sb)
    rotw	-8, 8191(sb)
    #
    rotw	7, -536870912(sb)
    rotw	-8, -536870912(sb)
    rotw	7, 536870911(sb)
    rotw	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    rotw	7, -64(pc)
    rotw	-8, -64(pc)
    rotw	7, 63(pc)
    rotw	-8, 63(pc)
    #
    rotw	7, -8192(pc)
    rotw	-8, -8192(pc)
    rotw	7, 8191(pc)
    rotw	-8, 8191(pc)
    #
    rotw	7, -536870912(pc)
    rotw	-8, -536870912(pc)
    rotw	7, 536870911(pc)
    rotw	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    rotw   15, r7[r0:b]
    rotw   -16, r6[r1:b]
    #
    # Register Relative
    rotw   15, 63(r5)[r2:b]
    rotw   -16, 63(r4)[r3:b]
    rotw   15, 8191(r3)[r4:b]
    rotw   -16, 8191(r2)[r5:b]
    rotw   15, 536870911(r1)[r6:b]
    rotw   -16, 536870911(r0)[r7:b]
    #
    # Memory Relative
    rotw   15, -64(63(fp))[r0:b]
    rotw   -16, -64(63(fp))[r1:b]
    rotw   15, -8192(8191(fp))[r2:b]
    rotw   -16, -8192(8191(fp))[r3:b]
    rotw   15, -536870912(536870911(fp))[r4:b]
    rotw   -16, -536870912(536870911(fp))[r5:b]
    rotw   15, -64(63(sp))[r7:b]
    rotw   -16, -64(63(sp))[r6:b]
    rotw   15, -8192(8191(sp))[r5:b]
    rotw   -16, -8192(8191(sp))[r4:b]
    rotw   15, -536870912(536870911(sp))[r3:b]
    rotw   -16, -536870912(536870911(sp))[r2:b]
    rotw   15, -64(63(sb))[r0:b]
    rotw   -16, -64(63(sb))[r1:b]
    rotw   15, -8192(8191(sb))[r2:b]
    rotw   -16, -8192(8191(sb))[r3:b]
    rotw   15, -536870912(536870911(sb))[r4:b]
    rotw   -16, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    rotw   15, @63[r0:b]
    rotw   -16, @63[r0:b]
    rotw   15, @8191[r0:b]
    rotw   -16, @8191[r0:b]
    rotw   15, @536870911[r0:b]
    rotw   -16, @536870911[r0:b]
    #
    # External
    rotw   15, ext(-64) + 63[r0:b]
    rotw   -16, ext(-64) + 63[r0:b]
    rotw   15, ext(-8192) + 8191[r0:b]
    rotw   -16, ext(-8192) + 8191[r0:b]
    rotw   15, ext(-536870912) + 536870911[r0:b]
    rotw   -16, ext(-536870912) + 536870911[r0:b]
    #
    # Top Of Stack
    rotw   15, tos[r0:b]
    rotw   -16, tos[r0:b]
    #
    # Memory Space
    rotw   15, 63(fp)[r0:b]
    rotw   -16, 63(fp)[r0:b]
    rotw   15, 8191(fp)[r0:b]
    rotw   -16, 8191(fp)[r0:b]
    rotw   15, 536870911(fp)[r0:b]
    rotw   -16, 536870911(fp)[r0:b]
    rotw   15, 63(sp)[r0:b]
    rotw   -16, 63(sp)[r0:b]
    rotw   15, 8191(sp)[r0:b]
    rotw   -16, 8191(sp)[r0:b]
    rotw   15, 536870911(sp)[r0:b]
    rotw   -16, 536870911(sp)[r0:b]
    rotw   15, 63(sb)[r0:b]
    rotw   -16, 63(sb)[r0:b]
    rotw   15, 8191(sb)[r0:b]
    rotw   -16, 8191(sb)[r0:b]
    rotw   15, 536870911(sb)[r0:b]
    rotw   -16, 536870911(sb)[r0:b]
    rotw   15, 63(pc)[r0:b]
    rotw   -16, 63(pc)[r0:b]
    rotw   15, 8191(pc)[r0:b]
    rotw   -16, 8191(pc)[r0:b]
    rotw   15, 536870911(pc)[r0:b]
    rotw   -16, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    rotw   15, r1[r0:w]
    rotw   -16, r1[r0:w]
    #
    # Register Relative
    rotw   15, 63(r1)[r0:w]
    rotw   -16, 63(r1)[r0:w]
    rotw   15, 8191(r1)[r0:w]
    rotw   -16, 8191(r1)[r0:w]
    rotw   15, 536870911(r1)[r0:w]
    rotw   -16, 536870911(r1)[r0:w]
    #
    # Memory Relative
    rotw   15, -64(63(fp))[r0:w]
    rotw   -16, -64(63(fp))[r0:w]
    rotw   15, -8192(8191(fp))[r0:w]
    rotw   -16, -8192(8191(fp))[r0:w]
    rotw   15, -536870912(536870911(fp))[r0:w]
    rotw   -16, -536870912(536870911(fp))[r0:w]
    #
    # Absolute
    rotw   15, @63[r0:w]
    rotw   -16, @63[r0:w]
    rotw   15, @8191[r0:w]
    rotw   -16, @8191[r0:w]
    rotw   15, @536870911[r0:w]
    rotw   -16, @536870911[r0:w]
    #
    # External
    rotw   15, ext(-64) + 63[r0:w]
    rotw   -16, ext(-64) + 63[r0:w]
    rotw   15, ext(-8192) + 8191[r0:w]
    rotw   -16, ext(-8192) + 8191[r0:w]
    rotw   15, ext(-536870912) + 536870911[r0:w]
    rotw   -16, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    rotw   15, tos[r0:w]
    rotw   -16, tos[r0:w]
    #
    # Memory Space
    rotw   15, 63(fp)[r0:w]
    rotw   -16, 63(fp)[r0:w]
    rotw   15, 8191(fp)[r0:w]
    rotw   -16, 8191(fp)[r0:w]
    rotw   15, 536870911(fp)[r0:w]
    rotw   -16, 536870911(fp)[r0:w]
    rotw   15, 63(sp)[r0:w]
    rotw   -16, 63(sp)[r0:w]
    rotw   15, 8191(sp)[r0:w]
    rotw   -16, 8191(sp)[r0:w]
    rotw   15, 536870911(sp)[r0:w]
    rotw   -16, 536870911(sp)[r0:w]
    rotw   15, 63(sb)[r0:w]
    rotw   -16, 63(sb)[r0:w]
    rotw   15, 8191(sb)[r0:w]
    rotw   -16, 8191(sb)[r0:w]
    rotw   15, 536870911(sb)[r0:w]
    rotw   -16, 536870911(sb)[r0:w]
    rotw   15, 63(pc)[r0:w]
    rotw   -16, 63(pc)[r0:w]
    rotw   15, 8191(pc)[r0:w]
    rotw   -16, 8191(pc)[r0:w]
    rotw   15, 536870911(pc)[r0:w]
    rotw   -16, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    rotw   15, r1[r0:d]
    rotw   -16, r1[r0:d]
    #
    # Register Relative
    rotw   15, 63(r1)[r0:d]
    rotw   -16, 63(r1)[r0:d]
    rotw   15, 8191(r1)[r0:d]
    rotw   -16, 8191(r1)[r0:d]
    rotw   15, 536870911(r1)[r0:d]
    rotw   -16, 536870911(r1)[r0:d]
    #
    # Memory Relative
    rotw   15, -64(63(fp))[r0:d]
    rotw   -16, -64(63(fp))[r0:d]
    rotw   15, -8192(8191(fp))[r0:d]
    rotw   -16, -8192(8191(fp))[r0:d]
    rotw   15, -536870912(536870911(fp))[r0:d]
    rotw   -16, -536870912(536870911(fp))[r0:d]
    #
    # Absolute
    rotw   15, @63[r0:d]
    rotw   -16, @63[r0:d]
    rotw   15, @8191[r0:d]
    rotw   -16, @8191[r0:d]
    rotw   15, @536870911[r0:d]
    rotw   -16, @536870911[r0:d]
    #
    # External
    rotw   15, ext(-64) + 63[r0:d]
    rotw   -16, ext(-64) + 63[r0:d]
    rotw   15, ext(-8192) + 8191[r0:d]
    rotw   -16, ext(-8192) + 8191[r0:d]
    rotw   15, ext(-536870912) + 536870911[r0:d]
    rotw   -16, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    rotw   15, tos[r0:d]
    rotw   -16, tos[r0:d]
    #
    # Memory Space
    rotw   15, 63(fp)[r0:d]
    rotw   -16, 63(fp)[r0:d]
    rotw   15, 8191(fp)[r0:d]
    rotw   -16, 8191(fp)[r0:d]
    rotw   15, 536870911(fp)[r0:d]
    rotw   -16, 536870911(fp)[r0:d]
    rotw   15, 63(sp)[r0:d]
    rotw   -16, 63(sp)[r0:d]
    rotw   15, 8191(sp)[r0:d]
    rotw   -16, 8191(sp)[r0:d]
    rotw   15, 536870911(sp)[r0:d]
    rotw   -16, 536870911(sp)[r0:d]
    rotw   15, 63(sb)[r0:d]
    rotw   -16, 63(sb)[r0:d]
    rotw   15, 8191(sb)[r0:d]
    rotw   -16, 8191(sb)[r0:d]
    rotw   15, 536870911(sb)[r0:d]
    rotw   -16, 536870911(sb)[r0:d]
    rotw   15, 63(pc)[r0:d]
    rotw   -16, 63(pc)[r0:d]
    rotw   15, 8191(pc)[r0:d]
    rotw   -16, 8191(pc)[r0:d]
    rotw   15, 536870911(pc)[r0:d]
    rotw   -16, 536870911(pc)[r0:d]

#
# DOUBLE WORD
#
#
# Register
#
    rotd   31, r0
    rotd   -32, r0
    rotd   31, r1
    rotd   -32, r1
    rotd   31, r2
    rotd   -32, r2
    rotd   31, r3
    rotd   -32, r3
    rotd   31, r4
    rotd   -32, r4
    rotd   31, r5
    rotd   -32, r5
    rotd   31, r6
    rotd   -32, r6
    rotd   31, r7
    rotd   -32, r7
#
# Register relative
#
    rotd   31, 63(r0)
    rotd   -32, -64(r0)
    rotd   31, 8191(r0)
    rotd   -32, -8192(r0)
    rotd   31, 536870911(r0)
    rotd   -32, -536870912(r0)
    #
    rotd   31, 63(r1)
    rotd   -32, -64(r1)
    rotd   31, 8191(r1)
    rotd   -32, -8192(r1)
    rotd   31, 536870911(r1)
    rotd   -32, -536870912(r1)
    #
    rotd   31, 63(r2)
    rotd   -32, -64(r2)
    rotd   31, 8191(r2)
    rotd   -32, -8192(r2)
    rotd   31, 536870911(r2)
    rotd   -32, -536870912(r2)
    #
    rotd   31, 63(r3)
    rotd   -32, -64(r3)
    rotd   31, 8191(r3)
    rotd   -32, -8192(r3)
    rotd   31, 536870911(r3)
    rotd   -32, -536870912(r3)
    #
    rotd   31, 63(r4)
    rotd   -32, -64(r4)
    rotd   31, 8191(r4)
    rotd   -32, -8192(r4)
    rotd   31, 536870911(r4)
    rotd   -32, -536870912(r4)
    #
    rotd   31, 63(r5)
    rotd   -32, -64(r5)
    rotd   31, 8191(r5)
    rotd   -32, -8192(r5)
    rotd   31, 536870911(r5)
    rotd   -32, -536870912(r5)
    #
    rotd   31, 63(r6)
    rotd   -32, -64(r6)
    rotd   31, 8191(r6)
    rotd   -32, -8192(r6)
    rotd   31, 536870911(r6)
    rotd   -32, -536870912(r6)
    #
    rotd   31, 63(r7)
    rotd   -32, -64(r7)
    rotd   31, 8191(r7)
    rotd   -32, -8192(r7)
    rotd   31, 536870911(r7)
    rotd   -32, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    rotd   31, -64(63(fp))
    rotd   -32, -64(63(fp))
    rotd   31, 63(-64(fp))
    rotd   -32, 63(-64(fp))
    #
    rotd   31, -8192(63(fp))
    rotd   -32, -64(8191(fp))
    rotd   31, 63(-8192(fp))
    rotd   -32, 8191(-64(fp))
    #
    rotd   31, -8192(8191(fp))
    rotd   -32, -8192(8191(fp))
    rotd   31, 8191(-8192(fp))
    rotd   -32, 8191(-8192(fp))
    #
    rotd   31, -536870912(8191(fp))
    rotd   -32, -8192(536870911(fp))
    rotd   31, 8191(-536870912(fp))
    rotd   -32, 536870911(-8192(fp))
    #
    rotd   31, 536870911(-536870912(fp))
    rotd   -32, 536870911(-536870912(fp))
    rotd   31, -536870912(536870911(fp))
    rotd   -32, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    rotd   31, -64(63(sp))
    rotd   -32, -64(63(sp))
    rotd   31, 63(-64(sp))
    rotd   -32, 63(-64(sp))
    #
    rotd   31, -8192(63(sp))
    rotd   -32, -64(8191(sp))
    rotd   31, 63(-8192(sp))
    rotd   -32, 8191(-64(sp))
    #
    rotd   31, -8192(8191(sp))
    rotd   -32, -8192(8191(sp))
    rotd   31, 8191(-8192(sp))
    rotd   -32, 8191(-8192(sp))
    #
    rotd   31, -536870912(8191(sp))
    rotd   -32, -8192(536870911(sp))
    rotd   31, 8191(-536870912(sp))
    rotd   -32, 536870911(-8192(sp))
    #
    rotd   31, 536870911(-536870912(sp))
    rotd   -32, 536870911(-536870912(sp))
    rotd   31, -536870912(536870911(sp))
    rotd   -32, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    rotd   31, -64(63(sb))
    rotd   -32, -64(63(sb))
    rotd   31, 63(-64(sb))
    rotd   -32, 63(-64(sb))
    #
    rotd   31, -8192(63(sb))
    rotd   -32, -64(8191(sb))
    rotd   31, 63(-8192(sb))
    rotd   -32, 8191(-64(sb))
    #
    rotd   31, -8192(8191(sb))
    rotd   -32, -8192(8191(sb))
    rotd   31, 8191(-8192(sb))
    rotd   -32, 8191(-8192(sb))
    #
    rotd   31, -536870912(8191(sb))
    rotd   -32, -8192(536870911(sb))
    rotd   31, 8191(-536870912(sb))
    rotd   -32, 536870911(-8192(sb))
    #
    rotd   31, 536870911(-536870912(sb))
    rotd   -32, 536870911(-536870912(sb))
    rotd   31, -536870912(536870911(sb))
    rotd   -32, -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    rotd	7, @-64
    rotd	-8, @-64
    rotd	7, @63
    rotd	-8, @63
    rotd	7, @-65
    rotd	-8, @-65
    rotd	7, @64
    rotd	-8, @64
    #
    rotd	7, @-8192
    rotd	-8, @-8192
    rotd	7, @8191
    rotd	-8, @8191
    rotd	7, @-8193
    rotd	-8, @-8193
    rotd	7, @8192
    rotd	-8, @8192
    #
    rotd	7, @-536870912
    rotd	-8, @536870911
#
# External
#
    rotd	7, ext(-64) + 63
    rotd	-8, ext(-64) + 63
    rotd	7, ext(63) + -64
    rotd	-8, ext(63) + -64
    #
    rotd	7, ext(-65) + 63
    rotd	-8, ext(-65) + 63
    rotd	7, ext(63) + -65
    rotd	-8, ext(63) + -65
    rotd	7, ext(-64) + 64
    rotd	-8, ext(-64) + 64
    rotd	7, ext(64) + -64
    rotd	-8, ext(64) + -64
    #
    rotd	7, ext(-8192) + 8191
    rotd	-8, ext(-8192) + 8191
    rotd	7, ext(8191) + -8192
    rotd	-8, ext(8191) + -8192
    #
    rotd	7, ext(-536870912) + 8191
    rotd	-8, ext(-536870912) + 8191
    rotd	7, ext(8191) + -536870912
    rotd	-8, ext(8191) + -536870912
    rotd	7, ext(-8192) + 536870911
    rotd	-8, ext(-8192) + 536870911
    rotd	7, ext(536870911) + -8192
    rotd	-8, ext(536870911) + -8192
    #
    rotd	7, ext(-536870912) + 536870911
    rotd	-8, ext(-536870912) + 536870911
    rotd	7, ext(536870911) + -536870912
    rotd	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    rotd	7, tos
    rotd	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    rotd	7, -64(fp)
    rotd	-8, -64(fp)
    rotd	7, 63(fp)
    rotd	-8, 63(fp)
    #
    rotd	7, -8192(fp)
    rotd	-8, -8192(fp)
    rotd	7, 8191(fp)
    rotd	-8, 8191(fp)
    #
    rotd	7, -536870912(fp)
    rotd	-8, -536870912(fp)
    rotd	7, 536870911(fp)
    rotd	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    rotd	7, -64(sp)
    rotd	-8, -64(sp)
    rotd	7, 63(sp)
    rotd	-8, 63(sp)
    #
    rotd	7, -8192(sp)
    rotd	-8, -8192(sp)
    rotd	7, 8191(sp)
    rotd	-8, 8191(sp)
    #
    rotd	7, -536870912(sp)
    rotd	-8, -536870912(sp)
    rotd	7, 536870911(sp)
    rotd	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    rotd	7, -64(sb)
    rotd	-8, -64(sb)
    rotd	7, 63(sb)
    rotd	-8, 63(sb)
    #
    rotd	7, -8192(sb)
    rotd	-8, -8192(sb)
    rotd	7, 8191(sb)
    rotd	-8, 8191(sb)
    #
    rotd	7, -536870912(sb)
    rotd	-8, -536870912(sb)
    rotd	7, 536870911(sb)
    rotd	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    rotd	7, -64(pc)
    rotd	-8, -64(pc)
    rotd	7, 63(pc)
    rotd	-8, 63(pc)
    #
    rotd	7, -8192(pc)
    rotd	-8, -8192(pc)
    rotd	7, 8191(pc)
    rotd	-8, 8191(pc)
    #
    rotd	7, -536870912(pc)
    rotd	-8, -536870912(pc)
    rotd	7, 536870911(pc)
    rotd	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    rotd   31, r7[r0:b]
    rotd   -32, r6[r1:b]
    #
    # Register Relative
    rotd   31, 63(r5)[r2:b]
    rotd   -32, 63(r4)[r3:b]
    rotd   31, 8191(r3)[r4:b]
    rotd   -32, 8191(r2)[r5:b]
    rotd   31, 536870911(r1)[r6:b]
    rotd   -32, 536870911(r0)[r7:b]
    #
    # Memory Relative
    rotd   31, -64(63(fp))[r0:b]
    rotd   -32, -64(63(fp))[r0:b]
    rotd   31, -8192(8191(fp))[r0:b]
    rotd   -32, -8192(8191(fp))[r0:b]
    rotd   31, -536870912(536870911(fp))[r0:b]
    rotd   -32, -536870912(536870911(fp))[r0:b]
    #
    # Absolute
    rotd   31, @63[r0:b]
    rotd   -32, @63[r0:b]
    rotd   31, @8191[r0:b]
    rotd   -32, @8191[r0:b]
    rotd   31, @536870911[r0:b]
    rotd   -32, @536870911[r0:b]
    #
    # External
    rotd   31, ext(-64) + 63[r0:b]
    rotd   -32, ext(-64) + 63[r0:b]
    rotd   31, ext(-8192) + 8191[r0:b]
    rotd   -32, ext(-8192) + 8191[r0:b]
    rotd   31, ext(-536870912) + 536870911[r0:b]
    rotd   -32, ext(-536870912) + 536870911[r0:b]
    #
    # Top Of Stack
    rotd   31, tos[r0:b]
    rotd   -32, tos[r0:b]
    #
    # Memory Space
    rotd   31, 63(fp)[r0:b]
    rotd   -32, 63(fp)[r0:b]
    rotd   31, 8191(fp)[r0:b]
    rotd   -32, 8191(fp)[r0:b]
    rotd   31, 536870911(fp)[r0:b]
    rotd   -32, 536870911(fp)[r0:b]
    rotd   31, 63(sp)[r0:b]
    rotd   -32, 63(sp)[r0:b]
    rotd   31, 8191(sp)[r0:b]
    rotd   -32, 8191(sp)[r0:b]
    rotd   31, 536870911(sp)[r0:b]
    rotd   -32, 536870911(sp)[r0:b]
    rotd   31, 63(sb)[r0:b]
    rotd   -32, 63(sb)[r0:b]
    rotd   31, 8191(sb)[r0:b]
    rotd   -32, 8191(sb)[r0:b]
    rotd   31, 536870911(sb)[r0:b]
    rotd   -32, 536870911(sb)[r0:b]
    rotd   31, 63(pc)[r0:b]
    rotd   -32, 63(pc)[r0:b]
    rotd   31, 8191(pc)[r0:b]
    rotd   -32, 8191(pc)[r0:b]
    rotd   31, 536870911(pc)[r0:b]
    rotd   -32, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    rotd   31, r1[r0:w]
    rotd   -32, r1[r0:w]
    #
    # Register Relative
    rotd   31, 63(r1)[r0:w]
    rotd   -32, 63(r1)[r0:w]
    rotd   31, 8191(r1)[r0:w]
    rotd   -32, 8191(r1)[r0:w]
    rotd   31, 536870911(r1)[r0:w]
    rotd   -32, 536870911(r1)[r0:w]
    #
    # Memory Relative
    rotd   31, -64(63(fp))[r0:w]
    rotd   -32, -64(63(fp))[r0:w]
    rotd   31, -8192(8191(fp))[r0:w]
    rotd   -32, -8192(8191(fp))[r0:w]
    rotd   31, -536870912(536870911(fp))[r0:w]
    rotd   -32, -536870912(536870911(fp))[r0:w]
    #
    # Absolute
    rotd   31, @63[r0:w]
    rotd   -32, @63[r0:w]
    rotd   31, @8191[r0:w]
    rotd   -32, @8191[r0:w]
    rotd   31, @536870911[r0:w]
    rotd   -32, @536870911[r0:w]
    #
    # External
    rotd   31, ext(-64) + 63[r0:w]
    rotd   -32, ext(-64) + 63[r0:w]
    rotd   31, ext(-8192) + 8191[r0:w]
    rotd   -32, ext(-8192) + 8191[r0:w]
    rotd   31, ext(-536870912) + 536870911[r0:w]
    rotd   -32, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    rotd   31, tos[r0:w]
    rotd   -32, tos[r0:w]
    #
    # Memory Space
    rotd   31, 63(fp)[r0:w]
    rotd   -32, 63(fp)[r0:w]
    rotd   31, 8191(fp)[r0:w]
    rotd   -32, 8191(fp)[r0:w]
    rotd   31, 536870911(fp)[r0:w]
    rotd   -32, 536870911(fp)[r0:w]
    rotd   31, 63(sp)[r0:w]
    rotd   -32, 63(sp)[r0:w]
    rotd   31, 8191(sp)[r0:w]
    rotd   -32, 8191(sp)[r0:w]
    rotd   31, 536870911(sp)[r0:w]
    rotd   -32, 536870911(sp)[r0:w]
    rotd   31, 63(sb)[r0:w]
    rotd   -32, 63(sb)[r0:w]
    rotd   31, 8191(sb)[r0:w]
    rotd   -32, 8191(sb)[r0:w]
    rotd   31, 536870911(sb)[r0:w]
    rotd   -32, 536870911(sb)[r0:w]
    rotd   31, 63(pc)[r0:w]
    rotd   -32, 63(pc)[r0:w]
    rotd   31, 8191(pc)[r0:w]
    rotd   -32, 8191(pc)[r0:w]
    rotd   31, 536870911(pc)[r0:w]
    rotd   -32, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    rotd   31, r1[r0:d]
    rotd   -32, r1[r0:d]
    #
    # Register Relative
    rotd   31, 63(r1)[r0:d]
    rotd   -32, 63(r1)[r0:d]
    rotd   31, 8191(r1)[r0:d]
    rotd   -32, 8191(r1)[r0:d]
    rotd   31, 536870911(r1)[r0:d]
    rotd   -32, 536870911(r1)[r0:d]
    #
    # Memory Relative
    rotd   31, -64(63(fp))[r0:d]
    rotd   -32, -64(63(fp))[r0:d]
    rotd   31, -8192(8191(fp))[r0:d]
    rotd   -32, -8192(8191(fp))[r0:d]
    rotd   31, -536870912(536870911(fp))[r0:d]
    rotd   -32, -536870912(536870911(fp))[r0:d]
    #
    # Absolute
    rotd   31, @63[r0:d]
    rotd   -32, @63[r0:d]
    rotd   31, @8191[r0:d]
    rotd   -32, @8191[r0:d]
    rotd   31, @536870911[r0:d]
    rotd   -32, @536870911[r0:d]
    #
    # External
    rotd   31, ext(-64) + 63[r0:d]
    rotd   -32, ext(-64) + 63[r0:d]
    rotd   31, ext(-8192) + 8191[r0:d]
    rotd   -32, ext(-8192) + 8191[r0:d]
    rotd   31, ext(-536870912) + 536870911[r0:d]
    rotd   -32, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    rotd   31, tos[r0:d]
    rotd   -32, tos[r0:d]
    #
    # Memory Space
    rotd   31, 63(fp)[r0:d]
    rotd   -32, 63(fp)[r0:d]
    rotd   31, 8191(fp)[r0:d]
    rotd   -32, 8191(fp)[r0:d]
    rotd   31, 536870911(fp)[r0:d]
    rotd   -32, 536870911(fp)[r0:d]
    rotd   31, 63(sp)[r0:d]
    rotd   -32, 63(sp)[r0:d]
    rotd   31, 8191(sp)[r0:d]
    rotd   -32, 8191(sp)[r0:d]
    rotd   31, 536870911(sp)[r0:d]
    rotd   -32, 536870911(sp)[r0:d]
    rotd   31, 63(sb)[r0:d]
    rotd   -32, 63(sb)[r0:d]
    rotd   31, 8191(sb)[r0:d]
    rotd   -32, 8191(sb)[r0:d]
    rotd   31, 536870911(sb)[r0:d]
    rotd   -32, 536870911(sb)[r0:d]
    rotd   31, 63(pc)[r0:d]
    rotd   -32, 63(pc)[r0:d]
    rotd   31, 8191(pc)[r0:d]
    rotd   -32, 8191(pc)[r0:d]
    rotd   31, 536870911(pc)[r0:d]
    rotd   -32, 536870911(pc)[r0:d]
#
end:
