.psize 0, 143   # suppress form feed, 143 columns
#
#
# Test case for FORMAT 2 instructions
#
# Instruction under test: CMPQi
#
# Syntax:   CMPQi   src     dest
#                   quick   gen
#                           read.i
#
#           !  dest   !  src  !     CMPQi   !
#           +---------+-------+---------+---+
#           !   gen   ! quick !0 0 1 1 1! i !
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# The CMPQi instruction compares the src1 and src2 operands and sets the Z, N, and
# L flags to indicate the comparison result. Before the comparison, src1 is
# sign-extended to the length of src2. Leading "0"s are supplied for a positive
# src operand value; leading "1"s for a negative value.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
# SPR quick BYTE 
#
#
# Register
#
    cmpqb   0, r0
    cmpqb   -8, r0
    cmpqb   1, r1
    cmpqb   -7, r1
    cmpqb   2, r2
    cmpqb   -6, r2
    cmpqb   3, r3
    cmpqb   -5, r3
    cmpqb   4, r4
    cmpqb   -4, r4
    cmpqb   5, r5
    cmpqb   -3, r5
    cmpqb   6, r6
    cmpqb   -2, r6
    cmpqb   7, r7
    cmpqb   -1, r7
#
# Register relative
#
    cmpqb   7, 63(r0)
    cmpqb   -8, -64(r0)
    cmpqb   7, 8191(r0)
    cmpqb   -8, -8192(r0)
    cmpqb   7, 536870911(r0)
    cmpqb   -8, -536870912(r0)
    #
    cmpqb   7, 63(r1)
    cmpqb   -8, -64(r1)
    cmpqb   7, 8191(r1)
    cmpqb   -8, -8192(r1)
    cmpqb   7, 536870911(r1)
    cmpqb   -8, -536870912(r1)
    #
    cmpqb   7, 63(r2)
    cmpqb   -8, -64(r2)
    cmpqb   7, 8191(r2)
    cmpqb   -8, -8192(r2)
    cmpqb   7, 536870911(r2)
    cmpqb   -8, -536870912(r2)
    #
    cmpqb   7, 63(r3)
    cmpqb   -8, -64(r3)
    cmpqb   7, 8191(r3)
    cmpqb   -8, -8192(r3)
    cmpqb   7, 536870911(r3)
    cmpqb   -8, -536870912(r3)
    #
    cmpqb   7, 63(r4)
    cmpqb   -8, -64(r4)
    cmpqb   7, 8191(r4)
    cmpqb   -8, -8192(r4)
    cmpqb   7, 536870911(r4)
    cmpqb   -8, -536870912(r4)
    #
    cmpqb   7, 63(r5)
    cmpqb   -8, -64(r5)
    cmpqb   7, 8191(r5)
    cmpqb   -8, -8192(r5)
    cmpqb   7, 536870911(r5)
    cmpqb   -8, -536870912(r5)
    #
    cmpqb   7, 63(r6)
    cmpqb   -8, -64(r6)
    cmpqb   7, 8191(r6)
    cmpqb   -8, -8192(r6)
    cmpqb   7, 536870911(r6)
    cmpqb   -8, -536870912(r6)
    #
    cmpqb   7, 63(r7)
    cmpqb   -8, -64(r7)
    cmpqb   7, 8191(r7)
    cmpqb   -8, -8192(r7)
    cmpqb   7, 536870911(r7)
    cmpqb   -8, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    cmpqb   7, -64(63(fp))
    cmpqb   -8, -64(63(fp))
    cmpqb   7, 63(-64(fp))
    cmpqb   -8, 63(-64(fp))
    #
    cmpqb   7, -8192(63(fp))
    cmpqb   -8, -64(8191(fp))
    cmpqb   7, 63(-8192(fp))
    cmpqb   -8, 8191(-64(fp))
    #
    cmpqb   7, -8192(8191(fp))
    cmpqb   -8, -8192(8191(fp))
    cmpqb   7, 8191(-8192(fp))
    cmpqb   -8, 8191(-8192(fp))
    #
    cmpqb   7, -536870912(8191(fp))
    cmpqb   -8, -8192(536870911(fp))
    cmpqb   7, 8191(-536870912(fp))
    cmpqb   -8, 536870911(-8192(fp))
    #
    cmpqb   7, 536870911(-536870912(fp))
    cmpqb   -8, 536870911(-536870912(fp))
    cmpqb   7, -536870912(536870911(fp))
    cmpqb   -8, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    cmpqb   7, -64(63(sp))
    cmpqb   -8, -64(63(sp))
    cmpqb   7, 63(-64(sp))
    cmpqb   -8, 63(-64(sp))
    #
    cmpqb   7, -8192(63(sp))
    cmpqb   -8, -64(8191(sp))
    cmpqb   7, 63(-8192(sp))
    cmpqb   -8, 8191(-64(sp))
    #
    cmpqb   7, -8192(8191(sp))
    cmpqb   -8, -8192(8191(sp))
    cmpqb   7, 8191(-8192(sp))
    cmpqb   -8, 8191(-8192(sp))
    #
    cmpqb   7, -536870912(8191(sp))
    cmpqb   -8, -8192(536870911(sp))
    cmpqb   7, 8191(-536870912(sp))
    cmpqb   -8, 536870911(-8192(sp))
    #
    cmpqb   7, 536870911(-536870912(sp))
    cmpqb   -8, 536870911(-536870912(sp))
    cmpqb   7, -536870912(536870911(sp))
    cmpqb   -8, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    cmpqb   7, -64(63(sb))
    cmpqb   -8, -64(63(sb))
    cmpqb   7, 63(-64(sb))
    cmpqb   -8, 63(-64(sb))
    #
    cmpqb   7, -8192(63(sb))
    cmpqb   -8, -64(8191(sb))
    cmpqb   7, 63(-8192(sb))
    cmpqb   -8, 8191(-64(sb))
    #
    cmpqb   7, -8192(8191(sb))
    cmpqb   -8, -8192(8191(sb))
    cmpqb   7, 8191(-8192(sb))
    cmpqb   -8, 8191(-8192(sb))
    #
    cmpqb   7, -536870912(8191(sb))
    cmpqb   -8, -8192(536870911(sb))
    cmpqb   7, 8191(-536870912(sb))
    cmpqb   -8, 536870911(-8192(sb))
    #
    cmpqb   7, 536870911(-536870912(sb))
    cmpqb   -8, 536870911(-536870912(sb))
    cmpqb   7, -536870912(536870911(sb))
    cmpqb   -8, -536870912(536870911(sb))

#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    cmpqb	7, @-64
    cmpqb	-8, @-64
    cmpqb	7, @63
    cmpqb	-8, @63
    cmpqb	7, @-65
    cmpqb	-8, @-65
    cmpqb	7, @64
    cmpqb	-8, @64
    #
    cmpqb	7, @-8192
    cmpqb	-8, @-8192
    cmpqb	7, @8191
    cmpqb	-8, @8191
    cmpqb	7, @-8193
    cmpqb	-8, @-8193
    cmpqb	7, @8192
    cmpqb	-8, @8192
    #
    cmpqb	7, @-536870912
    cmpqb	-8, @536870911
#
# External
#
    cmpqb	7, ext(-64) + 63
    cmpqb	-8, ext(-64) + 63
    cmpqb	7, ext(63) + -64
    cmpqb	-8, ext(63) + -64
    #
    cmpqb	7, ext(-65) + 63
    cmpqb	-8, ext(-65) + 63
    cmpqb	7, ext(63) + -65
    cmpqb	-8, ext(63) + -65
    cmpqb	7, ext(-64) + 64
    cmpqb	-8, ext(-64) + 64
    cmpqb	7, ext(64) + -64
    cmpqb	-8, ext(64) + -64
    #
    cmpqb	7, ext(-8192) + 8191
    cmpqb	-8, ext(-8192) + 8191
    cmpqb	7, ext(8191) + -8192
    cmpqb	-8, ext(8191) + -8192
    #
    cmpqb	7, ext(-536870912) + 8191
    cmpqb	-8, ext(-536870912) + 8191
    cmpqb	7, ext(8191) + -536870912
    cmpqb	-8, ext(8191) + -536870912
    cmpqb	7, ext(-8192) + 536870911
    cmpqb	-8, ext(-8192) + 536870911
    cmpqb	7, ext(536870911) + -8192
    cmpqb	-8, ext(536870911) + -8192
    #
    cmpqb	7, ext(-536870912) + 536870911
    cmpqb	-8, ext(-536870912) + 536870911
    cmpqb	7, ext(536870911) + -536870912
    cmpqb	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    cmpqb	7, tos
    cmpqb	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    cmpqb	7, -64(fp)
    cmpqb	-8, -64(fp)
    cmpqb	7, 63(fp)
    cmpqb	-8, 63(fp)
    #
    cmpqb	7, -8192(fp)
    cmpqb	-8, -8192(fp)
    cmpqb	7, 8191(fp)
    cmpqb	-8, 8191(fp)
    #
    cmpqb	7, -536870912(fp)
    cmpqb	-8, -536870912(fp)
    cmpqb	7, 536870911(fp)
    cmpqb	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    cmpqb	7, -64(sp)
    cmpqb	-8, -64(sp)
    cmpqb	7, 63(sp)
    cmpqb	-8, 63(sp)
    #
    cmpqb	7, -8192(sp)
    cmpqb	-8, -8192(sp)
    cmpqb	7, 8191(sp)
    cmpqb	-8, 8191(sp)
    #
    cmpqb	7, -536870912(sp)
    cmpqb	-8, -536870912(sp)
    cmpqb	7, 536870911(sp)
    cmpqb	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    cmpqb	7, -64(sb)
    cmpqb	-8, -64(sb)
    cmpqb	7, 63(sb)
    cmpqb	-8, 63(sb)
    #
    cmpqb	7, -8192(sb)
    cmpqb	-8, -8192(sb)
    cmpqb	7, 8191(sb)
    cmpqb	-8, 8191(sb)
    #
    cmpqb	7, -536870912(sb)
    cmpqb	-8, -536870912(sb)
    cmpqb	7, 536870911(sb)
    cmpqb	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    cmpqb	7, -64(pc)
    cmpqb	-8, -64(pc)
    cmpqb	7, 63(pc)
    cmpqb	-8, 63(pc)
    #
    cmpqb	7, -8192(pc)
    cmpqb	-8, -8192(pc)
    cmpqb	7, 8191(pc)
    cmpqb	-8, 8191(pc)
    #
    cmpqb	7, -536870912(pc)
    cmpqb	-8, -536870912(pc)
    cmpqb	7, 536870911(pc)
    cmpqb	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    cmpqb   7, r7[r0:b]
    cmpqb   -8, r6[r1:b]
    #
    # Register Relative
    cmpqb   7, 63(r5)[r2:b]
    cmpqb   -8, 63(r4)[r3:b]
    cmpqb   7, 8191(r3)[r4:b]
    cmpqb   -8, 8191(r2)[r5:b]
    cmpqb   7, 536870911(r1)[r6:b]
    cmpqb   -8, 536870911(r0)[r7:b]
    #
    # Memory Relative
    cmpqb   7, -64(63(fp))[r0:b]
    cmpqb   -8, -64(63(fp))[r1:b]
    cmpqb   7, -8192(8191(fp))[r2:b]
    cmpqb   -8, -8192(8191(fp))[r3:b]
    cmpqb   7, -536870912(536870911(fp))[r4:b]
    cmpqb   -8, -536870912(536870911(fp))[r5:b]
    cmpqb   7, -64(63(sp))[r7:b]
    cmpqb   -8, -64(63(sp))[r6:b]
    cmpqb   7, -8192(8191(sp))[r5:b]
    cmpqb   -8, -8192(8191(sp))[r4:b]
    cmpqb   7, -536870912(536870911(sp))[r3:b]
    cmpqb   -8, -536870912(536870911(sp))[r2:b]
    cmpqb   7, -64(63(sb))[r0:b]
    cmpqb   -8, -64(63(sb))[r1:b]
    cmpqb   7, -8192(8191(sb))[r2:b]
    cmpqb   -8, -8192(8191(sb))[r3:b]
    cmpqb   7, -536870912(536870911(sb))[r4:b]
    cmpqb   -8, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    cmpqb   7, @63[r0:b]
    cmpqb   -8, @63[r1:b]
    cmpqb   7, @8191[r2:b]
    cmpqb   -8, @8191[r3:b]
    cmpqb   7, @536870911[r4:b]
    cmpqb   -8, @536870911[r5:b]
    #
    # External
    cmpqb   7, ext(-64) + 63[r7:b]
    cmpqb   -8, ext(-64) + 63[r6:b]
    cmpqb   7, ext(-8192) + 8191[r5:b]
    cmpqb   -8, ext(-8192) + 8191[r4:b]
    cmpqb   7, ext(-536870912) + 536870911[r3:b]
    cmpqb   -8, ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    cmpqb   7, tos[r0:b]
    cmpqb   -8, tos[r7:b]
    #
    # Memory Space
    cmpqb   7, 63(fp)[r0:b]
    cmpqb   -8, 63(fp)[r0:b]
    cmpqb   7, 8191(fp)[r0:b]
    cmpqb   -8, 8191(fp)[r0:b]
    cmpqb   7, 536870911(fp)[r0:b]
    cmpqb   -8, 536870911(fp)[r0:b]
    cmpqb   7, 63(sp)[r0:b]
    cmpqb   -8, 63(sp)[r0:b]
    cmpqb   7, 8191(sp)[r0:b]
    cmpqb   -8, 8191(sp)[r0:b]
    cmpqb   7, 536870911(sp)[r0:b]
    cmpqb   -8, 536870911(sp)[r0:b]
    cmpqb   7, 63(sb)[r0:b]
    cmpqb   -8, 63(sb)[r0:b]
    cmpqb   7, 8191(sb)[r0:b]
    cmpqb   -8, 8191(sb)[r0:b]
    cmpqb   7, 536870911(sb)[r0:b]
    cmpqb   -8, 536870911(sb)[r0:b]
    cmpqb   7, 63(pc)[r0:b]
    cmpqb   -8, 63(pc)[r0:b]
    cmpqb   7, 8191(pc)[r0:b]
    cmpqb   -8, 8191(pc)[r0:b]
    cmpqb   7, 536870911(pc)[r0:b]
    cmpqb   -8, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    cmpqb   7, r1[r0:w]
    cmpqb   -8, r1[r0:w]
    #
    # Register Relative
    cmpqb   7, 63(r1)[r0:w]
    cmpqb   -8, 63(r1)[r0:w]
    cmpqb   7, 8191(r1)[r0:w]
    cmpqb   -8, 8191(r1)[r0:w]
    cmpqb   7, 536870911(r1)[r0:w]
    cmpqb   -8, 536870911(r1)[r0:w]
    #
    # Memory Relative
    cmpqb   7, -64(63(fp))[r0:w]
    cmpqb   -8, -64(63(fp))[r1:w]
    cmpqb   7, -8192(8191(fp))[r2:w]
    cmpqb   -8, -8192(8191(fp))[r3:w]
    cmpqb   7, -536870912(536870911(fp))[r4:w]
    cmpqb   -8, -536870912(536870911(fp))[r5:w]
    cmpqb   7, -64(63(sp))[r7:w]
    cmpqb   -8, -64(63(sp))[r6:w]
    cmpqb   7, -8192(8191(sp))[r5:w]
    cmpqb   -8, -8192(8191(sp))[r4:w]
    cmpqb   7, -536870912(536870911(sp))[r3:w]
    cmpqb   -8, -536870912(536870911(sp))[r2:w]
    cmpqb   7, -64(63(sb))[r0:w]
    cmpqb   -8, -64(63(sb))[r1:w]
    cmpqb   7, -8192(8191(sb))[r2:w]
    cmpqb   -8, -8192(8191(sb))[r3:w]
    cmpqb   7, -536870912(536870911(sb))[r4:w]
    cmpqb   -8, -536870912(536870911(sb))[r5:w]
    #
    # Absolute
    cmpqb   7, @63[r0:w]
    cmpqb   -8, @63[r0:w]
    cmpqb   7, @8191[r0:w]
    cmpqb   -8, @8191[r0:w]
    cmpqb   7, @536870911[r0:w]
    cmpqb   -8, @536870911[r0:w]
    #
    # External
    cmpqb   7, ext(-64) + 63[r0:w]
    cmpqb   -8, ext(-64) + 63[r0:w]
    cmpqb   7, ext(-8192) + 8191[r0:w]
    cmpqb   -8, ext(-8192) + 8191[r0:w]
    cmpqb   7, ext(-536870912) + 536870911[r0:w]
    cmpqb   -8, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    cmpqb   7, tos[r0:w]
    cmpqb   -8, tos[r0:w]
    #
    # Memory Space
    cmpqb   7, 63(fp)[r0:w]
    cmpqb   -8, 63(fp)[r0:w]
    cmpqb   7, 8191(fp)[r0:w]
    cmpqb   -8, 8191(fp)[r0:w]
    cmpqb   7, 536870911(fp)[r0:w]
    cmpqb   -8, 536870911(fp)[r0:w]
    cmpqb   7, 63(sp)[r0:w]
    cmpqb   -8, 63(sp)[r0:w]
    cmpqb   7, 8191(sp)[r0:w]
    cmpqb   -8, 8191(sp)[r0:w]
    cmpqb   7, 536870911(sp)[r0:w]
    cmpqb   -8, 536870911(sp)[r0:w]
    cmpqb   7, 63(sb)[r0:w]
    cmpqb   -8, 63(sb)[r0:w]
    cmpqb   7, 8191(sb)[r0:w]
    cmpqb   -8, 8191(sb)[r0:w]
    cmpqb   7, 536870911(sb)[r0:w]
    cmpqb   -8, 536870911(sb)[r0:w]
    cmpqb   7, 63(pc)[r0:w]
    cmpqb   -8, 63(pc)[r0:w]
    cmpqb   7, 8191(pc)[r0:w]
    cmpqb   -8, 8191(pc)[r0:w]
    cmpqb   7, 536870911(pc)[r0:w]
    cmpqb   -8, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    cmpqb   7, r1[r0:d]
    cmpqb   -8, r1[r0:d]
    #
    # Register Relative
    cmpqb   7, 63(r1)[r0:d]
    cmpqb   -8, 63(r1)[r0:d]
    cmpqb   7, 8191(r1)[r0:d]
    cmpqb   -8, 8191(r1)[r0:d]
    cmpqb   7, 536870911(r1)[r0:d]
    cmpqb   -8, 536870911(r1)[r0:d]
    #
    # Memory Relative
    cmpqb   7, -64(63(fp))[r0:d]
    cmpqb   -8, -64(63(fp))[r1:d]
    cmpqb   7, -8192(8191(fp))[r2:d]
    cmpqb   -8, -8192(8191(fp))[r3:d]
    cmpqb   7, -536870912(536870911(fp))[r4:d]
    cmpqb   -8, -536870912(536870911(fp))[r5:d]
    cmpqb   7, -64(63(sp))[r7:d]
    cmpqb   -8, -64(63(sp))[r6:d]
    cmpqb   7, -8192(8191(sp))[r5:d]
    cmpqb   -8, -8192(8191(sp))[r4:d]
    cmpqb   7, -536870912(536870911(sp))[r3:d]
    cmpqb   -8, -536870912(536870911(sp))[r2:d]
    cmpqb   7, -64(63(sb))[r0:d]
    cmpqb   -8, -64(63(sb))[r1:d]
    cmpqb   7, -8192(8191(sb))[r2:d]
    cmpqb   -8, -8192(8191(sb))[r3:d]
    cmpqb   7, -536870912(536870911(sb))[r4:d]
    cmpqb   -8, -536870912(536870911(sb))[r5:d]
    #
    # Absolute
    cmpqb   7, @63[r0:d]
    cmpqb   -8, @63[r0:d]
    cmpqb   7, @8191[r0:d]
    cmpqb   -8, @8191[r0:d]
    cmpqb   7, @536870911[r0:d]
    cmpqb   -8, @536870911[r0:d]
    #
    # External
    cmpqb   7, ext(-64) + 63[r0:d]
    cmpqb   -8, ext(-64) + 63[r0:d]
    cmpqb   7, ext(-8192) + 8191[r0:d]
    cmpqb   -8, ext(-8192) + 8191[r0:d]
    cmpqb   7, ext(-536870912) + 536870911[r0:d]
    cmpqb   -8, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    cmpqb   7, tos[r0:d]
    cmpqb   -8, tos[r0:d]
    #
    # Memory Space
    cmpqb   7, 63(fp)[r0:d]
    cmpqb   -8, 63(fp)[r0:d]
    cmpqb   7, 8191(fp)[r0:d]
    cmpqb   -8, 8191(fp)[r0:d]
    cmpqb   7, 536870911(fp)[r0:d]
    cmpqb   -8, 536870911(fp)[r0:d]
    cmpqb   7, 63(sp)[r0:d]
    cmpqb   -8, 63(sp)[r0:d]
    cmpqb   7, 8191(sp)[r0:d]
    cmpqb   -8, 8191(sp)[r0:d]
    cmpqb   7, 536870911(sp)[r0:d]
    cmpqb   -8, 536870911(sp)[r0:d]
    cmpqb   7, 63(sb)[r0:d]
    cmpqb   -8, 63(sb)[r0:d]
    cmpqb   7, 8191(sb)[r0:d]
    cmpqb   -8, 8191(sb)[r0:d]
    cmpqb   7, 536870911(sb)[r0:d]
    cmpqb   -8, 536870911(sb)[r0:d]
    cmpqb   7, 63(pc)[r0:d]
    cmpqb   -8, 63(pc)[r0:d]
    cmpqb   7, 8191(pc)[r0:d]
    cmpqb   -8, 8191(pc)[r0:d]
    cmpqb   7, 536870911(pc)[r0:d]
    cmpqb   -8, 536870911(pc)[r0:d]
#
# SPR quick WORD
#
#
# Register
#
    cmpqw   7, r0
    cmpqw   -8, r0
    cmpqw   7, r1
    cmpqw   -8, r1
    cmpqw   7, r2
    cmpqw   -8, r2
    cmpqw   7, r3
    cmpqw   -8, r3
    cmpqw   7, r4
    cmpqw   -8, r4
    cmpqw   7, r5
    cmpqw   -8, r5
    cmpqw   7, r6
    cmpqw   -8, r6
    cmpqw   7, r7
    cmpqw   -8, r7
#
# Register relative
#
    cmpqw   7, 63(r0)
    cmpqw   -8, -64(r0)
    cmpqw   7, 8191(r0)
    cmpqw   -8, -8192(r0)
    cmpqw   7, 536870911(r0)
    cmpqw   -8, -536870912(r0)
    #
    cmpqw   7, 63(r1)
    cmpqw   -8, -64(r1)
    cmpqw   7, 8191(r1)
    cmpqw   -8, -8192(r1)
    cmpqw   7, 536870911(r1)
    cmpqw   -8, -536870912(r1)
    #
    cmpqw   7, 63(r2)
    cmpqw   -8, -64(r2)
    cmpqw   7, 8191(r2)
    cmpqw   -8, -8192(r2)
    cmpqw   7, 536870911(r2)
    cmpqw   -8, -536870912(r2)
    #
    cmpqw   7, 63(r3)
    cmpqw   -8, -64(r3)
    cmpqw   7, 8191(r3)
    cmpqw   -8, -8192(r3)
    cmpqw   7, 536870911(r3)
    cmpqw   -8, -536870912(r3)
    #
    cmpqw   7, 63(r4)
    cmpqw   -8, -64(r4)
    cmpqw   7, 8191(r4)
    cmpqw   -8, -8192(r4)
    cmpqw   7, 536870911(r4)
    cmpqw   -8, -536870912(r4)
    #
    cmpqw   7, 63(r5)
    cmpqw   -8, -64(r5)
    cmpqw   7, 8191(r5)
    cmpqw   -8, -8192(r5)
    cmpqw   7, 536870911(r5)
    cmpqw   -8, -536870912(r5)
    #
    cmpqw   7, 63(r6)
    cmpqw   -8, -64(r6)
    cmpqw   7, 8191(r6)
    cmpqw   -8, -8192(r6)
    cmpqw   7, 536870911(r6)
    cmpqw   -8, -536870912(r6)
    #
    cmpqw   7, 63(r7)
    cmpqw   -8, -64(r7)
    cmpqw   7, 8191(r7)
    cmpqw   -8, -8192(r7)
    cmpqw   7, 536870911(r7)
    cmpqw   -8, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    cmpqw   7, -64(63(fp))
    cmpqw   -8, -64(63(fp))
    cmpqw   7, 63(-64(fp))
    cmpqw   -8, 63(-64(fp))
    #
    cmpqw   7, -8192(63(fp))
    cmpqw   -8, -64(8191(fp))
    cmpqw   7, 63(-8192(fp))
    cmpqw   -8, 8191(-64(fp))
    #
    cmpqw   7, -8192(8191(fp))
    cmpqw   -8, -8192(8191(fp))
    cmpqw   7, 8191(-8192(fp))
    cmpqw   -8, 8191(-8192(fp))
    #
    cmpqw   7, -536870912(8191(fp))
    cmpqw   -8, -8192(536870911(fp))
    cmpqw   7, 8191(-536870912(fp))
    cmpqw   -8, 536870911(-8192(fp))
    #
    cmpqw   7, 536870911(-536870912(fp))
    cmpqw   -8, 536870911(-536870912(fp))
    cmpqw   7, -536870912(536870911(fp))
    cmpqw   -8, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    cmpqw   7, -64(63(sp))
    cmpqw   -8, -64(63(sp))
    cmpqw   7, 63(-64(sp))
    cmpqw   -8, 63(-64(sp))
    #
    cmpqw   7, -8192(63(sp))
    cmpqw   -8, -64(8191(sp))
    cmpqw   7, 63(-8192(sp))
    cmpqw   -8, 8191(-64(sp))
    #
    cmpqw   7, -8192(8191(sp))
    cmpqw   -8, -8192(8191(sp))
    cmpqw   7, 8191(-8192(sp))
    cmpqw   -8, 8191(-8192(sp))
    #
    cmpqw   7, -536870912(8191(sp))
    cmpqw   -8, -8192(536870911(sp))
    cmpqw   7, 8191(-536870912(sp))
    cmpqw   -8, 536870911(-8192(sp))
    #
    cmpqw   7, 536870911(-536870912(sp))
    cmpqw   -8, 536870911(-536870912(sp))
    cmpqw   7, -536870912(536870911(sp))
    cmpqw   -8, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    cmpqw   7, -64(63(sb))
    cmpqw   -8, -64(63(sb))
    cmpqw   7, 63(-64(sb))
    cmpqw   -8, 63(-64(sb))
    #
    cmpqw   7, -8192(63(sb))
    cmpqw   -8, -64(8191(sb))
    cmpqw   7, 63(-8192(sb))
    cmpqw   -8, 8191(-64(sb))
    #
    cmpqw   7, -8192(8191(sb))
    cmpqw   -8, -8192(8191(sb))
    cmpqw   7, 8191(-8192(sb))
    cmpqw   -8, 8191(-8192(sb))
    #
    cmpqw   7, -536870912(8191(sb))
    cmpqw   -8, -8192(536870911(sb))
    cmpqw   7, 8191(-536870912(sb))
    cmpqw   -8, 536870911(-8192(sb))
    #
    cmpqw   7, 536870911(-536870912(sb))
    cmpqw   -8, 536870911(-536870912(sb))
    cmpqw   7, -536870912(536870911(sb))
    cmpqw   -8, -536870912(536870911(sb))

#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    cmpqw	7, @-64
    cmpqw	-8, @-64
    cmpqw	7, @63
    cmpqw	-8, @63
    cmpqw	7, @-65
    cmpqw	-8, @-65
    cmpqw	7, @64
    cmpqw	-8, @64
    #
    cmpqw	7, @-8192
    cmpqw	-8, @-8192
    cmpqw	7, @8191
    cmpqw	-8, @8191
    cmpqw	7, @-8193
    cmpqw	-8, @-8193
    cmpqw	7, @8192
    cmpqw	-8, @8192
    #
    cmpqw	7, @-536870912
    cmpqw	-8, @536870911
#
# External
#
    cmpqw	7, ext(-64) + 63
    cmpqw	-8, ext(-64) + 63
    cmpqw	7, ext(63) + -64
    cmpqw	-8, ext(63) + -64
    #
    cmpqw	7, ext(-65) + 63
    cmpqw	-8, ext(-65) + 63
    cmpqw	7, ext(63) + -65
    cmpqw	-8, ext(63) + -65
    cmpqw	7, ext(-64) + 64
    cmpqw	-8, ext(-64) + 64
    cmpqw	7, ext(64) + -64
    cmpqw	-8, ext(64) + -64
    #
    cmpqw	7, ext(-8192) + 8191
    cmpqw	-8, ext(-8192) + 8191
    cmpqw	7, ext(8191) + -8192
    cmpqw	-8, ext(8191) + -8192
    #
    cmpqw	7, ext(-536870912) + 8191
    cmpqw	-8, ext(-536870912) + 8191
    cmpqw	7, ext(8191) + -536870912
    cmpqw	-8, ext(8191) + -536870912
    cmpqw	7, ext(-8192) + 536870911
    cmpqw	-8, ext(-8192) + 536870911
    cmpqw	7, ext(536870911) + -8192
    cmpqw	-8, ext(536870911) + -8192
    #
    cmpqw	7, ext(-536870912) + 536870911
    cmpqw	-8, ext(-536870912) + 536870911
    cmpqw	7, ext(536870911) + -536870912
    cmpqw	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    cmpqw	7, tos
    cmpqw	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    cmpqw	7, -64(fp)
    cmpqw	-8, -64(fp)
    cmpqw	7, 63(fp)
    cmpqw	-8, 63(fp)
    #
    cmpqw	7, -8192(fp)
    cmpqw	-8, -8192(fp)
    cmpqw	7, 8191(fp)
    cmpqw	-8, 8191(fp)
    #
    cmpqw	7, -536870912(fp)
    cmpqw	-8, -536870912(fp)
    cmpqw	7, 536870911(fp)
    cmpqw	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    cmpqw	7, -64(sp)
    cmpqw	-8, -64(sp)
    cmpqw	7, 63(sp)
    cmpqw	-8, 63(sp)
    #
    cmpqw	7, -8192(sp)
    cmpqw	-8, -8192(sp)
    cmpqw	7, 8191(sp)
    cmpqw	-8, 8191(sp)
    #
    cmpqw	7, -536870912(sp)
    cmpqw	-8, -536870912(sp)
    cmpqw	7, 536870911(sp)
    cmpqw	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    cmpqw	7, -64(sb)
    cmpqw	-8, -64(sb)
    cmpqw	7, 63(sb)
    cmpqw	-8, 63(sb)
    #
    cmpqw	7, -8192(sb)
    cmpqw	-8, -8192(sb)
    cmpqw	7, 8191(sb)
    cmpqw	-8, 8191(sb)
    #
    cmpqw	7, -536870912(sb)
    cmpqw	-8, -536870912(sb)
    cmpqw	7, 536870911(sb)
    cmpqw	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    cmpqw	7, -64(pc)
    cmpqw	-8, -64(pc)
    cmpqw	7, 63(pc)
    cmpqw	-8, 63(pc)
    #
    cmpqw	7, -8192(pc)
    cmpqw	-8, -8192(pc)
    cmpqw	7, 8191(pc)
    cmpqw	-8, 8191(pc)
    #
    cmpqw	7, -536870912(pc)
    cmpqw	-8, -536870912(pc)
    cmpqw	7, 536870911(pc)
    cmpqw	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    cmpqw   7, r7[r0:b]
    cmpqw   -8, r6[r1:b]
    #
    # Register Relative
    cmpqw   7, 63(r5)[r2:b]
    cmpqw   -8, 63(r4)[r3:b]
    cmpqw   7, 8191(r3)[r4:b]
    cmpqw   -8, 8191(r2)[r5:b]
    cmpqw   7, 536870911(r1)[r6:b]
    cmpqw   -8, 536870911(r0)[r7:b]
    #
    # Memory Relative
    cmpqw   7, -64(63(fp))[r0:b]
    cmpqw   -8, -64(63(fp))[r1:b]
    cmpqw   7, -8192(8191(fp))[r2:b]
    cmpqw   -8, -8192(8191(fp))[r3:b]
    cmpqw   7, -536870912(536870911(fp))[r4:b]
    cmpqw   -8, -536870912(536870911(fp))[r5:b]
    cmpqw   7, -64(63(sp))[r7:b]
    cmpqw   -8, -64(63(sp))[r6:b]
    cmpqw   7, -8192(8191(sp))[r5:b]
    cmpqw   -8, -8192(8191(sp))[r4:b]
    cmpqw   7, -536870912(536870911(sp))[r3:b]
    cmpqw   -8, -536870912(536870911(sp))[r2:b]
    cmpqw   7, -64(63(sb))[r0:b]
    cmpqw   -8, -64(63(sb))[r1:b]
    cmpqw   7, -8192(8191(sb))[r2:b]
    cmpqw   -8, -8192(8191(sb))[r3:b]
    cmpqw   7, -536870912(536870911(sb))[r4:b]
    cmpqw   -8, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    cmpqw   7, @63[r0:b]
    cmpqw   -8, @63[r0:b]
    cmpqw   7, @8191[r0:b]
    cmpqw   -8, @8191[r0:b]
    cmpqw   7, @536870911[r0:b]
    cmpqw   -8, @536870911[r0:b]
    #
    # External
    cmpqw   7, ext(-64) + 63[r0:b]
    cmpqw   -8, ext(-64) + 63[r0:b]
    cmpqw   7, ext(-8192) + 8191[r0:b]
    cmpqw   -8, ext(-8192) + 8191[r0:b]
    cmpqw   7, ext(-536870912) + 536870911[r0:b]
    cmpqw   -8, ext(-536870912) + 536870911[r0:b]
    #
    # Top Of Stack
    cmpqw   7, tos[r0:b]
    cmpqw   -8, tos[r0:b]
    #
    # Memory Space
    cmpqw   7, 63(fp)[r0:b]
    cmpqw   -8, 63(fp)[r0:b]
    cmpqw   7, 8191(fp)[r0:b]
    cmpqw   -8, 8191(fp)[r0:b]
    cmpqw   7, 536870911(fp)[r0:b]
    cmpqw   -8, 536870911(fp)[r0:b]
    cmpqw   7, 63(sp)[r0:b]
    cmpqw   -8, 63(sp)[r0:b]
    cmpqw   7, 8191(sp)[r0:b]
    cmpqw   -8, 8191(sp)[r0:b]
    cmpqw   7, 536870911(sp)[r0:b]
    cmpqw   -8, 536870911(sp)[r0:b]
    cmpqw   7, 63(sb)[r0:b]
    cmpqw   -8, 63(sb)[r0:b]
    cmpqw   7, 8191(sb)[r0:b]
    cmpqw   -8, 8191(sb)[r0:b]
    cmpqw   7, 536870911(sb)[r0:b]
    cmpqw   -8, 536870911(sb)[r0:b]
    cmpqw   7, 63(pc)[r0:b]
    cmpqw   -8, 63(pc)[r0:b]
    cmpqw   7, 8191(pc)[r0:b]
    cmpqw   -8, 8191(pc)[r0:b]
    cmpqw   7, 536870911(pc)[r0:b]
    cmpqw   -8, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    cmpqw   7, r1[r0:w]
    cmpqw   -8, r1[r0:w]
    #
    # Register Relative
    cmpqw   7, 63(r1)[r0:w]
    cmpqw   -8, 63(r1)[r0:w]
    cmpqw   7, 8191(r1)[r0:w]
    cmpqw   -8, 8191(r1)[r0:w]
    cmpqw   7, 536870911(r1)[r0:w]
    cmpqw   -8, 536870911(r1)[r0:w]
    #
    # Memory Relative
    cmpqw   7, -64(63(fp))[r0:w]
    cmpqw   -8, -64(63(fp))[r0:w]
    cmpqw   7, -8192(8191(fp))[r0:w]
    cmpqw   -8, -8192(8191(fp))[r0:w]
    cmpqw   7, -536870912(536870911(fp))[r0:w]
    cmpqw   -8, -536870912(536870911(fp))[r0:w]
    #
    # Absolute
    cmpqw   7, @63[r0:w]
    cmpqw   -8, @63[r0:w]
    cmpqw   7, @8191[r0:w]
    cmpqw   -8, @8191[r0:w]
    cmpqw   7, @536870911[r0:w]
    cmpqw   -8, @536870911[r0:w]
    #
    # External
    cmpqw   7, ext(-64) + 63[r0:w]
    cmpqw   -8, ext(-64) + 63[r0:w]
    cmpqw   7, ext(-8192) + 8191[r0:w]
    cmpqw   -8, ext(-8192) + 8191[r0:w]
    cmpqw   7, ext(-536870912) + 536870911[r0:w]
    cmpqw   -8, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    cmpqw   7, tos[r0:w]
    cmpqw   -8, tos[r0:w]
    #
    # Memory Space
    cmpqw   7, 63(fp)[r0:w]
    cmpqw   -8, 63(fp)[r0:w]
    cmpqw   7, 8191(fp)[r0:w]
    cmpqw   -8, 8191(fp)[r0:w]
    cmpqw   7, 536870911(fp)[r0:w]
    cmpqw   -8, 536870911(fp)[r0:w]
    cmpqw   7, 63(sp)[r0:w]
    cmpqw   -8, 63(sp)[r0:w]
    cmpqw   7, 8191(sp)[r0:w]
    cmpqw   -8, 8191(sp)[r0:w]
    cmpqw   7, 536870911(sp)[r0:w]
    cmpqw   -8, 536870911(sp)[r0:w]
    cmpqw   7, 63(sb)[r0:w]
    cmpqw   -8, 63(sb)[r0:w]
    cmpqw   7, 8191(sb)[r0:w]
    cmpqw   -8, 8191(sb)[r0:w]
    cmpqw   7, 536870911(sb)[r0:w]
    cmpqw   -8, 536870911(sb)[r0:w]
    cmpqw   7, 63(pc)[r0:w]
    cmpqw   -8, 63(pc)[r0:w]
    cmpqw   7, 8191(pc)[r0:w]
    cmpqw   -8, 8191(pc)[r0:w]
    cmpqw   7, 536870911(pc)[r0:w]
    cmpqw   -8, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    cmpqw   7, r1[r0:d]
    cmpqw   -8, r1[r0:d]
    #
    # Register Relative
    cmpqw   7, 63(r1)[r0:d]
    cmpqw   -8, 63(r1)[r0:d]
    cmpqw   7, 8191(r1)[r0:d]
    cmpqw   -8, 8191(r1)[r0:d]
    cmpqw   7, 536870911(r1)[r0:d]
    cmpqw   -8, 536870911(r1)[r0:d]
    #
    # Memory Relative
    cmpqw   7, -64(63(fp))[r0:d]
    cmpqw   -8, -64(63(fp))[r0:d]
    cmpqw   7, -8192(8191(fp))[r0:d]
    cmpqw   -8, -8192(8191(fp))[r0:d]
    cmpqw   7, -536870912(536870911(fp))[r0:d]
    cmpqw   -8, -536870912(536870911(fp))[r0:d]
    #
    # Absolute
    cmpqw   7, @63[r0:d]
    cmpqw   -8, @63[r0:d]
    cmpqw   7, @8191[r0:d]
    cmpqw   -8, @8191[r0:d]
    cmpqw   7, @536870911[r0:d]
    cmpqw   -8, @536870911[r0:d]
    #
    # External
    cmpqw   7, ext(-64) + 63[r0:d]
    cmpqw   -8, ext(-64) + 63[r0:d]
    cmpqw   7, ext(-8192) + 8191[r0:d]
    cmpqw   -8, ext(-8192) + 8191[r0:d]
    cmpqw   7, ext(-536870912) + 536870911[r0:d]
    cmpqw   -8, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    cmpqw   7, tos[r0:d]
    cmpqw   -8, tos[r0:d]
    #
    # Memory Space
    cmpqw   7, 63(fp)[r0:d]
    cmpqw   -8, 63(fp)[r0:d]
    cmpqw   7, 8191(fp)[r0:d]
    cmpqw   -8, 8191(fp)[r0:d]
    cmpqw   7, 536870911(fp)[r0:d]
    cmpqw   -8, 536870911(fp)[r0:d]
    cmpqw   7, 63(sp)[r0:d]
    cmpqw   -8, 63(sp)[r0:d]
    cmpqw   7, 8191(sp)[r0:d]
    cmpqw   -8, 8191(sp)[r0:d]
    cmpqw   7, 536870911(sp)[r0:d]
    cmpqw   -8, 536870911(sp)[r0:d]
    cmpqw   7, 63(sb)[r0:d]
    cmpqw   -8, 63(sb)[r0:d]
    cmpqw   7, 8191(sb)[r0:d]
    cmpqw   -8, 8191(sb)[r0:d]
    cmpqw   7, 536870911(sb)[r0:d]
    cmpqw   -8, 536870911(sb)[r0:d]
    cmpqw   7, 63(pc)[r0:d]
    cmpqw   -8, 63(pc)[r0:d]
    cmpqw   7, 8191(pc)[r0:d]
    cmpqw   -8, 8191(pc)[r0:d]
    cmpqw   7, 536870911(pc)[r0:d]
    cmpqw   -8, 536870911(pc)[r0:d]

#
# SPR quick DOUBLE WORD
#
#
# Register
#
    cmpqd   7, r0
    cmpqd   -8, r0
    cmpqd   7, r1
    cmpqd   -8, r1
    cmpqd   7, r2
    cmpqd   -8, r2
    cmpqd   7, r3
    cmpqd   -8, r3
    cmpqd   7, r4
    cmpqd   -8, r4
    cmpqd   7, r5
    cmpqd   -8, r5
    cmpqd   7, r6
    cmpqd   -8, r6
    cmpqd   7, r7
    cmpqd   -8, r7
#
# Register relative
#
    cmpqd   7, 63(r0)
    cmpqd   -8, -64(r0)
    cmpqd   7, 8191(r0)
    cmpqd   -8, -8192(r0)
    cmpqd   7, 536870911(r0)
    cmpqd   -8, -536870912(r0)
    #
    cmpqd   7, 63(r1)
    cmpqd   -8, -64(r1)
    cmpqd   7, 8191(r1)
    cmpqd   -8, -8192(r1)
    cmpqd   7, 536870911(r1)
    cmpqd   -8, -536870912(r1)
    #
    cmpqd   7, 63(r2)
    cmpqd   -8, -64(r2)
    cmpqd   7, 8191(r2)
    cmpqd   -8, -8192(r2)
    cmpqd   7, 536870911(r2)
    cmpqd   -8, -536870912(r2)
    #
    cmpqd   7, 63(r3)
    cmpqd   -8, -64(r3)
    cmpqd   7, 8191(r3)
    cmpqd   -8, -8192(r3)
    cmpqd   7, 536870911(r3)
    cmpqd   -8, -536870912(r3)
    #
    cmpqd   7, 63(r4)
    cmpqd   -8, -64(r4)
    cmpqd   7, 8191(r4)
    cmpqd   -8, -8192(r4)
    cmpqd   7, 536870911(r4)
    cmpqd   -8, -536870912(r4)
    #
    cmpqd   7, 63(r5)
    cmpqd   -8, -64(r5)
    cmpqd   7, 8191(r5)
    cmpqd   -8, -8192(r5)
    cmpqd   7, 536870911(r5)
    cmpqd   -8, -536870912(r5)
    #
    cmpqd   7, 63(r6)
    cmpqd   -8, -64(r6)
    cmpqd   7, 8191(r6)
    cmpqd   -8, -8192(r6)
    cmpqd   7, 536870911(r6)
    cmpqd   -8, -536870912(r6)
    #
    cmpqd   7, 63(r7)
    cmpqd   -8, -64(r7)
    cmpqd   7, 8191(r7)
    cmpqd   -8, -8192(r7)
    cmpqd   7, 536870911(r7)
    cmpqd   -8, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    cmpqd   7, -64(63(fp))
    cmpqd   -8, -64(63(fp))
    cmpqd   7, 63(-64(fp))
    cmpqd   -8, 63(-64(fp))
    #
    cmpqd   7, -8192(63(fp))
    cmpqd   -8, -64(8191(fp))
    cmpqd   7, 63(-8192(fp))
    cmpqd   -8, 8191(-64(fp))
    #
    cmpqd   7, -8192(8191(fp))
    cmpqd   -8, -8192(8191(fp))
    cmpqd   7, 8191(-8192(fp))
    cmpqd   -8, 8191(-8192(fp))
    #
    cmpqd   7, -536870912(8191(fp))
    cmpqd   -8, -8192(536870911(fp))
    cmpqd   7, 8191(-536870912(fp))
    cmpqd   -8, 536870911(-8192(fp))
    #
    cmpqd   7, 536870911(-536870912(fp))
    cmpqd   -8, 536870911(-536870912(fp))
    cmpqd   7, -536870912(536870911(fp))
    cmpqd   -8, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    cmpqd   7, -64(63(sp))
    cmpqd   -8, -64(63(sp))
    cmpqd   7, 63(-64(sp))
    cmpqd   -8, 63(-64(sp))
    #
    cmpqd   7, -8192(63(sp))
    cmpqd   -8, -64(8191(sp))
    cmpqd   7, 63(-8192(sp))
    cmpqd   -8, 8191(-64(sp))
    #
    cmpqd   7, -8192(8191(sp))
    cmpqd   -8, -8192(8191(sp))
    cmpqd   7, 8191(-8192(sp))
    cmpqd   -8, 8191(-8192(sp))
    #
    cmpqd   7, -536870912(8191(sp))
    cmpqd   -8, -8192(536870911(sp))
    cmpqd   7, 8191(-536870912(sp))
    cmpqd   -8, 536870911(-8192(sp))
    #
    cmpqd   7, 536870911(-536870912(sp))
    cmpqd   -8, 536870911(-536870912(sp))
    cmpqd   7, -536870912(536870911(sp))
    cmpqd   -8, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    cmpqd   7, -64(63(sb))
    cmpqd   -8, -64(63(sb))
    cmpqd   7, 63(-64(sb))
    cmpqd   -8, 63(-64(sb))
    #
    cmpqd   7, -8192(63(sb))
    cmpqd   -8, -64(8191(sb))
    cmpqd   7, 63(-8192(sb))
    cmpqd   -8, 8191(-64(sb))
    #
    cmpqd   7, -8192(8191(sb))
    cmpqd   -8, -8192(8191(sb))
    cmpqd   7, 8191(-8192(sb))
    cmpqd   -8, 8191(-8192(sb))
    #
    cmpqd   7, -536870912(8191(sb))
    cmpqd   -8, -8192(536870911(sb))
    cmpqd   7, 8191(-536870912(sb))
    cmpqd   -8, 536870911(-8192(sb))
    #
    cmpqd   7, 536870911(-536870912(sb))
    cmpqd   -8, 536870911(-536870912(sb))
    cmpqd   7, -536870912(536870911(sb))
    cmpqd   -8, -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    cmpqd	7, @-64
    cmpqd	-8, @-64
    cmpqd	7, @63
    cmpqd	-8, @63
    cmpqd	7, @-65
    cmpqd	-8, @-65
    cmpqd	7, @64
    cmpqd	-8, @64
    #
    cmpqd	7, @-8192
    cmpqd	-8, @-8192
    cmpqd	7, @8191
    cmpqd	-8, @8191
    cmpqd	7, @-8193
    cmpqd	-8, @-8193
    cmpqd	7, @8192
    cmpqd	-8, @8192
    #
    cmpqd	7, @-536870912
    cmpqd	-8, @536870911
#
# External
#
    cmpqd	7, ext(-64) + 63
    cmpqd	-8, ext(-64) + 63
    cmpqd	7, ext(63) + -64
    cmpqd	-8, ext(63) + -64
    #
    cmpqd	7, ext(-65) + 63
    cmpqd	-8, ext(-65) + 63
    cmpqd	7, ext(63) + -65
    cmpqd	-8, ext(63) + -65
    cmpqd	7, ext(-64) + 64
    cmpqd	-8, ext(-64) + 64
    cmpqd	7, ext(64) + -64
    cmpqd	-8, ext(64) + -64
    #
    cmpqd	7, ext(-8192) + 8191
    cmpqd	-8, ext(-8192) + 8191
    cmpqd	7, ext(8191) + -8192
    cmpqd	-8, ext(8191) + -8192
    #
    cmpqd	7, ext(-536870912) + 8191
    cmpqd	-8, ext(-536870912) + 8191
    cmpqd	7, ext(8191) + -536870912
    cmpqd	-8, ext(8191) + -536870912
    cmpqd	7, ext(-8192) + 536870911
    cmpqd	-8, ext(-8192) + 536870911
    cmpqd	7, ext(536870911) + -8192
    cmpqd	-8, ext(536870911) + -8192
    #
    cmpqd	7, ext(-536870912) + 536870911
    cmpqd	-8, ext(-536870912) + 536870911
    cmpqd	7, ext(536870911) + -536870912
    cmpqd	-8, ext(536870911) + -536870912
#
# Top Of Stack
#
    cmpqd	7, tos
    cmpqd	-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    cmpqd	7, -64(fp)
    cmpqd	-8, -64(fp)
    cmpqd	7, 63(fp)
    cmpqd	-8, 63(fp)
    #
    cmpqd	7, -8192(fp)
    cmpqd	-8, -8192(fp)
    cmpqd	7, 8191(fp)
    cmpqd	-8, 8191(fp)
    #
    cmpqd	7, -536870912(fp)
    cmpqd	-8, -536870912(fp)
    cmpqd	7, 536870911(fp)
    cmpqd	-8, 536870911(fp)
    #
    # Stack Pointer (sp)
    #
    cmpqd	7, -64(sp)
    cmpqd	-8, -64(sp)
    cmpqd	7, 63(sp)
    cmpqd	-8, 63(sp)
    #
    cmpqd	7, -8192(sp)
    cmpqd	-8, -8192(sp)
    cmpqd	7, 8191(sp)
    cmpqd	-8, 8191(sp)
    #
    cmpqd	7, -536870912(sp)
    cmpqd	-8, -536870912(sp)
    cmpqd	7, 536870911(sp)
    cmpqd	-8, 536870911(sp)
    #
    # Static Memory (sb)
    #
    cmpqd	7, -64(sb)
    cmpqd	-8, -64(sb)
    cmpqd	7, 63(sb)
    cmpqd	-8, 63(sb)
    #
    cmpqd	7, -8192(sb)
    cmpqd	-8, -8192(sb)
    cmpqd	7, 8191(sb)
    cmpqd	-8, 8191(sb)
    #
    cmpqd	7, -536870912(sb)
    cmpqd	-8, -536870912(sb)
    cmpqd	7, 536870911(sb)
    cmpqd	-8, 536870911(sb)
    #
    # Program Counter (pc)
    #
    cmpqd	7, -64(pc)
    cmpqd	-8, -64(pc)
    cmpqd	7, 63(pc)
    cmpqd	-8, 63(pc)
    #
    cmpqd	7, -8192(pc)
    cmpqd	-8, -8192(pc)
    cmpqd	7, 8191(pc)
    cmpqd	-8, 8191(pc)
    #
    cmpqd	7, -536870912(pc)
    cmpqd	-8, -536870912(pc)
    cmpqd	7, 536870911(pc)
    cmpqd	-8, 536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    cmpqd   7, r7[r0:b]
    cmpqd   -8, r6[r1:b]
    #
    # Register Relative
    cmpqd   7, 63(r5)[r2:b]
    cmpqd   -8, 63(r4)[r3:b]
    cmpqd   7, 8191(r3)[r4:b]
    cmpqd   -8, 8191(r2)[r5:b]
    cmpqd   7, 536870911(r1)[r6:b]
    cmpqd   -8, 536870911(r0)[r7:b]
    #
    # Memory Relative
    cmpqd   7, -64(63(fp))[r0:b]
    cmpqd   -8, -64(63(fp))[r0:b]
    cmpqd   7, -8192(8191(fp))[r0:b]
    cmpqd   -8, -8192(8191(fp))[r0:b]
    cmpqd   7, -536870912(536870911(fp))[r0:b]
    cmpqd   -8, -536870912(536870911(fp))[r0:b]
    #
    # Absolute
    cmpqd   7, @63[r0:b]
    cmpqd   -8, @63[r0:b]
    cmpqd   7, @8191[r0:b]
    cmpqd   -8, @8191[r0:b]
    cmpqd   7, @536870911[r0:b]
    cmpqd   -8, @536870911[r0:b]
    #
    # External
    cmpqd   7, ext(-64) + 63[r0:b]
    cmpqd   -8, ext(-64) + 63[r0:b]
    cmpqd   7, ext(-8192) + 8191[r0:b]
    cmpqd   -8, ext(-8192) + 8191[r0:b]
    cmpqd   7, ext(-536870912) + 536870911[r0:b]
    cmpqd   -8, ext(-536870912) + 536870911[r0:b]
    #
    # Top Of Stack
    cmpqd   7, tos[r0:b]
    cmpqd   -8, tos[r0:b]
    #
    # Memory Space
    cmpqd   7, 63(fp)[r0:b]
    cmpqd   -8, 63(fp)[r0:b]
    cmpqd   7, 8191(fp)[r0:b]
    cmpqd   -8, 8191(fp)[r0:b]
    cmpqd   7, 536870911(fp)[r0:b]
    cmpqd   -8, 536870911(fp)[r0:b]
    cmpqd   7, 63(sp)[r0:b]
    cmpqd   -8, 63(sp)[r0:b]
    cmpqd   7, 8191(sp)[r0:b]
    cmpqd   -8, 8191(sp)[r0:b]
    cmpqd   7, 536870911(sp)[r0:b]
    cmpqd   -8, 536870911(sp)[r0:b]
    cmpqd   7, 63(sb)[r0:b]
    cmpqd   -8, 63(sb)[r0:b]
    cmpqd   7, 8191(sb)[r0:b]
    cmpqd   -8, 8191(sb)[r0:b]
    cmpqd   7, 536870911(sb)[r0:b]
    cmpqd   -8, 536870911(sb)[r0:b]
    cmpqd   7, 63(pc)[r0:b]
    cmpqd   -8, 63(pc)[r0:b]
    cmpqd   7, 8191(pc)[r0:b]
    cmpqd   -8, 8191(pc)[r0:b]
    cmpqd   7, 536870911(pc)[r0:b]
    cmpqd   -8, 536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    cmpqd   7, r1[r0:w]
    cmpqd   -8, r1[r0:w]
    #
    # Register Relative
    cmpqd   7, 63(r1)[r0:w]
    cmpqd   -8, 63(r1)[r0:w]
    cmpqd   7, 8191(r1)[r0:w]
    cmpqd   -8, 8191(r1)[r0:w]
    cmpqd   7, 536870911(r1)[r0:w]
    cmpqd   -8, 536870911(r1)[r0:w]
    #
    # Memory Relative
    cmpqd   7, -64(63(fp))[r0:w]
    cmpqd   -8, -64(63(fp))[r0:w]
    cmpqd   7, -8192(8191(fp))[r0:w]
    cmpqd   -8, -8192(8191(fp))[r0:w]
    cmpqd   7, -536870912(536870911(fp))[r0:w]
    cmpqd   -8, -536870912(536870911(fp))[r0:w]
    #
    # Absolute
    cmpqd   7, @63[r0:w]
    cmpqd   -8, @63[r0:w]
    cmpqd   7, @8191[r0:w]
    cmpqd   -8, @8191[r0:w]
    cmpqd   7, @536870911[r0:w]
    cmpqd   -8, @536870911[r0:w]
    #
    # External
    cmpqd   7, ext(-64) + 63[r0:w]
    cmpqd   -8, ext(-64) + 63[r0:w]
    cmpqd   7, ext(-8192) + 8191[r0:w]
    cmpqd   -8, ext(-8192) + 8191[r0:w]
    cmpqd   7, ext(-536870912) + 536870911[r0:w]
    cmpqd   -8, ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    cmpqd   7, tos[r0:w]
    cmpqd   -8, tos[r0:w]
    #
    # Memory Space
    cmpqd   7, 63(fp)[r0:w]
    cmpqd   -8, 63(fp)[r0:w]
    cmpqd   7, 8191(fp)[r0:w]
    cmpqd   -8, 8191(fp)[r0:w]
    cmpqd   7, 536870911(fp)[r0:w]
    cmpqd   -8, 536870911(fp)[r0:w]
    cmpqd   7, 63(sp)[r0:w]
    cmpqd   -8, 63(sp)[r0:w]
    cmpqd   7, 8191(sp)[r0:w]
    cmpqd   -8, 8191(sp)[r0:w]
    cmpqd   7, 536870911(sp)[r0:w]
    cmpqd   -8, 536870911(sp)[r0:w]
    cmpqd   7, 63(sb)[r0:w]
    cmpqd   -8, 63(sb)[r0:w]
    cmpqd   7, 8191(sb)[r0:w]
    cmpqd   -8, 8191(sb)[r0:w]
    cmpqd   7, 536870911(sb)[r0:w]
    cmpqd   -8, 536870911(sb)[r0:w]
    cmpqd   7, 63(pc)[r0:w]
    cmpqd   -8, 63(pc)[r0:w]
    cmpqd   7, 8191(pc)[r0:w]
    cmpqd   -8, 8191(pc)[r0:w]
    cmpqd   7, 536870911(pc)[r0:w]
    cmpqd   -8, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    cmpqd   7, r1[r0:d]
    cmpqd   -8, r1[r0:d]
    #
    # Register Relative
    cmpqd   7, 63(r1)[r0:d]
    cmpqd   -8, 63(r1)[r0:d]
    cmpqd   7, 8191(r1)[r0:d]
    cmpqd   -8, 8191(r1)[r0:d]
    cmpqd   7, 536870911(r1)[r0:d]
    cmpqd   -8, 536870911(r1)[r0:d]
    #
    # Memory Relative
    cmpqd   7, -64(63(fp))[r0:d]
    cmpqd   -8, -64(63(fp))[r0:d]
    cmpqd   7, -8192(8191(fp))[r0:d]
    cmpqd   -8, -8192(8191(fp))[r0:d]
    cmpqd   7, -536870912(536870911(fp))[r0:d]
    cmpqd   -8, -536870912(536870911(fp))[r0:d]
    #
    # Absolute
    cmpqd   7, @63[r0:d]
    cmpqd   -8, @63[r0:d]
    cmpqd   7, @8191[r0:d]
    cmpqd   -8, @8191[r0:d]
    cmpqd   7, @536870911[r0:d]
    cmpqd   -8, @536870911[r0:d]
    #
    # External
    cmpqd   7, ext(-64) + 63[r0:d]
    cmpqd   -8, ext(-64) + 63[r0:d]
    cmpqd   7, ext(-8192) + 8191[r0:d]
    cmpqd   -8, ext(-8192) + 8191[r0:d]
    cmpqd   7, ext(-536870912) + 536870911[r0:d]
    cmpqd   -8, ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    cmpqd   7, tos[r0:d]
    cmpqd   -8, tos[r0:d]
    #
    # Memory Space
    cmpqd   7, 63(fp)[r0:d]
    cmpqd   -8, 63(fp)[r0:d]
    cmpqd   7, 8191(fp)[r0:d]
    cmpqd   -8, 8191(fp)[r0:d]
    cmpqd   7, 536870911(fp)[r0:d]
    cmpqd   -8, 536870911(fp)[r0:d]
    cmpqd   7, 63(sp)[r0:d]
    cmpqd   -8, 63(sp)[r0:d]
    cmpqd   7, 8191(sp)[r0:d]
    cmpqd   -8, 8191(sp)[r0:d]
    cmpqd   7, 536870911(sp)[r0:d]
    cmpqd   -8, 536870911(sp)[r0:d]
    cmpqd   7, 63(sb)[r0:d]
    cmpqd   -8, 63(sb)[r0:d]
    cmpqd   7, 8191(sb)[r0:d]
    cmpqd   -8, 8191(sb)[r0:d]
    cmpqd   7, 536870911(sb)[r0:d]
    cmpqd   -8, 536870911(sb)[r0:d]
    cmpqd   7, 63(pc)[r0:d]
    cmpqd   -8, 63(pc)[r0:d]
    cmpqd   7, 8191(pc)[r0:d]
    cmpqd   -8, 8191(pc)[r0:d]
    cmpqd   7, 536870911(pc)[r0:d]
    cmpqd   -8, 536870911(pc)[r0:d]
