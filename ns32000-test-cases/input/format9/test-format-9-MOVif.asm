.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 9 instructions
#
# Instruction under test: MOVif
#
# Syntax.   MOVif   src,    dest
# `                 gen     gen
#                   read.i  write.f
#
#           !   src   !   dest  !           MOVif           !
#           +---------+---------+-------+---+---------------+
#           !   gen   !   gen   !0 0 0!f! i !0 0 1 1 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
#
# The MOVif instruction converts the integer operand src to a single- or double-
# precision floating-point number and places the result in the dest operand
# location.
#
# Register
#
    movbf   r7, r0
    movdl   r6, r1
    movwf   r5, r2
    movbl   r4, r3
    movdf   r3, r4
    movwl   r2, r5
    movbf   r1, r6
    movdl   r0, r7
#
# Register relative
#
    movbf   -64(r7), r0
    movdl   63(r6), r1
    movwf   8191(r5), r2
    movbl   -8192(r4), r3
    movdf   -536870912(r3), r4
    movwl   536870911(r2), r5
    movbf   -64(r1), r6
    movdl   63(r0), r7
#
    movbf   r7, -64(r7)
    movdl   r6, 63(r6)
    movwf   r5, 8191(r5)
    movbl   r4, -8192(r4)
    movdf   r3, -536870912(r3)
    movwl   r2, 536870911(r2)
    movbf   r1, -64(r1)
    movdl   r0, 63(r0)
#
    movbf   63(r0), -64(r7)
    movdl   -64(r1), 63(r6)
    movwf   536870911(r2), 8191(r5)
    movbl   -536870912(r3), -8192(r4)
    movdf   -8192(r4), -536870912(r3)
    movwl    8191(r5), 536870911(r2)
    movbf   63(r6), -64(r1)
    movdl   -64(r7), 63(r0)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    movbf   r0, -64(63(fp))
    movdl   r1, -64(63(fp))
    movwf   r2, 63(-64(fp))
    movbl   r3, 63(-64(fp))
    #
    movdf   r0, -8192(63(fp))
    movwl   r1, -64(8191(fp))
    movbf   r2, 63(-8192(fp))
    movdl   r3, 8191(-64(fp))
    #
    movwf   r4, -8192(8191(fp))
    movbl   r5, -8192(8191(fp))
    movdf   r6, 8191(-8192(fp))
    movwl   r7, 8191(-8192(fp))
    #
    movbf   63(r0), -536870912(8191(fp))
    movdl   -64(r1), -8192(536870911(fp))
    movwf   536870911(r2), 8191(-536870912(fp))
    movbl   -536870912(r3), 536870911(-8192(fp))
    #
    movdf   -8192(r4), 536870911(-536870912(fp))
    movwl   8191(r5), 536870911(-536870912(fp))
    movbf   63(r6), -536870912(536870911(fp))
    movdl   -64(r7), -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    movbf   r0, -64(63(sp))
    movwl   r1, -64(63(sp))
    movdf   r2, 63(-64(sp))
    movbl   r3, 63(-64(sp))
    #
    movwf   -8192(8191(fp)), -8192(63(sp))
    movdl   -8192(8191(fp)), -64(8191(sp))
    movbf   8191(-8192(fp)), 63(-8192(sp))
    movwl   8191(-8192(fp)), 8191(-64(sp))
    #
    movdf   -8192(8191(sp)), -8192(8191(fp))
    movbl   -8192(8191(sp)), -8192(8191(fp))
    movwf   8191(-8192(sp)), 8191(-8192(fp))
    movdl   8191(-8192(sp)), 8191(-8192(fp))
    #
    movbf   r4, -536870912(8191(sp))
    movwl   r5, -8192(536870911(sp))
    movdf   r6, 8191(-536870912(sp))
    movbl   r7, 536870911(-8192(sp))
    #
    movwf   63(r0), 536870911(-536870912(sp))
    movdl   -64(r1), 536870911(-536870912(sp))
    movbf   536870911(r2), -536870912(536870911(sp))
    movwl   -536870912(r3), -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    movbf   r0, -64(63(sb))
    movwl   r1, -64(63(sb))
    movdf   r2, 63(-64(sb))
    movbl   r3, 63(-64(sb))
    #
    movwf   -8192(8191(fp)), -8192(63(sb))
    movdl   -8192(8191(fp)), -64(8191(sb))
    movbf   8191(-8192(fp)), 63(-8192(sb))
    movwl   8191(-8192(fp)), 8191(-64(sb))
    #
    movdf   -8192(8191(sp)), -8192(8191(sb))
    movbl   -8192(8191(sp)), -8192(8191(sb))
    movwf   8191(-8192(sp)), 8191(-8192(sb))
    movdl   8191(-8192(sp)), 8191(-8192(sb))
    #
    movbf   r4, -536870912(8191(sb))
    movwl   r5, -8192(536870911(sb))
    movdf   r6, 8191(-536870912(sb))
    movbl   r7, 536870911(-8192(sb))
    #
    movwf   63(r0), 536870911(-536870912(sb))
    movdl   -64(r1), 536870911(-536870912(sb))
    movbf   536870911(r2), -536870912(536870911(sb))
    movwl   -536870912(r3), -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    movbf	r0, @-64
    movwl	r1, @-64
    movdf	r2, @63
    movbl	r3, @63
    movwf	7(r4), @-65
    movdl	-8(r5), @-65
    movbf	7(r6), @64
    movwl	-8(r7), @64
    #
    movdf	-8192(63(fp)), @-8192
    movbl	@-8192, -8192(536870911(sp))
    movwf	@8191, r0
    movdl	@8191, -64(r1)
    movbf	@-8193, -8192(63(sb))
    movwl	@-8193, -536870912(-8192(fp))
    movdf	@8192, r2
    movbl	@8192, -1(r4)
    #
    movwf	r5, @-536870912
    movdl	r6, @536870911
#
#
# External
#
    movbf	ext(-64) + 63, r0
    movwl	ext(-64) + 63, r1
    movdf	ext(63) + -64, r2
    movbl	ext(63) + -64, r3
    #
    movwf	ext(-65) + 63, 7(r4)
    movdl	ext(-65) + 63, -8(r5)
    movbf	ext(63) + -65, 7(r6)
    movwl	ext(63) + -65, -8(r7)
    movdf	7(r4), ext(-64) + 64
    movbl	-8(r5), ext(-64) + 64
    movwf	7(r6), ext(64) + -64
    movdl	-8(r7), ext(64) + -64
    #
    movbf	@8191, ext(-8192) + 8191
    movwl	@-8192, ext(-8192) + 81916
    movdf	@536870911, ext(8191) + -8192
    movbl	@-536870912, ext(8191) + -8192
    #
    movwf	ext(-536870912) + 8191, -8192(8191(sp))
    movdl	ext(-536870912) + 8191, -8192(8191(sp))
    movbf	ext(8191) + -536870912, 8191(-8192(sp))
    movwl	ext(8191) + -536870912, 8191(-8192(sp))
    movdf	ext(-8192) + 536870911, @8191
    movbl	ext(-8192) + 536870911, @-8192
    movwf	ext(536870911) + -8192, ext(64) + -64
    movdl	ext(536870911) + -8192, ext(8191) + -8192
    #
    movbf	-64(63(fp)), ext(-536870912) + 536870911
    movwl	-64(63(fp)), ext(-536870912) + 536870911
    movdf	ext(536870911) + -536870912, 63(-64(fp))
    movbl	ext(536870911) + -536870912, 63(-64(fp))
#
# Top Of Stack
#
    movbf	r0, tos
    movwl	tos, 7(r1)
    movdf	@-8, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    movbl	r0, -64(fp)
    movwf	-64(fp), r1
    movdl	7(r2), 63(fp) 
    movbf	63(fp), -8(r3)
    #
    movwl	63(fp), -8192(fp)
    movdf	-8192(fp), 63(fp)
    movbl	-64(fp), 8191(fp)
    movwf	8191(fp), -64(fp)
    #
    movdl	-64(63(fp)), -536870912(fp)
    movbf	ext(-536870912) + 536870911, -536870912(fp)
    movwl	536870911(fp), @-8192
    movdf	536870911(fp), -8192(8191(sp))
    #
    # Stack Pointer (sp)
    #
    movbl	r0, -64(sp)
    movwf	-64(sp), r1
    movdl	7(r2), 63(sp) 
    movbf	63(sp), -8(r3)
    #
    movwl	63(sp), -8192(sp)
    movdf	-8192(sp), 63(fp)
    movbl	-64(sp), 8191(sp)
    movwf	8191(fp), -64(sp)
    #
    movdl	-64(63(sp)), -536870912(sp)
    movbf	ext(-536870912) + 536870911, -536870912(sp)
    movwl	536870911(sp), @-8192
    movdf	536870911(fp), -8192(8191(sp))
    #
    # Static Memory (sb)
    #
    movbl	r0, -64(sb)
    movwf	-64(sb), r1
    movdl	7(r2), 63(sb)
    movbf	63(sb), -8(r3)
    #
    movwl	63(fp), -8192(sb)
    movdf	-8192(sb), 63(sb)
    movbl	-64(sb), 8191(sp)
    movwf	8191(sb), -64(sp)
    #
    movdl	-64(63(fp)), -536870912(fp)
    movbf	ext(-536870912) + 536870911, -536870912(fp)
    movwl	536870911(fp), @-8192
    movdf	536870911(fp), -8192(8191(sp))
    #
    # Program Counter (pc)
    #
    movbl	r0, -64(pc)
    movwf	-64(pc), r1
    movdl	7(r2), 63(pc) 
    movbf	63(pc), -8(r3)
    #
    movwl	63(pc), -8192(fp)
    movdf	-8192(fp), 63(pc)
    movbl	-64(sp), 8191(pc)
    movwf	8191(pc), -64(pc)
    #
    movdl	-64(63(fp)), -536870912(pc)
    movbf	ext(-536870912) + 536870911, -536870912(pc)
    movwl	536870911(pc), @-8192
    movdf	536870911(pc), -8192(8191(sp))
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    movbl   r0, r7[r0:b]
    movbf   r1, r6[r1:b]
    #
    # Register Relative
    movbl   7(r2), 63(r5)[r2:b]
    movbf   -8(r3), 63(r4)[r3:b]
    movbl   8191(r3)[r4:b], -8192(fp)
    movbf   8191(r2)[r5:b], @-8192
    movbl   ext(-536870912) + 536870911, 536870911(r1)[r6:b]
    movbf   -64(pc), 536870911(r0)[r7:b]
    #
    # Memory Relative
    movbl   r7, -64(63(fp))[r0:b]
    movbf   -64(63(fp))[r1:b], -8(r6)
    movbl   -8192(8191(fp))[r3:b], -8192(8191(fp))[r2:b]
    movbf   @-8, -8192(8191(fp))[r3:b]
    movbl   -536870912(536870911(fp))[r4:b], ext(8191) + -536870912
    movbf   -536870912(536870911(fp))[r4:b], -536870912(536870911(fp))[r5:b]
    movbl   ext(8191) + -536870912, -64(63(sp))[r7:b]
    movbf   -64(63(sp))[r6:b], -64(r5)
    movbl   -64(63(sp))[r6:b], -8192(8191(sp))[r5:b]
    movbf   -8192(8191(sp))[r4:b], -64(pc)
    movbl   -536870912(536870911(sp))[r3:b], 63(r5)[r2:b]
    movbf   -536870912(536870911(sp))[r3:b], -536870912(536870911(sp))[r2:b]
    movbl   7(r4), -64(63(sb))[r0:b]
    movbf   -64(63(sb))[r1:b], 8192(r5)[r2:b]
    movbl   -64(63(sb))[r1:b], -8192(8191(sb))[r2:b]
    movbf   -8(r1), -8192(8191(sb))[r3:b]
    movbl   -536870912(536870911(sb))[r4:b], -8192(sp)
    movbf   -536870912(536870911(sb))[r4:b], -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    movbl   7(r2), @63[r0:b]
    movbf   @63[r1:b], -8(r1)
    movbl   @63[r1:b], @8191[r2:b]
    movbf   -8192(sp), @8191[r3:b]
    movbl   @536870911[r4:b], -8192(pc)
    movbf   @536870911[r4:b], @536870911[r5:b]
    #
    # External
    movbl   7(r2), ext(-64) + 63[r7:b]
    movbf   ext(-64) + 63[r6:b], -8(r1)
    movbl   ext(-64) + 63[r6:b], ext(-8192) + 8191[r5:b]
    movbf   -8192(sp), ext(-8192) + 8191[r4:b]
    movbl   ext(-536870912) + 536870911[r3:b], -8192(fp)
    movbf   ext(-536870912) + 536870911[r3:b], ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    movbl   ext(-536870912) + 536870911[r2:b], tos[r0:b]
    movbf   7(r1), tos[r0:b]
    movbl   tos[r7:b], @63[r0:b]
    #
    # Memory Space
    movbf   7(r7), 63(fp)[r0:b]
    movbl   63(fp)[r0:b], @63[r0:b]
    movbf   63(fp)[r0:b], 8191(fp)[r0:b]
    movbl   -8(r1), 8191(fp)[r0:b]
    movbf   8191(fp)[r0:b], 536870911(fp)[r0:b]
    movbl   @63[r0:b], 536870911(fp)[r0:b]
    movbf   @63[r0:b], 63(sp)[r0:b]
    movbl   63(sp)[r0:b], @63[r0:b]
    movbf   63(sp)[r0:b], 8191(sp)[r0:b]
    movbl   -8(r6), 8191(sp)[r0:b]
    movbf   536870911(sp)[r0:b], ext(-536870912) + 536870911[r3:b]
    movbl   536870911(sp)[r0:b], 536870911(sp)[r0:b]
    movbf   7(r1), 63(sb)[r0:b]
    movbl   63(sb)[r0:b], r3
    movbf   63(sb)[r0:b], 8191(sb)[r0:b]
    movbl   8191(sb)[r0:b], @63[r0:b]
    movbf   536870911(sb)[r0:b], ext(-536870912) + 536870911[r3:b]
    movbl   536870911(sb)[r0:b], 536870911(sb)[r0:b]
    movbf   7(r6), 63(pc)[r0:b]
    movbl   63(pc)[r0:b], @63[r0:b]
    movbf   63(pc)[r0:b], 8191(pc)[r0:b]
    movbl   -8(r1), 8191(pc)[r0:b]
    movbf   536870911(pc)[r0:b], ext(-536870912) + 536870911[r3:b]
    movbl   536870911(pc)[r0:b], 536870911(pc)[r0:b]
#
end:
