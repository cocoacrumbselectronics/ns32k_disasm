.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: CXP
#
# Syntax:   CXP     index
#                   disp
#
#           !      CXP      !
#           +---------------+
#           !0 0 1 0 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The CXP instruction calls a procedure which is outside the current module (an
# "external" procedure).
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
    cxp     63
    cxp     -64
    cxp     8191
    cxp     -8192
    cxp     536870911
    cxp     -536870912
#
    cxp     5
    cxp     -18
    cxp     4025
    cxp     -125
    cxp     5987621
    cxp     -24869248
#
end:
