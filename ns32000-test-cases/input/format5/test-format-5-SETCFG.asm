.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 5 instructions
#
# Instruction under test: SETCFG
#
# Syntax:   SETCFG  cfglist
#                   short
#
#           !         !cfglist!          SETCFG             !
#           +---------+-+-+-+-+-----------------------------+
#           !0 0 0 0 0!C!M!F!I!0 0 0 1 0 1 1 0 0 0 0 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#           23            16 15            8 7             0
#
# The SETCFG instruction loads the Configuration Register (CFG), enabling or
# disabling optional system features.
#
    setcfg  []
#
    setcfg  [i]
    setcfg  [f]
    setcfg  [m]
    setcfg  [c]
#
    setcfg  [f, i]
    setcfg  [m, i]
    setcfg  [m, f]
    setcfg  [c, i]
    setcfg  [c, f]
    setcfg  [c, m]
#
    setcfg  [m, f, i]
# 
    setcfg  [c, f, i]
    setcfg  [c, m, i]
    setcfg  [c, m, f]
#
    setcfg  [c, m, f, i]
#
end:
