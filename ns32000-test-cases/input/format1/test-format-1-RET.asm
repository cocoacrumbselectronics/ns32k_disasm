.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: BSR
#
# Syntax:   RET     constant
#                   disp
#
#           !      RET      !
#           +---------------+
#           !0 0 0 1 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The RET instruction returns execution control from a local procedure and removes
# procedure parameters from the stack.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
    ret     63
    ret     -64
    ret     8191
    ret     -8192
    ret     536870911
    ret     -536870912
#
    ret     5
    ret     -18
    ret     4025
    ret     -125
    ret     5987621
    ret     -24869248
#
end:
