.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: BPT
#
# Syntax:   BPT
#
#           !      BPT      !
#           +---------------+
#           !1 1 1 1 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The BPT instruction activates the Breakpoint Trap (BPT). The return address
# pushed on the Interrupt Stack is the address of the BPT instruction itself.
#
    bpt
#
end:
