.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 3 instructions
#
# Instruction under test: CASE
#
# Syntax:   CASE    src
#                   gen
#                   read.i
#
#           !  desc   !        CASEi        !
#           +---------+---------------------+
#           !   gen   !1 1 1 0 1 1 1 1 1! i !
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# The CASEi instruction branches to a nonsequential instruction by adding the src
# operand to the PC register. The src operand is interpreted as a signed integer,
# and is sign-extended to 32 bits before the addition is performed.
# 
# A Case Branch instruction, using Scaled Indexing and a table of branch offsets,
# may be used to implement a multiway branch.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
# Register
#
    caseb   r0
    casew   r1
    cased   r2
    caseb   r3
    casew   r4
    cased   r5
    caseb   r6
    casew   r7
#
# Register relative
#
    cased   63(r0)
    caseb   -64(r0)
    casew   8191(r0)
    cased   -8192(r0)
    caseb   536870911(r0)
    casew   -536870912(r0)
    #
    cased   63(r1)
    caseb   -64(r1)
    casew   8191(r1)
    cased   -8192(r1)
    caseb   536870911(r1)
    casew   -536870912(r1)
    #
    cased   63(r2)
    caseb   -64(r2)
    casew   8191(r2)
    cased   -8192(r2)
    caseb   536870911(r2)
    casew   -536870912(r2)
    #
    cased   63(r3)
    caseb   -64(r3)
    casew   8191(r3)
    cased   -8192(r3)
    caseb   536870911(r3)
    casew   -536870912(r3)
    #
    cased   63(r4)
    caseb   -64(r4)
    casew   8191(r4)
    cased   -8192(r4)
    caseb   536870911(r4)
    casew   -536870912(r4)
    #
    cased   63(r5)
    caseb   -64(r5)
    casew   8191(r5)
    cased   -8192(r5)
    caseb   536870911(r5)
    casew   -536870912(r5)
    #
    cased   63(r6)
    caseb   -64(r6)
    casew   8191(r6)
    cased   -8192(r6)
    caseb   536870911(r6)
    casew   -536870912(r6)
    #
    cased   63(r7)
    caseb   -64(r7)
    casew   8191(r7)
    cased   -8192(r7)
    caseb   536870911(r7)
    casew   -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    cased   -64(63(fp))
    caseb   63(-64(fp))
    casew   -8192(63(fp))
    cased   -64(8191(fp))
    caseb   63(-8192(fp))
    casew   8191(-64(fp))
    cased   -8192(8191(fp))
    caseb   8191(-8192(fp))
    casew   -536870912(8191(fp))
    cased   -8192(536870911(fp))
    caseb   8191(-536870912(fp))
    casew   536870911(-8192(fp))
    cased   536870911(-536870912(fp))
    caseb   -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    casew   -64(63(sp))
    cased   63(-64(sp))
    caseb   -8192(63(sp))
    casew   -64(8191(sp))
    cased   63(-8192(sp))
    caseb   8191(-64(sp))
    casew   -8192(8191(sp))
    cased   8191(-8192(sp))
    caseb   -536870912(8191(sp))
    casew   -8192(536870911(sp))
    cased   8191(-536870912(sp))
    caseb   536870911(-8192(sp))
    casew   536870911(-536870912(sp))
    cased   -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    caseb   -64(63(sb))
    casew   63(-64(sb))
    cased   -8192(63(sb))
    caseb   -64(8191(sb))
    casew   63(-8192(sb))
    cased   8191(-64(sb))
    caseb   -8192(8191(sb))
    casew   8191(-8192(sb))
    cased   -536870912(8191(sb))
    caseb   -8192(536870911(sb))
    casew   8191(-536870912(sb))
    cased   536870911(-8192(sb))
    caseb   536870911(-536870912(sb))
    casew   -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    cased	@-64
    caseb	@63
    casew	@-8192
    cased	@8191
    caseb	@-536870912
    casew	@536870911
#
# External
#
    cased	ext(-64) + 63
    caseb	ext(63) + -64
    casew	ext(-8192) + 8191
    cased	ext(8191) + -8192
    caseb	ext(-536870912) + 8191
    casew	ext(8191) + -536870912
    cased	ext(-8192) + 536870911
    caseb	ext(536870911) + -8192
    casew	ext(-536870912) + 536870911
    cased	ext(536870911) + -536870912
#
# Top Of Stack
#
    caseb	tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    casew	-64(fp)
    cased	63(fp)
    caseb	-8192(fp)
    casew	8191(fp)
    cased	-536870912(fp)
    caseb	536870911(fp)
    #
    # Stack Pointer (sp)
    #
    casew	-64(sp)
    cased	63(sp)
    caseb	-8192(sp)
    casew	8191(sp)
    cased	-536870912(sp)
    caseb	536870911(sp)
    #
    # Static Memory (sb)
    #
    casew	-64(sb)
    cased	63(sb)
    caseb	-8192(sb)
    casew	8191(sb)
    cased	-536870912(sb)
    caseb	536870911(sb)
    #
    # Program Counter (pc)
    #
    casew	-64(pc)
    cased	63(pc)
    caseb	-8192(pc)
    casew	8191(pc)
    cased	-536870912(pc)
    caseb	536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    casew   r7[r0:b]
    #
    # Register Relative
    cased   63(r5)[r2:b]
    caseb   8191(r3)[r4:b]
    casew   536870911(r1)[r6:b]
    #
    # Memory Relative
    cased   -64(63(fp))[r0:b]
    caseb   -8192(8191(fp))[r2:b]
    casew   -536870912(536870911(fp))[r4:b]
    cased   -64(63(sp))[r7:b]
    caseb   -8192(8191(sp))[r5:b]
    casew   -536870912(536870911(sp))[r3:b]
    cased   -64(63(sb))[r0:b]
    caseb   -8192(8191(sb))[r2:b]
    casew   -536870912(536870911(sb))[r4:b]
    #
    # Absolute
    cased   @63[r0:b]
    caseb   @8191[r2:b]
    casew   @536870911[r4:b]
    #
    # External
    cased   ext(-64) + 63[r7:b]
    caseb   ext(-8192) + 8191[r5:b]
    casew   ext(-536870912) + 536870911[r3:b]
    #
    # Top Of Stack
    cased   tos[r0:b]
    #
    # Memory Space
    caseb   63(fp)[r0:b]
    casew   8191(fp)[r0:b]
    cased   536870911(fp)[r0:b]
    caseb   63(sp)[r0:b]
    casew   8191(sp)[r0:b]
    cased   536870911(sp)[r0:b]
    caseb   63(sb)[r0:b]
    casew   8191(sb)[r0:b]
    cased   536870911(sb)[r0:b]
    caseb   63(pc)[r0:b]
    casew   8191(pc)[r0:b]
    cased   536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    caseb  r1[r0:w]
    #
    # Register Relative
    casew   63(r1)[r0:w]
    cased   8191(r1)[r0:w]
    caseb   536870911(r1)[r0:w]
    #
    # Memory Relative
    casew   -64(63(fp))[r0:w]
    cased   -8192(8191(fp))[r2:w]
    caseb   -536870912(536870911(fp))[r4:w]
    casew   -64(63(sp))[r7:w]
    cased   -8192(8191(sp))[r5:w]
    caseb   -536870912(536870911(sp))[r3:w]
    casew   -64(63(sb))[r0:w]
    cased   -8192(8191(sb))[r2:w]
    caseb   -536870912(536870911(sb))[r4:w]
    #
    # Absolute
    casew   @63[r0:w]
    cased   @8191[r0:w]
    caseb   @536870911[r0:w]
    #
    # External
    casew   ext(-64) + 63[r0:w]
    cased   ext(-8192) + 8191[r0:w]
    caseb   ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    casew   tos[r0:w]
    #
    # Memory Space
    cased   63(fp)[r0:w]
    caseb   8191(fp)[r0:w]
    casew   536870911(fp)[r0:w]
    cased   63(sp)[r0:w]
    caseb   8191(sp)[r0:w]
    casew   536870911(sp)[r0:w]
    cased   63(sb)[r0:w]
    caseb   8191(sb)[r0:w]
    casew   536870911(sb)[r0:w]
    cased   63(pc)[r0:w]
    caseb   8191(pc)[r0:w]
    casew   536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    cased   r1[r0:d]
    #
    # Register Relative
    caseb   63(r1)[r0:d]
    casew   8191(r1)[r0:d]
    cased   536870911(r1)[r0:d]
    #
    # Memory Relative
    caseb   -64(63(fp))[r0:d]
    casew   -8192(8191(fp))[r2:d]
    cased   -536870912(536870911(fp))[r4:d]
    caseb   -64(63(sp))[r7:d]
    casew   -8192(8191(sp))[r5:d]
    cased   -536870912(536870911(sp))[r3:d]
    caseb   -64(63(sb))[r0:d]
    casew   -8192(8191(sb))[r2:d]
    cased   -536870912(536870911(sb))[r4:d]
    #
    # Absolute
    caseb   @63[r0:d]
    casew   @8191[r0:d]
    cased   @536870911[r0:d]
    #
    # External
    caseb   ext(-64) + 63[r0:d]
    casew   ext(-8192) + 8191[r0:d]
    cased   ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    caseb   tos[r0:d]
    #
    # Memory Space
    casew   63(fp)[r0:d]
    cased   8191(fp)[r0:d]
    caseb   536870911(fp)[r0:d]
    casew   63(sp)[r0:d]
    cased   8191(sp)[r0:d]
    caseb   536870911(sp)[r0:d]
    casew   63(sb)[r0:d]
    cased   8191(sb)[r0:d]
    caseb   536870911(sb)[r0:d]
    casew   63(pc)[r0:d]
    cased   8191(pc)[r0:d]
    caseb   536870911(pc)[r0:d]
#
end:
