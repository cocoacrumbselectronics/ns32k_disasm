.psize 0, 143   # suppress form feed, 143 columns
#
#
# Test case for FORMAT 2 instructions
#
# Instruction under test: SPRi
#
# Syntax:   SPRi    procreg dest
#                   short   gen
#                           write.i
#
#           !  dest   !procreg!     SPRi    !
#           +---------+-------+---------+---+
#           !   gen   ! short !0 1 0 1 1! i !
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# The SPRi instruction stores the contents of the specified by procreg in the dest operand location.
# dedicated processor register
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
#
# Register
#
    sprb    us, r0
    sprb    dcr, r1
    sprb    bpc, r2
    sprb    dsr, r3
    sprb    car, r4
    sprb    fp, r5
    sprb    sp, r6
    sprb    sb, r7
    sprb    usp, r0
    sprb    cfg, r1
    sprb    psr, r2
    sprb    intbase, r3
    sprb    mod, r4
#
# Register relative
#
    sprb   us, 63(r0)
    sprb   us, -64(r0)
    sprb   us, 8191(r0)
    sprb   us, -8192(r0)
    sprb   us, 536870911(r0)
    sprb   us, -536870912(r0)
    #
    sprb   dcr, 63(r1)
    sprb   dcr, -64(r1)
    sprb   dcr, 8191(r1)
    sprb   dcr, -8192(r1)
    sprb   dcr, 536870911(r1)
    sprb   dcr, -536870912(r1)
    #
    sprb   bpc, 63(r2)
    sprb   bpc, -64(r2)
    sprb   bpc, 8191(r2)
    sprb   bpc, -8192(r2)
    sprb   bpc, 536870911(r2)
    sprb   bpc, -536870912(r2)
    #
    sprb   dsr, 63(r3)
    sprb   dsr, -64(r3)
    sprb   dsr, 8191(r3)
    sprb   dsr, -8192(r3)
    sprb   dsr, 536870911(r3)
    sprb   dsr, -536870912(r3)
    #
    sprb   car, 63(r4)
    sprb   car, -64(r4)
    sprb   car, 8191(r4)
    sprb   car, -8192(r4)
    sprb   car, 536870911(r4)
    sprb   car, -536870912(r4)
    #
    sprb   fp, 63(r5)
    sprb   fp, -64(r5)
    sprb   fp, 8191(r5)
    sprb   fp, -8192(r5)
    sprb   fp, 536870911(r5)
    sprb   fp, -536870912(r5)
    #
    sprb   sp, 63(r6)
    sprb   sp, -64(r6)
    sprb   sp, 8191(r6)
    sprb   sp, -8192(r6)
    sprb   sp, 536870911(r6)
    sprb   sp, -536870912(r6)
    #
    sprb   sb, 63(r7)
    sprb   sb, -64(r7)
    sprb   sb, 8191(r7)
    sprb   sb, -8192(r7)
    sprb   sb, 536870911(r7)
    sprb   sb, -536870912(r7)
    #
    sprb   usp, 63(r7)
    sprb   usp, -64(r7)
    sprb   usp, 8191(r7)
    sprb   usp, -8192(r7)
    sprb   usp, 536870911(r7)
    sprb   usp, -536870912(r7)
    #
    sprb   cfg, 63(r7)
    sprb   cfg, -64(r7)
    sprb   cfg, 8191(r7)
    sprb   cfg, -8192(r7)
    sprb   cfg, 536870911(r7)
    sprb   cfg, -536870912(r7)
    #
    sprb   psr, 63(r7)
    sprb   psr, -64(r7)
    sprb   psr, 8191(r7)
    sprb   psr, -8192(r7)
    sprb   psr, 536870911(r7)
    sprb   psr, -536870912(r7)
    #
    sprb   intbase, 63(r7)
    sprb   intbase, -64(r7)
    sprb   intbase, 8191(r7)
    sprb   intbase, -8192(r7)
    sprb   intbase, 536870911(r7)
    sprb   intbase, -536870912(r7)
    #
    sprb   mod, 63(r7)
    sprb   mod, -64(r7)
    sprb   mod, 8191(r7)
    sprb   mod, -8192(r7)
    sprb   mod, 536870911(r7)
    sprb   mod, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    sprb   us, -64(63(fp))
    sprb   dcr, -64(63(fp))
    sprb   bpc, 63(-64(fp))
    sprb   dsr, 63(-64(fp))
    #
    sprb   car, -8192(63(fp))
    sprb   fp, -64(8191(fp))
    sprb   sp, 63(-8192(fp))
    sprb   sb, 8191(-64(fp))
    #
    sprb   usp, -8192(8191(fp))
    sprb   cfg, -8192(8191(fp))
    sprb   psr, 8191(-8192(fp))
    sprb   intbase, 8191(-8192(fp))
    #
    sprb   mod, -536870912(8191(fp))
    sprb   us, -8192(536870911(fp))
    sprb   dcr, 8191(-536870912(fp))
    sprb   bpc, 536870911(-8192(fp))
    #
    sprb   dsr, 536870911(-536870912(fp))
    sprb   car, 536870911(-536870912(fp))
    sprb   fp, -536870912(536870911(fp))
    sprb   sb, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    sprb   us, -64(63(sp))
    sprb   dcr, -64(63(sp))
    sprb   bpc, 63(-64(sp))
    sprb   dsr, 63(-64(sp))
    
    sprb   car, -8192(63(sp))
    sprb   fp, -64(8191(sp))
    sprb   sp, 63(-8192(sp))
    sprb   sb, 8191(-64(sp))
    #
    sprb   usp, -8192(8191(sp))
    sprb   cfg, -8192(8191(sp))
    sprb   psr, 8191(-8192(sp))
    sprb   intbase, 8191(-8192(sp))
    #
    sprb   mod, -536870912(8191(sp))
    sprb   us, -8192(536870911(sp))
    sprb   dcr, 8191(-536870912(sp))
    sprb   bpc, 536870911(-8192(sp))
    #
    sprb   dsr, 536870911(-536870912(sp))
    sprb   car, 536870911(-536870912(sp))
    sprb   fp, -536870912(536870911(sp))
    sprb   sb, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    sprb   us, -64(63(sb))
    sprb   dcr, -64(63(sb))
    sprb   bpc, 63(-64(sb))
    sprb   dsr, 63(-64(sb))
    #
    sprb   car, -8192(63(sb))
    sprb   fp, -64(8191(sb))
    sprb   sp, 63(-8192(sb))
    sprb   sb, 8191(-64(sb))
    #
    sprb   usp, -8192(8191(sb))
    sprb   cfg, -8192(8191(sb))
    sprb   psr, 8191(-8192(sb))
    sprb   intbase, 8191(-8192(sb))
    #
    sprb   mod, -536870912(8191(sb))
    sprb   us, -8192(536870911(sb))
    sprb   dcr, 8191(-536870912(sb))
    sprb   bpc, 536870911(-8192(sb))
    #
    sprb   dsr, 536870911(-536870912(sb))
    sprb   car, 536870911(-536870912(sb))
    sprb   fp, -536870912(536870911(sb))
    sprb   sb, -536870912(536870911(sb))
# 
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    sprb	us, @-64
    sprb	dcr, @-64
    sprb	bpc, @63
    sprb	dsr, @63
    sprb	car, @-65
    sprb	fp, @-65
    sprb	sp, @64
    sprb	sb, @64
    #
    sprb	usp, @-8192
    sprb	cfg, @-8192
    sprb	psr, @8191
    sprb	intbase, @8191
    sprb	mod, @-8193
    sprb	us, @-8193
    sprb	dcr, @8192
    sprb	bpc, @8192
    #
    sprb	dsr, @-536870912
    sprb	car, @536870911
#
# External
#
    sprb	us, ext(-64) + 63
    sprb	dcr, ext(-64) + 63
    sprb	bpc, ext(63) + -64
    sprb	dsr, ext(63) + -64
    #
    sprb	car, ext(-65) + 63
    sprb	fp, ext(-65) + 63
    sprb	sp, ext(63) + -65
    sprb	sb, ext(63) + -65
    sprb	usp, ext(-64) + 64
    sprb	cfg, ext(-64) + 64
    sprb	psr, ext(64) + -64
    sprb	intbase, ext(64) + -64
    #
    sprb	mod, ext(-8192) + 8191
    sprb	us, ext(-8192) + 8191
    sprb	dcr, ext(8191) + -8192
    sprb	bpc, ext(8191) + -8192
    #
    sprb	dsr, ext(-536870912) + 8191
    sprb	car, ext(-536870912) + 8191
    sprb	fp, ext(8191) + -536870912
    sprb	sp, ext(8191) + -536870912
    sprb	sb, ext(-8192) + 536870911
    sprb	usp, ext(-8192) + 536870911
    sprb	cfg, ext(536870911) + -8192
    sprb	psr, ext(536870911) + -8192
    #
    sprb	intbase, ext(-536870912) + 536870911
    sprb	mod, ext(-536870912) + 536870911
    sprb	us, ext(536870911) + -536870912
    sprb	dcr, ext(536870911) + -536870912
#
# Top Of Stack
#
    sprb    us, tos
    sprb    dcr, tos
    sprb    bpc, tos
    sprb    dsr, tos
    sprb    car, tos
    sprb    fp, tos
    sprb    sp, tos
    sprb    sb, tos
    sprb    usp, tos
    sprb    cfg, tos
    sprb    psr, tos
    sprb    intbase, tos
    sprb    mod, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    sprb	us, -64(fp)
    sprb	dcr, -64(fp)
    sprb	bpc, 63(fp)
    sprb	dsr, 63(fp)
    #
    sprb	car, -8192(fp)
    sprb	fp, -8192(fp)
    sprb	sp, 8191(fp)
    sprb	sb, 8191(fp)
    #
    sprb	usp, -536870912(fp)
    sprb	cfg, -536870912(fp)
    sprb	psr, 536870911(fp)
    sprb	intbase, 536870911(fp)
    sprb	mod, -64(fp)
    #
    # Stack Pointer (sp)
    #
    sprb	us, -64(sp)
    sprb	dcr, -64(sp)
    sprb	bpc, 63(sp)
    sprb	dsr, 63(sp)
    #
    sprb	car, -8192(sp)
    sprb	fp, -8192(sp)
    sprb	sp, 8191(sp)
    sprb	sb, 8191(sp)
    #
    sprb	usp, -536870912(sp)
    sprb	cfg, -536870912(sp)
    sprb	psr, 536870911(sp)
    sprb	intbase, 536870911(sp)
    sprb	mod, -64(sp)
    #
    # Static Memory (sb)
    #
    sprb	us, -64(sb)
    sprb	dcr, -64(sb)
    sprb	bpc, 63(sb)
    sprb	dsr, 63(sb)
    #
    sprb	car, -8192(sb)
    sprb	fp, -8192(sb)
    sprb	sp, 8191(sb)
    sprb	sb, 8191(sb)
    #
    sprb	usp, -536870912(sb)
    sprb	cfg, -536870912(sb)
    sprb	psr, 536870911(sb)
    sprb	intbase, 536870911(sb)
    sprb	mod, -64(sb)
    #
    # Program Counter (pc)
    #
    sprb	us, -64(pc)
    sprb	dcr, -64(pc)
    sprb	bpc, 63(pc)
    sprb	dsr, 63(pc)
    #
    sprb	car, -8192(pc)
    sprb	fp, -8192(pc)
    sprb	sp, 8191(pc)
    sprb	sb, 8191(pc)
    #
    sprb	usp, -536870912(pc)
    sprb	cfg, -536870912(pc)
    sprb	psr, 536870911(pc)
    sprb	intbase, 536870911(pc)
    sprb	mod, -64(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    sprb   us, r7[r0:b]
    sprb   dcr, r6[r1:b]
    #
    # Register Relative
    sprb   bpc, 63(r5)[r2:b]
    sprb   dsr, 63(r4)[r3:b]
    sprb   car, 8191(r3)[r4:b]
    sprb   fp, 8191(r2)[r5:b]
    sprb   sp, 536870911(r1)[r6:b]
    sprb   sb, 536870911(r0)[r7:b]
    #
    # Memory Relative
    sprb   usp, -64(63(fp))[r0:b]
    sprb   cfg, -64(63(fp))[r1:b]
    sprb   psr, -8192(8191(fp))[r2:b]
    sprb   intbase, -8192(8191(fp))[r3:b]
    sprb   mod, -536870912(536870911(fp))[r4:b]
    sprb   us, -536870912(536870911(fp))[r5:b]
    sprb   dcr, -64(63(sp))[r7:b]
    sprb   bpc, -64(63(sp))[r6:b]
    sprb   dsr, -8192(8191(sp))[r5:b]
    sprb   car, -8192(8191(sp))[r4:b]
    sprb   fp, -536870912(536870911(sp))[r3:b]
    sprb   sp, -536870912(536870911(sp))[r2:b]
    sprb   sb, -64(63(sb))[r0:b]
    sprb   usp, -64(63(sb))[r1:b]
    sprb   cfg, -8192(8191(sb))[r2:b]
    sprb   psr, -8192(8191(sb))[r3:b]
    sprb   intbase, -536870912(536870911(sb))[r4:b]
    sprb   mod, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    sprb   us, @63[r0:b]
    sprb   dcr, @63[r1:b]
    sprb   bpc, @8191[r2:b]
    sprb   dsr, @8191[r3:b]
    sprb   car, @536870911[r4:b]
    sprb   fp, @536870911[r5:b]
    #
    # External
    sprb   sp, ext(-64) + 63[r7:b]
    sprb   sb, ext(-64) + 63[r6:b]
    sprb   usp, ext(-8192) + 8191[r5:b]
    sprb   cfg, ext(-8192) + 8191[r4:b]
    sprb   psr, ext(-536870912) + 536870911[r3:b]
    sprb   intbase, ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    sprb   mod, tos[r0:b]
    #
    # Memory Space
    sprb   us, 63(fp)[r0:b]
    sprb   dcr, 63(fp)[r0:b]
    sprb   bpc, 8191(fp)[r0:b]
    sprb   dsr, 8191(fp)[r0:b]
    sprb   car, 536870911(fp)[r0:b]
    sprb   fp, 536870911(fp)[r0:b]
    sprb   sp, 63(sp)[r0:b]
    sprb   sb, 63(sp)[r0:b]
    sprb   usp, 8191(sp)[r0:b]
    sprb   cfg, 8191(sp)[r0:b]
    sprb   psr, 536870911(sp)[r0:b]
    sprb   intbase, 536870911(sp)[r0:b]
    sprb   mod, 63(sb)[r0:b]
    sprb   us, 63(sb)[r0:b]
    sprb   dcr, 8191(sb)[r0:b]
    sprb   bpc, 8191(sb)[r0:b]
    sprb   dsr, 536870911(sb)[r0:b]
    sprb   car, 536870911(sb)[r0:b]
    sprb   fp, 63(pc)[r0:b]
    sprb   sp, 63(pc)[r0:b]
    sprb   sb, 8191(pc)[r0:b]
    sprb   usp, 8191(pc)[r0:b]
    sprb   cfg, 536870911(pc)[r0:b]
    sprb   psr, 536870911(pc)[r0:b]
    #
    # WORD
    #
    sprb   us, r7[r0:w]
    sprb   dcr, r6[r1:w]
    #
    # Register Relative
    sprb   bpc, 63(r5)[r2:w]
    sprb   dsr, 63(r4)[r3:w]
    sprb   car, 8191(r3)[r4:w]
    sprb   fp, 8191(r2)[r5:w]
    sprb   sp, 536870911(r1)[r6:w]
    sprb   sb, 536870911(r0)[r7:w]
    #
    # Memory Relative
    sprb   usp, -64(63(fp))[r0:w]
    sprb   cfg, -64(63(fp))[r1:w]
    sprb   psr, -8192(8191(fp))[r2:w]
    sprb   intbase, -8192(8191(fp))[r3:w]
    sprb   mod, -536870912(536870911(fp))[r4:w]
    sprb   us, -536870912(536870911(fp))[r5:w]
    sprb   dcr, -64(63(sp))[r7:w]
    sprb   bpc, -64(63(sp))[r6:w]
    sprb   dsr, -8192(8191(sp))[r5:w]
    sprb   car, -8192(8191(sp))[r4:w]
    sprb   fp, -536870912(536870911(sp))[r3:w]
    sprb   sp, -536870912(536870911(sp))[r2:w]
    sprb   sb, -64(63(sb))[r0:w]
    sprb   usp, -64(63(sb))[r1:w]
    sprb   cfg, -8192(8191(sb))[r2:w]
    sprb   psr, -8192(8191(sb))[r3:w]
    sprb   intbase, -536870912(536870911(sb))[r4:w]
    sprb   mod, -536870912(536870911(sb))[r5:w]
    #
    # Absolute
    sprb   us, @63[r0:w]
    sprb   dcr, @63[r1:w]
    sprb   bpc, @8191[r2:w]
    sprb   dsr, @8191[r3:w]
    sprb   car, @536870911[r4:w]
    sprb   fp, @536870911[r5:w]
    #
    # External
    sprb   sp, ext(-64) + 63[r7:w]
    sprb   sb, ext(-64) + 63[r6:w]
    sprb   usp, ext(-8192) + 8191[r5:w]
    sprb   cfg, ext(-8192) + 8191[r4:w]
    sprb   psr, ext(-536870912) + 536870911[r3:w]
    sprb   intbase, ext(-536870912) + 536870911[r2:w]
    #
    # Top Of Stack
    sprb   mod, tos[r0:w]
    #
    # Memory Space
    sprb   us, 63(fp)[r0:w]
    sprb   dcr, 63(fp)[r0:w]
    sprb   bpc, 8191(fp)[r0:w]
    sprb   dsr, 8191(fp)[r0:w]
    sprb   car, 536870911(fp)[r0:w]
    sprb   fp, 536870911(fp)[r0:w]
    sprb   sp, 63(sp)[r0:w]
    sprb   sb, 63(sp)[r0:w]
    sprb   usp, 8191(sp)[r0:w]
    sprb   cfg, 8191(sp)[r0:w]
    sprb   psr, 536870911(sp)[r0:w]
    sprb   intbase, 536870911(sp)[r0:w]
    sprb   mod, 63(sb)[r0:w]
    sprb   us, 63(sb)[r0:w]
    sprb   dcr, 8191(sb)[r0:w]
    sprb   bpc, 8191(sb)[r0:w]
    sprb   dsr, 536870911(sb)[r0:w]
    sprb   car, 536870911(sb)[r0:w]
    sprb   fp, 63(pc)[r0:w]
    sprb   sp, 63(pc)[r0:w]
    sprb   sb, 8191(pc)[r0:w]
    sprb   usp, 8191(pc)[r0:w]
    sprb   cfg, 536870911(pc)[r0:w]
    sprb   psr, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    sprb   us, r7[r0:d]
    sprb   dcr, r6[r1:d]
    #
    # Register Relative
    sprb   bpc, 63(r5)[r2:d]
    sprb   dsr, 63(r4)[r3:d]
    sprb   car, 8191(r3)[r4:d]
    sprb   fp, 8191(r2)[r5:d]
    sprb   sp, 536870911(r1)[r6:d]
    sprb   sb, 536870911(r0)[r7:d]
    #
    # Memory Relative
    sprb   usp, -64(63(fp))[r0:d]
    sprb   cfg, -64(63(fp))[r1:d]
    sprb   psr, -8192(8191(fp))[r2:d]
    sprb   intbase, -8192(8191(fp))[r3:d]
    sprb   mod, -536870912(536870911(fp))[r4:d]
    sprb   us, -536870912(536870911(fp))[r5:d]
    sprb   dcr, -64(63(sp))[r7:d]
    sprb   bpc, -64(63(sp))[r6:d]
    sprb   dsr, -8192(8191(sp))[r5:d]
    sprb   car, -8192(8191(sp))[r4:d]
    sprb   fp, -536870912(536870911(sp))[r3:d]
    sprb   sp, -536870912(536870911(sp))[r2:d]
    sprb   sb, -64(63(sb))[r0:d]
    sprb   usp, -64(63(sb))[r1:d]
    sprb   cfg, -8192(8191(sb))[r2:d]
    sprb   psr, -8192(8191(sb))[r3:d]
    sprb   intbase, -536870912(536870911(sb))[r4:d]
    sprb   mod, -536870912(536870911(sb))[r5:d]
    #
    # Absolute
    sprb   us, @63[r0:d]
    sprb   dcr, @63[r1:d]
    sprb   bpc, @8191[r2:d]
    sprb   dsr, @8191[r3:d]
    sprb   car, @536870911[r4:d]
    sprb   fp, @536870911[r5:d]
    #
    # External
    sprb   sp, ext(-64) + 63[r7:d]
    sprb   sb, ext(-64) + 63[r6:d]
    sprb   usp, ext(-8192) + 8191[r5:d]
    sprb   cfg, ext(-8192) + 8191[r4:d]
    sprb   psr, ext(-536870912) + 536870911[r3:d]
    sprb   intbase, ext(-536870912) + 536870911[r2:d]
    #
    # Top Of Stack
    sprb   mod, tos[r0:b]
    #
    # Memory Space
    sprb   us, 63(fp)[r0:d]
    sprb   dcr, 63(fp)[r0:d]
    sprb   bpc, 8191(fp)[r0:d]
    sprb   dsr, 8191(fp)[r0:d]
    sprb   car, 536870911(fp)[r0:d]
    sprb   fp, 536870911(fp)[r0:d]
    sprb   sp, 63(sp)[r0:d]
    sprb   sb, 63(sp)[r0:d]
    sprb   usp, 8191(sp)[r0:d]
    sprb   cfg, 8191(sp)[r0:d]
    sprb   psr, 536870911(sp)[r0:d]
    sprb   intbase, 536870911(sp)[r0:d]
    sprb   mod, 63(sb)[r0:d]
    sprb   us, 63(sb)[r0:d]
    sprb   dcr, 8191(sb)[r0:d]
    sprb   bpc, 8191(sb)[r0:d]
    sprb   dsr, 536870911(sb)[r0:d]
    sprb   car, 536870911(sb)[r0:d]
    sprb   fp, 63(pc)[r0:d]
    sprb   sp, 63(pc)[r0:d]
    sprb   sb, 8191(pc)[r0:d]
    sprb   usp, 8191(pc)[r0:d]
    sprb   cfg, 536870911(pc)[r0:d]
    sprb   psr, 536870911(pc)[r0:d]





#
# SPR WORD
#
#
# Register
#
    sprw    us, r0
    sprw    dcr, r1
    sprw    bpc, r2
    sprw    dsr, r3
    sprw    car, r4
    sprw    fp, r5
    sprw    sp, r6
    sprw    sb, r7
    sprw    usp, r0
    sprw    cfg, r1
    sprw    psr, r2
    sprw    intbase, r3
    sprw    mod, r4
#
# Register relative
#
    sprw   us, 63(r0)
    sprw   us, -64(r0)
    sprw   us, 8191(r0)
    sprw   us, -8192(r0)
    sprw   us, 536870911(r0)
    sprw   us, -536870912(r0)
    #
    sprw   dcr, 63(r1)
    sprw   dcr, -64(r1)
    sprw   dcr, 8191(r1)
    sprw   dcr, -8192(r1)
    sprw   dcr, 536870911(r1)
    sprw   dcr, -536870912(r1)
    #
    sprw   bpc, 63(r2)
    sprw   bpc, -64(r2)
    sprw   bpc, 8191(r2)
    sprw   bpc, -8192(r2)
    sprw   bpc, 536870911(r2)
    sprw   bpc, -536870912(r2)
    #
    sprw   dsr, 63(r3)
    sprw   dsr, -64(r3)
    sprw   dsr, 8191(r3)
    sprw   dsr, -8192(r3)
    sprw   dsr, 536870911(r3)
    sprw   dsr, -536870912(r3)
    #
    sprw   car, 63(r4)
    sprw   car, -64(r4)
    sprw   car, 8191(r4)
    sprw   car, -8192(r4)
    sprw   car, 536870911(r4)
    sprw   car, -536870912(r4)
    #
    sprw   fp, 63(r5)
    sprw   fp, -64(r5)
    sprw   fp, 8191(r5)
    sprw   fp, -8192(r5)
    sprw   fp, 536870911(r5)
    sprw   fp, -536870912(r5)
    #
    sprw   sp, 63(r6)
    sprw   sp, -64(r6)
    sprw   sp, 8191(r6)
    sprw   sp, -8192(r6)
    sprw   sp, 536870911(r6)
    sprw   sp, -536870912(r6)
    #
    sprw   sb, 63(r7)
    sprw   sb, -64(r7)
    sprw   sb, 8191(r7)
    sprw   sb, -8192(r7)
    sprw   sb, 536870911(r7)
    sprw   sb, -536870912(r7)
    #
    sprw   usp, 63(r7)
    sprw   usp, -64(r7)
    sprw   usp, 8191(r7)
    sprw   usp, -8192(r7)
    sprw   usp, 536870911(r7)
    sprw   usp, -536870912(r7)
    #
    sprw   cfg, 63(r7)
    sprw   cfg, -64(r7)
    sprw   cfg, 8191(r7)
    sprw   cfg, -8192(r7)
    sprw   cfg, 536870911(r7)
    sprw   cfg, -536870912(r7)
    #
    sprw   psr, 63(r7)
    sprw   psr, -64(r7)
    sprw   psr, 8191(r7)
    sprw   psr, -8192(r7)
    sprw   psr, 536870911(r7)
    sprw   psr, -536870912(r7)
    #
    sprw   intbase, 63(r7)
    sprw   intbase, -64(r7)
    sprw   intbase, 8191(r7)
    sprw   intbase, -8192(r7)
    sprw   intbase, 536870911(r7)
    sprw   intbase, -536870912(r7)
    #
    sprw   mod, 63(r7)
    sprw   mod, -64(r7)
    sprw   mod, 8191(r7)
    sprw   mod, -8192(r7)
    sprw   mod, 536870911(r7)
    sprw   mod, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    sprw   us, -64(63(fp))
    sprw   dcr, -64(63(fp))
    sprw   bpc, 63(-64(fp))
    sprw   dsr, 63(-64(fp))
    #
    sprw   car, -8192(63(fp))
    sprw   fp, -64(8191(fp))
    sprw   sp, 63(-8192(fp))
    sprw   sb, 8191(-64(fp))
    #
    sprw   usp, -8192(8191(fp))
    sprw   cfg, -8192(8191(fp))
    sprw   psr, 8191(-8192(fp))
    sprw   intbase, 8191(-8192(fp))
    #
    sprw   mod, -536870912(8191(fp))
    sprw   us, -8192(536870911(fp))
    sprw   dcr, 8191(-536870912(fp))
    sprw   bpc, 536870911(-8192(fp))
    #
    sprw   dsr, 536870911(-536870912(fp))
    sprw   car, 536870911(-536870912(fp))
    sprw   fp, -536870912(536870911(fp))
    sprw   sb, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    sprw   us, -64(63(sp))
    sprw   dcr, -64(63(sp))
    sprw   bpc, 63(-64(sp))
    sprw   dsr, 63(-64(sp))
    
    sprw   car, -8192(63(sp))
    sprw   fp, -64(8191(sp))
    sprw   sp, 63(-8192(sp))
    sprw   sb, 8191(-64(sp))
    #
    sprw   usp, -8192(8191(sp))
    sprw   cfg, -8192(8191(sp))
    sprw   psr, 8191(-8192(sp))
    sprw   intbase, 8191(-8192(sp))
    #
    sprw   mod, -536870912(8191(sp))
    sprw   us, -8192(536870911(sp))
    sprw   dcr, 8191(-536870912(sp))
    sprw   bpc, 536870911(-8192(sp))
    #
    sprw   dsr, 536870911(-536870912(sp))
    sprw   car, 536870911(-536870912(sp))
    sprw   fp, -536870912(536870911(sp))
    sprw   sb, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    sprw   us, -64(63(sb))
    sprw   dcr, -64(63(sb))
    sprw   bpc, 63(-64(sb))
    sprw   dsr, 63(-64(sb))
    #
    sprw   car, -8192(63(sb))
    sprw   fp, -64(8191(sb))
    sprw   sp, 63(-8192(sb))
    sprw   sb, 8191(-64(sb))
    #
    sprw   usp, -8192(8191(sb))
    sprw   cfg, -8192(8191(sb))
    sprw   psr, 8191(-8192(sb))
    sprw   intbase, 8191(-8192(sb))
    #
    sprw   mod, -536870912(8191(sb))
    sprw   us, -8192(536870911(sb))
    sprw   dcr, 8191(-536870912(sb))
    sprw   bpc, 536870911(-8192(sb))
    #
    sprw   dsr, 536870911(-536870912(sb))
    sprw   car, 536870911(-536870912(sb))
    sprw   fp, -536870912(536870911(sb))
    sprw   sb, -536870912(536870911(sb))
# 
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    sprw	us, @-64
    sprw	dcr, @-64
    sprw	bpc, @63
    sprw	dsr, @63
    sprw	car, @-65
    sprw	fp, @-65
    sprw	sp, @64
    sprw	sb, @64
    #
    sprw	usp, @-8192
    sprw	cfg, @-8192
    sprw	psr, @8191
    sprw	intbase, @8191
    sprw	mod, @-8193
    sprw	us, @-8193
    sprw	dcr, @8192
    sprw	bpc, @8192
    #
    sprw	dsr, @-536870912
    sprw	car, @536870911
#
# External
#
    sprw	us, ext(-64) + 63
    sprw	dcr, ext(-64) + 63
    sprw	bpc, ext(63) + -64
    sprw	dsr, ext(63) + -64
    #
    sprw	car, ext(-65) + 63
    sprw	fp, ext(-65) + 63
    sprw	sp, ext(63) + -65
    sprw	sb, ext(63) + -65
    sprw	usp, ext(-64) + 64
    sprw	cfg, ext(-64) + 64
    sprw	psr, ext(64) + -64
    sprw	intbase, ext(64) + -64
    #
    sprw	mod, ext(-8192) + 8191
    sprw	us, ext(-8192) + 8191
    sprw	dcr, ext(8191) + -8192
    sprw	bpc, ext(8191) + -8192
    #
    sprw	dsr, ext(-536870912) + 8191
    sprw	car, ext(-536870912) + 8191
    sprw	fp, ext(8191) + -536870912
    sprw	sp, ext(8191) + -536870912
    sprw	sb, ext(-8192) + 536870911
    sprw	usp, ext(-8192) + 536870911
    sprw	cfg, ext(536870911) + -8192
    sprw	psr, ext(536870911) + -8192
    #
    sprw	intbase, ext(-536870912) + 536870911
    sprw	mod, ext(-536870912) + 536870911
    sprw	us, ext(536870911) + -536870912
    sprw	dcr, ext(536870911) + -536870912
#
# Top Of Stack
#
    sprw    us, tos
    sprw    dcr, tos
    sprw    bpc, tos
    sprw    dsr, tos
    sprw    car, tos
    sprw    fp, tos
    sprw    sp, tos
    sprw    sb, tos
    sprw    usp, tos
    sprw    cfg, tos
    sprw    psr, tos
    sprw    intbase, tos
    sprw    mod, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    sprw	us, -64(fp)
    sprw	dcr, -64(fp)
    sprw	bpc, 63(fp)
    sprw	dsr, 63(fp)
    #
    sprw	car, -8192(fp)
    sprw	fp, -8192(fp)
    sprw	sp, 8191(fp)
    sprw	sb, 8191(fp)
    #
    sprw	usp, -536870912(fp)
    sprw	cfg, -536870912(fp)
    sprw	psr, 536870911(fp)
    sprw	intbase, 536870911(fp)
    sprw	mod, -64(fp)
    #
    # Stack Pointer (sp)
    #
    sprw	us, -64(sp)
    sprw	dcr, -64(sp)
    sprw	bpc, 63(sp)
    sprw	dsr, 63(sp)
    #
    sprw	car, -8192(sp)
    sprw	fp, -8192(sp)
    sprw	sp, 8191(sp)
    sprw	sb, 8191(sp)
    #
    sprw	usp, -536870912(sp)
    sprw	cfg, -536870912(sp)
    sprw	psr, 536870911(sp)
    sprw	intbase, 536870911(sp)
    sprw	mod, -64(sp)
    #
    # Static Memory (sb)
    #
    sprw	us, -64(sb)
    sprw	dcr, -64(sb)
    sprw	bpc, 63(sb)
    sprw	dsr, 63(sb)
    #
    sprw	car, -8192(sb)
    sprw	fp, -8192(sb)
    sprw	sp, 8191(sb)
    sprw	sb, 8191(sb)
    #
    sprw	usp, -536870912(sb)
    sprw	cfg, -536870912(sb)
    sprw	psr, 536870911(sb)
    sprw	intbase, 536870911(sb)
    sprw	mod, -64(sb)
    #
    # Program Counter (pc)
    #
    sprw	us, -64(pc)
    sprw	dcr, -64(pc)
    sprw	bpc, 63(pc)
    sprw	dsr, 63(pc)
    #
    sprw	car, -8192(pc)
    sprw	fp, -8192(pc)
    sprw	sp, 8191(pc)
    sprw	sb, 8191(pc)
    #
    sprw	usp, -536870912(pc)
    sprw	cfg, -536870912(pc)
    sprw	psr, 536870911(pc)
    sprw	intbase, 536870911(pc)
    sprw	mod, -64(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    sprw   us, r7[r0:b]
    sprw   dcr, r6[r1:b]
    #
    # Register Relative
    sprw   bpc, 63(r5)[r2:b]
    sprw   dsr, 63(r4)[r3:b]
    sprw   car, 8191(r3)[r4:b]
    sprw   fp, 8191(r2)[r5:b]
    sprw   sp, 536870911(r1)[r6:b]
    sprw   sb, 536870911(r0)[r7:b]
    #
    # Memory Relative
    sprw   usp, -64(63(fp))[r0:b]
    sprw   cfg, -64(63(fp))[r1:b]
    sprw   psr, -8192(8191(fp))[r2:b]
    sprw   intbase, -8192(8191(fp))[r3:b]
    sprw   mod, -536870912(536870911(fp))[r4:b]
    sprw   us, -536870912(536870911(fp))[r5:b]
    sprw   dcr, -64(63(sp))[r7:b]
    sprw   bpc, -64(63(sp))[r6:b]
    sprw   dsr, -8192(8191(sp))[r5:b]
    sprw   car, -8192(8191(sp))[r4:b]
    sprw   fp, -536870912(536870911(sp))[r3:b]
    sprw   sp, -536870912(536870911(sp))[r2:b]
    sprw   sb, -64(63(sb))[r0:b]
    sprw   usp, -64(63(sb))[r1:b]
    sprw   cfg, -8192(8191(sb))[r2:b]
    sprw   psr, -8192(8191(sb))[r3:b]
    sprw   intbase, -536870912(536870911(sb))[r4:b]
    sprw   mod, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    sprw   us, @63[r0:b]
    sprw   dcr, @63[r1:b]
    sprw   bpc, @8191[r2:b]
    sprw   dsr, @8191[r3:b]
    sprw   car, @536870911[r4:b]
    sprw   fp, @536870911[r5:b]
    #
    # External
    sprw   sp, ext(-64) + 63[r7:b]
    sprw   sb, ext(-64) + 63[r6:b]
    sprw   usp, ext(-8192) + 8191[r5:b]
    sprw   cfg, ext(-8192) + 8191[r4:b]
    sprw   psr, ext(-536870912) + 536870911[r3:b]
    sprw   intbase, ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    sprw   mod, tos[r0:b]
    #
    # Memory Space
    sprw   us, 63(fp)[r0:b]
    sprw   dcr, 63(fp)[r0:b]
    sprw   bpc, 8191(fp)[r0:b]
    sprw   dsr, 8191(fp)[r0:b]
    sprw   car, 536870911(fp)[r0:b]
    sprw   fp, 536870911(fp)[r0:b]
    sprw   sp, 63(sp)[r0:b]
    sprw   sb, 63(sp)[r0:b]
    sprw   usp, 8191(sp)[r0:b]
    sprw   cfg, 8191(sp)[r0:b]
    sprw   psr, 536870911(sp)[r0:b]
    sprw   intbase, 536870911(sp)[r0:b]
    sprw   mod, 63(sb)[r0:b]
    sprw   us, 63(sb)[r0:b]
    sprw   dcr, 8191(sb)[r0:b]
    sprw   bpc, 8191(sb)[r0:b]
    sprw   dsr, 536870911(sb)[r0:b]
    sprw   car, 536870911(sb)[r0:b]
    sprw   fp, 63(pc)[r0:b]
    sprw   sp, 63(pc)[r0:b]
    sprw   sb, 8191(pc)[r0:b]
    sprw   usp, 8191(pc)[r0:b]
    sprw   cfg, 536870911(pc)[r0:b]
    sprw   psr, 536870911(pc)[r0:b]
    #
    # WORD
    #
    sprw   us, r7[r0:w]
    sprw   dcr, r6[r1:w]
    #
    # Register Relative
    sprw   bpc, 63(r5)[r2:w]
    sprw   dsr, 63(r4)[r3:w]
    sprw   car, 8191(r3)[r4:w]
    sprw   fp, 8191(r2)[r5:w]
    sprw   sp, 536870911(r1)[r6:w]
    sprw   sb, 536870911(r0)[r7:w]
    #
    # Memory Relative
    sprw   usp, -64(63(fp))[r0:w]
    sprw   cfg, -64(63(fp))[r1:w]
    sprw   psr, -8192(8191(fp))[r2:w]
    sprw   intbase, -8192(8191(fp))[r3:w]
    sprw   mod, -536870912(536870911(fp))[r4:w]
    sprw   us, -536870912(536870911(fp))[r5:w]
    sprw   dcr, -64(63(sp))[r7:w]
    sprw   bpc, -64(63(sp))[r6:w]
    sprw   dsr, -8192(8191(sp))[r5:w]
    sprw   car, -8192(8191(sp))[r4:w]
    sprw   fp, -536870912(536870911(sp))[r3:w]
    sprw   sp, -536870912(536870911(sp))[r2:w]
    sprw   sb, -64(63(sb))[r0:w]
    sprw   usp, -64(63(sb))[r1:w]
    sprw   cfg, -8192(8191(sb))[r2:w]
    sprw   psr, -8192(8191(sb))[r3:w]
    sprw   intbase, -536870912(536870911(sb))[r4:w]
    sprw   mod, -536870912(536870911(sb))[r5:w]
    #
    # Absolute
    sprw   us, @63[r0:w]
    sprw   dcr, @63[r1:w]
    sprw   bpc, @8191[r2:w]
    sprw   dsr, @8191[r3:w]
    sprw   car, @536870911[r4:w]
    sprw   fp, @536870911[r5:w]
    #
    # External
    sprw   sp, ext(-64) + 63[r7:w]
    sprw   sb, ext(-64) + 63[r6:w]
    sprw   usp, ext(-8192) + 8191[r5:w]
    sprw   cfg, ext(-8192) + 8191[r4:w]
    sprw   psr, ext(-536870912) + 536870911[r3:w]
    sprw   intbase, ext(-536870912) + 536870911[r2:w]
    #
    # Top Of Stack
    sprw   mod, tos[r0:w]
    #
    # Memory Space
    sprw   us, 63(fp)[r0:w]
    sprw   dcr, 63(fp)[r0:w]
    sprw   bpc, 8191(fp)[r0:w]
    sprw   dsr, 8191(fp)[r0:w]
    sprw   car, 536870911(fp)[r0:w]
    sprw   fp, 536870911(fp)[r0:w]
    sprw   sp, 63(sp)[r0:w]
    sprw   sb, 63(sp)[r0:w]
    sprw   usp, 8191(sp)[r0:w]
    sprw   cfg, 8191(sp)[r0:w]
    sprw   psr, 536870911(sp)[r0:w]
    sprw   intbase, 536870911(sp)[r0:w]
    sprw   mod, 63(sb)[r0:w]
    sprw   us, 63(sb)[r0:w]
    sprw   dcr, 8191(sb)[r0:w]
    sprw   bpc, 8191(sb)[r0:w]
    sprw   dsr, 536870911(sb)[r0:w]
    sprw   car, 536870911(sb)[r0:w]
    sprw   fp, 63(pc)[r0:w]
    sprw   sp, 63(pc)[r0:w]
    sprw   sb, 8191(pc)[r0:w]
    sprw   usp, 8191(pc)[r0:w]
    sprw   cfg, 536870911(pc)[r0:w]
    sprw   psr, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    sprw   us, r7[r0:d]
    sprw   dcr, r6[r1:d]
    #
    # Register Relative
    sprw   bpc, 63(r5)[r2:d]
    sprw   dsr, 63(r4)[r3:d]
    sprw   car, 8191(r3)[r4:d]
    sprw   fp, 8191(r2)[r5:d]
    sprw   sp, 536870911(r1)[r6:d]
    sprw   sb, 536870911(r0)[r7:d]
    #
    # Memory Relative
    sprw   usp, -64(63(fp))[r0:d]
    sprw   cfg, -64(63(fp))[r1:d]
    sprw   psr, -8192(8191(fp))[r2:d]
    sprw   intbase, -8192(8191(fp))[r3:d]
    sprw   mod, -536870912(536870911(fp))[r4:d]
    sprw   us, -536870912(536870911(fp))[r5:d]
    sprw   dcr, -64(63(sp))[r7:d]
    sprw   bpc, -64(63(sp))[r6:d]
    sprw   dsr, -8192(8191(sp))[r5:d]
    sprw   car, -8192(8191(sp))[r4:d]
    sprw   fp, -536870912(536870911(sp))[r3:d]
    sprw   sp, -536870912(536870911(sp))[r2:d]
    sprw   sb, -64(63(sb))[r0:d]
    sprw   usp, -64(63(sb))[r1:d]
    sprw   cfg, -8192(8191(sb))[r2:d]
    sprw   psr, -8192(8191(sb))[r3:d]
    sprw   intbase, -536870912(536870911(sb))[r4:d]
    sprw   mod, -536870912(536870911(sb))[r5:d]
    #
    # Absolute
    sprw   us, @63[r0:d]
    sprw   dcr, @63[r1:d]
    sprw   bpc, @8191[r2:d]
    sprw   dsr, @8191[r3:d]
    sprw   car, @536870911[r4:d]
    sprw   fp, @536870911[r5:d]
    #
    # External
    sprw   sp, ext(-64) + 63[r7:d]
    sprw   sb, ext(-64) + 63[r6:d]
    sprw   usp, ext(-8192) + 8191[r5:d]
    sprw   cfg, ext(-8192) + 8191[r4:d]
    sprw   psr, ext(-536870912) + 536870911[r3:d]
    sprw   intbase, ext(-536870912) + 536870911[r2:d]
    #
    # Top Of Stack
    sprw   mod, tos[r0:b]
    #
    # Memory Space
    sprw   us, 63(fp)[r0:d]
    sprw   dcr, 63(fp)[r0:d]
    sprw   bpc, 8191(fp)[r0:d]
    sprw   dsr, 8191(fp)[r0:d]
    sprw   car, 536870911(fp)[r0:d]
    sprw   fp, 536870911(fp)[r0:d]
    sprw   sp, 63(sp)[r0:d]
    sprw   sb, 63(sp)[r0:d]
    sprw   usp, 8191(sp)[r0:d]
    sprw   cfg, 8191(sp)[r0:d]
    sprw   psr, 536870911(sp)[r0:d]
    sprw   intbase, 536870911(sp)[r0:d]
    sprw   mod, 63(sb)[r0:d]
    sprw   us, 63(sb)[r0:d]
    sprw   dcr, 8191(sb)[r0:d]
    sprw   bpc, 8191(sb)[r0:d]
    sprw   dsr, 536870911(sb)[r0:d]
    sprw   car, 536870911(sb)[r0:d]
    sprw   fp, 63(pc)[r0:d]
    sprw   sp, 63(pc)[r0:d]
    sprw   sb, 8191(pc)[r0:d]
    sprw   usp, 8191(pc)[r0:d]
    sprw   cfg, 536870911(pc)[r0:d]
    sprw   psr, 536870911(pc)[r0:d]
#
# SPR DOUBLE WORD
#
#
# Register
#
    sprd    us, r0
    sprd    dcr, r1
    sprd    bpc, r2
    sprd    dsr, r3
    sprd    car, r4
    sprd    fp, r5
    sprd    sp, r6
    sprd    sb, r7
    sprd    usp, r0
    sprd    cfg, r1
    sprd    psr, r2
    sprd    intbase, r3
    sprd    mod, r4
#
# Register relative
#
    sprd   us, 63(r0)
    sprd   us, -64(r0)
    sprd   us, 8191(r0)
    sprd   us, -8192(r0)
    sprd   us, 536870911(r0)
    sprd   us, -536870912(r0)
    #
    sprd   dcr, 63(r1)
    sprd   dcr, -64(r1)
    sprd   dcr, 8191(r1)
    sprd   dcr, -8192(r1)
    sprd   dcr, 536870911(r1)
    sprd   dcr, -536870912(r1)
    #
    sprd   bpc, 63(r2)
    sprd   bpc, -64(r2)
    sprd   bpc, 8191(r2)
    sprd   bpc, -8192(r2)
    sprd   bpc, 536870911(r2)
    sprd   bpc, -536870912(r2)
    #
    sprd   dsr, 63(r3)
    sprd   dsr, -64(r3)
    sprd   dsr, 8191(r3)
    sprd   dsr, -8192(r3)
    sprd   dsr, 536870911(r3)
    sprd   dsr, -536870912(r3)
    #
    sprd   car, 63(r4)
    sprd   car, -64(r4)
    sprd   car, 8191(r4)
    sprd   car, -8192(r4)
    sprd   car, 536870911(r4)
    sprd   car, -536870912(r4)
    #
    sprd   fp, 63(r5)
    sprd   fp, -64(r5)
    sprd   fp, 8191(r5)
    sprd   fp, -8192(r5)
    sprd   fp, 536870911(r5)
    sprd   fp, -536870912(r5)
    #
    sprd   sp, 63(r6)
    sprd   sp, -64(r6)
    sprd   sp, 8191(r6)
    sprd   sp, -8192(r6)
    sprd   sp, 536870911(r6)
    sprd   sp, -536870912(r6)
    #
    sprd   sb, 63(r7)
    sprd   sb, -64(r7)
    sprd   sb, 8191(r7)
    sprd   sb, -8192(r7)
    sprd   sb, 536870911(r7)
    sprd   sb, -536870912(r7)
    #
    sprd   usp, 63(r7)
    sprd   usp, -64(r7)
    sprd   usp, 8191(r7)
    sprd   usp, -8192(r7)
    sprd   usp, 536870911(r7)
    sprd   usp, -536870912(r7)
    #
    sprd   cfg, 63(r7)
    sprd   cfg, -64(r7)
    sprd   cfg, 8191(r7)
    sprd   cfg, -8192(r7)
    sprd   cfg, 536870911(r7)
    sprd   cfg, -536870912(r7)
    #
    sprd   psr, 63(r7)
    sprd   psr, -64(r7)
    sprd   psr, 8191(r7)
    sprd   psr, -8192(r7)
    sprd   psr, 536870911(r7)
    sprd   psr, -536870912(r7)
    #
    sprd   intbase, 63(r7)
    sprd   intbase, -64(r7)
    sprd   intbase, 8191(r7)
    sprd   intbase, -8192(r7)
    sprd   intbase, 536870911(r7)
    sprd   intbase, -536870912(r7)
    #
    sprd   mod, 63(r7)
    sprd   mod, -64(r7)
    sprd   mod, 8191(r7)
    sprd   mod, -8192(r7)
    sprd   mod, 536870911(r7)
    sprd   mod, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    sprd   us, -64(63(fp))
    sprd   dcr, -64(63(fp))
    sprd   bpc, 63(-64(fp))
    sprd   dsr, 63(-64(fp))
    #
    sprd   car, -8192(63(fp))
    sprd   fp, -64(8191(fp))
    sprd   sp, 63(-8192(fp))
    sprd   sb, 8191(-64(fp))
    #
    sprd   usp, -8192(8191(fp))
    sprd   cfg, -8192(8191(fp))
    sprd   psr, 8191(-8192(fp))
    sprd   intbase, 8191(-8192(fp))
    #
    sprd   mod, -536870912(8191(fp))
    sprd   us, -8192(536870911(fp))
    sprd   dcr, 8191(-536870912(fp))
    sprd   bpc, 536870911(-8192(fp))
    #
    sprd   dsr, 536870911(-536870912(fp))
    sprd   car, 536870911(-536870912(fp))
    sprd   fp, -536870912(536870911(fp))
    sprd   sb, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    sprd   us, -64(63(sp))
    sprd   dcr, -64(63(sp))
    sprd   bpc, 63(-64(sp))
    sprd   dsr, 63(-64(sp))
    
    sprd   car, -8192(63(sp))
    sprd   fp, -64(8191(sp))
    sprd   sp, 63(-8192(sp))
    sprd   sb, 8191(-64(sp))
    #
    sprd   usp, -8192(8191(sp))
    sprd   cfg, -8192(8191(sp))
    sprd   psr, 8191(-8192(sp))
    sprd   intbase, 8191(-8192(sp))
    #
    sprd   mod, -536870912(8191(sp))
    sprd   us, -8192(536870911(sp))
    sprd   dcr, 8191(-536870912(sp))
    sprd   bpc, 536870911(-8192(sp))
    #
    sprd   dsr, 536870911(-536870912(sp))
    sprd   car, 536870911(-536870912(sp))
    sprd   fp, -536870912(536870911(sp))
    sprd   sb, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    sprd   us, -64(63(sb))
    sprd   dcr, -64(63(sb))
    sprd   bpc, 63(-64(sb))
    sprd   dsr, 63(-64(sb))
    #
    sprd   car, -8192(63(sb))
    sprd   fp, -64(8191(sb))
    sprd   sp, 63(-8192(sb))
    sprd   sb, 8191(-64(sb))
    #
    sprd   usp, -8192(8191(sb))
    sprd   cfg, -8192(8191(sb))
    sprd   psr, 8191(-8192(sb))
    sprd   intbase, 8191(-8192(sb))
    #
    sprd   mod, -536870912(8191(sb))
    sprd   us, -8192(536870911(sb))
    sprd   dcr, 8191(-536870912(sb))
    sprd   bpc, 536870911(-8192(sb))
    #
    sprd   dsr, 536870911(-536870912(sb))
    sprd   car, 536870911(-536870912(sb))
    sprd   fp, -536870912(536870911(sb))
    sprd   sb, -536870912(536870911(sb))
# 
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    sprd	us, @-64
    sprd	dcr, @-64
    sprd	bpc, @63
    sprd	dsr, @63
    sprd	car, @-65
    sprd	fp, @-65
    sprd	sp, @64
    sprd	sb, @64
    #
    sprd	usp, @-8192
    sprd	cfg, @-8192
    sprd	psr, @8191
    sprd	intbase, @8191
    sprd	mod, @-8193
    sprd	us, @-8193
    sprd	dcr, @8192
    sprd	bpc, @8192
    #
    sprd	dsr, @-536870912
    sprd	car, @536870911
#
# External
#
    sprd	us, ext(-64) + 63
    sprd	dcr, ext(-64) + 63
    sprd	bpc, ext(63) + -64
    sprd	dsr, ext(63) + -64
    #
    sprd	car, ext(-65) + 63
    sprd	fp, ext(-65) + 63
    sprd	sp, ext(63) + -65
    sprd	sb, ext(63) + -65
    sprd	usp, ext(-64) + 64
    sprd	cfg, ext(-64) + 64
    sprd	psr, ext(64) + -64
    sprd	intbase, ext(64) + -64
    #
    sprd	mod, ext(-8192) + 8191
    sprd	us, ext(-8192) + 8191
    sprd	dcr, ext(8191) + -8192
    sprd	bpc, ext(8191) + -8192
    #
    sprd	dsr, ext(-536870912) + 8191
    sprd	car, ext(-536870912) + 8191
    sprd	fp, ext(8191) + -536870912
    sprd	sp, ext(8191) + -536870912
    sprd	sb, ext(-8192) + 536870911
    sprd	usp, ext(-8192) + 536870911
    sprd	cfg, ext(536870911) + -8192
    sprd	psr, ext(536870911) + -8192
    #
    sprd	intbase, ext(-536870912) + 536870911
    sprd	mod, ext(-536870912) + 536870911
    sprd	us, ext(536870911) + -536870912
    sprd	dcr, ext(536870911) + -536870912
#
# Top Of Stack
#
    sprd    us, tos
    sprd    dcr, tos
    sprd    bpc, tos
    sprd    dsr, tos
    sprd    car, tos
    sprd    fp, tos
    sprd    sp, tos
    sprd    sb, tos
    sprd    usp, tos
    sprd    cfg, tos
    sprd    psr, tos
    sprd    intbase, tos
    sprd    mod, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    sprd	us, -64(fp)
    sprd	dcr, -64(fp)
    sprd	bpc, 63(fp)
    sprd	dsr, 63(fp)
    #
    sprd	car, -8192(fp)
    sprd	fp, -8192(fp)
    sprd	sp, 8191(fp)
    sprd	sb, 8191(fp)
    #
    sprd	usp, -536870912(fp)
    sprd	cfg, -536870912(fp)
    sprd	psr, 536870911(fp)
    sprd	intbase, 536870911(fp)
    sprd	mod, -64(fp)
    #
    # Stack Pointer (sp)
    #
    sprd	us, -64(sp)
    sprd	dcr, -64(sp)
    sprd	bpc, 63(sp)
    sprd	dsr, 63(sp)
    #
    sprd	car, -8192(sp)
    sprd	fp, -8192(sp)
    sprd	sp, 8191(sp)
    sprd	sb, 8191(sp)
    #
    sprd	usp, -536870912(sp)
    sprd	cfg, -536870912(sp)
    sprd	psr, 536870911(sp)
    sprd	intbase, 536870911(sp)
    sprd	mod, -64(sp)
    #
    # Static Memory (sb)
    #
    sprd	us, -64(sb)
    sprd	dcr, -64(sb)
    sprd	bpc, 63(sb)
    sprd	dsr, 63(sb)
    #
    sprd	car, -8192(sb)
    sprd	fp, -8192(sb)
    sprd	sp, 8191(sb)
    sprd	sb, 8191(sb)
    #
    sprd	usp, -536870912(sb)
    sprd	cfg, -536870912(sb)
    sprd	psr, 536870911(sb)
    sprd	intbase, 536870911(sb)
    sprd	mod, -64(sb)
    #
    # Program Counter (pc)
    #
    sprd	us, -64(pc)
    sprd	dcr, -64(pc)
    sprd	bpc, 63(pc)
    sprd	dsr, 63(pc)
    #
    sprd	car, -8192(pc)
    sprd	fp, -8192(pc)
    sprd	sp, 8191(pc)
    sprd	sb, 8191(pc)
    #
    sprd	usp, -536870912(pc)
    sprd	cfg, -536870912(pc)
    sprd	psr, 536870911(pc)
    sprd	intbase, 536870911(pc)
    sprd	mod, -64(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    sprd   us, r7[r0:b]
    sprd   dcr, r6[r1:b]
    #
    # Register Relative
    sprd   bpc, 63(r5)[r2:b]
    sprd   dsr, 63(r4)[r3:b]
    sprd   car, 8191(r3)[r4:b]
    sprd   fp, 8191(r2)[r5:b]
    sprd   sp, 536870911(r1)[r6:b]
    sprd   sb, 536870911(r0)[r7:b]
    #
    # Memory Relative
    sprd   usp, -64(63(fp))[r0:b]
    sprd   cfg, -64(63(fp))[r1:b]
    sprd   psr, -8192(8191(fp))[r2:b]
    sprd   intbase, -8192(8191(fp))[r3:b]
    sprd   mod, -536870912(536870911(fp))[r4:b]
    sprd   us, -536870912(536870911(fp))[r5:b]
    sprd   dcr, -64(63(sp))[r7:b]
    sprd   bpc, -64(63(sp))[r6:b]
    sprd   dsr, -8192(8191(sp))[r5:b]
    sprd   car, -8192(8191(sp))[r4:b]
    sprd   fp, -536870912(536870911(sp))[r3:b]
    sprd   sp, -536870912(536870911(sp))[r2:b]
    sprd   sb, -64(63(sb))[r0:b]
    sprd   usp, -64(63(sb))[r1:b]
    sprd   cfg, -8192(8191(sb))[r2:b]
    sprd   psr, -8192(8191(sb))[r3:b]
    sprd   intbase, -536870912(536870911(sb))[r4:b]
    sprd   mod, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    sprd   us, @63[r0:b]
    sprd   dcr, @63[r1:b]
    sprd   bpc, @8191[r2:b]
    sprd   dsr, @8191[r3:b]
    sprd   car, @536870911[r4:b]
    sprd   fp, @536870911[r5:b]
    #
    # External
    sprd   sp, ext(-64) + 63[r7:b]
    sprd   sb, ext(-64) + 63[r6:b]
    sprd   usp, ext(-8192) + 8191[r5:b]
    sprd   cfg, ext(-8192) + 8191[r4:b]
    sprd   psr, ext(-536870912) + 536870911[r3:b]
    sprd   intbase, ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    sprd   mod, tos[r0:b]
    #
    # Memory Space
    sprd   us, 63(fp)[r0:b]
    sprd   dcr, 63(fp)[r0:b]
    sprd   bpc, 8191(fp)[r0:b]
    sprd   dsr, 8191(fp)[r0:b]
    sprd   car, 536870911(fp)[r0:b]
    sprd   fp, 536870911(fp)[r0:b]
    sprd   sp, 63(sp)[r0:b]
    sprd   sb, 63(sp)[r0:b]
    sprd   usp, 8191(sp)[r0:b]
    sprd   cfg, 8191(sp)[r0:b]
    sprd   psr, 536870911(sp)[r0:b]
    sprd   intbase, 536870911(sp)[r0:b]
    sprd   mod, 63(sb)[r0:b]
    sprd   us, 63(sb)[r0:b]
    sprd   dcr, 8191(sb)[r0:b]
    sprd   bpc, 8191(sb)[r0:b]
    sprd   dsr, 536870911(sb)[r0:b]
    sprd   car, 536870911(sb)[r0:b]
    sprd   fp, 63(pc)[r0:b]
    sprd   sp, 63(pc)[r0:b]
    sprd   sb, 8191(pc)[r0:b]
    sprd   usp, 8191(pc)[r0:b]
    sprd   cfg, 536870911(pc)[r0:b]
    sprd   psr, 536870911(pc)[r0:b]
    #
    # WORD
    #
    sprd   us, r7[r0:w]
    sprd   dcr, r6[r1:w]
    #
    # Register Relative
    sprd   bpc, 63(r5)[r2:w]
    sprd   dsr, 63(r4)[r3:w]
    sprd   car, 8191(r3)[r4:w]
    sprd   fp, 8191(r2)[r5:w]
    sprd   sp, 536870911(r1)[r6:w]
    sprd   sb, 536870911(r0)[r7:w]
    #
    # Memory Relative
    sprd   usp, -64(63(fp))[r0:w]
    sprd   cfg, -64(63(fp))[r1:w]
    sprd   psr, -8192(8191(fp))[r2:w]
    sprd   intbase, -8192(8191(fp))[r3:w]
    sprd   mod, -536870912(536870911(fp))[r4:w]
    sprd   us, -536870912(536870911(fp))[r5:w]
    sprd   dcr, -64(63(sp))[r7:w]
    sprd   bpc, -64(63(sp))[r6:w]
    sprd   dsr, -8192(8191(sp))[r5:w]
    sprd   car, -8192(8191(sp))[r4:w]
    sprd   fp, -536870912(536870911(sp))[r3:w]
    sprd   sp, -536870912(536870911(sp))[r2:w]
    sprd   sb, -64(63(sb))[r0:w]
    sprd   usp, -64(63(sb))[r1:w]
    sprd   cfg, -8192(8191(sb))[r2:w]
    sprd   psr, -8192(8191(sb))[r3:w]
    sprd   intbase, -536870912(536870911(sb))[r4:w]
    sprd   mod, -536870912(536870911(sb))[r5:w]
    #
    # Absolute
    sprd   us, @63[r0:w]
    sprd   dcr, @63[r1:w]
    sprd   bpc, @8191[r2:w]
    sprd   dsr, @8191[r3:w]
    sprd   car, @536870911[r4:w]
    sprd   fp, @536870911[r5:w]
    #
    # External
    sprd   sp, ext(-64) + 63[r7:w]
    sprd   sb, ext(-64) + 63[r6:w]
    sprd   usp, ext(-8192) + 8191[r5:w]
    sprd   cfg, ext(-8192) + 8191[r4:w]
    sprd   psr, ext(-536870912) + 536870911[r3:w]
    sprd   intbase, ext(-536870912) + 536870911[r2:w]
    #
    # Top Of Stack
    sprd   mod, tos[r0:w]
    #
    # Memory Space
    sprd   us, 63(fp)[r0:w]
    sprd   dcr, 63(fp)[r0:w]
    sprd   bpc, 8191(fp)[r0:w]
    sprd   dsr, 8191(fp)[r0:w]
    sprd   car, 536870911(fp)[r0:w]
    sprd   fp, 536870911(fp)[r0:w]
    sprd   sp, 63(sp)[r0:w]
    sprd   sb, 63(sp)[r0:w]
    sprd   usp, 8191(sp)[r0:w]
    sprd   cfg, 8191(sp)[r0:w]
    sprd   psr, 536870911(sp)[r0:w]
    sprd   intbase, 536870911(sp)[r0:w]
    sprd   mod, 63(sb)[r0:w]
    sprd   us, 63(sb)[r0:w]
    sprd   dcr, 8191(sb)[r0:w]
    sprd   bpc, 8191(sb)[r0:w]
    sprd   dsr, 536870911(sb)[r0:w]
    sprd   car, 536870911(sb)[r0:w]
    sprd   fp, 63(pc)[r0:w]
    sprd   sp, 63(pc)[r0:w]
    sprd   sb, 8191(pc)[r0:w]
    sprd   usp, 8191(pc)[r0:w]
    sprd   cfg, 536870911(pc)[r0:w]
    sprd   psr, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    sprd   us, r7[r0:d]
    sprd   dcr, r6[r1:d]
    #
    # Register Relative
    sprd   bpc, 63(r5)[r2:d]
    sprd   dsr, 63(r4)[r3:d]
    sprd   car, 8191(r3)[r4:d]
    sprd   fp, 8191(r2)[r5:d]
    sprd   sp, 536870911(r1)[r6:d]
    sprd   sb, 536870911(r0)[r7:d]
    #
    # Memory Relative
    sprd   usp, -64(63(fp))[r0:d]
    sprd   cfg, -64(63(fp))[r1:d]
    sprd   psr, -8192(8191(fp))[r2:d]
    sprd   intbase, -8192(8191(fp))[r3:d]
    sprd   mod, -536870912(536870911(fp))[r4:d]
    sprd   us, -536870912(536870911(fp))[r5:d]
    sprd   dcr, -64(63(sp))[r7:d]
    sprd   bpc, -64(63(sp))[r6:d]
    sprd   dsr, -8192(8191(sp))[r5:d]
    sprd   car, -8192(8191(sp))[r4:d]
    sprd   fp, -536870912(536870911(sp))[r3:d]
    sprd   sp, -536870912(536870911(sp))[r2:d]
    sprd   sb, -64(63(sb))[r0:d]
    sprd   usp, -64(63(sb))[r1:d]
    sprd   cfg, -8192(8191(sb))[r2:d]
    sprd   psr, -8192(8191(sb))[r3:d]
    sprd   intbase, -536870912(536870911(sb))[r4:d]
    sprd   mod, -536870912(536870911(sb))[r5:d]
    #
    # Absolute
    sprd   us, @63[r0:d]
    sprd   dcr, @63[r1:d]
    sprd   bpc, @8191[r2:d]
    sprd   dsr, @8191[r3:d]
    sprd   car, @536870911[r4:d]
    sprd   fp, @536870911[r5:d]
    #
    # External
    sprd   sp, ext(-64) + 63[r7:d]
    sprd   sb, ext(-64) + 63[r6:d]
    sprd   usp, ext(-8192) + 8191[r5:d]
    sprd   cfg, ext(-8192) + 8191[r4:d]
    sprd   psr, ext(-536870912) + 536870911[r3:d]
    sprd   intbase, ext(-536870912) + 536870911[r2:d]
    #
    # Top Of Stack
    sprd   mod, tos[r0:b]
    #
    # Memory Space
    sprd   us, 63(fp)[r0:d]
    sprd   dcr, 63(fp)[r0:d]
    sprd   bpc, 8191(fp)[r0:d]
    sprd   dsr, 8191(fp)[r0:d]
    sprd   car, 536870911(fp)[r0:d]
    sprd   fp, 536870911(fp)[r0:d]
    sprd   sp, 63(sp)[r0:d]
    sprd   sb, 63(sp)[r0:d]
    sprd   usp, 8191(sp)[r0:d]
    sprd   cfg, 8191(sp)[r0:d]
    sprd   psr, 536870911(sp)[r0:d]
    sprd   intbase, 536870911(sp)[r0:d]
    sprd   mod, 63(sb)[r0:d]
    sprd   us, 63(sb)[r0:d]
    sprd   dcr, 8191(sb)[r0:d]
    sprd   bpc, 8191(sb)[r0:d]
    sprd   dsr, 536870911(sb)[r0:d]
    sprd   car, 536870911(sb)[r0:d]
    sprd   fp, 63(pc)[r0:d]
    sprd   sp, 63(pc)[r0:d]
    sprd   sb, 8191(pc)[r0:d]
    sprd   usp, 8191(pc)[r0:d]
    sprd   cfg, 536870911(pc)[r0:d]
    sprd   psr, 536870911(pc)[r0:d]
#
end:
