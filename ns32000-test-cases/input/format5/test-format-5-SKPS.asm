.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 5 instructions
#
# Instruction under test: SKPS
#
# Syntax:   SKPSi options
#
#           !                     SKPSi                     !
#           +---------+---+-+-+---------+---+---------------+
#           !0 0 0 0 0!UW !B!0!0 0 0 0 0! i !0 0 0 0 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
# 
# Syntax:   SKPST options
#
#           !                     SKPST                     !
#           +---------+---+-+-+---------+---+---------------+
#           !0 0 0 0 0!UW !B!1!0 0 0 0 0!0 0!0 0 0 0 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
#
# The SKPSi instruction examines and skips over consecutive elements in String 1
# until either an Until/While condition is met or register R0 is decremented to 0
# (i.e., the string is exhausted). After each element is examined, the CPU sets
# register R1 to the address of the next element to be examined and register R0 to
# the number of integers remaining to be examined. # Register R2 is not used or
# affected.
#
    skpsb   []
    skpsw   []
    skpsd   []
    skpst   []
#
    skpsb   [b]
    skpsb   [w]
    skpsb   [b, w]
    skpsb   [u]
    skpsb   [b, u]
#
    skpsw   [b]
    skpsw   [w]
    skpsw   [b, w]
    skpsw   [u]
    skpsw   [b, u]
#
    skpsd   [b]
    skpsd   [w]
    skpsd   [b, w]
    skpsd   [u]
    skpsd   [b, u]
#
    skpst   [b]
    skpst   [w]
    skpst   [b, w]
    skpst   [u]
    skpst   [b, u]
#
end:
