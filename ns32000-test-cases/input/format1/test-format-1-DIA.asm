.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: DIA
#
# Syntax:   DIA
#
#           !      DIA      !
#           +---------------+
#           !1 1 0 0 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The DIA instruction is intended to support breakpointing circuitry, and is not
# intended for use in a program. It is a 1-byte instruction which performs a
# branch to itself, establishing an "infinite loop" which is interruptible. When
# the loop thus established is interrupted, the return address pushed onto the
# Interrupt Stack is the address of the DIA instruction itself.
#
    dia
#
end:
