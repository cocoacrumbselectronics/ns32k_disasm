.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 8 instructions
#
# Instruction under test: EXT
#
# Syntax.   EXTi    offset, base,   dest,   length
# `                 reg     gen     gen     disp
#                           regaddr write.i
#
#                                 off-
#           !  base   !  dest   ! set !        EXTi         !
#           +---------+---------+-----+-+---+---------------+
#           !   gen   !   gen   ! reg !0! i !0 0 1 0 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
#
# The EXTi instruction copies the bit field specified by base, offset and length to
# the dest operand location. The field is right-justified in dest. High-order
# bits are zero-filled if the field is shorter than dest or discarded if the field
# is longer than dest.
#
    extw    r0, 0(r1), r2, 7


#
# Regression test cases:
#
#    extb   -8192(8191(fp))[r3:b], -8192(8191(fp))[r2:b], r0, 7
#    extb   -8192(8191(fp))[r2:b], -8192(8191(fp))[r3:b], 256
#    extb   -536870912(536870911(fp))[r3:b], -536870912(536870911(fp))[r2:b], 256
#    extb   -536870912(536870911(fp))[r2:b], -536870912(536870911(fp))[r3:b], 256
#    extb   -8192(r0), -8192(8191(fp))[r3:b], 256
#    extb   -8192(8191(fp))[r3:b], -8192(r0), 256
#    extb   -536870912(r0), -536870912(536870911(fp))[r3:b], 256
#    extb   -536870912(536870911(fp))[r3:b], -536870912(r0), 256
#
# Register
#
    extb r7, r0, r5, 2
    extd r6, r1, r5, 2
    extw r5, r2, r5, 2
    extb r4, r3, r5, 2
    extd r3, r4, r5, 2
    extw r2, r5, r5, 2
    extb r1, r6, r5, 2
    extd r0, r7, r5, 2
#
# Register relative
#
     extd r0, 63(r6), r1, 2
     extw r1, 8191(r5), r2, 2
     extb r2, -8192(r4), r3, 2
     extd r3, -536870912(r3), r4, 2
     extw r4, 536870911(r2), r5, 2
     extb r5, -64(r1), r6, 2
     extd r6, 63(r0), r7, 2
#
    extb r7, r7, -64(r7), 2
    extd r0, r6, 63(r6), 2
    extw r1, r5, 8191(r5), 2
    extb r2, r4, -8192(r4), 2
    extd r3, r3, -536870912(r3), 2
    extw r4, r2, 536870911(r2), 2
    extb r5, r1, -64(r1), 2
    extd r6, r0, 63(r0), 2
#
    extb r7, 63(r0), -64(r7), 2
    extd r0, -64(r1), 63(r6), 2
    extw r1, 536870911(r2), 8191(r5), 2
    extb r2, -536870912(r3), -8192(r4), 2
    extd r3, -8192(r4), -536870912(r3), 2
    extw r4, 8191(r5), 536870911(r2), 2
    extb r5, 63(r6), -64(r1), 2
    extd r6, -64(r7), 63(r0), 2
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    extb   r7, r0, -64(63(fp)), 2
    extd   r0, r1, -64(63(fp)), 2
    extw   r1, r2, 63(-64(fp)), 2
    extb   r2, r3, 63(-64(fp)), 2
    #
    extd   r3, r0, -8192(63(fp)), 4
    extw   r4, r1, -64(8191(fp)), 4
    extb   r5, r2, 63(-8192(fp)), 4
    extd   r6, r3, 8191(-64(fp)), 4
    #
    extw   r7, r4, -8192(8191(fp)), 32
    extb   r0, r5, -8192(8191(fp)), 31
    extd   r1, r6, 8191(-8192(fp)), 30
    extw   r2, r7, 8191(-8192(fp)), 29
    #
    extb   r3, 63(r0), -536870912(8191(fp)), 28
    extd   r4, -64(r1), -8192(536870911(fp)), 28
    extw   r5, 536870911(r2), 8191(-536870912(fp)), 28
    extb   r6, -536870912(r3), 536870911(-8192(fp)), 28
    #
    extd   r7, -8192(r4), 536870911(-536870912(fp)), 27
    extw   r0, 8191(r5), 536870911(-536870912(fp)), 27
    extb   r1, 63(r6), -536870912(536870911(fp)), 27
    extd   r2, -64(r7), -536870912(536870911(fp)), 27
    #
    # Stack Pointer (sp)
    #
    extb   r3, r0, -64(63(sp)), 0
    extw   r4, r1, -64(63(sp)), 0
    extd   r5, r2, 63(-64(sp)), 0
    extb   r6, r3, 63(-64(sp)), 0
    #
    extw   r7, -8192(8191(fp)), -8192(63(sp)), 2
    extd   r0, -8192(8191(fp)), -64(8191(sp)), 2
    extb   r1, 8191(-8192(fp)), 63(-8192(sp)), 2
    extw   r2, 8191(-8192(fp)), 8191(-64(sp)), 2
    #
    extd   r3, -8192(8191(sp)), -8192(8191(fp)), 4
    extb   r4, -8192(8191(sp)), -8192(8191(fp)), 4
    extw   r5, 8191(-8192(sp)), 8191(-8192(fp)), 4
    extd   r6, 8191(-8192(sp)), 8191(-8192(fp)), 4
    #
    extb   r7, r4, -536870912(8191(sp)), 26
    extw   r0, r5, -8192(536870911(sp)), 26
    extd   r1, r6, 8191(-536870912(sp)), 26
    extb   r2, r7, 536870911(-8192(sp)), 26
    #
    extw   r3, 63(r0), 536870911(-536870912(sp)), 25
    extd   r4, -64(r1), 536870911(-536870912(sp)), 25
    extb   r5, 536870911(r2), -536870912(536870911(sp)), 25
    extw   r6, -536870912(r3), -536870912(536870911(sp)), 25
    #
    # Static Memory (sb)
    #
    extb   r7, r0, -64(63(sb)), 0
    extw   r0, r1, -64(63(sb)), 0
    extd   r1, r2, 63(-64(sb)), 0
    extb   r2, r3, 63(-64(sb)), 0
    #
    extw   r3, -8192(8191(fp)), -8192(63(sb)), 2
    extd   r4, -8192(8191(fp)), -64(8191(sb)), 2
    extb   r5, 8191(-8192(fp)), 63(-8192(sb)), 2
    extw   r6, 8191(-8192(fp)), 8191(-64(sb)), 2
    #
    extd   r7, -8192(8191(sp)), -8192(8191(sb)), 4
    extb   r0, -8192(8191(sp)), -8192(8191(sb)), 4
    extw   r1, 8191(-8192(sp)), 8191(-8192(sb)), 4
    extd   r2, 8191(-8192(sp)), 8191(-8192(sb)), 4
    #
    extb   r3, r4, -536870912(8191(sb)), 24
    extw   r4, r5, -8192(536870911(sb)), 24
    extd   r5, r6, 8191(-536870912(sb)), 24
    extb   r6, r7, 536870911(-8192(sb)), 24
    #
    extw   r7, 63(r0), 536870911(-536870912(sb)), 23
    extd   r0, -64(r1), 536870911(-536870912(sb)), 23
    extb   r1, 536870911(r2), -536870912(536870911(sb)), 23
    extw   r2, -536870912(r3), -536870912(536870911(sb)), 23
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    extb	r3, r0, @-64, 0
    extw	r4, r1, @-64, 1
    extd	r5, r2, @63, 2
    extb	r6, r3, @63, 3
    extw	r7, 7(r4), @-65, 4
    extd	r0, -8(r5), @-65, 4
    extb	r1, 7(r6), @64, 4
    extw	r2, -8(r7), @64, 4
    #
    extd	r3, -8192(63(fp)), @-8192, 8
    extb	r4, @-8192, -8192(536870911(sp)), 16
    extw	r5, @8191, r0, 22
    extd	r6, @8191, -64(r1), 22
    extb	r7, @-8193, -8192(63(sb)), 22
    extw	r0, @-8193, -536870912(-8192(fp)), 22
    extd	r1, @8192, r2, 22
    extb	r2, @8192, -1(r4), 22
    #
    extw	r3, r5, @-536870912, 21
    extd	r4, r6, @536870911, 21
#
#
# External
#
    extb	r5, ext(-64) + 63, r0, 0
    extw	r6, ext(-64) + 63, r1, 1
    extd	r7, ext(63) + -64, r2, 2
    extb	r0, ext(63) + -64, r3, 3
    #
    extw	r1, ext(-65) + 63, 7(r4), 4
    extd	r2, ext(-65) + 63, -8(r5), 8
    extb	r3, ext(63) + -65, 7(r6), 16
    extw	r4, ext(63) + -65, -8(r7), 20
    extd	r5, 7(r4), ext(-64) + 64, 20
    extb	r6, -8(r5), ext(-64) + 64, 20
    extw	r7, 7(r6), ext(64) + -64, 20
    extd	r0, -8(r7), ext(64) + -64, 20
    #
    extb	r1, @8191, ext(-8192) + 8191, 19
    extw	r2, @-8192, ext(-8192) + 8191, 19
    extd	r3, @536870911, ext(8191) + -8192, 19
    extb	r4, @-536870912, ext(8191) + -8192, 19
    #
    extw	r5, ext(-536870912) + 8191, -8192(8191(sp)), 18
    extd	r6, ext(-536870912) + 8191, -8192(8191(sp)), 18
    extb	r7, ext(8191) + -536870912, 8191(-8192(sp)), 18
    extw	r0, ext(8191) + -536870912, 8191(-8192(sp)), 18
    extd	r1, ext(-8192) + 536870911, @8191, 0
    extb	r2, ext(-8192) + 536870911, @-8192, 1
    extw	r3, ext(536870911) + -8192, ext(64) + -64, 2
    extd	r4, ext(536870911) + -8192, ext(8191) + -8192, 4
    #
    extb	r5, -64(63(fp)), ext(-536870912) + 536870911, 8
    extw	r6, -64(63(fp)), ext(-536870912) + 536870911, 16 
    extd	r7, ext(536870911) + -536870912, 63(-64(fp)), 32
    extb	r0, ext(536870911) + -536870912, 63(-64(fp)), 17
#
# Top Of Stack
#
    extb	r1, r0, tos, 17
    extw	r2, tos, 7(r1), 17
    extd	r3, @-8, tos, 17
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    extb	r4, r0, -64(fp), 0
    extw	r5, -64(fp), r1, 1
    extd	r6, 7(r2), 63(fp), 2 
    extb	r7, 63(fp), -8(r3), 3
    #
    extw	r0, 63(fp), -8192(fp), 4
    extd	r1, -8192(fp), 63(fp), 8
    extb	r2, -64(fp), 8191(fp), 16
    extw	r3, 8191(fp), -64(fp), 15
    #
    extd	r4, -64(63(fp)), -536870912(fp), 15
    extb	r5, ext(-536870912) + 536870911, -536870912(fp), 15
    extw	r6, 536870911(fp), @-8192, 15
    extd	r7, 536870911(fp), -8192(8191(sp)), 15
    #
    # Stack Pointer (sp)
    #
    extb	r0, r0, -64(sp), 0
    extw	r1, -64(sp), r1, 1
    extd	r2, 7(r2), 63(sp), 2 
    extb	r3, 63(sp), -8(r3), 3
    #
    extw	r4, 63(sp), -8192(sp), 4
    extd	r5, -8192(sp), 63(fp), 8
    extb	r6, -64(sp), 8191(sp), 16
    extw	r7, 8191(fp), -64(sp), 14
    #
    extd	r0, -64(63(sp)), -536870912(sp), 14
    extb	r1, ext(-536870912) + 536870911, -536870912(sp), 14
    extw	r2, 536870911(sp), @-8192, 14
    extd	r3, 536870911(fp), -8192(8191(sp)), 14
    #
    # Static Memory (sb)
    #
    extb	r4, r0, -64(sb), 0
    extw	r5, -64(sb), r1, 1
    extd	r6, 7(r2), 63(sb), 2 
    extb	r7, 63(sb), -8(r3), 3
    #
    extw	r0, 63(fp), -8192(sb), 4
    extd	r1, -8192(sb), 63(sb), 8
    extb	r2, -64(sb), 8191(sp), 16
    extw	r3, 8191(sb), -64(sp), 13
    #
    extd	r4, -64(63(fp)), -536870912(fp), 13
    extb	r5, ext(-536870912) + 536870911, -536870912(fp), 13
    extw	r6, 536870911(fp), @-8192, 13
    extd	r7, 536870911(fp), -8192(8191(sp)), 13
    #
    # Program Counter (pc)
    #
    extb	r0, r0, -64(pc), 0
    extw	r1, -64(pc), r1, 1
    extd	r2, 7(r2), 63(pc), 2 
    extb	r3, 63(pc), -8(r3), 3
    #
    extw	r4, 63(pc), -8192(fp), 4
    extd	r5, -8192(fp), 63(pc), 8
    extb	r6, -64(sp), 8191(pc), 16
    extw	r7, 8191(pc), -64(pc), 32
    #
    extd	r0, -64(63(fp)), -536870912(pc), 12
    extb	r1, ext(-536870912) + 536870911, -536870912(pc), 12
    extw	r2, 536870911(pc), @-8192, 12
    extd	r3, 536870911(pc), -8192(8191(sp)), 12
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    extb   r4, r0, r7[r0:b], 0
    extb   r5, r1, r6[r1:b], 0
    #
    # Register Relative
    extb   r6, 7(r2), 63(r5)[r2:b], 1
    extb   r7, -8(r3), 63(r4)[r3:b], 2
    extb   r0, 8191(r3)[r4:b], -8192(fp), 4
    extb   r1, 8191(r2)[r5:b], @-8192, 8
    extb   r2, ext(-536870912) + 536870911, 536870911(r1)[r6:b], 16
    extb   r3, -64(pc), 536870911(r0)[r7:b], 32
    #
    # Memory Relative
    extb   r4, r7, -64(63(fp))[r0:b], 11
    extb   r5, -64(63(fp))[r1:b], -8(r6), 11
    extb   r6, -8192(8191(fp))[r3:b], -8192(8191(fp))[r2:b], 11
    extb   r7, @-8, -8192(8191(fp))[r3:b], 11
    extb   r0, -536870912(536870911(fp))[r4:b], ext(8191) + -536870912, 11
    extb   r1, -536870912(536870911(fp))[r4:b], -536870912(536870911(fp))[r5:b], 11
    extb   r2, ext(8191) + -536870912, -64(63(sp))[r7:b], 11
    extb   r3, -64(63(sp))[r6:b], -64(r5), 11
    extb   r4, -64(63(sp))[r6:b], -8192(8191(sp))[r5:b], 11
    extb   r5, -8192(8191(sp))[r4:b], -64(pc), 11
    extb   r6, -536870912(536870911(sp))[r3:b], 63(r5)[r2:b], 11
    extb   r7, -536870912(536870911(sp))[r3:b], -536870912(536870911(sp))[r2:b], 11
    extb   r0, 7(r4), -64(63(sb))[r0:b], 11
    extb   r1, -64(63(sb))[r1:b], 8192(r5)[r2:b], 0
    extb   r2, -64(63(sb))[r1:b], -8192(8191(sb))[r2:b], 1
    extb   r3, -8(r1), -8192(8191(sb))[r3:b], 3
    extb   r4, -536870912(536870911(sb))[r4:b], -8192(sp), 9
    extb   r5, -536870912(536870911(sb))[r4:b], -536870912(536870911(sb))[r5:b], 27
    #
    # Absolute
    extb   r6, 7(r2), @63[r0:b], 10
    extb   r7, @63[r1:b], -8(r1), 10
    extb   r0, @63[r1:b], @8191[r2:b], 10
    extb   r1, -8192(sp), @8191[r3:b], 10
    extb   r2, @536870911[r4:b], -8192(pc), 2
    extb   r3, @536870911[r4:b], @536870911[r5:b], 4
    #
    # External
    extb   r4, 7(r2), ext(-64) + 63[r7:b], 6
    extb   r5, ext(-64) + 63[r6:b], -8(r1), 7
    extb   r6, ext(-64) + 63[r6:b], ext(-8192) + 8191[r5:b], 8
    extb   r7, -8192(sp), ext(-8192) + 8191[r4:b], 9
    extb   r0, ext(-536870912) + 536870911[r3:b], -8192(fp), 10
    extb   r1, ext(-536870912) + 536870911[r3:b], ext(-536870912) + 536870911[r2:b], 11
    #
    # Top Of Stack
    extb   r2, ext(-536870912) + 536870911[r2:b], tos[r0:b], 13
    extb   r3, 7(r1), tos[r0:b], 13
    extb   r4, tos[r7:b], @63[r0:b], 14
    #
    # Memory Space
    extb   r5, 7(r7), 63(fp)[r0:b], 15
    extb   r6, 63(fp)[r0:b], @63[r0:b], 16
    extb   r7, 63(fp)[r0:b], 8191(fp)[r0:b], 17
    extb   r0, -8(r1), 8191(fp)[r0:b], 18
    extb   r1, 8191(fp)[r0:b], 536870911(fp)[r0:b], 19
    extb   r2, @63[r0:b], 536870911(fp)[r0:b], 20
    extb   r3, @63[r0:b], 63(sp)[r0:b], 21
    extb   r4, 63(sp)[r0:b], @63[r0:b], 22
    extb   r5, 63(sp)[r0:b], 8191(sp)[r0:b], 23
    extb   r6, -8(r6), 8191(sp)[r0:b], 24
    extb   r7, 536870911(sp)[r0:b], ext(-536870912) + 536870911[r3:b], 25
    extb   r0, 536870911(sp)[r0:b], 536870911(sp)[r0:b], 26
    extb   r1, 7(r1), 63(sb)[r0:b], 27
    extb   r2, 63(sb)[r0:b], r3, 28
    extb   r3, 63(sb)[r0:b], 8191(sb)[r0:b], 29
    extb   r4, 8191(sb)[r0:b], @63[r0:b], 30
    extb   r5, 536870911(sb)[r0:b], ext(-536870912) + 536870911[r3:b], 31
    extb   r6, 536870911(sb)[r0:b], 536870911(sb)[r0:b], 32
    extb   r7, 7(r6), 63(pc)[r0:b], 32
    extb   r0, 63(pc)[r0:b], @63[r0:b], 32
    extb   r1, 63(pc)[r0:b], 8191(pc)[r0:b], 32
    extb   r2, -8(r1), 8191(pc)[r0:b], 32
    extb   r3, 536870911(pc)[r0:b], ext(-536870912) + 536870911[r3:b], 32
    extb   r4, 536870911(pc)[r0:b], 536870911(pc)[r0:b], 32
#
end:
