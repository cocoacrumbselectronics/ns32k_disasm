.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 0 instructions
#
# Instruction under test: Bcond
#
# Syntax:   Bcond   dest
#                   disp
#
#           !  cond !   B   !
#           +-------+-------+
#           ! short !1 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The Bcond instruction branches to the location specified as dest.
#
#   Condition               Abbr.   True State          Short Field
#   Equal                   EQ      Z flag set          0000
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
    beq     63
    beq     -64
    beq     8191
    beq     -8192
    beq     536870911
    beq     -536870912
#
#   Not Equal               NE      Z flag clear        0001
#
    bne     63
    bne     -64
    bne     8191
    bne     -8192
    bne     536870911
    bne     -536870912
#
#   Carry Set               CS      C flag set          0010
#
    bcs     63
    bcs     -64
    bcs     8191
    bcs     -8192
    bcs     536870911
    bcs     -536870912
#
#   Carry Clear             CC      C flag clear        0011
#
    bcc     63
    bcc     -64
    bcc     8191
    bcc     -8192
    bcc     536870911
    bcc     -536870912
#
#   Higher                  HI      L flag set          0100
#
    bhi     63
    bhi     -64
    bhi     8191
    bhi     -8192
    bhi     536870911
    bhi     -536870912
#
#   Lower or Same           LS      L flag clear        0101
#
    bls     63
    bls     -64
    bls     8191
    bls     -8192
    bls     536870911
    bls     -536870912
#
#   Greater Than            GT      N flag set          0110
#
    bgt     63
    bgt     -64
    bgt     8191
    bgt     -8192
    bgt     536870911
    bgt     -536870912
#
#   Less Than or Equal      LE      N flag clear        0111
#
    ble     63
    ble     -64
    ble     8191
    ble     -8192
    ble     536870911
    ble     -536870912
#
#   Flag Set                FS      F flag set          1000
#
    bfs     63
    bfs     -64
    bfs     8191
    bfs     -8192
    bfs     536870911
    bfs     -536870912
#
#   Flag Clear              FC      F flag clear        1001
#
    bfc     63
    bfc     -64
    bfc     8191
    bfc     -8192
    bfc     536870911
    bfc     -536870912
#
#   Lower                   LO      Z and L flags clear 1010
#
    blo     63
    blo     -64
    blo     8191
    blo     -8192
    blo     536870911
    blo     -536870912
#
#   Higher or Same          HS      Z or L flags set    1011
#
    bhs     63
    bhs     -64
    bhs     8191
    bhs     -8192
    bhs     536870911
    bhs     -536870912
#
#   Less Than               LT      Z and N flags clear 1100
#
    blt     63
    blt     -64
    blt     8191
    blt     -8192
    blt     536870911
    blt     -536870912
#
#   Greater Than or Equal   GE      Z or N flags set    1101
#
    bge     63
    bge     -64
    bge     8191
    bge     -8192
    bge     536870911
    bge     -536870912
#
# Instruction under test: BR (Unconditional Branch)
#
# Syntax:   BR  dest
#               disp
#
#           !      BR       !
#           +---------------+
#           !1 1 1 0 1 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The BR instruction branches to the location specified as dest.
#
    br      63
    br      -64
    br      8191
    br      -8192
    br      536870911
    br      -536870912
#
end:
