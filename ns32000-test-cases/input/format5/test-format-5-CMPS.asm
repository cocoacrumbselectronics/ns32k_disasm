.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 5 instructions
#
# Instruction under test: CMPS
#
# Syntax:   CMPSi options
#
#           !                     CMPSi                     !
#           +---------+---+-+-+---------+---+---------------+
#           !0 0 0 0 0!UW !B!0!0 0 0 0 0! i !0 0 0 0 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
# 
# Syntax:   CMPST options
#
#           !                     CMPST                     !
#           +---------+---+-+-+---------+---+---------------+
#           !0 0 0 0 0!UW !B!1!0 0 0 0 0!0 0!0 0 0 0 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
#
# The CMPSi instruction compares corresponding integer elements from String 1
# (address in R1) and String 2 (address in R2) and sets the Z, N and L flags to
# indicate the comparison results (see "Flags Affected" below). If the current two
# elements are equal, the instruction compares the next two elements; otherwise, it
# terminates. # After each comparison, the instruction sets register R0 to the
# number of elements remaining to be compared and sets registers R1 and R2 to the
# addresses of the next elements to be compared.
#
    cmpsb   []
    cmpsw   []
    cmpsd   []
    cmpst   []
#
    cmpsb   [b]
    cmpsb   [w]
    cmpsb   [b, w]
    cmpsb   [u]
    cmpsb   [b, u]
#
    cmpsw   [b]
    cmpsw   [w]
    cmpsw   [b, w]
    cmpsw   [u]
    cmpsw   [b, u]
#
    cmpsd   [b]
    cmpsd   [w]
    cmpsd   [b, w]
    cmpsd   [u]
    cmpsd   [b, u]
#
    cmpst   [b]
    cmpst   [w]
    cmpst   [b, w]
    cmpst   [u]
    cmpst   [b, u]
#
end:
