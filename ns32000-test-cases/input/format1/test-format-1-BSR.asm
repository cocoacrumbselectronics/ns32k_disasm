.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: BSR
#
# Syntax:   BSR     dest
#                   disp
#
#           !     BSR       !
#           +---------------+
#           !0 0 0 0 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The BSR instruction calls the local procedure at the address specified as dest.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
    bsr     63
    bsr     -64
    bsr     8191
    bsr     -8192
    bsr     536870911
    bsr     -536870912
#
    bsr     5
    bsr     -18
    bsr     4025
    bsr     -125
    bsr     5987621
    bsr     -24869248
#
end:
