.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: SAVE
#
# Syntax:   SAVE    reglist
#                   imm
#
#           !      SAVE      !
#           +---------------+
#           !0 1 1 0 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The SAVE instruction saves the General-Purpose registers specified by reglist,
# pushing them onto the currently-selected stack.
#
    save    [r0]
    save    [r1]
    save    [r2]
    save    [r3]
    save    [r4]
    save    [r5]
    save    [r6]
    save    [r7]
#
    save    [r0, r1]
    save    [r0, r1, r2]
    save    [r0, r1, r2, r3]
    save    [r0, r1, r2, r3, r4]
    save    [r0, r1, r2, r3, r4, r5]
    save    [r0, r1, r2, r3, r4, r5, r6]
    save    [r0, r1, r2, r3, r4, r5, r6, r7]
#
end:
