.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 3 instructions
#
# Instruction under test: JSR
#
# Syntax:   JSR     dest
#                   gen
#                   addr
#
#           !  dest   !        JSR          !
#           +---------+---------------------+
#           !   gen   !1 1 0 0 1 1 1 1 1 1 1!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# The JSR instruction jsrs to the procedure at the address specified by dest after
# saving the return address on the stack. The return address is the address of the
# next sequential instruction.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
# Register
#
    jsr   r0
    jsr   r1
    jsr   r2
    jsr   r3
    jsr   r4
    jsr   r5
    jsr   r6
    jsr   r7
#
# Register relative
#
    jsr   63(r0)
    jsr   -64(r0)
    jsr   8191(r0)
    jsr   -8192(r0)
    jsr   536870911(r0)
    jsr   -536870912(r0)
    #
    jsr   63(r1)
    jsr   -64(r1)
    jsr   8191(r1)
    jsr   -8192(r1)
    jsr   536870911(r1)
    jsr   -536870912(r1)
    #
    jsr   63(r2)
    jsr   -64(r2)
    jsr   8191(r2)
    jsr   -8192(r2)
    jsr   536870911(r2)
    jsr   -536870912(r2)
    #
    jsr   63(r3)
    jsr   -64(r3)
    jsr   8191(r3)
    jsr   -8192(r3)
    jsr   536870911(r3)
    jsr   -536870912(r3)
    #
    jsr   63(r4)
    jsr   -64(r4)
    jsr   8191(r4)
    jsr   -8192(r4)
    jsr   536870911(r4)
    jsr   -536870912(r4)
    #
    jsr   63(r5)
    jsr   -64(r5)
    jsr   8191(r5)
    jsr   -8192(r5)
    jsr   536870911(r5)
    jsr   -536870912(r5)
    #
    jsr   63(r6)
    jsr   -64(r6)
    jsr   8191(r6)
    jsr   -8192(r6)
    jsr   536870911(r6)
    jsr   -536870912(r6)
    #
    jsr   63(r7)
    jsr   -64(r7)
    jsr   8191(r7)
    jsr   -8192(r7)
    jsr   536870911(r7)
    jsr   -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    jsr   -64(63(fp))
    jsr   63(-64(fp))
    jsr   -8192(63(fp))
    jsr   -64(8191(fp))
    jsr   63(-8192(fp))
    jsr   8191(-64(fp))
    jsr   -8192(8191(fp))
    jsr   8191(-8192(fp))
    jsr   -536870912(8191(fp))
    jsr   -8192(536870911(fp))
    jsr   8191(-536870912(fp))
    jsr   536870911(-8192(fp))
    jsr   536870911(-536870912(fp))
    jsr   -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    jsr   -64(63(sp))
    jsr   63(-64(sp))
    jsr   -8192(63(sp))
    jsr   -64(8191(sp))
    jsr   63(-8192(sp))
    jsr   8191(-64(sp))
    jsr   -8192(8191(sp))
    jsr   8191(-8192(sp))
    jsr   -536870912(8191(sp))
    jsr   -8192(536870911(sp))
    jsr   8191(-536870912(sp))
    jsr   536870911(-8192(sp))
    jsr   536870911(-536870912(sp))
    jsr   -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    jsr   -64(63(sb))
    jsr   63(-64(sb))
    jsr   -8192(63(sb))
    jsr   -64(8191(sb))
    jsr   63(-8192(sb))
    jsr   8191(-64(sb))
    jsr   -8192(8191(sb))
    jsr   8191(-8192(sb))
    jsr   -536870912(8191(sb))
    jsr   -8192(536870911(sb))
    jsr   8191(-536870912(sb))
    jsr   536870911(-8192(sb))
    jsr   536870911(-536870912(sb))
    jsr   -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    jsr	@-64
    jsr	@63
    jsr	@-8192
    jsr	@8191
    jsr	@-536870912
    jsr	@536870911
#
# External
#
    jsr	ext(-64) + 63
    jsr	ext(63) + -64
    jsr	ext(-8192) + 8191
    jsr	ext(8191) + -8192
    jsr	ext(-536870912) + 8191
    jsr	ext(8191) + -536870912
    jsr	ext(-8192) + 536870911
    jsr	ext(536870911) + -8192
    jsr	ext(-536870912) + 536870911
    jsr	ext(536870911) + -536870912
#
# Top Of Stack
#
    jsr	tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    jsr	-64(fp)
    jsr	63(fp)
    jsr	-8192(fp)
    jsr	8191(fp)
    jsr	-536870912(fp)
    jsr	536870911(fp)
    #
    # Stack Pointer (sp)
    #
    jsr	-64(sp)
    jsr	63(sp)
    jsr	-8192(sp)
    jsr	8191(sp)
    jsr	-536870912(sp)
    jsr	536870911(sp)
    #
    # Static Memory (sb)
    #
    jsr	-64(sb)
    jsr	63(sb)
    jsr	-8192(sb)
    jsr	8191(sb)
    jsr	-536870912(sb)
    jsr	536870911(sb)
    #
    # Program Counter (pc)
    #
    jsr	-64(pc)
    jsr	63(pc)
    jsr	-8192(pc)
    jsr	8191(pc)
    jsr	-536870912(pc)
    jsr	536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    jsr   r7[r0:b]
    #
    # Register Relative
    jsr   63(r5)[r2:b]
    jsr   8191(r3)[r4:b]
    jsr   536870911(r1)[r6:b]
    #
    # Memory Relative
    jsr   -64(63(fp))[r0:b]
    jsr   -8192(8191(fp))[r2:b]
    jsr   -536870912(536870911(fp))[r4:b]
    jsr   -64(63(sp))[r7:b]
    jsr   -8192(8191(sp))[r5:b]
    jsr   -536870912(536870911(sp))[r3:b]
    jsr   -64(63(sb))[r0:b]
    jsr   -8192(8191(sb))[r2:b]
    jsr   -536870912(536870911(sb))[r4:b]
    #
    # Absolute
    jsr   @63[r0:b]
    jsr   @8191[r2:b]
    jsr   @536870911[r4:b]
    #
    # External
    jsr   ext(-64) + 63[r7:b]
    jsr   ext(-8192) + 8191[r5:b]
    jsr   ext(-536870912) + 536870911[r3:b]
    #
    # Top Of Stack
    jsr   tos[r0:b]
    #
    # Memory Space
    jsr   63(fp)[r0:b]
    jsr   8191(fp)[r0:b]
    jsr   536870911(fp)[r0:b]
    jsr   63(sp)[r0:b]
    jsr   8191(sp)[r0:b]
    jsr   536870911(sp)[r0:b]
    jsr   63(sb)[r0:b]
    jsr   8191(sb)[r0:b]
    jsr   536870911(sb)[r0:b]
    jsr   63(pc)[r0:b]
    jsr   8191(pc)[r0:b]
    jsr   536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    jsr   r1[r0:w]
    #
    # Register Relative
    jsr   63(r1)[r0:w]
    jsr   8191(r1)[r0:w]
    jsr   536870911(r1)[r0:w]
    #
    # Memory Relative
    jsr   -64(63(fp))[r0:w]
    jsr   -8192(8191(fp))[r2:w]
    jsr   -536870912(536870911(fp))[r4:w]
    jsr   -64(63(sp))[r7:w]
    jsr   -8192(8191(sp))[r5:w]
    jsr   -536870912(536870911(sp))[r3:w]
    jsr   -64(63(sb))[r0:w]
    jsr   -8192(8191(sb))[r2:w]
    jsr   -536870912(536870911(sb))[r4:w]
    #
    # Absolute
    jsr   @63[r0:w]
    jsr   @8191[r0:w]
    jsr   @536870911[r0:w]
    #
    # External
    jsr   ext(-64) + 63[r0:w]
    jsr   ext(-8192) + 8191[r0:w]
    jsr   ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    jsr   tos[r0:w]
    #
    # Memory Space
    jsr   63(fp)[r0:w]
    jsr   8191(fp)[r0:w]
    jsr   536870911(fp)[r0:w]
    jsr   63(sp)[r0:w]
    jsr   8191(sp)[r0:w]
    jsr   536870911(sp)[r0:w]
    jsr   63(sb)[r0:w]
    jsr   8191(sb)[r0:w]
    jsr   536870911(sb)[r0:w]
    jsr   63(pc)[r0:w]
    jsr   8191(pc)[r0:w]
    jsr   536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    jsr   r1[r0:d]
    #
    # Register Relative
    jsr   63(r1)[r0:d]
    jsr   8191(r1)[r0:d]
    jsr   536870911(r1)[r0:d]
    #
    # Memory Relative
    jsr   -64(63(fp))[r0:d]
    jsr   -8192(8191(fp))[r2:d]
    jsr   -536870912(536870911(fp))[r4:d]
    jsr   -64(63(sp))[r7:d]
    jsr   -8192(8191(sp))[r5:d]
    jsr   -536870912(536870911(sp))[r3:d]
    jsr   -64(63(sb))[r0:d]
    jsr   -8192(8191(sb))[r2:d]
    jsr   -536870912(536870911(sb))[r4:d]
    #
    # Absolute
    jsr   @63[r0:d]
    jsr   @8191[r0:d]
    jsr   @536870911[r0:d]
    #
    # External
    jsr   ext(-64) + 63[r0:d]
    jsr   ext(-8192) + 8191[r0:d]
    jsr   ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    jsr   tos[r0:d]
    #
    # Memory Space
    jsr   63(fp)[r0:d]
    jsr   8191(fp)[r0:d]
    jsr   536870911(fp)[r0:d]
    jsr   63(sp)[r0:d]
    jsr   8191(sp)[r0:d]
    jsr   536870911(sp)[r0:d]
    jsr   63(sb)[r0:d]
    jsr   8191(sb)[r0:d]
    jsr   536870911(sb)[r0:d]
    jsr   63(pc)[r0:d]
    jsr   8191(pc)[r0:d]
    jsr   536870911(pc)[r0:d]
#
end:
