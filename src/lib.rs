use std::io::{Error, ErrorKind};

pub struct Ns32kDisassembler<'a> {
    byte_stream: &'a Vec<u8>,
    program_counter: usize,
} /* end struct Ns32kDisassembler */

impl Ns32kDisassembler<'_> {
    pub fn new(bytes: &Vec<u8>, start_address: usize) -> Ns32kDisassembler {
        Ns32kDisassembler {
            byte_stream: &bytes,
            program_counter: start_address,
        } /* end Ns32kDisassembler */
    } /* end new */

    pub fn disassemble_instruction_at_address(&self, address: usize) -> Result<(String, String, String, usize), std::io::Error> {
        let format = self.determine_format_from_address(address)?;
        match format {
            0 => return Ok(self.decode_format_0_instruction(address)),
            1 => return Ok(self.decode_format_1_instruction(address)),
            2 => return Ok(self.decode_format_2_instruction(address)),
            3 => return Ok(self.decode_format_3_instruction(address)),
            5 => return Ok(self.decode_format_5_instruction(address)),
            6 => return Ok(self.decode_format_6_instruction(address)),
            7 => return Ok(self.decode_format_7_instruction(address)),
            8 => return Ok(self.decode_format_8_instruction(address)),
            9 => return Ok(self.decode_format_9_instruction(address)),
            11 => return Ok(self.decode_format_11_instruction(address)),
            14 => return Ok(self.decode_format_14_instruction(address)),
            _ => {
                return Err(Error::new(
                    ErrorKind::Other,
                    format!(
                        "disassemble_instruction_at_address: Format not yet decoded for input byte: {:#04X}",
                        format
                    ),
                ));
            }
        } /* end match */
    } /* end disassemble_next_instruction */

    pub fn disassemble_next_instruction(&mut self) -> Result<(String, String, String), std::io::Error> {
        let address = self.get_program_counter();

        let disassembled_instruction = self.disassemble_instruction_at_address(address)?;

        self.set_program_counter(address + disassembled_instruction.3);

        return Ok((disassembled_instruction.0, disassembled_instruction.1, disassembled_instruction.2));
    } /* end disassemble_next_instruction */

    /* ********************************************************************* */

    pub fn print_instruction_to_screen(&self, address_string: String, hex_string: String, instruction_string: String) {
        print!("{}:  ", address_string);
        print!("{}", hex_string);

        // Padding
        for _elem in hex_string.len()..=69 {
            print!(" ");
        } /* end for */
        println!("\t{}", instruction_string);
    } /* end print_instruction_to_screen */

    /* ********************************************************************* */

    pub fn get_program_counter(&self) -> usize {
        return self.program_counter;
    } /* end get_program_counter */

    fn set_program_counter(&mut self, new_program_counter: usize) {
        self.program_counter = new_program_counter;
    } /* end set_program_counter */

    fn fetch_byte_at_address(&self, address: usize) -> u8 {
        return self.byte_stream[address];
    } /* end fetch_byte_at_address */

    /* ********************************************************************* */

    fn decode_condition_code_field(&self, condition_code: u8) -> String {
        match condition_code {
            0b0000 => return "eq".to_string(),
            0b0001 => return "ne".to_string(),
            0b0010 => return "cs".to_string(),
            0b0011 => return "cc".to_string(),
            0b0100 => return "hi".to_string(),
            0b0101 => return "ls".to_string(),
            0b0110 => return "gt".to_string(),
            0b0111 => return "le".to_string(),
            0b1000 => return "fs".to_string(),
            0b1001 => return "fc".to_string(),
            0b1010 => return "lo".to_string(),
            0b1011 => return "hs".to_string(),
            0b1100 => return "lt".to_string(),
            0b1101 => return "ge".to_string(),
            0b1110 => return "r".to_string(),
            0b1111 => return "(Unconditionally False)".to_string(),
            _ => return "Unknown condition code".to_string(),
        } /* end match */
    } /* end decode_condition_code_field */

    fn decode_integer_type_field(&self, integer_type: u8) -> String {
        match integer_type {
            0b00 => return "b".to_string(),
            0b01 => return "w".to_string(),
            0b11 => return "d".to_string(),
            _ => return "Unknown integer type".to_string(),
        } /* end match */
    } /* end decode_integer_type_field */

    fn decode_general_addressing_mode(
        &self,
        gen_addr_mode: u8,
        address: usize,
        displacement_address: usize,
        address_for_mode_byte: usize,
        variable_length_constant: bool,
    ) -> (String, u8) {
        let mut displacement1_decimal: isize = 0;
        let mut displacement2_decimal: isize = 0;
        let mut immediate_displacement: isize = 0;
        let mut nr_of_bytes_used_for_displacement1: u8 = 0;
        let mut nr_of_bytes_used_for_displacement2: u8 = 0;
        let mut nr_of_bytes_used_for_immediate_displacement: u8 = 0;

        match gen_addr_mode {
            0b01000..=0b01111 | 0b10101 | 0b11000..=0b11011 => {
                let (displacement_decimal, nr_of_bytes_used_for_displacement) =
                    self.determine_variable_length_displacement_in_decimal(address);
                displacement1_decimal = displacement_decimal;
                nr_of_bytes_used_for_displacement1 = nr_of_bytes_used_for_displacement;
            }
            0b10000..=0b10010 | 0b10110 => {
                let (displacement_decimal, nr_of_bytes_used_for_displacement) =
                    self.determine_variable_length_displacement_in_decimal(address);
                displacement1_decimal = displacement_decimal;
                nr_of_bytes_used_for_displacement1 = nr_of_bytes_used_for_displacement;

                let (displacement_decimal, nr_of_bytes_used_for_displacement) =
                    self.determine_variable_length_displacement_in_decimal(address + nr_of_bytes_used_for_displacement1 as usize);
                displacement2_decimal = displacement_decimal;
                nr_of_bytes_used_for_displacement2 = nr_of_bytes_used_for_displacement;
            }
            0b10100 => {
                if variable_length_constant == true {
                    let (displacement_decimal, nr_of_bytes_used_for_displacement) =
                        self.determine_variable_length_displacement_in_decimal(address);
                    displacement1_decimal = displacement_decimal;
                    nr_of_bytes_used_for_displacement1 = nr_of_bytes_used_for_displacement;
                } else {
                    immediate_displacement = self.fetch_byte_at_address(address) as isize;
                    if immediate_displacement & 0x80 != 0 {
                        immediate_displacement = -(256 - immediate_displacement);
                    } /* end if */
                    nr_of_bytes_used_for_immediate_displacement = 1;
                } /* end if */
            }
            _ => (),
        } /* end match */
        let register_number = gen_addr_mode & 0x07;
        match gen_addr_mode {
            0b00000..=0b00111 => return (format!("r{}", register_number).to_string(), 0),
            0b01000..=0b01111 => {
                return (
                    format!("{}(r{})", displacement1_decimal, register_number).to_string(),
                    nr_of_bytes_used_for_displacement1,
                )
            }
            0b10000 => {
                return (
                    format!("{}({}(fp))", displacement2_decimal, displacement1_decimal).to_string(),
                    nr_of_bytes_used_for_displacement1 + nr_of_bytes_used_for_displacement2,
                )
            } // disp2(disp1(fp))
            0b10001 => {
                return (
                    format!("{}({}(sp))", displacement2_decimal, displacement1_decimal).to_string(),
                    nr_of_bytes_used_for_displacement1 + nr_of_bytes_used_for_displacement2,
                )
            } // disp2(disp1(sp))
            0b10010 => {
                return (
                    format!("{}({}(sb))", displacement2_decimal, displacement1_decimal).to_string(),
                    nr_of_bytes_used_for_displacement1 + nr_of_bytes_used_for_displacement2,
                )
            } // disp2(disp1(sb))
            0b10011 => return ("Reserved for future use.".to_string(), 0),
            0b10100 => {
                if variable_length_constant == true {
                    return (format!("{}", displacement1_decimal).to_string(), nr_of_bytes_used_for_displacement1);
                } else {
                    return (
                        format!("{}", immediate_displacement).to_string(),
                        nr_of_bytes_used_for_immediate_displacement,
                    );
                }
            } // Immediate mode
            0b10101 => {
                return (
                    format!("@{}", displacement1_decimal).to_string(),
                    nr_of_bytes_used_for_displacement1,
                )
            } // @disp
            0b10110 => {
                return (
                    format!("ext({}) + {}", displacement1_decimal, displacement2_decimal).to_string(),
                    nr_of_bytes_used_for_displacement1 + nr_of_bytes_used_for_displacement2,
                )
            } // ext(disp1)+disp2
            0b10111 => return ("tos".to_string(), 0),
            0b11000 => {
                return (
                    format!("{}(fp)", displacement1_decimal).to_string(),
                    nr_of_bytes_used_for_displacement1,
                )
            } // disp(fp)
            0b11001 => {
                return (
                    format!("{}(sp)", displacement1_decimal).to_string(),
                    nr_of_bytes_used_for_displacement1,
                )
            } // disp(sp)
            0b11010 => {
                return (
                    format!("{}(sb)", displacement1_decimal).to_string(),
                    nr_of_bytes_used_for_displacement1,
                )
            } // disp(sb)
            0b11011 => {
                return (
                    format!("{}(pc)", displacement1_decimal).to_string(),
                    nr_of_bytes_used_for_displacement1,
                )
            } // disp(pc)
            0b11100..=0b11111 => {
                return self.decode_scaled_index_addressing_mode(gen_addr_mode & 0x03, displacement_address, address_for_mode_byte)
            }
            _ => return ("Unknown general addressing mode.".to_string(), 0),
        } /* end match */
    } /* end decode_general_addressing_mode */

    fn decode_dual_general_addressing_mode(
        &self,
        instruction_size: u8,
        gen1_addr_mode: u8,
        gen2_addr_mode: u8,
        address: usize,
        variable_length_constant: bool,
    ) -> (String, String, u8) {
        let mut total_nr_of_bytes_used: u8 = instruction_size;
        let mut gen1_mode_displacement: u8 = 0;
        let mut gen2_mode_displacement: u8 = 0;

        let gen1_addr_mode_str: String;
        let gen2_addr_mode_str: String;

        if (gen1_addr_mode >= 28) && (gen1_addr_mode <= 31) {
            gen1_mode_displacement = 1;
        } /* end if */
        if (gen2_addr_mode >= 28) && (gen2_addr_mode <= 31) {
            gen2_mode_displacement = 1;
        } /* end if */

        if (gen1_mode_displacement != 0) && (gen2_mode_displacement != 0) {
            let (gen_addr_mode_str, nr_of_bytes_used_for_displacement1) =
                self.decode_scaled_index_addressing_mode(gen1_addr_mode & 0x03, address + 4 + gen2_mode_displacement as usize, address + 3);

            gen1_addr_mode_str = gen_addr_mode_str;
            total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_used_for_displacement1;

            let (gen_addr_mode_str, nr_of_bytes_used_for_displacement1) = self.decode_scaled_index_addressing_mode(
                gen2_addr_mode & 0x03,
                address + 3 + gen1_mode_displacement as usize + nr_of_bytes_used_for_displacement1 as usize,
                address + 3 + gen1_mode_displacement as usize,
            );
            gen2_addr_mode_str = gen_addr_mode_str;
            total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_used_for_displacement1;
        } else {
            if (gen1_mode_displacement != 0) && (gen2_mode_displacement == 0) {
                let (gen_addr_mode_str, nr_of_bytes_used_for_displacement1) = self.decode_scaled_index_addressing_mode(
                    gen1_addr_mode & 0x03,
                    address + 4 + gen2_mode_displacement as usize,
                    address + 3,
                );
                gen1_addr_mode_str = gen_addr_mode_str;
                total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_used_for_displacement1;

                let (gen_addr_mode_str, nr_of_bytes_used_for_displacement2) = self.decode_general_addressing_mode(
                    gen2_addr_mode,
                    address + 3 + nr_of_bytes_used_for_displacement1 as usize,
                    address + 3 + nr_of_bytes_used_for_displacement1 as usize,
                    address + 3,
                    true,
                );
                gen2_addr_mode_str = gen_addr_mode_str;
                total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_used_for_displacement2;
            } else {
                if (gen1_mode_displacement == 0) && (gen2_mode_displacement != 0) {
                    let (gen_addr_mode_str, nr_of_bytes_used_for_displacement1) = self.decode_general_addressing_mode(
                        gen1_addr_mode,
                        address + 3 + gen2_mode_displacement as usize,
                        address + 3 + gen2_mode_displacement as usize,
                        address + 3,
                        variable_length_constant,
                    );
                    gen1_addr_mode_str = gen_addr_mode_str;
                    total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_used_for_displacement1;

                    let (gen_addr_mode_str, nr_of_bytes_used_for_displacement2) = self.decode_scaled_index_addressing_mode(
                        gen2_addr_mode & 0x03,
                        address + 3 + 1 + nr_of_bytes_used_for_displacement1 as usize,
                        address + 3,
                    );
                    gen2_addr_mode_str = gen_addr_mode_str;
                    total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_used_for_displacement2;
                } else {
                    let (gen_addr_mode_str, nr_of_bytes_used_for_displacement1) =
                        self.decode_general_addressing_mode(gen1_addr_mode, address + 3, address + 3, address + 3, variable_length_constant);
                    gen1_addr_mode_str = gen_addr_mode_str;
                    total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_used_for_displacement1;

                    let (gen_addr_mode_str, nr_of_bytes_used_for_displacement2) = self.decode_general_addressing_mode(
                        gen2_addr_mode,
                        address + 3 + nr_of_bytes_used_for_displacement1 as usize,
                        address + 3 + nr_of_bytes_used_for_displacement1 as usize,
                        address + 3,
                        true,
                    );
                    gen2_addr_mode_str = gen_addr_mode_str;
                    total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_used_for_displacement2;
                } /* end if */
            } /* end if */
        } /* end if */

        return (gen1_addr_mode_str, gen2_addr_mode_str, total_nr_of_bytes_used);
    } /* end decode_dual_general_addressing_mode*/

    fn decode_scaled_index_addressing_mode(
        &self,
        scaled_index_mode: u8,
        displacement_address: usize,
        address_for_mode_byte: usize,
    ) -> (String, u8) {
        let nr_of_bytes_used: u8 = 1; // Minimum extra byte needed for the scaled index addressing mode

        let byte_size_string: String;

        let mode_byte = self.fetch_byte_at_address(address_for_mode_byte);
        let mode = (mode_byte >> 3) & 0x1F;
        let register_number = mode_byte & 0x07;

        match scaled_index_mode {
            0b00 => byte_size_string = String::from("b"),
            0b01 => byte_size_string = String::from("w"),
            0b10 => byte_size_string = String::from("d"),
            0b11 => byte_size_string = String::from("q"),
            _ => byte_size_string = String::from(""),
        }
        match mode {
            0b00000..=0b00111 => {
                return (
                    format!("r{}[r{}:{}]", mode & 0x07, register_number, byte_size_string),
                    nr_of_bytes_used,
                );
            }
            0b01000..=0b01111 => {
                let (displacement1_decimal, nr_of_bytes_used_for_displacement1) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address);
                return (
                    format!(
                        "{}(r{})[r{}:{}]",
                        displacement1_decimal,
                        mode & 0x07,
                        register_number,
                        byte_size_string
                    ),
                    nr_of_bytes_used + nr_of_bytes_used_for_displacement1,
                );
            }
            0b10000 => {
                let (displacement1_decimal, nr_of_bytes_used_for_displacement1) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address);
                let (displacement2_decimal, nr_of_bytes_used_for_displacement2) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address + nr_of_bytes_used_for_displacement1 as usize);
                return (
                    format!(
                        "{}({}(fp))[r{}:{}]",
                        displacement2_decimal, displacement1_decimal, register_number, byte_size_string
                    ),
                    nr_of_bytes_used + nr_of_bytes_used_for_displacement1 + nr_of_bytes_used_for_displacement2,
                );
            }
            0b10001 => {
                let (displacement1_decimal, nr_of_bytes_used_for_displacement1) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address);
                let (displacement2_decimal, nr_of_bytes_used_for_displacement2) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address + nr_of_bytes_used_for_displacement1 as usize);
                return (
                    format!(
                        "{}({}(sp))[r{}:{}]",
                        displacement2_decimal, displacement1_decimal, register_number, byte_size_string
                    ),
                    nr_of_bytes_used + nr_of_bytes_used_for_displacement1 + nr_of_bytes_used_for_displacement2,
                );
            }
            0b10010 => {
                let (displacement1_decimal, nr_of_bytes_used_for_displacement1) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address);
                let (displacement2_decimal, nr_of_bytes_used_for_displacement2) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address + nr_of_bytes_used_for_displacement1 as usize);
                return (
                    format!(
                        "{}({}(sb))[r{}:{}]",
                        displacement2_decimal, displacement1_decimal, register_number, byte_size_string
                    ),
                    nr_of_bytes_used + nr_of_bytes_used_for_displacement1 + nr_of_bytes_used_for_displacement2,
                );
            }
            0b10011 => {
                return ("Reserved for future use.".to_string(), nr_of_bytes_used);
            }
            0b10100 => {
                return ("Immediate (to be implemented).".to_string(), nr_of_bytes_used);
            }
            0b10101 => {
                let (displacement1_decimal, nr_of_bytes_used_for_displacement1) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address);
                return (
                    format!("@{}[r{}:{}]", displacement1_decimal, register_number, byte_size_string),
                    nr_of_bytes_used + nr_of_bytes_used_for_displacement1,
                );
            }
            0b10110 => {
                let (displacement1_decimal, nr_of_bytes_used_for_displacement1) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address);
                let (displacement2_decimal, nr_of_bytes_used_for_displacement2) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address + nr_of_bytes_used_for_displacement1 as usize);
                return (
                    format!(
                        "ext({}) + {}[r{}:{}]",
                        displacement1_decimal, displacement2_decimal, register_number, byte_size_string
                    ),
                    nr_of_bytes_used + nr_of_bytes_used_for_displacement1 + nr_of_bytes_used_for_displacement2,
                );
            }
            0b10111 => {
                return (format!("tos[r{}:{}]", register_number, byte_size_string), nr_of_bytes_used);
            }
            0b11000 => {
                let (displacement1_decimal, nr_of_bytes_used_for_displacement1) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address);
                return (
                    format!("{}(fp)[r{}:{}]", displacement1_decimal, register_number, byte_size_string),
                    nr_of_bytes_used + nr_of_bytes_used_for_displacement1,
                );
            }
            0b11001 => {
                let (displacement1_decimal, nr_of_bytes_used_for_displacement1) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address);
                return (
                    format!("{}(sp)[r{}:{}]", displacement1_decimal, register_number, byte_size_string),
                    nr_of_bytes_used + nr_of_bytes_used_for_displacement1,
                );
            }
            0b11010 => {
                let (displacement1_decimal, nr_of_bytes_used_for_displacement1) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address);
                return (
                    format!("{}(sb)[r{}:{}]", displacement1_decimal, register_number, byte_size_string),
                    nr_of_bytes_used + nr_of_bytes_used_for_displacement1,
                );
            }
            0b11011 => {
                let (displacement1_decimal, nr_of_bytes_used_for_displacement1) =
                    self.determine_variable_length_displacement_in_decimal(displacement_address);
                return (
                    format!("{}(pc)[r{}:{}]", displacement1_decimal, register_number, byte_size_string),
                    nr_of_bytes_used + nr_of_bytes_used_for_displacement1,
                );
            }
            _ => (),
        } /* end match */
        return ("HELP".to_string(), 1);
    } /* end decode_scaled_index_addressing_mode */

    /* ********************************************************************* */

    fn decode_format_1_op_code_field(&self, operation_code_format_1: u8) -> String {
        match operation_code_format_1 {
            0b0000 => return "bsr".to_string(),
            0b0001 => return "ret".to_string(),
            0b0010 => return "cxp".to_string(),
            0b0011 => return "rxp".to_string(),
            0b0100 => return "rett".to_string(),
            0b0101 => return "reti".to_string(),
            0b0110 => return "save".to_string(),
            0b0111 => return "restore".to_string(),
            0b1000 => return "enter".to_string(),
            0b1001 => return "exit".to_string(),
            0b1010 => return "nop".to_string(),
            0b1011 => return "wait".to_string(),
            0b1100 => return "dia".to_string(),
            0b1101 => return "flag".to_string(),
            0b1110 => return "svc".to_string(),
            0b1111 => return "bpt".to_string(),
            _ => return "Unknown op code for format 1 instructions.".to_string(),
        } /* end match */
    } /* end decode_format_1_op_code_field */

    fn decode_format_2_op_code_field(&self, operation_code: u8) -> String {
        match operation_code {
            0b000 => return "addq".to_string(),
            0b001 => return "cmpq".to_string(),
            0b010 => return "spr".to_string(),
            0b011 => return "scond".to_string(),
            0b100 => return "acb".to_string(),
            0b101 => return "movq".to_string(),
            0b110 => return "lpr".to_string(),
            0b111 => return "Reserved operation code".to_string(),
            _ => return "Unknown op code for format 2 instructions.".to_string(),
        } /* end match */
    } /* end decode_format_2_op_code_field */

    fn decode_format_3_op_code_field(&self, operation_code: u8) -> String {
        match operation_code {
            0b0000 => return "cxpd".to_string(),
            0b0001 => return "trap (und)".to_string(),
            0b0010 => return "bicpsr".to_string(),
            0b0011 => return "trap (und)".to_string(),
            0b0100 => return "jump".to_string(),
            0b0101 => return "trap (und)".to_string(),
            0b0110 => return "bispsr".to_string(),
            0b0111 => return "trap (und)".to_string(),
            0b1010 => return "adjsp".to_string(),
            0b1011 => return "trap (und)".to_string(),
            0b1100 => return "jsr".to_string(),
            0b1101 => return "trap (und)".to_string(),
            0b1110 => return "case".to_string(),
            0b1111 => return "trap (und)".to_string(),
            _ => return "Unknown op code for format 3 instructions.".to_string(),
        } /* end match */
    } /* end decode_format_3_op_code_field */

    fn decode_format_5_op_code_field(&self, operation_code: u8) -> String {
        match operation_code {
            0b0000 => return "movs".to_string(),
            0b0001 => return "cmps".to_string(),
            0b0010 => return "setcfg".to_string(),
            0b0011 => return "skps".to_string(),
            0b0100 => return "trap (und)".to_string(),
            0b0101 => return "trap (und)".to_string(),
            0b0110 => return "trap (und)".to_string(),
            0b0111 => return "trap (und)".to_string(),
            0b1000 => return "trap (und)".to_string(),
            0b1001 => return "trap (und)".to_string(),
            0b1010 => return "trap (und)".to_string(),
            0b1011 => return "trap (und)".to_string(),
            0b1100 => return "trap (und)".to_string(),
            0b1101 => return "trap (und)".to_string(),
            0b1110 => return "trap (und)".to_string(),
            0b1111 => return "trap (und)".to_string(),
            _ => return "Unknown op code for format 5 instructions.".to_string(),
        } /* end match */
    } /* end decode_format_5_op_code_field */

    fn decode_format_6_op_code_field(&self, op_code: u8) -> String {
        match op_code {
            0b0000 => return "rot".to_string(),
            0b0001 => return "ash".to_string(),
            0b0010 => return "cbit".to_string(),
            0b0011 => return "cbiti".to_string(),
            0b0100 => return "trap (UND)".to_string(),
            0b0101 => return "lsh".to_string(),
            0b0110 => return "sbit".to_string(),
            0b0111 => return "sbiti".to_string(),
            0b1000 => return "neg".to_string(),
            0b1001 => return "not".to_string(),
            0b1010 => return "trap (UND)".to_string(),
            0b1011 => return "subp".to_string(),
            0b1100 => return "abs".to_string(),
            0b1101 => return "com".to_string(),
            0b1110 => return "ibit".to_string(),
            0b1111 => return "addp".to_string(),
            _ => return "Unknown op code for format 6 instructions.".to_string(),
        } /* end match */
    } /* end decode_format_6_op_code_field */

    fn decode_format_7_op_code_field(&self, op_code: u8) -> String {
        match op_code {
            0b0000 => return "movm".to_string(),
            0b0001 => return "cmpm".to_string(),
            0b0010 => return "inss".to_string(),
            0b0011 => return "exts".to_string(),
            0b0100 => return "movxbw".to_string(),
            0b0101 => return "movzbw".to_string(),
            0b0110 => return "movzid".to_string(),
            0b0111 => return "movxid".to_string(),
            0b1000 => return "mul".to_string(),
            0b1001 => return "mei".to_string(),
            0b1010 => return "trap (UND)".to_string(),
            0b1011 => return "dei".to_string(),
            0b1100 => return "quo".to_string(),
            0b1101 => return "rem".to_string(),
            0b1110 => return "mod".to_string(),
            0b1111 => return "div".to_string(),
            _ => return "Unknown op code for format 7 instructions.".to_string(),
        } /* end match */
    } /* end decode_format_7_op_code_field */

    fn decode_format_8_op_code_field(&self, op_code: u8) -> String {
        match op_code {
            0b000 => return "ext".to_string(),
            0b001 => return "cvtp".to_string(),
            0b010 => return "ins".to_string(),
            0b011 => return "check".to_string(),
            0b100 => return "index".to_string(),
            0b101 => return "ffs".to_string(),
            0b110 => return "mov".to_string(),
            _ => return "Unknown op code for format 8 instructions.".to_string(),
        } /* end match */
    } /* end decode_format_8_op_code_field */

    fn decode_format_9_op_code_field(&self, op_code: u8) -> String {
        match op_code {
            0b000 => return "mov".to_string(),
            0b001 => return "lfsr".to_string(),
            0b010 => return "movlf".to_string(),
            0b011 => return "movfl".to_string(),
            0b100 => return "round".to_string(),
            0b101 => return "trunc".to_string(),
            0b110 => return "sfsr".to_string(),
            0b111 => return "floor".to_string(),
            _ => return "Unknown op code for format 9 instructions.".to_string(),
        } /* end match */
    } /* end decode_format_9_op_code_field */

    fn decode_format_11_op_code_field(&self, op_code: u8) -> String {
        match op_code {
            0b0000 => return "add".to_string(),
            0b0001 => return "mov".to_string(),
            0b0010 => return "cmp".to_string(),
            0b0011 => return "trap (SLAVE)".to_string(),
            0b0100 => return "sub".to_string(),
            0b0101 => return "neg".to_string(),
            0b0110 => return "trap (UND)".to_string(),
            0b0111 => return "trap (UND)".to_string(),
            0b1000 => return "div".to_string(),
            0b1001 => return "trap (slave)".to_string(),
            0b1010 => return "trap (UND)".to_string(),
            0b1011 => return "trap (UND)".to_string(),
            0b1100 => return "mul".to_string(),
            0b1101 => return "abs".to_string(),
            0b1110 => return "trap (UND)".to_string(),
            0b1111 => return "trap (UND)".to_string(),
            _ => return "Unknown op code for format 11 instructions.".to_string(),
        } /* end match */
    } /* end decode_format_11_op_code_field */

    fn decode_format_14_op_code_field(&self, op_code: u8) -> String {
        match op_code {
            0b00 => return "rdval".to_string(),
            0b01 => return "wrval".to_string(),
            0b10 => return "lmr".to_string(),
            0b11 => return "smr".to_string(),
            _ => return "Unknown op code for format 14 instructions.".to_string(),
        } /* end match */
    } /* end decode_format_14_op_code_field */

    /* ********************************************************************* */

    fn convert_displacement_to_signed_decimal(&self, displacement: usize, nr_of_bytes_used: u8) -> isize {
        match nr_of_bytes_used {
            1 => {
                if displacement & 0x40 == 0 {
                    return (displacement & 0x3F) as isize;
                } else {
                    return -(64 - (displacement & 0x3F) as isize);
                } /* end if */
            } /* end 1 */
            2 => {
                if displacement & 0x2000 == 0 {
                    return (displacement & 0x1FFF) as isize;
                } else {
                    return -(8192 - (displacement & 0x1FFF) as isize);
                } /* end if */
            } /* end 2 */
            4 => {
                if displacement & 0x20000000 == 0 {
                    return (displacement & 0x1FFFFFFF) as isize;
                } else {
                    return -(536870912 - (displacement & 0x1FFFFFFF) as isize);
                } /* end if */
            } /* end 4 */
            _ => return 0,
        } /* end match */
    } /* end convert_displacement_to_signed_decimal */

    fn convert_short_field_to_immediate_value(&self, short_field_code: u8) -> i8 {
        if short_field_code & 0x08 == 0 {
            return (short_field_code & 0x07) as i8;
        } else {
            return -(8 - (short_field_code & 0x07) as i8);
        } /* end if */
    } /* end convert_short_field_to_immediate_value */

    fn convert_short_field_to_processor_register(&self, processor_register_code: u8) -> String {
        match processor_register_code {
            0b0000 => return "us".to_string(),
            0b0001 => return "dcr".to_string(),
            0b0010 => return "bpc".to_string(),
            0b0011 => return "dsr".to_string(),
            0b0100 => return "car".to_string(),
            0b0101 => return "reserved".to_string(),
            0b0110 => return "reserved".to_string(),
            0b0111 => return "reserved".to_string(),
            0b1000 => return "fp".to_string(),
            0b1001 => return "sp".to_string(),
            0b1010 => return "sb".to_string(),
            0b1011 => return "usp".to_string(),
            0b1100 => return "cfg".to_string(),
            0b1101 => return "psr".to_string(),
            0b1110 => return "intbase".to_string(),
            0b1111 => return "mod".to_string(),
            _ => return "Unknown processor register code.".to_string(),
        } /* end match */
    } /* end convert_short_field_to_processor_register */

    fn convert_uw_field_format_5_to_string(&self, uw_field_code: u8) -> String {
        match uw_field_code {
            0b00 => return "".to_string(),
            0b01 => return "w".to_string(),
            0b10 => return "reserved".to_string(),
            0b11 => return "u".to_string(),
            _ => return "Unknown format 5 short code.".to_string(),
        } /* end match */
    } /* end convert_short_field_format_5_to_string */

    fn convert_immediate_value_to_register_list_save(&self, immediate_value: u8) -> String {
        let mut register_list_string = String::from("");
        let mut first_register = true;

        if immediate_value & 0x01 != 0 {
            if first_register {
                register_list_string = format!("{}r0", register_list_string).to_string();
                first_register = false;
            } else {
                register_list_string = format!("{}, r0", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x02 != 0 {
            if first_register {
                register_list_string = format!("{}r1", register_list_string).to_string();
                first_register = false;
            } else {
                register_list_string = format!("{}, r1", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x04 != 0 {
            if first_register {
                register_list_string = format!("{}r2", register_list_string).to_string();
                first_register = false;
            } else {
                register_list_string = format!("{}, r2", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x08 != 0 {
            if first_register {
                register_list_string = format!("{}r3", register_list_string).to_string();
                first_register = false;
            } else {
                register_list_string = format!("{}, r3", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x10 != 0 {
            if first_register {
                register_list_string = format!("{}r4", register_list_string).to_string();
                first_register = false;
            } else {
                register_list_string = format!("{}, r4", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x20 != 0 {
            if first_register {
                register_list_string = format!("{}r5", register_list_string).to_string();
                first_register = false;
            } else {
                register_list_string = format!("{}, r5", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x40 != 0 {
            if first_register {
                register_list_string = format!("{}r6", register_list_string).to_string();
                first_register = false;
            } else {
                register_list_string = format!("{}, r6", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x80 != 0 {
            if first_register {
                register_list_string = format!("{}r7", register_list_string).to_string();
            } else {
                register_list_string = format!("{}, r7", register_list_string).to_string();
            } /* end if */
        } /* end if */

        return register_list_string;
    } /* end convert_immediate_value_to_register_list_save */

    fn convert_immediate_value_to_register_list_exit(&self, immediate_value: u8) -> String {
        let mut register_list_string = String::from("");
        let mut first_register = true;

        if immediate_value & 0x80 != 0 {
            if first_register {
                first_register = false;
                register_list_string = format!("{}r0", register_list_string).to_string();
            } else {
                register_list_string = format!("{}, r0", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x40 != 0 {
            if first_register {
                first_register = false;
                register_list_string = format!("{}r1", register_list_string).to_string();
            } else {
                register_list_string = format!("{}, r1", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x20 != 0 {
            if first_register {
                first_register = false;
                register_list_string = format!("{}r2", register_list_string).to_string();
            } else {
                register_list_string = format!("{}, r2", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x10 != 0 {
            if first_register {
                first_register = false;
                register_list_string = format!("{}r3", register_list_string).to_string();
            } else {
                register_list_string = format!("{}, r3", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x08 != 0 {
            if first_register {
                first_register = false;
                register_list_string = format!("{}r4", register_list_string).to_string();
            } else {
                register_list_string = format!("{}, r4", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x04 != 0 {
            if first_register {
                first_register = false;
                register_list_string = format!("{}r5", register_list_string).to_string();
            } else {
                register_list_string = format!("{}, r5", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x02 != 0 {
            if first_register {
                first_register = false;
                register_list_string = format!("{}r6", register_list_string).to_string();
            } else {
                register_list_string = format!("{}, r6", register_list_string).to_string();
            } /* end if */
        } /* end if */
        if immediate_value & 0x01 != 0 {
            if first_register {
                register_list_string = format!("{}r7", register_list_string).to_string();
            } else {
                register_list_string = format!("{}, r7", register_list_string).to_string();
            } /* end if */
        } /* end if */

        return register_list_string;
    } /* end convert_immediate_value_to_register_list_exit */

    fn convert_format_5_cfg_list_to_string(&self, cfg_list_code: u8) -> String {
        let mut cfg_list_string = String::from("");
        let mut first_cfg = true;

        if cfg_list_code & 0x08 != 0 {
            if first_cfg {
                first_cfg = false;
                cfg_list_string = format!("{}c", cfg_list_string).to_string();
            } else {
                cfg_list_string = format!("{}, c", cfg_list_string).to_string();
            } /* end if */
        } /* end if */
        if cfg_list_code & 0x04 != 0 {
            if first_cfg {
                first_cfg = false;
                cfg_list_string = format!("{}m", cfg_list_string).to_string();
            } else {
                cfg_list_string = format!("{}, m", cfg_list_string).to_string();
            } /* end if */
        } /* end if */
        if cfg_list_code & 0x02 != 0 {
            if first_cfg {
                first_cfg = false;
                cfg_list_string = format!("{}f", cfg_list_string).to_string();
            } else {
                cfg_list_string = format!("{}, f", cfg_list_string).to_string();
            } /* end if */
        } /* end if */
        if cfg_list_code & 0x01 != 0 {
            if first_cfg {
                cfg_list_string = format!("{}i", cfg_list_string).to_string();
            } else {
                cfg_list_string = format!("{}, i", cfg_list_string).to_string();
            } /* end if */
        } /* end if */

        return cfg_list_string;
    } /* end convert_format_5_cfg_list_to_string */

    /* ********************************************************************* */

    fn determine_format_from_address(&self, address: usize) -> Result<u8, std::io::Error> {
        let first_byte = self.fetch_byte_at_address(address);

        match first_byte {
            0b0000_1110 => {
                let third_byte = self.fetch_byte_at_address(address + 2);
                match third_byte & 0xF8 {
                    0b0000_0000 => return Ok(5),
                    _ => {
                        return Err(Error::new(
                            ErrorKind::Other,
                            format!(
                                "determine_format_from_address. Not yet decoded format byte with hex value. '{:#04X}' at address: {:X}",
                                first_byte, address
                            ),
                        ))
                    }
                } /* end match */
            } /* end match */
            0b0100_1110 => return Ok(6),
            0b1100_1110 => return Ok(7),
            0b0011_1110 => return Ok(9),
            0b0111_1110 => return Ok(10),
            0b1011_1110 => return Ok(11),
            0b1111_1110 => return Ok(12),
            0b1001_1110 => return Ok(13),
            0b0001_1110 => return Ok(14),
            0b0101_1110 => return Ok(16),
            0b1101_1110 => return Ok(17),
            0b1000_1110 => return Ok(18),
            _ => (),
        }; /* end match */

        match first_byte & 0x3F {
            0b101110 => return Ok(8),
            _ => (),
        } /* end match */

        match first_byte & 0x7C {
            0b0111_1100 => return Ok(3),
            _ => (),
        } /* end match */

        match first_byte & 0x0F {
            0b1010 => return Ok(0),
            0b0010 => return Ok(1),
            _ => match first_byte & 0x0C {
                0b1100 => return Ok(2),
                _ => (),
            }, /* end match */
        } /* end match */
        return Err(Error::new(
            ErrorKind::Other,
            format!(
                "determine_format_from_address. Not yet decoded format byte with hex value. '{:#04X}' at address: {:X}",
                first_byte, address
            ),
        ));
    } /* end determine_format_from_address */

    fn determine_variable_length_displacement(&self, start_address: usize) -> (usize, u8) {
        let mut displacement: usize;
        let nr_of_bytes_used_for_displacement;
        let first_byte = self.fetch_byte_at_address(start_address);

        // Check for byte displacement
        if first_byte & 0x80 == 0x00 {
            displacement = first_byte as usize;
            nr_of_bytes_used_for_displacement = 1;
        } else {
            // Check for word displacement range
            let second_byte = self.fetch_byte_at_address(start_address + 1);
            if first_byte & 0xC0 == 0x80 {
                displacement = ((first_byte as usize) * 256) + second_byte as usize;
                nr_of_bytes_used_for_displacement = 2;
            } else {
                // Use double word displacement
                let third_byte = self.fetch_byte_at_address(start_address + 2);
                let fourth_byte = self.fetch_byte_at_address(start_address + 3);
                displacement = (first_byte as usize) * 16_777_216;
                displacement = displacement + ((second_byte as usize) * 65_536);
                displacement = displacement + ((third_byte as usize) * 256);
                displacement = displacement + (fourth_byte as usize);
                nr_of_bytes_used_for_displacement = 4
            } /* end if */
        } /* end if */

        return (displacement, nr_of_bytes_used_for_displacement);
    } /* end determine_variable_length_displacement */

    fn determine_variable_length_displacement_in_decimal(&self, start_address: usize) -> (isize, u8) {
        let (displacement, nr_of_bytes_used) = self.determine_variable_length_displacement(start_address);

        return (
            self.convert_displacement_to_signed_decimal(displacement, nr_of_bytes_used),
            nr_of_bytes_used,
        );
    } /* end determine_variable_length_displacement_in_decimal */

    fn determine_immediate_value(&self, start_address: usize) -> (u8, u8) {
        return (self.fetch_byte_at_address(start_address), 1);
    } /* end determine_immediate_value */

    fn determine_count_value(&self, start_address: usize, integer_type_code: u8) -> (String, u8) {
        let (mut count, nr_of_bytes_used_for_displacement) = self.determine_variable_length_displacement_in_decimal(start_address);

        match integer_type_code {
            0 => count = count + 1,
            1 => count = (count / 2) + 1,
            3 => count = (count / 4) + 1,
            _ => (),
        } /* end match */

        return (format!("{}", count), nr_of_bytes_used_for_displacement);
    } /* end determine_count_value */

    /* ********************************************************************* */

    fn address_string(&self, address: usize) -> String {
        return format!("{:08X}", address).to_string();
    } /* end address_string */

    fn bytes_string(&self, address: usize, nr_of_bytes: u8) -> String {
        let mut bytes_string = String::from("");

        // Print out all bytes used in the instruction
        for elem in 0..nr_of_bytes {
            bytes_string.push_str(format!("{:02X} ", self.fetch_byte_at_address(address + elem as usize)).as_str());
        } /* end for */

        return bytes_string;
    } /* end bytes_string */

    fn decimal_displacement_string(&self, displacement: usize, nr_of_bytes_used_for_displacement: u8) -> String {
        let decimal_displacement = self.convert_displacement_to_signed_decimal(displacement, nr_of_bytes_used_for_displacement);
        match nr_of_bytes_used_for_displacement {
            1 | 2 | 4 => return format!("{}", decimal_displacement),
            _ => return format!("Unknown format for a decimal displacement."),
        } /* end match */
    } /* end displacement_string */

    /* ********************************************************************* */

    fn decode_format_0_instruction(&self, address: usize) -> (String, String, String, usize) {
        // A format 0 instruction is easy since it can only be a [conditional] branch
        let op_code = self.fetch_byte_at_address(address);
        let mnenomic = format!("b{}", self.decode_condition_code_field((op_code >> 4) & 0x0F)).to_string();
        let (displacement, nr_of_bytes_for_disp) = self.determine_variable_length_displacement(address + 1);

        let total_nr_of_bytes_consumed = 1 + nr_of_bytes_for_disp;
        return (
            self.address_string(address),
            self.bytes_string(address, total_nr_of_bytes_consumed),
            format!(
                "{} {}",
                mnenomic,
                self.decimal_displacement_string(displacement, nr_of_bytes_for_disp)
            ),
            total_nr_of_bytes_consumed as usize,
        );
    } /* end decode_format_0_instruction */

    /* *** */

    fn decode_format_1_instruction(&self, address: usize) -> (String, String, String, usize) {
        let op_code = self.fetch_byte_at_address(address);
        let mut total_nr_of_bytes_consumed: u8 = 1;
        let mnenomic = format!("{}", self.decode_format_1_op_code_field((op_code >> 4) & 0x0F)).to_string();

        match mnenomic.as_str() {
            "bsr" | "ret" | "cxp" | "rxp" | "rett" => {
                let (displacement, nr_of_bytes_for_disp) = self.determine_variable_length_displacement(address + 1);

                total_nr_of_bytes_consumed = total_nr_of_bytes_consumed + nr_of_bytes_for_disp;

                return (
                    self.address_string(address),
                    self.bytes_string(address, total_nr_of_bytes_consumed),
                    format!(
                        "{} {}",
                        mnenomic,
                        self.decimal_displacement_string(displacement, nr_of_bytes_for_disp)
                    ),
                    total_nr_of_bytes_consumed as usize,
                );
            }
            "reti" | "nop" | "wait" | "dia" | "flag" | "svc" | "bpt" => {
                return (
                    self.address_string(address),
                    self.bytes_string(address, total_nr_of_bytes_consumed),
                    format!("{}", mnenomic),
                    total_nr_of_bytes_consumed as usize,
                )
            }
            "save" => {
                let (immediate_value, nr_of_bytes_for_imm_value) = self.determine_immediate_value(address + 1);
                let register_list = self.convert_immediate_value_to_register_list_save(immediate_value);
                total_nr_of_bytes_consumed = total_nr_of_bytes_consumed + nr_of_bytes_for_imm_value;

                return (
                    self.address_string(address),
                    self.bytes_string(address, total_nr_of_bytes_consumed),
                    format!("{} [{}]", mnenomic, register_list),
                    total_nr_of_bytes_consumed as usize,
                );
            }
            "exit" | "restore" => {
                let (immediate_value, nr_of_bytes_for_imm_value) = self.determine_immediate_value(address + 1);
                let register_list = self.convert_immediate_value_to_register_list_exit(immediate_value);
                total_nr_of_bytes_consumed = total_nr_of_bytes_consumed + nr_of_bytes_for_imm_value;

                return (
                    self.address_string(address),
                    self.bytes_string(address, total_nr_of_bytes_consumed),
                    format!("{} [{}]", mnenomic, register_list),
                    total_nr_of_bytes_consumed as usize,
                );
            }
            "enter" => {
                let (immediate_value, nr_of_bytes_for_imm_value) = self.determine_immediate_value(address + 1);
                let register_list = self.convert_immediate_value_to_register_list_save(immediate_value);

                let (displacement, nr_of_bytes_for_disp) = self.determine_variable_length_displacement(address + 2);

                total_nr_of_bytes_consumed = total_nr_of_bytes_consumed + nr_of_bytes_for_imm_value + nr_of_bytes_for_disp;

                return (
                    self.address_string(address),
                    self.bytes_string(address, total_nr_of_bytes_consumed),
                    format!(
                        "{} [{}], {}",
                        mnenomic,
                        register_list,
                        self.decimal_displacement_string(displacement, nr_of_bytes_for_disp)
                    ),
                    total_nr_of_bytes_consumed as usize,
                );
            }
            _ => return ("".to_string(), "".to_string(), "".to_string(), 1),
        } /* end match */
    } /* end decode_format_1_instruction */

    /* *** */

    fn decode_format_2_instruction(&self, address: usize) -> (String, String, String, usize) {
        let mut nr_of_bytes_used = 2; // minimum nr of bytes for format 2 instructions

        let first_op_code_byte = self.fetch_byte_at_address(address);
        let second_op_code_byte = self.fetch_byte_at_address(address + 1);

        let op_code = (first_op_code_byte & 0x70) >> 4;
        let op_code_string = self.decode_format_2_op_code_field(op_code);

        let integer_type_code = first_op_code_byte & 0x03;
        let integer_type_string = self.decode_integer_type_field(integer_type_code);

        let short_field_code = ((second_op_code_byte & 0x07) << 1) + ((first_op_code_byte & 0x80) >> 7);
        let gen_addr_mode = (second_op_code_byte & 0xF8) >> 3;

        let instruction_string: String;

        let mnenomic = format!("{}{}", op_code_string, integer_type_string);
        match op_code_string.as_str() {
            "addq" | "cmpq" | "movq" => {
                let immediate_value = self.convert_short_field_to_immediate_value(short_field_code);
                let (gen_addr_mode_str, nr_of_bytes_used_for_displacement) =
                    self.decode_general_addressing_mode(gen_addr_mode, address + 2, address + 3, address + 2, true);
                instruction_string = format!("{}\t{}, {}", mnenomic, immediate_value, gen_addr_mode_str);
                nr_of_bytes_used = nr_of_bytes_used + nr_of_bytes_used_for_displacement;
            }
            "spr" | "lpr" => {
                let processor_register_str = self.convert_short_field_to_processor_register(short_field_code);
                let (gen_addr_mode_str, nr_of_bytes_used_for_displacement) =
                    self.decode_general_addressing_mode(gen_addr_mode, address + 2, address + 3, address + 2, true);
                instruction_string = format!("{}\t{}, {}", mnenomic, processor_register_str, gen_addr_mode_str);
                nr_of_bytes_used = nr_of_bytes_used + nr_of_bytes_used_for_displacement;
            }
            "scond" => {
                let condition_str = self.decode_condition_code_field(short_field_code);
                let (gen_addr_mode_str, nr_of_bytes_used_for_displacement) =
                    self.decode_general_addressing_mode(gen_addr_mode, address + 2, address + 3, address + 2, true);
                instruction_string = format!("s{}{}\t{}", condition_str, integer_type_string, gen_addr_mode_str);
                nr_of_bytes_used = nr_of_bytes_used + nr_of_bytes_used_for_displacement;
            }
            "acb" => {
                let immediate_value = self.convert_short_field_to_immediate_value(short_field_code);
                let (gen_addr_mode_str, nr_of_bytes_used_for_displacement) =
                    self.decode_general_addressing_mode(gen_addr_mode, address + 2, address + 3, address + 2, true);
                let (branch, nr_of_bytes_used_for_displacement_for_branch) =
                    self.determine_variable_length_displacement_in_decimal(address + 2 + nr_of_bytes_used_for_displacement as usize);
                instruction_string = format!("{}\t{}, {}, {}", mnenomic, immediate_value, gen_addr_mode_str, branch);
                nr_of_bytes_used = nr_of_bytes_used + nr_of_bytes_used_for_displacement + nr_of_bytes_used_for_displacement_for_branch;
            }
            _ => instruction_string = format!("Unknown format 2 instruction"),
        } /* end match */

        return (
            self.address_string(address),
            self.bytes_string(address, nr_of_bytes_used),
            instruction_string.to_string(),
            nr_of_bytes_used as usize,
        );
    } /* end decode_format_1_instruction */

    /* *** */

    fn decode_format_3_instruction(&self, address: usize) -> (String, String, String, usize) {
        let mut total_nr_of_bytes_used: u8 = 2;
        let first_byte = self.fetch_byte_at_address(address);
        let second_byte = self.fetch_byte_at_address(address + 1);

        let op_code = ((second_byte & 0x07) << 1) | ((first_byte & 0x80) >> 7);
        let op_code_string = self.decode_format_3_op_code_field(op_code);

        let integer_type_code = first_byte & 0x03;
        let integer_type_string = self.decode_integer_type_field(integer_type_code);

        let gen_addr_mode = (second_byte & 0xF8) >> 3;
        let (gen_addr_mode_str, nr_of_bytes_used_for_displacement) =
            self.decode_general_addressing_mode(gen_addr_mode, address + 2, address + 3, address + 2, true);

        let instruction_string: String;
        match op_code_string.as_str() {
            "cxpd" | "jump" | "jsr" => {
                instruction_string = format!("{}\t{}", op_code_string, gen_addr_mode_str);
            }
            "bicpsr" | "bispsr" | "adjsp" | "case" => {
                instruction_string = format!("{}{}\t{}", op_code_string, integer_type_string, gen_addr_mode_str);
            }
            _ => instruction_string = format!("Unknown format 3 instruction"),
        } /* end match */

        total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_used_for_displacement;
        return (
            self.address_string(address),
            self.bytes_string(address, total_nr_of_bytes_used),
            instruction_string.to_string(),
            total_nr_of_bytes_used as usize,
        );
    } /* end decode_format_3_instruction */

    /* *** */

    fn decode_format_5_instruction(&self, address: usize) -> (String, String, String, usize) {
        let second_byte = self.fetch_byte_at_address(address + 1);
        let third_byte = self.fetch_byte_at_address(address + 2);

        let op_code = (second_byte >> 2) & 0x0F;
        let op_code_string = self.decode_format_5_op_code_field(op_code);

        let integer_type_code = second_byte & 0x03;
        let integer_type_string = self.decode_integer_type_field(integer_type_code);

        let string_instruction = (second_byte >> 7) & 0x01;

        let uw_field_code = (third_byte >> 1) & 0x03;
        let uw_field_code_string = self.convert_uw_field_format_5_to_string(uw_field_code);

        let b_code = third_byte & 0x01;
        let direction_string: String;
        if b_code == 0 {
            direction_string = String::from("");
        } else {
            if uw_field_code == 0 {
                direction_string = String::from("b");
            } else {
                direction_string = String::from("b, ");
            } /* end if */
        } /* end if */

        let mnenomic: String;
        if string_instruction == 0 {
            mnenomic = format!("{}{}", op_code_string, integer_type_string);
        } else {
            mnenomic = format!("{}t", op_code_string);
        } /* end if */

        let instruction_string: String;
        match op_code_string.as_str() {
            "movs" | "cmps" | "skps" => {
                instruction_string = format!("{}\t[{}{}]", mnenomic, direction_string, uw_field_code_string);
            }
            "setcfg" => {
                let cfg_list_code = ((third_byte << 1) & 0x0E) + ((second_byte >> 7) & 0x01);
                let cfg_list_string = self.convert_format_5_cfg_list_to_string(cfg_list_code);
                instruction_string = format!("{}\t[{}]", op_code_string, cfg_list_string);
            }
            _ => instruction_string = format!("Unknown format 5 instruction"),
        } /* end match */

        return (
            self.address_string(address),
            self.bytes_string(address, 3),
            instruction_string.to_string(),
            3,
        );
    } /* end decode_format_5_instruction */

    /* *** */

    fn decode_format_6_instruction(&self, address: usize) -> (String, String, String, usize) {
        let second_byte = self.fetch_byte_at_address(address + 1);
        let third_byte = self.fetch_byte_at_address(address + 2);

        let op_code = (second_byte >> 2) & 0x0F;
        let op_code_string = self.decode_format_6_op_code_field(op_code);

        let integer_type_code = second_byte & 0x03;
        let integer_type_string = self.decode_integer_type_field(integer_type_code);

        let gen1_addr_mode = (third_byte & 0xF8) >> 3;
        let gen2_addr_mode = ((third_byte << 2) & 0x1C) + ((second_byte >> 6) & 0x03);

        let (gen1_addr_mode_str, gen2_addr_mode_str, total_nr_of_bytes_used) =
            self.decode_dual_general_addressing_mode(3, gen1_addr_mode, gen2_addr_mode, address, false);

        let instruction_string = format!(
            "{}{}\t{}, {}",
            op_code_string, integer_type_string, gen1_addr_mode_str, gen2_addr_mode_str
        );
        return (
            self.address_string(address),
            self.bytes_string(address, total_nr_of_bytes_used),
            instruction_string.to_string(),
            total_nr_of_bytes_used as usize,
        );
    } /* end decode_format_6_instruction */

    /* *** */

    fn decode_format_7_instruction(&self, address: usize) -> (String, String, String, usize) {
        let second_byte = self.fetch_byte_at_address(address + 1);
        let third_byte = self.fetch_byte_at_address(address + 2);

        let op_code = (second_byte >> 2) & 0x0F;
        let op_code_string = self.decode_format_7_op_code_field(op_code);

        let integer_type_code = second_byte & 0x03;
        let integer_type_string = self.decode_integer_type_field(integer_type_code);

        let gen1_addr_mode = (third_byte & 0xF8) >> 3;
        let gen2_addr_mode = ((third_byte << 2) & 0x1C) + ((second_byte >> 6) & 0x03);

        let (gen1_addr_mode_str, gen2_addr_mode_str, mut total_nr_of_bytes_used) =
            self.decode_dual_general_addressing_mode(3, gen1_addr_mode, gen2_addr_mode, address, true);

        let (count, nr_of_bytes_for_count) = self.determine_count_value(address + total_nr_of_bytes_used as usize, integer_type_code);

        total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_for_count;
        let instruction_string = format!(
            "{}{}\t{}, {}, {}",
            op_code_string, integer_type_string, gen1_addr_mode_str, gen2_addr_mode_str, count
        );
        return (
            self.address_string(address),
            self.bytes_string(address, total_nr_of_bytes_used),
            instruction_string.to_string(),
            total_nr_of_bytes_used as usize,
        );
    } /* end decode_format_7_instruction */

    /* *** */

    fn decode_format_8_instruction(&self, address: usize) -> (String, String, String, usize) {
        let first_byte = self.fetch_byte_at_address(address);
        let second_byte = self.fetch_byte_at_address(address + 1);
        let third_byte = self.fetch_byte_at_address(address + 2);

        let op_code = (second_byte & 0x04) | (first_byte >> 6) & 0x03;
        let mut op_code_string = self.decode_format_8_op_code_field(op_code);

        let integer_type_code = second_byte & 0x03;
        let integer_type_string = self.decode_integer_type_field(integer_type_code);

        let register_code = (second_byte >> 3) & 7;
        let register_code_string = format!("r{}", register_code);

        match op_code_string.as_str() {
            "mov" => match register_code {
                0b001 => op_code_string = "movsu".to_string(),
                0b011 => op_code_string = "movus".to_string(),
                _ => (),
            },
            _ => (),
        } /* end match */

        let gen1_addr_mode = (third_byte & 0xF8) >> 3;
        let gen2_addr_mode = ((third_byte << 2) & 0x1C) + ((second_byte >> 6) & 0x03);

        let (gen1_addr_mode_str, gen2_addr_mode_str, mut total_nr_of_bytes_used) =
            self.decode_dual_general_addressing_mode(3, gen1_addr_mode, gen2_addr_mode, address, true);

        let (count, nr_of_bytes_for_count) = self.determine_variable_length_displacement(address + total_nr_of_bytes_used as usize);

        total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_for_count;
        let instruction_string = format!(
            "{}{}\t{}, {}, {}, {}",
            op_code_string, integer_type_string, register_code_string, gen1_addr_mode_str, gen2_addr_mode_str, count
        );
        return (
            self.address_string(address),
            self.bytes_string(address, total_nr_of_bytes_used),
            instruction_string.to_string(),
            total_nr_of_bytes_used as usize,
        );
    } /* end decode_format_8_instruction */

    /* *** */

    fn decode_format_9_instruction(&self, address: usize) -> (String, String, String, usize) {
        // Remark: This instruction concerns floating point registers f0..f7. However the GCC ns32k
        // assembler doesn't make any distinction between r0..r7 and f0..f7. E.g. the following\
        // generates exactly the same byte stream
        //
        //      0000 3E0438         movbf   f7, f0
        //      0003 3E0438         movbf   r7, r0
        // To keep my code a bit simple, Idecided not to add a flag [yet] to return r of f type
        // of registers.
        //
        let second_byte = self.fetch_byte_at_address(address + 1);
        let third_byte = self.fetch_byte_at_address(address + 2);

        let op_code = (second_byte & 0x38) >> 3;
        let op_code_string = self.decode_format_9_op_code_field(op_code);

        let integer_type_code = second_byte & 0x03;
        let integer_type_string = self.decode_integer_type_field(integer_type_code);

        let float_type_code = (second_byte & 0x04) >> 2;
        let mut float_type_string: String = String::from("");
        match float_type_code {
            0b0 => float_type_string = format!("l"),
            0b1 => float_type_string = format!("f"),
            _ => (),
        } /* end match */

        let gen1_addr_mode = (third_byte & 0xF8) >> 3;
        let gen2_addr_mode = ((third_byte << 2) & 0x1C) + ((second_byte >> 6) & 0x03);

        let (gen1_addr_mode_str, gen2_addr_mode_str, total_nr_of_bytes_used) =
            self.decode_dual_general_addressing_mode(3, gen1_addr_mode, gen2_addr_mode, address, true);

        let instruction_string = format!(
            "{}{}{}\t{}, {}",
            op_code_string, integer_type_string, float_type_string, gen1_addr_mode_str, gen2_addr_mode_str
        );
        return (
            self.address_string(address),
            self.bytes_string(address, total_nr_of_bytes_used),
            instruction_string.to_string(),
            total_nr_of_bytes_used as usize,
        );
    } /* end decode_format_9_instruction */

    /* *** */

    fn decode_format_11_instruction(&self, address: usize) -> (String, String, String, usize) {
        let second_byte = self.fetch_byte_at_address(address + 1);
        let third_byte = self.fetch_byte_at_address(address + 2);

        let op_code = (second_byte & 0x3C) >> 3;
        let op_code_string = self.decode_format_11_op_code_field(op_code);

        let float_type_code = second_byte & 0x01;
        let mut float_type_string: String = String::from("");
        match float_type_code {
            0b0 => float_type_string = format!("l"),
            0b1 => float_type_string = format!("f"),
            _ => (),
        } /* end match */

        let gen1_addr_mode = (third_byte & 0xF8) >> 3;
        let gen2_addr_mode = ((third_byte << 2) & 0x1C) + ((second_byte >> 6) & 0x03);

        let (gen1_addr_mode_str, gen2_addr_mode_str, total_nr_of_bytes_used) =
            self.decode_dual_general_addressing_mode(3, gen1_addr_mode, gen2_addr_mode, address, true);

        let instruction_string = format!(
            "{}{}\t{}, {}",
            op_code_string, float_type_string, gen1_addr_mode_str, gen2_addr_mode_str
        );
        return (
            self.address_string(address),
            self.bytes_string(address, total_nr_of_bytes_used),
            instruction_string.to_string(),
            total_nr_of_bytes_used as usize,
        );
    } /* end decode_format_11_instruction */

    /* *** */

    fn decode_format_14_instruction(&self, address: usize) -> (String, String, String, usize) {
        let mut total_nr_of_bytes_used: u8 = 3;

        let second_byte = self.fetch_byte_at_address(address + 1);
        let third_byte = self.fetch_byte_at_address(address + 2);

        let op_code = (second_byte & 0x3C) >> 3;
        let op_code_string = self.decode_format_14_op_code_field(op_code);

        // The 4 bits short code seems to be ignored by the GCC ns32k assembler
        // let short_code = ((third_byte & 0x07) << 1) | ((second_byte & 0x80) >> 7);
        // let short_code_str = self.convert_short_field_to_immediate_value(short_code);

        let gen_addr_mode = (third_byte & 0xF8) >> 3;
        let (gen_addr_mode_str, nr_of_bytes_used_for_displacement) =
            self.decode_general_addressing_mode(gen_addr_mode, address + 3, address + 4, address + 3, true);

        total_nr_of_bytes_used = total_nr_of_bytes_used + nr_of_bytes_used_for_displacement;

        let instruction_string = format!("{}\t{}", op_code_string, gen_addr_mode_str);
        return (
            self.address_string(address),
            self.bytes_string(address, total_nr_of_bytes_used),
            instruction_string.to_string(),
            total_nr_of_bytes_used as usize,
        );
    } /* end decode_format_14_instruction */

    /* ********************************************************************* */
} /* end impl Ns32kDisassembler */
