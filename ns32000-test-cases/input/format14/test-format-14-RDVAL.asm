.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 14 instructions
#
# Instruction under test: RDVAL
#
# Syntax.   RDVAL   loc
# `                 gen
#                   addr
#
#           !   src   !               RDVAL                 !
#           +---------+-------------------------------------+
#           !   gen   !0 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
#
# The RDVAL instruction checks the protection level assigned to the user-mode
# virtual memory address specified as loc. If the address is allowed to be read
# while the CPU is in user mode, the F flag in the PSR is cleared. If the address
# is not allowed to be read, the F flag in the PSR is set. An address which is
# protected against reading is also protected against writing, and is therefore
# inaccessible for any use by a user-mode program.
#
    rdval 512(r0)
#
# Register
#
    rdval   r0
    rdval   r1
    rdval   r2
    rdval   r3
    rdval   r4
    rdval   r5
    rdval   r6
    rdval   r7
#
# Register relative
#
    rdval   -64(r7)
    rdval   63(r6)
    rdval   8191(r5)
    rdval   -8192(r4)
    rdval   -536870912(r3)
    rdval   536870911(r2)
    rdval   -64(r1)
    rdval   63(r0)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    rdval   -64(63(fp))
    rdval   -64(63(fp))
    rdval   63(-64(fp))
    rdval   63(-64(fp))
    rdval   -8192(63(fp))
    rdval   -64(8191(fp))
    rdval   63(-8192(fp))
    rdval   8191(-64(fp))
    rdval   536870911(-64(fp))
    rdval   -536870912(-8192(fp))
    #
    # Stack Pointer (sp)
    #
    rdval   -64(63(sp))
    rdval   -64(63(sp))
    rdval   63(-64(sp))
    rdval   63(-64(sp))
    rdval   -8192(63(sp))
    rdval   -64(8191(sp))
    rdval   63(-8192(sp))
    rdval   8191(-64(sp))
    rdval   536870911(-64(sp))
    rdval   -536870912(-8192(sp))
    #
    # Static Memory (sb)
    #
    rdval   -64(63(sb))
    rdval   -64(63(sb))
    rdval   63(-64(sb))
    rdval   63(-64(sb))
    rdval   -8192(63(sb))
    rdval   -64(8191(sb))
    rdval   63(-8192(sb))
    rdval   8191(-64(sb))
    rdval   536870911(-64(sb))
    rdval   -536870912(-8192(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    rdval	@-64
    rdval	@-65
    rdval	@-8192
    rdval	@8191
    rdval	@-536870912
    rdval	@536870911 
#
#
# External
#
    rdval	ext(-64) + 63
    rdval	ext(-64) + 63
    rdval	ext(63) + -64
    rdval	ext(63) + -64
    rdval	ext(-536870912) + 8191
    rdval	ext(-536870912) + 8191
    rdval	ext(8191) + -536870912 
    rdval	ext(8191) + -536870912 
#
# Top Of Stack
#
    rdval	tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    rdval	-64(fp)
    rdval	-8192(fp)
    rdval	-536870912(fp)
    #
    # Stack Pointer (sp)
    #
    rdval	-64(sp)
    rdval	-8192(sp)
    rdval	-536870912(sp)
    #
    # Static Memory (sb)
    #
    rdval	-64(sb)
    rdval	-8192(sb)
    rdval	-536870912(sb)
    #
    # Program Counter (pc)
    #
    rdval	-64(pc)
    rdval	-8192(pc)
    rdval	-536870912(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    rdval   r7[r0:b]
    rdval   r6[r1:b]
    #
    # Register Relative
    rdval   63(r5)[r2:b]
    rdval   63(r4)[r3:b]
    rdval   8191(r3)[r4:b]
    rdval   536870911(r1)[r6:b]
    #
    # Memory Relative
    rdval   -64(63(fp))[r0:b]
    rdval   -8192(8191(fp))[r3:b]
    rdval   -536870912(536870911(fp))[r4:b]
    #
    # Absolute
    rdval   @63[r0:b]
    rdval   @8191[r3:b]
    rdval   @536870911[r4:b]
    #
    # External
    rdval   ext(-64) + 63[r7:b]
    rdval   ext(-8192) + 63[r5:b]
    rdval   ext(-536870912) + 536870911[r3:b]
    #
    # Top Of Stack
    rdval   tos[r0:b]
    #
    # Memory Space
    rdval   63(fp)[r0:b]
    rdval   8191(fp)[r0:b]
    rdval   536870911(fp)[r0:b]
    rdval   63(sp)[r0:b]
    rdval   8191(sp)[r0:b]
    rdval   536870911(sp)[r0:b]
    rdval   63(sb)[r0:b]
    rdval   8191(sb)[r0:b]
    rdval   536870911(sb)[r0:b]
    rdval   63(pc)[r0:b]
    rdval   8191(pc)[r0:b]
    rdval   536870911(pc)[r0:b]
#
end:
# 
