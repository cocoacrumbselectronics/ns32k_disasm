.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 5 instructions
#
# Instruction under test: MOVS
#
# Syntax:   MOVSi options
#
#           !                     MOVSi                     !
#           +---------+---+-+-+---------+---+---------------+
#           !0 0 0 0 0!UW !B!0!0 0 0 0 0! i !0 0 0 0 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
# 
# Syntax:   MOVST options
#
#           !                     MOVST                     !
#           +---------+---+-+-+---------+---+---------------+
#           !0 0 0 0 0!UW !B!1!0 0 0 0 0!0 0!0 0 0 0 1 1 1 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            23           16 15            8 7             0
#
# The MOVSi instruction copies consecutive elements of String 1 (address in R1) to
# consecutive element locations in String 2 (address in R2). After an element is
# copied, the instruction sets register R1 to the address of the next element to
# copy, sets register R2 to the address of the next location to receive an element,
# and sets R0 to the number of elements remaining to be copied. See Section 3.7
# for the exact sequences followed by String instructions.
#
# The MOVST instruction copies one-byte elements from String 1, after translation,
# to String 2. The translated value to be copied is found by adding the current
# element from String 1 as an unsigned integer to the translation table address
# found in register R3. The instruction copies elements and sets registers as
# described above. See Section 3.7 for details of string translation.
#
    movsb   []
    movsw   []
    movsd   []
    movst   []
#
    movsb   [b]
    movsb   [w]
    movsb   [b, w]
    movsb   [u]
    movsb   [b, u]
#
    movsw   [b]
    movsw   [w]
    movsw   [b, w]
    movsw   [u]
    movsw   [b, u]
#
    movsd   [b]
    movsd   [w]
    movsd   [b, w]
    movsd   [u]
    movsd   [b, u]
#
    movst   [b]
    movst   [w]
    movst   [b, w]
    movst   [u]
    movst   [b, u]
#
end:
