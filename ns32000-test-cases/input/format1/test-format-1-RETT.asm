.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: RETT
#
# Syntax:   RETT    constant
#                   disp
#
#           !     RETT      !
#           +---------------+
#           !0 1 0 0 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The RETT instruction returns control from a trap service procedure. It restores
# the PC, MOD and PSR. registers from the currently-selected stack, updates the SB
# register, and then removes any parameters passed by the procedure which caused
# the trap.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
    rett    63
    rett    -64
    rett    8191
    rett    -8192
    rett    536870911
    rett    -536870912
#
    rett    5
    rett    -18
    rett    4025
    rett    -125
    rett    5987621
    rett    -24869248
#
end:
