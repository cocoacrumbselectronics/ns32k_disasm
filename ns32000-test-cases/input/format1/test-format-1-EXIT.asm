.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: EXIT
#
# Syntax:   EXIT    reglist
#                   imm
#
#           !     EXIT      !
#           +---------------+
#           !1 0 0 1 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The EXIT instruction removes the frame of the current procedure from the stack,
# restores the former contents of the specified General-Purpose registers (i.e.,
# their contents prior to entering the current procedure), and restores the frame
# of the previous procedure as the current procedure context.
#
    exit    [r0]
    exit    [r1]
    exit    [r2]
    exit    [r3]
    exit    [r4]
    exit    [r5]
    exit    [r6]
    exit    [r7]
#
    exit    [r0, r1]
    exit    [r0, r1, r2]
    exit    [r0, r1, r2, r3]
    exit    [r0, r1, r2, r3, r4]
    exit    [r0, r1, r2, r3, r4, r5]
    exit    [r0, r1, r2, r3, r4, r5, r6]
    exit    [r0, r1, r2, r3, r4, r5, r6, r7]
#
end:
