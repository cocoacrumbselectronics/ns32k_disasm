.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 3 instructions
#
# Instruction under test: BISPSR
#
# Syntax:   BISPSRB src
#                   gen
#                   read.B
#
#           !   src   !        BISPSRB      !
#           +---------+---------------------+
#           !   gen   !0 1 1 0 1 1 1 1 1 0 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# Syntax:   BISPSRW src
#                   gen
#                   read.W
#
#           !   src   !        bispsrW      !
#           +---------+---------------------+
#           !   gen   !0 1 1 0 1 1 1 1 1 0 1!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
# 
# The BISPSRB and BISPSRW instructions set the bits in the PSR corresponding to the
# "1" bits in the src operand.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
# Register
#
    bispsrb   r0
    bispsrw   r1
    bispsrb   r2
    bispsrw   r3
    bispsrb   r4
    bispsrw   r5
    bispsrb   r6
    bispsrw   r7
#
# Register relative
#
    bispsrb   63(r0)
    bispsrw   -64(r0)
    bispsrb   8191(r0)
    bispsrw   -8192(r0)
    bispsrb   536870911(r0)
    bispsrw   -536870912(r0)
    #
    bispsrb   63(r1)
    bispsrw   -64(r1)
    bispsrb   8191(r1)
    bispsrw   -8192(r1)
    bispsrb   536870911(r1)
    bispsrw   -536870912(r1)
    #
    bispsrb   63(r2)
    bispsrw   -64(r2)
    bispsrb   8191(r2)
    bispsrw   -8192(r2)
    bispsrb   536870911(r2)
    bispsrw   -536870912(r2)
    #
    bispsrb   63(r3)
    bispsrw   -64(r3)
    bispsrb   8191(r3)
    bispsrw   -8192(r3)
    bispsrb   536870911(r3)
    bispsrw   -536870912(r3)
    #
    bispsrb   63(r4)
    bispsrw   -64(r4)
    bispsrb   8191(r4)
    bispsrw   -8192(r4)
    bispsrb   536870911(r4)
    bispsrw   -536870912(r4)
    #
    bispsrb   63(r5)
    bispsrw   -64(r5)
    bispsrb   8191(r5)
    bispsrw   -8192(r5)
    bispsrb   536870911(r5)
    bispsrw   -536870912(r5)
    #
    bispsrb   63(r6)
    bispsrw   -64(r6)
    bispsrb   8191(r6)
    bispsrw   -8192(r6)
    bispsrb   536870911(r6)
    bispsrw   -536870912(r6)
    #
    bispsrb   63(r7)
    bispsrw   -64(r7)
    bispsrb   8191(r7)
    bispsrw   -8192(r7)
    bispsrb   536870911(r7)
    bispsrw   -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    bispsrb   -64(63(fp))
    bispsrw   63(-64(fp))
    bispsrb   -8192(63(fp))
    bispsrw   -64(8191(fp))
    bispsrb   63(-8192(fp))
    bispsrw   8191(-64(fp))
    bispsrb   -8192(8191(fp))
    bispsrw   8191(-8192(fp))
    bispsrb   -536870912(8191(fp))
    bispsrw   -8192(536870911(fp))
    bispsrb   8191(-536870912(fp))
    bispsrw   536870911(-8192(fp))
    bispsrb   536870911(-536870912(fp))
    bispsrw   -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    bispsrb   -64(63(sp))
    bispsrw   63(-64(sp))
    bispsrb   -8192(63(sp))
    bispsrw   -64(8191(sp))
    bispsrb   63(-8192(sp))
    bispsrw   8191(-64(sp))
    bispsrb   -8192(8191(sp))
    bispsrw   8191(-8192(sp))
    bispsrb   -536870912(8191(sp))
    bispsrw   -8192(536870911(sp))
    bispsrb   8191(-536870912(sp))
    bispsrw   536870911(-8192(sp))
    bispsrb   536870911(-536870912(sp))
    bispsrw   -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    bispsrb   -64(63(sb))
    bispsrw   63(-64(sb))
    bispsrb   -8192(63(sb))
    bispsrw   -64(8191(sb))
    bispsrb   63(-8192(sb))
    bispsrw   8191(-64(sb))
    bispsrb   -8192(8191(sb))
    bispsrw   8191(-8192(sb))
    bispsrb   -536870912(8191(sb))
    bispsrw   -8192(536870911(sb))
    bispsrb   8191(-536870912(sb))
    bispsrw   536870911(-8192(sb))
    bispsrb   536870911(-536870912(sb))
    bispsrw   -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    bispsrb	@-64
    bispsrw	@63
    bispsrb	@-8192
    bispsrw	@8191
    bispsrb	@-536870912
    bispsrw	@536870911
#
# External
#
    bispsrb	ext(-64) + 63
    bispsrw	ext(63) + -64
    bispsrb	ext(-8192) + 8191
    bispsrw	ext(8191) + -8192
    bispsrb	ext(-536870912) + 8191
    bispsrw	ext(8191) + -536870912
    bispsrb	ext(-8192) + 536870911
    bispsrw	ext(536870911) + -8192
    bispsrb	ext(-536870912) + 536870911
    bispsrw	ext(536870911) + -536870912
#
# Top Of Stack
#
    bispsrb	tos
    bispsrw	tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    bispsrb	-64(fp)
    bispsrw	63(fp)
    bispsrb	-8192(fp)
    bispsrw	8191(fp)
    bispsrb	-536870912(fp)
    bispsrw	536870911(fp)
    #
    # Stack Pointer (sp)
    #
    bispsrb	-64(sp)
    bispsrw	63(sp)
    bispsrb	-8192(sp)
    bispsrw	8191(sp)
    bispsrb	-536870912(sp)
    bispsrw	536870911(sp)
    #
    # Static Memory (sb)
    #
    bispsrb	-64(sb)
    bispsrw	63(sb)
    bispsrb	-8192(sb)
    bispsrw	8191(sb)
    bispsrb	-536870912(sb)
    bispsrw	536870911(sb)
    #
    # Program Counter (pc)
    #
    bispsrb	-64(pc)
    bispsrw	63(pc)
    bispsrb	-8192(pc)
    bispsrw	8191(pc)
    bispsrb	-536870912(pc)
    bispsrw	536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    bispsrb   r7[r0:b]
    bispsrw   r7[r0:b]
    #
    # Register Relative
    bispsrb   63(r5)[r2:b]
    bispsrw   8191(r3)[r4:b]
    bispsrb   536870911(r1)[r6:b]
    #
    # Memory Relative
    bispsrw   -64(63(fp))[r0:b]
    bispsrb   -8192(8191(fp))[r2:b]
    bispsrw   -536870912(536870911(fp))[r4:b]
    bispsrb   -64(63(sp))[r7:b]
    bispsrw   -8192(8191(sp))[r5:b]
    bispsrb   -536870912(536870911(sp))[r3:b]
    bispsrw   -64(63(sb))[r0:b]
    bispsrb   -8192(8191(sb))[r2:b]
    bispsrw   -536870912(536870911(sb))[r4:b]
    #
    # Absolute
    bispsrb   @63[r0:b]
    bispsrw   @8191[r2:b]
    bispsrb   @536870911[r4:b]
    #
    # External
    bispsrw   ext(-64) + 63[r7:b]
    bispsrb   ext(-8192) + 8191[r5:b]
    bispsrw   ext(-536870912) + 536870911[r3:b]
    #
    # Top Of Stack
    bispsrb   tos[r0:b]
    bispsrw   tos[r0:b]
    #
    # Memory Space
    bispsrb   63(fp)[r0:b]
    bispsrw   8191(fp)[r0:b]
    bispsrb   536870911(fp)[r0:b]
    bispsrw   63(sp)[r0:b]
    bispsrb   8191(sp)[r0:b]
    bispsrw   536870911(sp)[r0:b]
    bispsrb   63(sb)[r0:b]
    bispsrw   8191(sb)[r0:b]
    bispsrb   536870911(sb)[r0:b]
    bispsrw   63(pc)[r0:b]
    bispsrb   8191(pc)[r0:b]
    bispsrw   536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    bispsrb   r1[r0:w]
    #
    # Register Relative
    bispsrw   63(r1)[r0:w]
    bispsrb   8191(r1)[r0:w]
    bispsrw   536870911(r1)[r0:w]
    #
    # Memory Relative
    bispsrb   -64(63(fp))[r0:w]
    bispsrw   -8192(8191(fp))[r2:w]
    bispsrb   -536870912(536870911(fp))[r4:w]
    bispsrw   -64(63(sp))[r7:w]
    bispsrb   -8192(8191(sp))[r5:w]
    bispsrw   -536870912(536870911(sp))[r3:w]
    bispsrb   -64(63(sb))[r0:w]
    bispsrw   -8192(8191(sb))[r2:w]
    bispsrb   -536870912(536870911(sb))[r4:w]
    #
    # Absolute
    bispsrw   @63[r0:w]
    bispsrb   @8191[r0:w]
    bispsrw   @536870911[r0:w]
    #
    # External
    bispsrb   ext(-64) + 63[r0:w]
    bispsrw   ext(-8192) + 8191[r0:w]
    bispsrb   ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    bispsrb   tos[r0:w]
    bispsrw   tos[r0:w]
    #
    # Memory Space
    bispsrb   63(fp)[r0:w]
    bispsrw   8191(fp)[r0:w]
    bispsrb   536870911(fp)[r0:w]
    bispsrw   63(sp)[r0:w]
    bispsrb   8191(sp)[r0:w]
    bispsrw   536870911(sp)[r0:w]
    bispsrb   63(sb)[r0:w]
    bispsrw   8191(sb)[r0:w]
    bispsrb   536870911(sb)[r0:w]
    bispsrw   63(pc)[r0:w]
    bispsrb   8191(pc)[r0:w]
    bispsrw   536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    bispsrb   r1[r0:d]
    #
    # Register Relative
    bispsrw   63(r1)[r0:d]
    bispsrb   8191(r1)[r0:d]
    bispsrw   536870911(r1)[r0:d]
    #
    # Memory Relative
    bispsrb   -64(63(fp))[r0:d]
    bispsrw   -8192(8191(fp))[r2:d]
    bispsrb   -536870912(536870911(fp))[r4:d]
    bispsrw   -64(63(sp))[r7:d]
    bispsrb   -8192(8191(sp))[r5:d]
    bispsrw   -536870912(536870911(sp))[r3:d]
    bispsrb   -64(63(sb))[r0:d]
    bispsrw   -8192(8191(sb))[r2:d]
    bispsrb   -536870912(536870911(sb))[r4:d]
    #
    # Absolute
    bispsrb   @63[r0:d]
    bispsrw   @8191[r0:d]
    bispsrb   @536870911[r0:d]
    #
    # External
    bispsrb   ext(-64) + 63[r0:d]
    bispsrw   ext(-8192) + 8191[r0:d]
    bispsrb   ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    bispsrb   tos[r0:d]
    bispsrw   tos[r0:d]
    #
    # Memory Space
    bispsrb   63(fp)[r0:d]
    bispsrw   8191(fp)[r0:d]
    bispsrb   536870911(fp)[r0:d]
    bispsrw   63(sp)[r0:d]
    bispsrb   8191(sp)[r0:d]
    bispsrw   536870911(sp)[r0:d]
    bispsrb   63(sb)[r0:d]
    bispsrw   8191(sb)[r0:d]
    bispsrb   536870911(sb)[r0:d]
    bispsrw   63(pc)[r0:d]
    bispsrb   8191(pc)[r0:d]
    bispsrw   536870911(pc)[r0:d]
#
end:
