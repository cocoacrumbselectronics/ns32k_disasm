.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 3 instructions
#
# Instruction under test: BICPSR
#
# Syntax:   BICPSRB src
#                   gen
#                   read.B
#
#           !   src   !        BICPSRB      !
#           +---------+---------------------+
#           !   gen   !0 0 1 0 1 1 1 1 1 0 0!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# Syntax:   BICPSRW src
#                   gen
#                   read.W
#
#           !   src   !        BICPSRW      !
#           +---------+---------------------+
#           !   gen   !0 0 1 0 1 1 1 1 1 0 1!
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
# 
# The Bit Clear in PSR instructions clear (set to 0) the bits in the PSR correspon-
# ding to the "1" bits in the src operand. The BICPSRB instruction affects only
# the low-order byte of the PSR; the BICPSRW instruction affects the entire PSR.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
# Register
#
    bicpsrb   r0
    bicpsrw   r1
    bicpsrb   r2
    bicpsrw   r3
    bicpsrb   r4
    bicpsrw   r5
    bicpsrb   r6
    bicpsrw   r7
#
# Register relative
#
    bicpsrb   63(r0)
    bicpsrw   -64(r0)
    bicpsrb   8191(r0)
    bicpsrw   -8192(r0)
    bicpsrb   536870911(r0)
    bicpsrw   -536870912(r0)
    #
    bicpsrb   63(r1)
    bicpsrw   -64(r1)
    bicpsrb   8191(r1)
    bicpsrw   -8192(r1)
    bicpsrb   536870911(r1)
    bicpsrw   -536870912(r1)
    #
    bicpsrb   63(r2)
    bicpsrw   -64(r2)
    bicpsrb   8191(r2)
    bicpsrw   -8192(r2)
    bicpsrb   536870911(r2)
    bicpsrw   -536870912(r2)
    #
    bicpsrb   63(r3)
    bicpsrw   -64(r3)
    bicpsrb   8191(r3)
    bicpsrw   -8192(r3)
    bicpsrb   536870911(r3)
    bicpsrw   -536870912(r3)
    #
    bicpsrb   63(r4)
    bicpsrw   -64(r4)
    bicpsrb   8191(r4)
    bicpsrw   -8192(r4)
    bicpsrb   536870911(r4)
    bicpsrw   -536870912(r4)
    #
    bicpsrb   63(r5)
    bicpsrw   -64(r5)
    bicpsrb   8191(r5)
    bicpsrw   -8192(r5)
    bicpsrb   536870911(r5)
    bicpsrw   -536870912(r5)
    #
    bicpsrb   63(r6)
    bicpsrw   -64(r6)
    bicpsrb   8191(r6)
    bicpsrw   -8192(r6)
    bicpsrb   536870911(r6)
    bicpsrw   -536870912(r6)
    #
    bicpsrb   63(r7)
    bicpsrw   -64(r7)
    bicpsrb   8191(r7)
    bicpsrw   -8192(r7)
    bicpsrb   536870911(r7)
    bicpsrw   -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    bicpsrb   -64(63(fp))
    bicpsrw   63(-64(fp))
    bicpsrb   -8192(63(fp))
    bicpsrw   -64(8191(fp))
    bicpsrb   63(-8192(fp))
    bicpsrw   8191(-64(fp))
    bicpsrb   -8192(8191(fp))
    bicpsrw   8191(-8192(fp))
    bicpsrb   -536870912(8191(fp))
    bicpsrw   -8192(536870911(fp))
    bicpsrb   8191(-536870912(fp))
    bicpsrw   536870911(-8192(fp))
    bicpsrb   536870911(-536870912(fp))
    bicpsrw   -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    bicpsrb   -64(63(sp))
    bicpsrw   63(-64(sp))
    bicpsrb   -8192(63(sp))
    bicpsrw   -64(8191(sp))
    bicpsrb   63(-8192(sp))
    bicpsrw   8191(-64(sp))
    bicpsrb   -8192(8191(sp))
    bicpsrw   8191(-8192(sp))
    bicpsrb   -536870912(8191(sp))
    bicpsrw   -8192(536870911(sp))
    bicpsrb   8191(-536870912(sp))
    bicpsrw   536870911(-8192(sp))
    bicpsrb   536870911(-536870912(sp))
    bicpsrw   -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    bicpsrb   -64(63(sb))
    bicpsrw   63(-64(sb))
    bicpsrb   -8192(63(sb))
    bicpsrw   -64(8191(sb))
    bicpsrb   63(-8192(sb))
    bicpsrw   8191(-64(sb))
    bicpsrb   -8192(8191(sb))
    bicpsrw   8191(-8192(sb))
    bicpsrb   -536870912(8191(sb))
    bicpsrw   -8192(536870911(sb))
    bicpsrb   8191(-536870912(sb))
    bicpsrw   536870911(-8192(sb))
    bicpsrb   536870911(-536870912(sb))
    bicpsrw   -536870912(536870911(sb))
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    bicpsrb	@-64
    bicpsrw	@63
    bicpsrb	@-8192
    bicpsrw	@8191
    bicpsrb	@-536870912
    bicpsrw	@536870911
#
# External
#
    bicpsrb	ext(-64) + 63
    bicpsrw	ext(63) + -64
    bicpsrb	ext(-8192) + 8191
    bicpsrw	ext(8191) + -8192
    bicpsrb	ext(-536870912) + 8191
    bicpsrw	ext(8191) + -536870912
    bicpsrb	ext(-8192) + 536870911
    bicpsrw	ext(536870911) + -8192
    bicpsrb	ext(-536870912) + 536870911
    bicpsrw	ext(536870911) + -536870912
#
# Top Of Stack
#
    bicpsrb	tos
    bicpsrw	tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    bicpsrb	-64(fp)
    bicpsrw	63(fp)
    bicpsrb	-8192(fp)
    bicpsrw	8191(fp)
    bicpsrb	-536870912(fp)
    bicpsrw	536870911(fp)
    #
    # Stack Pointer (sp)
    #
    bicpsrb	-64(sp)
    bicpsrw	63(sp)
    bicpsrb	-8192(sp)
    bicpsrw	8191(sp)
    bicpsrb	-536870912(sp)
    bicpsrw	536870911(sp)
    #
    # Static Memory (sb)
    #
    bicpsrb	-64(sb)
    bicpsrw	63(sb)
    bicpsrb	-8192(sb)
    bicpsrw	8191(sb)
    bicpsrb	-536870912(sb)
    bicpsrw	536870911(sb)
    #
    # Program Counter (pc)
    #
    bicpsrb	-64(pc)
    bicpsrw	63(pc)
    bicpsrb	-8192(pc)
    bicpsrw	8191(pc)
    bicpsrb	-536870912(pc)
    bicpsrw	536870911(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    bicpsrb   r7[r0:b]
    bicpsrw   r7[r0:b]
    #
    # Register Relative
    bicpsrb   63(r5)[r2:b]
    bicpsrw   8191(r3)[r4:b]
    bicpsrb   536870911(r1)[r6:b]
    #
    # Memory Relative
    bicpsrw   -64(63(fp))[r0:b]
    bicpsrb   -8192(8191(fp))[r2:b]
    bicpsrw   -536870912(536870911(fp))[r4:b]
    bicpsrb   -64(63(sp))[r7:b]
    bicpsrw   -8192(8191(sp))[r5:b]
    bicpsrb   -536870912(536870911(sp))[r3:b]
    bicpsrw   -64(63(sb))[r0:b]
    bicpsrb   -8192(8191(sb))[r2:b]
    bicpsrw   -536870912(536870911(sb))[r4:b]
    #
    # Absolute
    bicpsrb   @63[r0:b]
    bicpsrw   @8191[r2:b]
    bicpsrb   @536870911[r4:b]
    #
    # External
    bicpsrw   ext(-64) + 63[r7:b]
    bicpsrb   ext(-8192) + 8191[r5:b]
    bicpsrw   ext(-536870912) + 536870911[r3:b]
    #
    # Top Of Stack
    bicpsrb   tos[r0:b]
    bicpsrw   tos[r0:b]
    #
    # Memory Space
    bicpsrb   63(fp)[r0:b]
    bicpsrw   8191(fp)[r0:b]
    bicpsrb   536870911(fp)[r0:b]
    bicpsrw   63(sp)[r0:b]
    bicpsrb   8191(sp)[r0:b]
    bicpsrw   536870911(sp)[r0:b]
    bicpsrb   63(sb)[r0:b]
    bicpsrw   8191(sb)[r0:b]
    bicpsrb   536870911(sb)[r0:b]
    bicpsrw   63(pc)[r0:b]
    bicpsrb   8191(pc)[r0:b]
    bicpsrw   536870911(pc)[r0:b]
    #
    # WORD
    #
    # Register
    bicpsrb   r1[r0:w]
    #
    # Register Relative
    bicpsrw   63(r1)[r0:w]
    bicpsrb   8191(r1)[r0:w]
    bicpsrw   536870911(r1)[r0:w]
    #
    # Memory Relative
    bicpsrb   -64(63(fp))[r0:w]
    bicpsrw   -8192(8191(fp))[r2:w]
    bicpsrb   -536870912(536870911(fp))[r4:w]
    bicpsrw   -64(63(sp))[r7:w]
    bicpsrb   -8192(8191(sp))[r5:w]
    bicpsrw   -536870912(536870911(sp))[r3:w]
    bicpsrb   -64(63(sb))[r0:w]
    bicpsrw   -8192(8191(sb))[r2:w]
    bicpsrb   -536870912(536870911(sb))[r4:w]
    #
    # Absolute
    bicpsrw   @63[r0:w]
    bicpsrb   @8191[r0:w]
    bicpsrw   @536870911[r0:w]
    #
    # External
    bicpsrb   ext(-64) + 63[r0:w]
    bicpsrw   ext(-8192) + 8191[r0:w]
    bicpsrb   ext(-536870912) + 536870911[r0:w]
    #
    # Top Of Stack
    bicpsrb   tos[r0:w]
    bicpsrw   tos[r0:w]
    #
    # Memory Space
    bicpsrb   63(fp)[r0:w]
    bicpsrw   8191(fp)[r0:w]
    bicpsrb   536870911(fp)[r0:w]
    bicpsrw   63(sp)[r0:w]
    bicpsrb   8191(sp)[r0:w]
    bicpsrw   536870911(sp)[r0:w]
    bicpsrb   63(sb)[r0:w]
    bicpsrw   8191(sb)[r0:w]
    bicpsrb   536870911(sb)[r0:w]
    bicpsrw   63(pc)[r0:w]
    bicpsrb   8191(pc)[r0:w]
    bicpsrw   536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    # Register
    bicpsrb   r1[r0:d]
    #
    # Register Relative
    bicpsrw   63(r1)[r0:d]
    bicpsrb   8191(r1)[r0:d]
    bicpsrw   536870911(r1)[r0:d]
    #
    # Memory Relative
    bicpsrb   -64(63(fp))[r0:d]
    bicpsrw   -8192(8191(fp))[r2:d]
    bicpsrb   -536870912(536870911(fp))[r4:d]
    bicpsrw   -64(63(sp))[r7:d]
    bicpsrb   -8192(8191(sp))[r5:d]
    bicpsrw   -536870912(536870911(sp))[r3:d]
    bicpsrb   -64(63(sb))[r0:d]
    bicpsrw   -8192(8191(sb))[r2:d]
    bicpsrb   -536870912(536870911(sb))[r4:d]
    #
    # Absolute
    bicpsrb   @63[r0:d]
    bicpsrw   @8191[r0:d]
    bicpsrb   @536870911[r0:d]
    #
    # External
    bicpsrb   ext(-64) + 63[r0:d]
    bicpsrw   ext(-8192) + 8191[r0:d]
    bicpsrb   ext(-536870912) + 536870911[r0:d]
    #
    # Top Of Stack
    bicpsrb   tos[r0:d]
    bicpsrw   tos[r0:d]
    #
    # Memory Space
    bicpsrb   63(fp)[r0:d]
    bicpsrw   8191(fp)[r0:d]
    bicpsrb   536870911(fp)[r0:d]
    bicpsrw   63(sp)[r0:d]
    bicpsrb   8191(sp)[r0:d]
    bicpsrw   536870911(sp)[r0:d]
    bicpsrb   63(sb)[r0:d]
    bicpsrw   8191(sb)[r0:d]
    bicpsrb   536870911(sb)[r0:d]
    bicpsrw   63(pc)[r0:d]
    bicpsrb   8191(pc)[r0:d]
    bicpsrw   536870911(pc)[r0:d]
#
end:
