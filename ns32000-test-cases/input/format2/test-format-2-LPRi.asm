.psize 0, 143   # suppress form feed, 143 columns
#
#
# Test case for FORMAT 2 instructions
#
# Instruction under test: LPRi
#
# Syntax:   SPRi    procreg src
#                   short   gen
#                           read.i
#
#           !  dest   !procreg!     LPRi    !
#           +---------+-------+---------+---+
#           !   gen   ! short !0 1 0 1 1! i !
#           !-+-+-+-+-+-+-+-!-+-+-+-+-+-+-+-!
#            15            8 7             0
#
# The LPRi instruction copies the src operand to the dedicated register specified
# by procreg. See Section 2.2 for the formats of these registers.
#
#  536870911 ==  2^29 - 1
# -536870912 == -2^29
#
#
# Register
#
    lprb    us, r0
    lprb    dcr, r1
    lprb    bpc, r2
    lprb    dsr, r3
    lprb    car, r4
    lprb    fp, r5
    lprb    sp, r6
    lprb    sb, r7
    lprb    usp, r0
    lprb    cfg, r1
    lprb    psr, r2
    lprb    intbase, r3
    lprb    mod, r4
#
# Register relative
#
    lprb   us, 63(r0)
    lprb   us, -64(r0)
    lprb   us, 8191(r0)
    lprb   us, -8192(r0)
    lprb   us, 536870911(r0)
    lprb   us, -536870912(r0)
    #
    lprb   dcr, 63(r1)
    lprb   dcr, -64(r1)
    lprb   dcr, 8191(r1)
    lprb   dcr, -8192(r1)
    lprb   dcr, 536870911(r1)
    lprb   dcr, -536870912(r1)
    #
    lprb   bpc, 63(r2)
    lprb   bpc, -64(r2)
    lprb   bpc, 8191(r2)
    lprb   bpc, -8192(r2)
    lprb   bpc, 536870911(r2)
    lprb   bpc, -536870912(r2)
    #
    lprb   dsr, 63(r3)
    lprb   dsr, -64(r3)
    lprb   dsr, 8191(r3)
    lprb   dsr, -8192(r3)
    lprb   dsr, 536870911(r3)
    lprb   dsr, -536870912(r3)
    #
    lprb   car, 63(r4)
    lprb   car, -64(r4)
    lprb   car, 8191(r4)
    lprb   car, -8192(r4)
    lprb   car, 536870911(r4)
    lprb   car, -536870912(r4)
    #
    lprb   fp, 63(r5)
    lprb   fp, -64(r5)
    lprb   fp, 8191(r5)
    lprb   fp, -8192(r5)
    lprb   fp, 536870911(r5)
    lprb   fp, -536870912(r5)
    #
    lprb   sp, 63(r6)
    lprb   sp, -64(r6)
    lprb   sp, 8191(r6)
    lprb   sp, -8192(r6)
    lprb   sp, 536870911(r6)
    lprb   sp, -536870912(r6)
    #
    lprb   sb, 63(r7)
    lprb   sb, -64(r7)
    lprb   sb, 8191(r7)
    lprb   sb, -8192(r7)
    lprb   sb, 536870911(r7)
    lprb   sb, -536870912(r7)
    #
    lprb   usp, 63(r7)
    lprb   usp, -64(r7)
    lprb   usp, 8191(r7)
    lprb   usp, -8192(r7)
    lprb   usp, 536870911(r7)
    lprb   usp, -536870912(r7)
    #
    lprb   cfg, 63(r7)
    lprb   cfg, -64(r7)
    lprb   cfg, 8191(r7)
    lprb   cfg, -8192(r7)
    lprb   cfg, 536870911(r7)
    lprb   cfg, -536870912(r7)
    #
    lprb   psr, 63(r7)
    lprb   psr, -64(r7)
    lprb   psr, 8191(r7)
    lprb   psr, -8192(r7)
    lprb   psr, 536870911(r7)
    lprb   psr, -536870912(r7)
    #
    lprb   intbase, 63(r7)
    lprb   intbase, -64(r7)
    lprb   intbase, 8191(r7)
    lprb   intbase, -8192(r7)
    lprb   intbase, 536870911(r7)
    lprb   intbase, -536870912(r7)
    #
    lprb   mod, 63(r7)
    lprb   mod, -64(r7)
    lprb   mod, 8191(r7)
    lprb   mod, -8192(r7)
    lprb   mod, 536870911(r7)
    lprb   mod, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    lprb   us, -64(63(fp))
    lprb   dcr, -64(63(fp))
    lprb   bpc, 63(-64(fp))
    lprb   dsr, 63(-64(fp))
    #
    lprb   car, -8192(63(fp))
    lprb   fp, -64(8191(fp))
    lprb   sp, 63(-8192(fp))
    lprb   sb, 8191(-64(fp))
    #
    lprb   usp, -8192(8191(fp))
    lprb   cfg, -8192(8191(fp))
    lprb   psr, 8191(-8192(fp))
    lprb   intbase, 8191(-8192(fp))
    #
    lprb   mod, -536870912(8191(fp))
    lprb   us, -8192(536870911(fp))
    lprb   dcr, 8191(-536870912(fp))
    lprb   bpc, 536870911(-8192(fp))
    #
    lprb   dsr, 536870911(-536870912(fp))
    lprb   car, 536870911(-536870912(fp))
    lprb   fp, -536870912(536870911(fp))
    lprb   sb, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    lprb   us, -64(63(sp))
    lprb   dcr, -64(63(sp))
    lprb   bpc, 63(-64(sp))
    lprb   dsr, 63(-64(sp))
    
    lprb   car, -8192(63(sp))
    lprb   fp, -64(8191(sp))
    lprb   sp, 63(-8192(sp))
    lprb   sb, 8191(-64(sp))
    #
    lprb   usp, -8192(8191(sp))
    lprb   cfg, -8192(8191(sp))
    lprb   psr, 8191(-8192(sp))
    lprb   intbase, 8191(-8192(sp))
    #
    lprb   mod, -536870912(8191(sp))
    lprb   us, -8192(536870911(sp))
    lprb   dcr, 8191(-536870912(sp))
    lprb   bpc, 536870911(-8192(sp))
    #
    lprb   dsr, 536870911(-536870912(sp))
    lprb   car, 536870911(-536870912(sp))
    lprb   fp, -536870912(536870911(sp))
    lprb   sb, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    lprb   us, -64(63(sb))
    lprb   dcr, -64(63(sb))
    lprb   bpc, 63(-64(sb))
    lprb   dsr, 63(-64(sb))
    #
    lprb   car, -8192(63(sb))
    lprb   fp, -64(8191(sb))
    lprb   sp, 63(-8192(sb))
    lprb   sb, 8191(-64(sb))
    #
    lprb   usp, -8192(8191(sb))
    lprb   cfg, -8192(8191(sb))
    lprb   psr, 8191(-8192(sb))
    lprb   intbase, 8191(-8192(sb))
    #
    lprb   mod, -536870912(8191(sb))
    lprb   us, -8192(536870911(sb))
    lprb   dcr, 8191(-536870912(sb))
    lprb   bpc, 536870911(-8192(sb))
    #
    lprb   dsr, 536870911(-536870912(sb))
    lprb   car, 536870911(-536870912(sb))
    lprb   fp, -536870912(536870911(sb))
    lprb   sb, -536870912(536870911(sb))
# 
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    lprb	us, @-64
    lprb	dcr, @-64
    lprb	bpc, @63
    lprb	dsr, @63
    lprb	car, @-65
    lprb	fp, @-65
    lprb	sp, @64
    lprb	sb, @64
    #
    lprb	usp, @-8192
    lprb	cfg, @-8192
    lprb	psr, @8191
    lprb	intbase, @8191
    lprb	mod, @-8193
    lprb	us, @-8193
    lprb	dcr, @8192
    lprb	bpc, @8192
    #
    lprb	dsr, @-536870912
    lprb	car, @536870911
#
# External
#
    lprb	us, ext(-64) + 63
    lprb	dcr, ext(-64) + 63
    lprb	bpc, ext(63) + -64
    lprb	dsr, ext(63) + -64
    #
    lprb	car, ext(-65) + 63
    lprb	fp, ext(-65) + 63
    lprb	sp, ext(63) + -65
    lprb	sb, ext(63) + -65
    lprb	usp, ext(-64) + 64
    lprb	cfg, ext(-64) + 64
    lprb	psr, ext(64) + -64
    lprb	intbase, ext(64) + -64
    #
    lprb	mod, ext(-8192) + 8191
    lprb	us, ext(-8192) + 8191
    lprb	dcr, ext(8191) + -8192
    lprb	bpc, ext(8191) + -8192
    #
    lprb	dsr, ext(-536870912) + 8191
    lprb	car, ext(-536870912) + 8191
    lprb	fp, ext(8191) + -536870912
    lprb	sp, ext(8191) + -536870912
    lprb	sb, ext(-8192) + 536870911
    lprb	usp, ext(-8192) + 536870911
    lprb	cfg, ext(536870911) + -8192
    lprb	psr, ext(536870911) + -8192
    #
    lprb	intbase, ext(-536870912) + 536870911
    lprb	mod, ext(-536870912) + 536870911
    lprb	us, ext(536870911) + -536870912
    lprb	dcr, ext(536870911) + -536870912
#
# Top Of Stack
#
    lprb    us, tos
    lprb    dcr, tos
    lprb    bpc, tos
    lprb    dsr, tos
    lprb    car, tos
    lprb    fp, tos
    lprb    sp, tos
    lprb    sb, tos
    lprb    usp, tos
    lprb    cfg, tos
    lprb    psr, tos
    lprb    intbase, tos
    lprb    mod, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    lprb	us, -64(fp)
    lprb	dcr, -64(fp)
    lprb	bpc, 63(fp)
    lprb	dsr, 63(fp)
    #
    lprb	car, -8192(fp)
    lprb	fp, -8192(fp)
    lprb	sp, 8191(fp)
    lprb	sb, 8191(fp)
    #
    lprb	usp, -536870912(fp)
    lprb	cfg, -536870912(fp)
    lprb	psr, 536870911(fp)
    lprb	intbase, 536870911(fp)
    lprb	mod, -64(fp)
    #
    # Stack Pointer (sp)
    #
    lprb	us, -64(sp)
    lprb	dcr, -64(sp)
    lprb	bpc, 63(sp)
    lprb	dsr, 63(sp)
    #
    lprb	car, -8192(sp)
    lprb	fp, -8192(sp)
    lprb	sp, 8191(sp)
    lprb	sb, 8191(sp)
    #
    lprb	usp, -536870912(sp)
    lprb	cfg, -536870912(sp)
    lprb	psr, 536870911(sp)
    lprb	intbase, 536870911(sp)
    lprb	mod, -64(sp)
    #
    # Static Memory (sb)
    #
    lprb	us, -64(sb)
    lprb	dcr, -64(sb)
    lprb	bpc, 63(sb)
    lprb	dsr, 63(sb)
    #
    lprb	car, -8192(sb)
    lprb	fp, -8192(sb)
    lprb	sp, 8191(sb)
    lprb	sb, 8191(sb)
    #
    lprb	usp, -536870912(sb)
    lprb	cfg, -536870912(sb)
    lprb	psr, 536870911(sb)
    lprb	intbase, 536870911(sb)
    lprb	mod, -64(sb)
    #
    # Program Counter (pc)
    #
    lprb	us, -64(pc)
    lprb	dcr, -64(pc)
    lprb	bpc, 63(pc)
    lprb	dsr, 63(pc)
    #
    lprb	car, -8192(pc)
    lprb	fp, -8192(pc)
    lprb	sp, 8191(pc)
    lprb	sb, 8191(pc)
    #
    lprb	usp, -536870912(pc)
    lprb	cfg, -536870912(pc)
    lprb	psr, 536870911(pc)
    lprb	intbase, 536870911(pc)
    lprb	mod, -64(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    lprb   us, r7[r0:b]
    lprb   dcr, r6[r1:b]
    #
    # Register Relative
    lprb   bpc, 63(r5)[r2:b]
    lprb   dsr, 63(r4)[r3:b]
    lprb   car, 8191(r3)[r4:b]
    lprb   fp, 8191(r2)[r5:b]
    lprb   sp, 536870911(r1)[r6:b]
    lprb   sb, 536870911(r0)[r7:b]
    #
    # Memory Relative
    lprb   usp, -64(63(fp))[r0:b]
    lprb   cfg, -64(63(fp))[r1:b]
    lprb   psr, -8192(8191(fp))[r2:b]
    lprb   intbase, -8192(8191(fp))[r3:b]
    lprb   mod, -536870912(536870911(fp))[r4:b]
    lprb   us, -536870912(536870911(fp))[r5:b]
    lprb   dcr, -64(63(sp))[r7:b]
    lprb   bpc, -64(63(sp))[r6:b]
    lprb   dsr, -8192(8191(sp))[r5:b]
    lprb   car, -8192(8191(sp))[r4:b]
    lprb   fp, -536870912(536870911(sp))[r3:b]
    lprb   sp, -536870912(536870911(sp))[r2:b]
    lprb   sb, -64(63(sb))[r0:b]
    lprb   usp, -64(63(sb))[r1:b]
    lprb   cfg, -8192(8191(sb))[r2:b]
    lprb   psr, -8192(8191(sb))[r3:b]
    lprb   intbase, -536870912(536870911(sb))[r4:b]
    lprb   mod, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    lprb   us, @63[r0:b]
    lprb   dcr, @63[r1:b]
    lprb   bpc, @8191[r2:b]
    lprb   dsr, @8191[r3:b]
    lprb   car, @536870911[r4:b]
    lprb   fp, @536870911[r5:b]
    #
    # External
    lprb   sp, ext(-64) + 63[r7:b]
    lprb   sb, ext(-64) + 63[r6:b]
    lprb   usp, ext(-8192) + 8191[r5:b]
    lprb   cfg, ext(-8192) + 8191[r4:b]
    lprb   psr, ext(-536870912) + 536870911[r3:b]
    lprb   intbase, ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    lprb   mod, tos[r0:b]
    #
    # Memory Space
    lprb   us, 63(fp)[r0:b]
    lprb   dcr, 63(fp)[r0:b]
    lprb   bpc, 8191(fp)[r0:b]
    lprb   dsr, 8191(fp)[r0:b]
    lprb   car, 536870911(fp)[r0:b]
    lprb   fp, 536870911(fp)[r0:b]
    lprb   sp, 63(sp)[r0:b]
    lprb   sb, 63(sp)[r0:b]
    lprb   usp, 8191(sp)[r0:b]
    lprb   cfg, 8191(sp)[r0:b]
    lprb   psr, 536870911(sp)[r0:b]
    lprb   intbase, 536870911(sp)[r0:b]
    lprb   mod, 63(sb)[r0:b]
    lprb   us, 63(sb)[r0:b]
    lprb   dcr, 8191(sb)[r0:b]
    lprb   bpc, 8191(sb)[r0:b]
    lprb   dsr, 536870911(sb)[r0:b]
    lprb   car, 536870911(sb)[r0:b]
    lprb   fp, 63(pc)[r0:b]
    lprb   sp, 63(pc)[r0:b]
    lprb   sb, 8191(pc)[r0:b]
    lprb   usp, 8191(pc)[r0:b]
    lprb   cfg, 536870911(pc)[r0:b]
    lprb   psr, 536870911(pc)[r0:b]
    #
    # WORD
    #
    lprb   us, r7[r0:w]
    lprb   dcr, r6[r1:w]
    #
    # Register Relative
    lprb   bpc, 63(r5)[r2:w]
    lprb   dsr, 63(r4)[r3:w]
    lprb   car, 8191(r3)[r4:w]
    lprb   fp, 8191(r2)[r5:w]
    lprb   sp, 536870911(r1)[r6:w]
    lprb   sb, 536870911(r0)[r7:w]
    #
    # Memory Relative
    lprb   usp, -64(63(fp))[r0:w]
    lprb   cfg, -64(63(fp))[r1:w]
    lprb   psr, -8192(8191(fp))[r2:w]
    lprb   intbase, -8192(8191(fp))[r3:w]
    lprb   mod, -536870912(536870911(fp))[r4:w]
    lprb   us, -536870912(536870911(fp))[r5:w]
    lprb   dcr, -64(63(sp))[r7:w]
    lprb   bpc, -64(63(sp))[r6:w]
    lprb   dsr, -8192(8191(sp))[r5:w]
    lprb   car, -8192(8191(sp))[r4:w]
    lprb   fp, -536870912(536870911(sp))[r3:w]
    lprb   sp, -536870912(536870911(sp))[r2:w]
    lprb   sb, -64(63(sb))[r0:w]
    lprb   usp, -64(63(sb))[r1:w]
    lprb   cfg, -8192(8191(sb))[r2:w]
    lprb   psr, -8192(8191(sb))[r3:w]
    lprb   intbase, -536870912(536870911(sb))[r4:w]
    lprb   mod, -536870912(536870911(sb))[r5:w]
    #
    # Absolute
    lprb   us, @63[r0:w]
    lprb   dcr, @63[r1:w]
    lprb   bpc, @8191[r2:w]
    lprb   dsr, @8191[r3:w]
    lprb   car, @536870911[r4:w]
    lprb   fp, @536870911[r5:w]
    #
    # External
    lprb   sp, ext(-64) + 63[r7:w]
    lprb   sb, ext(-64) + 63[r6:w]
    lprb   usp, ext(-8192) + 8191[r5:w]
    lprb   cfg, ext(-8192) + 8191[r4:w]
    lprb   psr, ext(-536870912) + 536870911[r3:w]
    lprb   intbase, ext(-536870912) + 536870911[r2:w]
    #
    # Top Of Stack
    lprb   mod, tos[r0:w]
    #
    # Memory Space
    lprb   us, 63(fp)[r0:w]
    lprb   dcr, 63(fp)[r0:w]
    lprb   bpc, 8191(fp)[r0:w]
    lprb   dsr, 8191(fp)[r0:w]
    lprb   car, 536870911(fp)[r0:w]
    lprb   fp, 536870911(fp)[r0:w]
    lprb   sp, 63(sp)[r0:w]
    lprb   sb, 63(sp)[r0:w]
    lprb   usp, 8191(sp)[r0:w]
    lprb   cfg, 8191(sp)[r0:w]
    lprb   psr, 536870911(sp)[r0:w]
    lprb   intbase, 536870911(sp)[r0:w]
    lprb   mod, 63(sb)[r0:w]
    lprb   us, 63(sb)[r0:w]
    lprb   dcr, 8191(sb)[r0:w]
    lprb   bpc, 8191(sb)[r0:w]
    lprb   dsr, 536870911(sb)[r0:w]
    lprb   car, 536870911(sb)[r0:w]
    lprb   fp, 63(pc)[r0:w]
    lprb   sp, 63(pc)[r0:w]
    lprb   sb, 8191(pc)[r0:w]
    lprb   usp, 8191(pc)[r0:w]
    lprb   cfg, 536870911(pc)[r0:w]
    lprb   psr, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    lprb   us, r7[r0:d]
    lprb   dcr, r6[r1:d]
    #
    # Register Relative
    lprb   bpc, 63(r5)[r2:d]
    lprb   dsr, 63(r4)[r3:d]
    lprb   car, 8191(r3)[r4:d]
    lprb   fp, 8191(r2)[r5:d]
    lprb   sp, 536870911(r1)[r6:d]
    lprb   sb, 536870911(r0)[r7:d]
    #
    # Memory Relative
    lprb   usp, -64(63(fp))[r0:d]
    lprb   cfg, -64(63(fp))[r1:d]
    lprb   psr, -8192(8191(fp))[r2:d]
    lprb   intbase, -8192(8191(fp))[r3:d]
    lprb   mod, -536870912(536870911(fp))[r4:d]
    lprb   us, -536870912(536870911(fp))[r5:d]
    lprb   dcr, -64(63(sp))[r7:d]
    lprb   bpc, -64(63(sp))[r6:d]
    lprb   dsr, -8192(8191(sp))[r5:d]
    lprb   car, -8192(8191(sp))[r4:d]
    lprb   fp, -536870912(536870911(sp))[r3:d]
    lprb   sp, -536870912(536870911(sp))[r2:d]
    lprb   sb, -64(63(sb))[r0:d]
    lprb   usp, -64(63(sb))[r1:d]
    lprb   cfg, -8192(8191(sb))[r2:d]
    lprb   psr, -8192(8191(sb))[r3:d]
    lprb   intbase, -536870912(536870911(sb))[r4:d]
    lprb   mod, -536870912(536870911(sb))[r5:d]
    #
    # Absolute
    lprb   us, @63[r0:d]
    lprb   dcr, @63[r1:d]
    lprb   bpc, @8191[r2:d]
    lprb   dsr, @8191[r3:d]
    lprb   car, @536870911[r4:d]
    lprb   fp, @536870911[r5:d]
    #
    # External
    lprb   sp, ext(-64) + 63[r7:d]
    lprb   sb, ext(-64) + 63[r6:d]
    lprb   usp, ext(-8192) + 8191[r5:d]
    lprb   cfg, ext(-8192) + 8191[r4:d]
    lprb   psr, ext(-536870912) + 536870911[r3:d]
    lprb   intbase, ext(-536870912) + 536870911[r2:d]
    #
    # Top Of Stack
    lprb   mod, tos[r0:b]
    #
    # Memory Space
    lprb   us, 63(fp)[r0:d]
    lprb   dcr, 63(fp)[r0:d]
    lprb   bpc, 8191(fp)[r0:d]
    lprb   dsr, 8191(fp)[r0:d]
    lprb   car, 536870911(fp)[r0:d]
    lprb   fp, 536870911(fp)[r0:d]
    lprb   sp, 63(sp)[r0:d]
    lprb   sb, 63(sp)[r0:d]
    lprb   usp, 8191(sp)[r0:d]
    lprb   cfg, 8191(sp)[r0:d]
    lprb   psr, 536870911(sp)[r0:d]
    lprb   intbase, 536870911(sp)[r0:d]
    lprb   mod, 63(sb)[r0:d]
    lprb   us, 63(sb)[r0:d]
    lprb   dcr, 8191(sb)[r0:d]
    lprb   bpc, 8191(sb)[r0:d]
    lprb   dsr, 536870911(sb)[r0:d]
    lprb   car, 536870911(sb)[r0:d]
    lprb   fp, 63(pc)[r0:d]
    lprb   sp, 63(pc)[r0:d]
    lprb   sb, 8191(pc)[r0:d]
    lprb   usp, 8191(pc)[r0:d]
    lprb   cfg, 536870911(pc)[r0:d]
    lprb   psr, 536870911(pc)[r0:d]
#
# SPR WORD
#
#
# Register
#
    lprw    us, r0
    lprw    dcr, r1
    lprw    bpc, r2
    lprw    dsr, r3
    lprw    car, r4
    lprw    fp, r5
    lprw    sp, r6
    lprw    sb, r7
    lprw    usp, r0
    lprw    cfg, r1
    lprw    psr, r2
    lprw    intbase, r3
    lprw    mod, r4
#
# Register relative
#
    lprw   us, 63(r0)
    lprw   us, -64(r0)
    lprw   us, 8191(r0)
    lprw   us, -8192(r0)
    lprw   us, 536870911(r0)
    lprw   us, -536870912(r0)
    #
    lprw   dcr, 63(r1)
    lprw   dcr, -64(r1)
    lprw   dcr, 8191(r1)
    lprw   dcr, -8192(r1)
    lprw   dcr, 536870911(r1)
    lprw   dcr, -536870912(r1)
    #
    lprw   bpc, 63(r2)
    lprw   bpc, -64(r2)
    lprw   bpc, 8191(r2)
    lprw   bpc, -8192(r2)
    lprw   bpc, 536870911(r2)
    lprw   bpc, -536870912(r2)
    #
    lprw   dsr, 63(r3)
    lprw   dsr, -64(r3)
    lprw   dsr, 8191(r3)
    lprw   dsr, -8192(r3)
    lprw   dsr, 536870911(r3)
    lprw   dsr, -536870912(r3)
    #
    lprw   car, 63(r4)
    lprw   car, -64(r4)
    lprw   car, 8191(r4)
    lprw   car, -8192(r4)
    lprw   car, 536870911(r4)
    lprw   car, -536870912(r4)
    #
    lprw   fp, 63(r5)
    lprw   fp, -64(r5)
    lprw   fp, 8191(r5)
    lprw   fp, -8192(r5)
    lprw   fp, 536870911(r5)
    lprw   fp, -536870912(r5)
    #
    lprw   sp, 63(r6)
    lprw   sp, -64(r6)
    lprw   sp, 8191(r6)
    lprw   sp, -8192(r6)
    lprw   sp, 536870911(r6)
    lprw   sp, -536870912(r6)
    #
    lprw   sb, 63(r7)
    lprw   sb, -64(r7)
    lprw   sb, 8191(r7)
    lprw   sb, -8192(r7)
    lprw   sb, 536870911(r7)
    lprw   sb, -536870912(r7)
    #
    lprw   usp, 63(r7)
    lprw   usp, -64(r7)
    lprw   usp, 8191(r7)
    lprw   usp, -8192(r7)
    lprw   usp, 536870911(r7)
    lprw   usp, -536870912(r7)
    #
    lprw   cfg, 63(r7)
    lprw   cfg, -64(r7)
    lprw   cfg, 8191(r7)
    lprw   cfg, -8192(r7)
    lprw   cfg, 536870911(r7)
    lprw   cfg, -536870912(r7)
    #
    lprw   psr, 63(r7)
    lprw   psr, -64(r7)
    lprw   psr, 8191(r7)
    lprw   psr, -8192(r7)
    lprw   psr, 536870911(r7)
    lprw   psr, -536870912(r7)
    #
    lprw   intbase, 63(r7)
    lprw   intbase, -64(r7)
    lprw   intbase, 8191(r7)
    lprw   intbase, -8192(r7)
    lprw   intbase, 536870911(r7)
    lprw   intbase, -536870912(r7)
    #
    lprw   mod, 63(r7)
    lprw   mod, -64(r7)
    lprw   mod, 8191(r7)
    lprw   mod, -8192(r7)
    lprw   mod, 536870911(r7)
    lprw   mod, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    lprw   us, -64(63(fp))
    lprw   dcr, -64(63(fp))
    lprw   bpc, 63(-64(fp))
    lprw   dsr, 63(-64(fp))
    #
    lprw   car, -8192(63(fp))
    lprw   fp, -64(8191(fp))
    lprw   sp, 63(-8192(fp))
    lprw   sb, 8191(-64(fp))
    #
    lprw   usp, -8192(8191(fp))
    lprw   cfg, -8192(8191(fp))
    lprw   psr, 8191(-8192(fp))
    lprw   intbase, 8191(-8192(fp))
    #
    lprw   mod, -536870912(8191(fp))
    lprw   us, -8192(536870911(fp))
    lprw   dcr, 8191(-536870912(fp))
    lprw   bpc, 536870911(-8192(fp))
    #
    lprw   dsr, 536870911(-536870912(fp))
    lprw   car, 536870911(-536870912(fp))
    lprw   fp, -536870912(536870911(fp))
    lprw   sb, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    lprw   us, -64(63(sp))
    lprw   dcr, -64(63(sp))
    lprw   bpc, 63(-64(sp))
    lprw   dsr, 63(-64(sp))
    
    lprw   car, -8192(63(sp))
    lprw   fp, -64(8191(sp))
    lprw   sp, 63(-8192(sp))
    lprw   sb, 8191(-64(sp))
    #
    lprw   usp, -8192(8191(sp))
    lprw   cfg, -8192(8191(sp))
    lprw   psr, 8191(-8192(sp))
    lprw   intbase, 8191(-8192(sp))
    #
    lprw   mod, -536870912(8191(sp))
    lprw   us, -8192(536870911(sp))
    lprw   dcr, 8191(-536870912(sp))
    lprw   bpc, 536870911(-8192(sp))
    #
    lprw   dsr, 536870911(-536870912(sp))
    lprw   car, 536870911(-536870912(sp))
    lprw   fp, -536870912(536870911(sp))
    lprw   sb, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    lprw   us, -64(63(sb))
    lprw   dcr, -64(63(sb))
    lprw   bpc, 63(-64(sb))
    lprw   dsr, 63(-64(sb))
    #
    lprw   car, -8192(63(sb))
    lprw   fp, -64(8191(sb))
    lprw   sp, 63(-8192(sb))
    lprw   sb, 8191(-64(sb))
    #
    lprw   usp, -8192(8191(sb))
    lprw   cfg, -8192(8191(sb))
    lprw   psr, 8191(-8192(sb))
    lprw   intbase, 8191(-8192(sb))
    #
    lprw   mod, -536870912(8191(sb))
    lprw   us, -8192(536870911(sb))
    lprw   dcr, 8191(-536870912(sb))
    lprw   bpc, 536870911(-8192(sb))
    #
    lprw   dsr, 536870911(-536870912(sb))
    lprw   car, 536870911(-536870912(sb))
    lprw   fp, -536870912(536870911(sb))
    lprw   sb, -536870912(536870911(sb))
# 
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    lprw	us, @-64
    lprw	dcr, @-64
    lprw	bpc, @63
    lprw	dsr, @63
    lprw	car, @-65
    lprw	fp, @-65
    lprw	sp, @64
    lprw	sb, @64
    #
    lprw	usp, @-8192
    lprw	cfg, @-8192
    lprw	psr, @8191
    lprw	intbase, @8191
    lprw	mod, @-8193
    lprw	us, @-8193
    lprw	dcr, @8192
    lprw	bpc, @8192
    #
    lprw	dsr, @-536870912
    lprw	car, @536870911
#
# External
#
    lprw	us, ext(-64) + 63
    lprw	dcr, ext(-64) + 63
    lprw	bpc, ext(63) + -64
    lprw	dsr, ext(63) + -64
    #
    lprw	car, ext(-65) + 63
    lprw	fp, ext(-65) + 63
    lprw	sp, ext(63) + -65
    lprw	sb, ext(63) + -65
    lprw	usp, ext(-64) + 64
    lprw	cfg, ext(-64) + 64
    lprw	psr, ext(64) + -64
    lprw	intbase, ext(64) + -64
    #
    lprw	mod, ext(-8192) + 8191
    lprw	us, ext(-8192) + 8191
    lprw	dcr, ext(8191) + -8192
    lprw	bpc, ext(8191) + -8192
    #
    lprw	dsr, ext(-536870912) + 8191
    lprw	car, ext(-536870912) + 8191
    lprw	fp, ext(8191) + -536870912
    lprw	sp, ext(8191) + -536870912
    lprw	sb, ext(-8192) + 536870911
    lprw	usp, ext(-8192) + 536870911
    lprw	cfg, ext(536870911) + -8192
    lprw	psr, ext(536870911) + -8192
    #
    lprw	intbase, ext(-536870912) + 536870911
    lprw	mod, ext(-536870912) + 536870911
    lprw	us, ext(536870911) + -536870912
    lprw	dcr, ext(536870911) + -536870912
#
# Top Of Stack
#
    lprw    us, tos
    lprw    dcr, tos
    lprw    bpc, tos
    lprw    dsr, tos
    lprw    car, tos
    lprw    fp, tos
    lprw    sp, tos
    lprw    sb, tos
    lprw    usp, tos
    lprw    cfg, tos
    lprw    psr, tos
    lprw    intbase, tos
    lprw    mod, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    lprw	us, -64(fp)
    lprw	dcr, -64(fp)
    lprw	bpc, 63(fp)
    lprw	dsr, 63(fp)
    #
    lprw	car, -8192(fp)
    lprw	fp, -8192(fp)
    lprw	sp, 8191(fp)
    lprw	sb, 8191(fp)
    #
    lprw	usp, -536870912(fp)
    lprw	cfg, -536870912(fp)
    lprw	psr, 536870911(fp)
    lprw	intbase, 536870911(fp)
    lprw	mod, -64(fp)
    #
    # Stack Pointer (sp)
    #
    lprw	us, -64(sp)
    lprw	dcr, -64(sp)
    lprw	bpc, 63(sp)
    lprw	dsr, 63(sp)
    #
    lprw	car, -8192(sp)
    lprw	fp, -8192(sp)
    lprw	sp, 8191(sp)
    lprw	sb, 8191(sp)
    #
    lprw	usp, -536870912(sp)
    lprw	cfg, -536870912(sp)
    lprw	psr, 536870911(sp)
    lprw	intbase, 536870911(sp)
    lprw	mod, -64(sp)
    #
    # Static Memory (sb)
    #
    lprw	us, -64(sb)
    lprw	dcr, -64(sb)
    lprw	bpc, 63(sb)
    lprw	dsr, 63(sb)
    #
    lprw	car, -8192(sb)
    lprw	fp, -8192(sb)
    lprw	sp, 8191(sb)
    lprw	sb, 8191(sb)
    #
    lprw	usp, -536870912(sb)
    lprw	cfg, -536870912(sb)
    lprw	psr, 536870911(sb)
    lprw	intbase, 536870911(sb)
    lprw	mod, -64(sb)
    #
    # Program Counter (pc)
    #
    lprw	us, -64(pc)
    lprw	dcr, -64(pc)
    lprw	bpc, 63(pc)
    lprw	dsr, 63(pc)
    #
    lprw	car, -8192(pc)
    lprw	fp, -8192(pc)
    lprw	sp, 8191(pc)
    lprw	sb, 8191(pc)
    #
    lprw	usp, -536870912(pc)
    lprw	cfg, -536870912(pc)
    lprw	psr, 536870911(pc)
    lprw	intbase, 536870911(pc)
    lprw	mod, -64(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    lprw   us, r7[r0:b]
    lprw   dcr, r6[r1:b]
    #
    # Register Relative
    lprw   bpc, 63(r5)[r2:b]
    lprw   dsr, 63(r4)[r3:b]
    lprw   car, 8191(r3)[r4:b]
    lprw   fp, 8191(r2)[r5:b]
    lprw   sp, 536870911(r1)[r6:b]
    lprw   sb, 536870911(r0)[r7:b]
    #
    # Memory Relative
    lprw   usp, -64(63(fp))[r0:b]
    lprw   cfg, -64(63(fp))[r1:b]
    lprw   psr, -8192(8191(fp))[r2:b]
    lprw   intbase, -8192(8191(fp))[r3:b]
    lprw   mod, -536870912(536870911(fp))[r4:b]
    lprw   us, -536870912(536870911(fp))[r5:b]
    lprw   dcr, -64(63(sp))[r7:b]
    lprw   bpc, -64(63(sp))[r6:b]
    lprw   dsr, -8192(8191(sp))[r5:b]
    lprw   car, -8192(8191(sp))[r4:b]
    lprw   fp, -536870912(536870911(sp))[r3:b]
    lprw   sp, -536870912(536870911(sp))[r2:b]
    lprw   sb, -64(63(sb))[r0:b]
    lprw   usp, -64(63(sb))[r1:b]
    lprw   cfg, -8192(8191(sb))[r2:b]
    lprw   psr, -8192(8191(sb))[r3:b]
    lprw   intbase, -536870912(536870911(sb))[r4:b]
    lprw   mod, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    lprw   us, @63[r0:b]
    lprw   dcr, @63[r1:b]
    lprw   bpc, @8191[r2:b]
    lprw   dsr, @8191[r3:b]
    lprw   car, @536870911[r4:b]
    lprw   fp, @536870911[r5:b]
    #
    # External
    lprw   sp, ext(-64) + 63[r7:b]
    lprw   sb, ext(-64) + 63[r6:b]
    lprw   usp, ext(-8192) + 8191[r5:b]
    lprw   cfg, ext(-8192) + 8191[r4:b]
    lprw   psr, ext(-536870912) + 536870911[r3:b]
    lprw   intbase, ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    lprw   mod, tos[r0:b]
    #
    # Memory Space
    lprw   us, 63(fp)[r0:b]
    lprw   dcr, 63(fp)[r0:b]
    lprw   bpc, 8191(fp)[r0:b]
    lprw   dsr, 8191(fp)[r0:b]
    lprw   car, 536870911(fp)[r0:b]
    lprw   fp, 536870911(fp)[r0:b]
    lprw   sp, 63(sp)[r0:b]
    lprw   sb, 63(sp)[r0:b]
    lprw   usp, 8191(sp)[r0:b]
    lprw   cfg, 8191(sp)[r0:b]
    lprw   psr, 536870911(sp)[r0:b]
    lprw   intbase, 536870911(sp)[r0:b]
    lprw   mod, 63(sb)[r0:b]
    lprw   us, 63(sb)[r0:b]
    lprw   dcr, 8191(sb)[r0:b]
    lprw   bpc, 8191(sb)[r0:b]
    lprw   dsr, 536870911(sb)[r0:b]
    lprw   car, 536870911(sb)[r0:b]
    lprw   fp, 63(pc)[r0:b]
    lprw   sp, 63(pc)[r0:b]
    lprw   sb, 8191(pc)[r0:b]
    lprw   usp, 8191(pc)[r0:b]
    lprw   cfg, 536870911(pc)[r0:b]
    lprw   psr, 536870911(pc)[r0:b]
    #
    # WORD
    #
    lprw   us, r7[r0:w]
    lprw   dcr, r6[r1:w]
    #
    # Register Relative
    lprw   bpc, 63(r5)[r2:w]
    lprw   dsr, 63(r4)[r3:w]
    lprw   car, 8191(r3)[r4:w]
    lprw   fp, 8191(r2)[r5:w]
    lprw   sp, 536870911(r1)[r6:w]
    lprw   sb, 536870911(r0)[r7:w]
    #
    # Memory Relative
    lprw   usp, -64(63(fp))[r0:w]
    lprw   cfg, -64(63(fp))[r1:w]
    lprw   psr, -8192(8191(fp))[r2:w]
    lprw   intbase, -8192(8191(fp))[r3:w]
    lprw   mod, -536870912(536870911(fp))[r4:w]
    lprw   us, -536870912(536870911(fp))[r5:w]
    lprw   dcr, -64(63(sp))[r7:w]
    lprw   bpc, -64(63(sp))[r6:w]
    lprw   dsr, -8192(8191(sp))[r5:w]
    lprw   car, -8192(8191(sp))[r4:w]
    lprw   fp, -536870912(536870911(sp))[r3:w]
    lprw   sp, -536870912(536870911(sp))[r2:w]
    lprw   sb, -64(63(sb))[r0:w]
    lprw   usp, -64(63(sb))[r1:w]
    lprw   cfg, -8192(8191(sb))[r2:w]
    lprw   psr, -8192(8191(sb))[r3:w]
    lprw   intbase, -536870912(536870911(sb))[r4:w]
    lprw   mod, -536870912(536870911(sb))[r5:w]
    #
    # Absolute
    lprw   us, @63[r0:w]
    lprw   dcr, @63[r1:w]
    lprw   bpc, @8191[r2:w]
    lprw   dsr, @8191[r3:w]
    lprw   car, @536870911[r4:w]
    lprw   fp, @536870911[r5:w]
    #
    # External
    lprw   sp, ext(-64) + 63[r7:w]
    lprw   sb, ext(-64) + 63[r6:w]
    lprw   usp, ext(-8192) + 8191[r5:w]
    lprw   cfg, ext(-8192) + 8191[r4:w]
    lprw   psr, ext(-536870912) + 536870911[r3:w]
    lprw   intbase, ext(-536870912) + 536870911[r2:w]
    #
    # Top Of Stack
    lprw   mod, tos[r0:w]
    #
    # Memory Space
    lprw   us, 63(fp)[r0:w]
    lprw   dcr, 63(fp)[r0:w]
    lprw   bpc, 8191(fp)[r0:w]
    lprw   dsr, 8191(fp)[r0:w]
    lprw   car, 536870911(fp)[r0:w]
    lprw   fp, 536870911(fp)[r0:w]
    lprw   sp, 63(sp)[r0:w]
    lprw   sb, 63(sp)[r0:w]
    lprw   usp, 8191(sp)[r0:w]
    lprw   cfg, 8191(sp)[r0:w]
    lprw   psr, 536870911(sp)[r0:w]
    lprw   intbase, 536870911(sp)[r0:w]
    lprw   mod, 63(sb)[r0:w]
    lprw   us, 63(sb)[r0:w]
    lprw   dcr, 8191(sb)[r0:w]
    lprw   bpc, 8191(sb)[r0:w]
    lprw   dsr, 536870911(sb)[r0:w]
    lprw   car, 536870911(sb)[r0:w]
    lprw   fp, 63(pc)[r0:w]
    lprw   sp, 63(pc)[r0:w]
    lprw   sb, 8191(pc)[r0:w]
    lprw   usp, 8191(pc)[r0:w]
    lprw   cfg, 536870911(pc)[r0:w]
    lprw   psr, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    lprw   us, r7[r0:d]
    lprw   dcr, r6[r1:d]
    #
    # Register Relative
    lprw   bpc, 63(r5)[r2:d]
    lprw   dsr, 63(r4)[r3:d]
    lprw   car, 8191(r3)[r4:d]
    lprw   fp, 8191(r2)[r5:d]
    lprw   sp, 536870911(r1)[r6:d]
    lprw   sb, 536870911(r0)[r7:d]
    #
    # Memory Relative
    lprw   usp, -64(63(fp))[r0:d]
    lprw   cfg, -64(63(fp))[r1:d]
    lprw   psr, -8192(8191(fp))[r2:d]
    lprw   intbase, -8192(8191(fp))[r3:d]
    lprw   mod, -536870912(536870911(fp))[r4:d]
    lprw   us, -536870912(536870911(fp))[r5:d]
    lprw   dcr, -64(63(sp))[r7:d]
    lprw   bpc, -64(63(sp))[r6:d]
    lprw   dsr, -8192(8191(sp))[r5:d]
    lprw   car, -8192(8191(sp))[r4:d]
    lprw   fp, -536870912(536870911(sp))[r3:d]
    lprw   sp, -536870912(536870911(sp))[r2:d]
    lprw   sb, -64(63(sb))[r0:d]
    lprw   usp, -64(63(sb))[r1:d]
    lprw   cfg, -8192(8191(sb))[r2:d]
    lprw   psr, -8192(8191(sb))[r3:d]
    lprw   intbase, -536870912(536870911(sb))[r4:d]
    lprw   mod, -536870912(536870911(sb))[r5:d]
    #
    # Absolute
    lprw   us, @63[r0:d]
    lprw   dcr, @63[r1:d]
    lprw   bpc, @8191[r2:d]
    lprw   dsr, @8191[r3:d]
    lprw   car, @536870911[r4:d]
    lprw   fp, @536870911[r5:d]
    #
    # External
    lprw   sp, ext(-64) + 63[r7:d]
    lprw   sb, ext(-64) + 63[r6:d]
    lprw   usp, ext(-8192) + 8191[r5:d]
    lprw   cfg, ext(-8192) + 8191[r4:d]
    lprw   psr, ext(-536870912) + 536870911[r3:d]
    lprw   intbase, ext(-536870912) + 536870911[r2:d]
    #
    # Top Of Stack
    lprw   mod, tos[r0:b]
    #
    # Memory Space
    lprw   us, 63(fp)[r0:d]
    lprw   dcr, 63(fp)[r0:d]
    lprw   bpc, 8191(fp)[r0:d]
    lprw   dsr, 8191(fp)[r0:d]
    lprw   car, 536870911(fp)[r0:d]
    lprw   fp, 536870911(fp)[r0:d]
    lprw   sp, 63(sp)[r0:d]
    lprw   sb, 63(sp)[r0:d]
    lprw   usp, 8191(sp)[r0:d]
    lprw   cfg, 8191(sp)[r0:d]
    lprw   psr, 536870911(sp)[r0:d]
    lprw   intbase, 536870911(sp)[r0:d]
    lprw   mod, 63(sb)[r0:d]
    lprw   us, 63(sb)[r0:d]
    lprw   dcr, 8191(sb)[r0:d]
    lprw   bpc, 8191(sb)[r0:d]
    lprw   dsr, 536870911(sb)[r0:d]
    lprw   car, 536870911(sb)[r0:d]
    lprw   fp, 63(pc)[r0:d]
    lprw   sp, 63(pc)[r0:d]
    lprw   sb, 8191(pc)[r0:d]
    lprw   usp, 8191(pc)[r0:d]
    lprw   cfg, 536870911(pc)[r0:d]
    lprw   psr, 536870911(pc)[r0:d]
#
# SPR DOUBLE WORD
#
#
# Register
#
    lprd    us, r0
    lprd    dcr, r1
    lprd    bpc, r2
    lprd    dsr, r3
    lprd    car, r4
    lprd    fp, r5
    lprd    sp, r6
    lprd    sb, r7
    lprd    usp, r0
    lprd    cfg, r1
    lprd    psr, r2
    lprd    intbase, r3
    lprd    mod, r4
#
# Register relative
#
    lprd   us, 63(r0)
    lprd   us, -64(r0)
    lprd   us, 8191(r0)
    lprd   us, -8192(r0)
    lprd   us, 536870911(r0)
    lprd   us, -536870912(r0)
    #
    lprd   dcr, 63(r1)
    lprd   dcr, -64(r1)
    lprd   dcr, 8191(r1)
    lprd   dcr, -8192(r1)
    lprd   dcr, 536870911(r1)
    lprd   dcr, -536870912(r1)
    #
    lprd   bpc, 63(r2)
    lprd   bpc, -64(r2)
    lprd   bpc, 8191(r2)
    lprd   bpc, -8192(r2)
    lprd   bpc, 536870911(r2)
    lprd   bpc, -536870912(r2)
    #
    lprd   dsr, 63(r3)
    lprd   dsr, -64(r3)
    lprd   dsr, 8191(r3)
    lprd   dsr, -8192(r3)
    lprd   dsr, 536870911(r3)
    lprd   dsr, -536870912(r3)
    #
    lprd   car, 63(r4)
    lprd   car, -64(r4)
    lprd   car, 8191(r4)
    lprd   car, -8192(r4)
    lprd   car, 536870911(r4)
    lprd   car, -536870912(r4)
    #
    lprd   fp, 63(r5)
    lprd   fp, -64(r5)
    lprd   fp, 8191(r5)
    lprd   fp, -8192(r5)
    lprd   fp, 536870911(r5)
    lprd   fp, -536870912(r5)
    #
    lprd   sp, 63(r6)
    lprd   sp, -64(r6)
    lprd   sp, 8191(r6)
    lprd   sp, -8192(r6)
    lprd   sp, 536870911(r6)
    lprd   sp, -536870912(r6)
    #
    lprd   sb, 63(r7)
    lprd   sb, -64(r7)
    lprd   sb, 8191(r7)
    lprd   sb, -8192(r7)
    lprd   sb, 536870911(r7)
    lprd   sb, -536870912(r7)
    #
    lprd   usp, 63(r7)
    lprd   usp, -64(r7)
    lprd   usp, 8191(r7)
    lprd   usp, -8192(r7)
    lprd   usp, 536870911(r7)
    lprd   usp, -536870912(r7)
    #
    lprd   cfg, 63(r7)
    lprd   cfg, -64(r7)
    lprd   cfg, 8191(r7)
    lprd   cfg, -8192(r7)
    lprd   cfg, 536870911(r7)
    lprd   cfg, -536870912(r7)
    #
    lprd   psr, 63(r7)
    lprd   psr, -64(r7)
    lprd   psr, 8191(r7)
    lprd   psr, -8192(r7)
    lprd   psr, 536870911(r7)
    lprd   psr, -536870912(r7)
    #
    lprd   intbase, 63(r7)
    lprd   intbase, -64(r7)
    lprd   intbase, 8191(r7)
    lprd   intbase, -8192(r7)
    lprd   intbase, 536870911(r7)
    lprd   intbase, -536870912(r7)
    #
    lprd   mod, 63(r7)
    lprd   mod, -64(r7)
    lprd   mod, 8191(r7)
    lprd   mod, -8192(r7)
    lprd   mod, 536870911(r7)
    lprd   mod, -536870912(r7)
#
# Memory relative
#
    #
    # Frame Pointer (fp)
    #
    lprd   us, -64(63(fp))
    lprd   dcr, -64(63(fp))
    lprd   bpc, 63(-64(fp))
    lprd   dsr, 63(-64(fp))
    #
    lprd   car, -8192(63(fp))
    lprd   fp, -64(8191(fp))
    lprd   sp, 63(-8192(fp))
    lprd   sb, 8191(-64(fp))
    #
    lprd   usp, -8192(8191(fp))
    lprd   cfg, -8192(8191(fp))
    lprd   psr, 8191(-8192(fp))
    lprd   intbase, 8191(-8192(fp))
    #
    lprd   mod, -536870912(8191(fp))
    lprd   us, -8192(536870911(fp))
    lprd   dcr, 8191(-536870912(fp))
    lprd   bpc, 536870911(-8192(fp))
    #
    lprd   dsr, 536870911(-536870912(fp))
    lprd   car, 536870911(-536870912(fp))
    lprd   fp, -536870912(536870911(fp))
    lprd   sb, -536870912(536870911(fp))
    #
    # Stack Pointer (sp)
    #
    lprd   us, -64(63(sp))
    lprd   dcr, -64(63(sp))
    lprd   bpc, 63(-64(sp))
    lprd   dsr, 63(-64(sp))
    
    lprd   car, -8192(63(sp))
    lprd   fp, -64(8191(sp))
    lprd   sp, 63(-8192(sp))
    lprd   sb, 8191(-64(sp))
    #
    lprd   usp, -8192(8191(sp))
    lprd   cfg, -8192(8191(sp))
    lprd   psr, 8191(-8192(sp))
    lprd   intbase, 8191(-8192(sp))
    #
    lprd   mod, -536870912(8191(sp))
    lprd   us, -8192(536870911(sp))
    lprd   dcr, 8191(-536870912(sp))
    lprd   bpc, 536870911(-8192(sp))
    #
    lprd   dsr, 536870911(-536870912(sp))
    lprd   car, 536870911(-536870912(sp))
    lprd   fp, -536870912(536870911(sp))
    lprd   sb, -536870912(536870911(sp))
    #
    # Static Memory (sb)
    #
    lprd   us, -64(63(sb))
    lprd   dcr, -64(63(sb))
    lprd   bpc, 63(-64(sb))
    lprd   dsr, 63(-64(sb))
    #
    lprd   car, -8192(63(sb))
    lprd   fp, -64(8191(sb))
    lprd   sp, 63(-8192(sb))
    lprd   sb, 8191(-64(sb))
    #
    lprd   usp, -8192(8191(sb))
    lprd   cfg, -8192(8191(sb))
    lprd   psr, 8191(-8192(sb))
    lprd   intbase, 8191(-8192(sb))
    #
    lprd   mod, -536870912(8191(sb))
    lprd   us, -8192(536870911(sb))
    lprd   dcr, 8191(-536870912(sb))
    lprd   bpc, 536870911(-8192(sb))
    #
    lprd   dsr, 536870911(-536870912(sb))
    lprd   car, 536870911(-536870912(sb))
    lprd   fp, -536870912(536870911(sb))
    lprd   sb, -536870912(536870911(sb))
# 
#
# Immediate
#
# Immediate mode is legal only for operands of access class
# Any other use is undefined.
#
#
# Absolute
#
    lprd	us, @-64
    lprd	dcr, @-64
    lprd	bpc, @63
    lprd	dsr, @63
    lprd	car, @-65
    lprd	fp, @-65
    lprd	sp, @64
    lprd	sb, @64
    #
    lprd	usp, @-8192
    lprd	cfg, @-8192
    lprd	psr, @8191
    lprd	intbase, @8191
    lprd	mod, @-8193
    lprd	us, @-8193
    lprd	dcr, @8192
    lprd	bpc, @8192
    #
    lprd	dsr, @-536870912
    lprd	car, @536870911
#
# External
#
    lprd	us, ext(-64) + 63
    lprd	dcr, ext(-64) + 63
    lprd	bpc, ext(63) + -64
    lprd	dsr, ext(63) + -64
    #
    lprd	car, ext(-65) + 63
    lprd	fp, ext(-65) + 63
    lprd	sp, ext(63) + -65
    lprd	sb, ext(63) + -65
    lprd	usp, ext(-64) + 64
    lprd	cfg, ext(-64) + 64
    lprd	psr, ext(64) + -64
    lprd	intbase, ext(64) + -64
    #
    lprd	mod, ext(-8192) + 8191
    lprd	us, ext(-8192) + 8191
    lprd	dcr, ext(8191) + -8192
    lprd	bpc, ext(8191) + -8192
    #
    lprd	dsr, ext(-536870912) + 8191
    lprd	car, ext(-536870912) + 8191
    lprd	fp, ext(8191) + -536870912
    lprd	sp, ext(8191) + -536870912
    lprd	sb, ext(-8192) + 536870911
    lprd	usp, ext(-8192) + 536870911
    lprd	cfg, ext(536870911) + -8192
    lprd	psr, ext(536870911) + -8192
    #
    lprd	intbase, ext(-536870912) + 536870911
    lprd	mod, ext(-536870912) + 536870911
    lprd	us, ext(536870911) + -536870912
    lprd	dcr, ext(536870911) + -536870912
#
# Top Of Stack
#
    lprd    us, tos
    lprd    dcr, tos
    lprd    bpc, tos
    lprd    dsr, tos
    lprd    car, tos
    lprd    fp, tos
    lprd    sp, tos
    lprd    sb, tos
    lprd    usp, tos
    lprd    cfg, tos
    lprd    psr, tos
    lprd    intbase, tos
    lprd    mod, tos
#
# Memory space
#
    #
    # Frame Pointer (fp)
    #
    lprd	us, -64(fp)
    lprd	dcr, -64(fp)
    lprd	bpc, 63(fp)
    lprd	dsr, 63(fp)
    #
    lprd	car, -8192(fp)
    lprd	fp, -8192(fp)
    lprd	sp, 8191(fp)
    lprd	sb, 8191(fp)
    #
    lprd	usp, -536870912(fp)
    lprd	cfg, -536870912(fp)
    lprd	psr, 536870911(fp)
    lprd	intbase, 536870911(fp)
    lprd	mod, -64(fp)
    #
    # Stack Pointer (sp)
    #
    lprd	us, -64(sp)
    lprd	dcr, -64(sp)
    lprd	bpc, 63(sp)
    lprd	dsr, 63(sp)
    #
    lprd	car, -8192(sp)
    lprd	fp, -8192(sp)
    lprd	sp, 8191(sp)
    lprd	sb, 8191(sp)
    #
    lprd	usp, -536870912(sp)
    lprd	cfg, -536870912(sp)
    lprd	psr, 536870911(sp)
    lprd	intbase, 536870911(sp)
    lprd	mod, -64(sp)
    #
    # Static Memory (sb)
    #
    lprd	us, -64(sb)
    lprd	dcr, -64(sb)
    lprd	bpc, 63(sb)
    lprd	dsr, 63(sb)
    #
    lprd	car, -8192(sb)
    lprd	fp, -8192(sb)
    lprd	sp, 8191(sb)
    lprd	sb, 8191(sb)
    #
    lprd	usp, -536870912(sb)
    lprd	cfg, -536870912(sb)
    lprd	psr, 536870911(sb)
    lprd	intbase, 536870911(sb)
    lprd	mod, -64(sb)
    #
    # Program Counter (pc)
    #
    lprd	us, -64(pc)
    lprd	dcr, -64(pc)
    lprd	bpc, 63(pc)
    lprd	dsr, 63(pc)
    #
    lprd	car, -8192(pc)
    lprd	fp, -8192(pc)
    lprd	sp, 8191(pc)
    lprd	sb, 8191(pc)
    #
    lprd	usp, -536870912(pc)
    lprd	cfg, -536870912(pc)
    lprd	psr, 536870911(pc)
    lprd	intbase, 536870911(pc)
    lprd	mod, -64(pc)
#
# Scaled Index
#
    #
    # BYTE
    #
    # Register
    lprd   us, r7[r0:b]
    lprd   dcr, r6[r1:b]
    #
    # Register Relative
    lprd   bpc, 63(r5)[r2:b]
    lprd   dsr, 63(r4)[r3:b]
    lprd   car, 8191(r3)[r4:b]
    lprd   fp, 8191(r2)[r5:b]
    lprd   sp, 536870911(r1)[r6:b]
    lprd   sb, 536870911(r0)[r7:b]
    #
    # Memory Relative
    lprd   usp, -64(63(fp))[r0:b]
    lprd   cfg, -64(63(fp))[r1:b]
    lprd   psr, -8192(8191(fp))[r2:b]
    lprd   intbase, -8192(8191(fp))[r3:b]
    lprd   mod, -536870912(536870911(fp))[r4:b]
    lprd   us, -536870912(536870911(fp))[r5:b]
    lprd   dcr, -64(63(sp))[r7:b]
    lprd   bpc, -64(63(sp))[r6:b]
    lprd   dsr, -8192(8191(sp))[r5:b]
    lprd   car, -8192(8191(sp))[r4:b]
    lprd   fp, -536870912(536870911(sp))[r3:b]
    lprd   sp, -536870912(536870911(sp))[r2:b]
    lprd   sb, -64(63(sb))[r0:b]
    lprd   usp, -64(63(sb))[r1:b]
    lprd   cfg, -8192(8191(sb))[r2:b]
    lprd   psr, -8192(8191(sb))[r3:b]
    lprd   intbase, -536870912(536870911(sb))[r4:b]
    lprd   mod, -536870912(536870911(sb))[r5:b]
    #
    # Absolute
    lprd   us, @63[r0:b]
    lprd   dcr, @63[r1:b]
    lprd   bpc, @8191[r2:b]
    lprd   dsr, @8191[r3:b]
    lprd   car, @536870911[r4:b]
    lprd   fp, @536870911[r5:b]
    #
    # External
    lprd   sp, ext(-64) + 63[r7:b]
    lprd   sb, ext(-64) + 63[r6:b]
    lprd   usp, ext(-8192) + 8191[r5:b]
    lprd   cfg, ext(-8192) + 8191[r4:b]
    lprd   psr, ext(-536870912) + 536870911[r3:b]
    lprd   intbase, ext(-536870912) + 536870911[r2:b]
    #
    # Top Of Stack
    lprd   mod, tos[r0:b]
    #
    # Memory Space
    lprd   us, 63(fp)[r0:b]
    lprd   dcr, 63(fp)[r0:b]
    lprd   bpc, 8191(fp)[r0:b]
    lprd   dsr, 8191(fp)[r0:b]
    lprd   car, 536870911(fp)[r0:b]
    lprd   fp, 536870911(fp)[r0:b]
    lprd   sp, 63(sp)[r0:b]
    lprd   sb, 63(sp)[r0:b]
    lprd   usp, 8191(sp)[r0:b]
    lprd   cfg, 8191(sp)[r0:b]
    lprd   psr, 536870911(sp)[r0:b]
    lprd   intbase, 536870911(sp)[r0:b]
    lprd   mod, 63(sb)[r0:b]
    lprd   us, 63(sb)[r0:b]
    lprd   dcr, 8191(sb)[r0:b]
    lprd   bpc, 8191(sb)[r0:b]
    lprd   dsr, 536870911(sb)[r0:b]
    lprd   car, 536870911(sb)[r0:b]
    lprd   fp, 63(pc)[r0:b]
    lprd   sp, 63(pc)[r0:b]
    lprd   sb, 8191(pc)[r0:b]
    lprd   usp, 8191(pc)[r0:b]
    lprd   cfg, 536870911(pc)[r0:b]
    lprd   psr, 536870911(pc)[r0:b]
    #
    # WORD
    #
    lprd   us, r7[r0:w]
    lprd   dcr, r6[r1:w]
    #
    # Register Relative
    lprd   bpc, 63(r5)[r2:w]
    lprd   dsr, 63(r4)[r3:w]
    lprd   car, 8191(r3)[r4:w]
    lprd   fp, 8191(r2)[r5:w]
    lprd   sp, 536870911(r1)[r6:w]
    lprd   sb, 536870911(r0)[r7:w]
    #
    # Memory Relative
    lprd   usp, -64(63(fp))[r0:w]
    lprd   cfg, -64(63(fp))[r1:w]
    lprd   psr, -8192(8191(fp))[r2:w]
    lprd   intbase, -8192(8191(fp))[r3:w]
    lprd   mod, -536870912(536870911(fp))[r4:w]
    lprd   us, -536870912(536870911(fp))[r5:w]
    lprd   dcr, -64(63(sp))[r7:w]
    lprd   bpc, -64(63(sp))[r6:w]
    lprd   dsr, -8192(8191(sp))[r5:w]
    lprd   car, -8192(8191(sp))[r4:w]
    lprd   fp, -536870912(536870911(sp))[r3:w]
    lprd   sp, -536870912(536870911(sp))[r2:w]
    lprd   sb, -64(63(sb))[r0:w]
    lprd   usp, -64(63(sb))[r1:w]
    lprd   cfg, -8192(8191(sb))[r2:w]
    lprd   psr, -8192(8191(sb))[r3:w]
    lprd   intbase, -536870912(536870911(sb))[r4:w]
    lprd   mod, -536870912(536870911(sb))[r5:w]
    #
    # Absolute
    lprd   us, @63[r0:w]
    lprd   dcr, @63[r1:w]
    lprd   bpc, @8191[r2:w]
    lprd   dsr, @8191[r3:w]
    lprd   car, @536870911[r4:w]
    lprd   fp, @536870911[r5:w]
    #
    # External
    lprd   sp, ext(-64) + 63[r7:w]
    lprd   sb, ext(-64) + 63[r6:w]
    lprd   usp, ext(-8192) + 8191[r5:w]
    lprd   cfg, ext(-8192) + 8191[r4:w]
    lprd   psr, ext(-536870912) + 536870911[r3:w]
    lprd   intbase, ext(-536870912) + 536870911[r2:w]
    #
    # Top Of Stack
    lprd   mod, tos[r0:w]
    #
    # Memory Space
    lprd   us, 63(fp)[r0:w]
    lprd   dcr, 63(fp)[r0:w]
    lprd   bpc, 8191(fp)[r0:w]
    lprd   dsr, 8191(fp)[r0:w]
    lprd   car, 536870911(fp)[r0:w]
    lprd   fp, 536870911(fp)[r0:w]
    lprd   sp, 63(sp)[r0:w]
    lprd   sb, 63(sp)[r0:w]
    lprd   usp, 8191(sp)[r0:w]
    lprd   cfg, 8191(sp)[r0:w]
    lprd   psr, 536870911(sp)[r0:w]
    lprd   intbase, 536870911(sp)[r0:w]
    lprd   mod, 63(sb)[r0:w]
    lprd   us, 63(sb)[r0:w]
    lprd   dcr, 8191(sb)[r0:w]
    lprd   bpc, 8191(sb)[r0:w]
    lprd   dsr, 536870911(sb)[r0:w]
    lprd   car, 536870911(sb)[r0:w]
    lprd   fp, 63(pc)[r0:w]
    lprd   sp, 63(pc)[r0:w]
    lprd   sb, 8191(pc)[r0:w]
    lprd   usp, 8191(pc)[r0:w]
    lprd   cfg, 536870911(pc)[r0:w]
    lprd   psr, 536870911(pc)[r0:w]
    #
    # DOUBLE WORD
    #
    lprd   us, r7[r0:d]
    lprd   dcr, r6[r1:d]
    #
    # Register Relative
    lprd   bpc, 63(r5)[r2:d]
    lprd   dsr, 63(r4)[r3:d]
    lprd   car, 8191(r3)[r4:d]
    lprd   fp, 8191(r2)[r5:d]
    lprd   sp, 536870911(r1)[r6:d]
    lprd   sb, 536870911(r0)[r7:d]
    #
    # Memory Relative
    lprd   usp, -64(63(fp))[r0:d]
    lprd   cfg, -64(63(fp))[r1:d]
    lprd   psr, -8192(8191(fp))[r2:d]
    lprd   intbase, -8192(8191(fp))[r3:d]
    lprd   mod, -536870912(536870911(fp))[r4:d]
    lprd   us, -536870912(536870911(fp))[r5:d]
    lprd   dcr, -64(63(sp))[r7:d]
    lprd   bpc, -64(63(sp))[r6:d]
    lprd   dsr, -8192(8191(sp))[r5:d]
    lprd   car, -8192(8191(sp))[r4:d]
    lprd   fp, -536870912(536870911(sp))[r3:d]
    lprd   sp, -536870912(536870911(sp))[r2:d]
    lprd   sb, -64(63(sb))[r0:d]
    lprd   usp, -64(63(sb))[r1:d]
    lprd   cfg, -8192(8191(sb))[r2:d]
    lprd   psr, -8192(8191(sb))[r3:d]
    lprd   intbase, -536870912(536870911(sb))[r4:d]
    lprd   mod, -536870912(536870911(sb))[r5:d]
    #
    # Absolute
    lprd   us, @63[r0:d]
    lprd   dcr, @63[r1:d]
    lprd   bpc, @8191[r2:d]
    lprd   dsr, @8191[r3:d]
    lprd   car, @536870911[r4:d]
    lprd   fp, @536870911[r5:d]
    #
    # External
    lprd   sp, ext(-64) + 63[r7:d]
    lprd   sb, ext(-64) + 63[r6:d]
    lprd   usp, ext(-8192) + 8191[r5:d]
    lprd   cfg, ext(-8192) + 8191[r4:d]
    lprd   psr, ext(-536870912) + 536870911[r3:d]
    lprd   intbase, ext(-536870912) + 536870911[r2:d]
    #
    # Top Of Stack
    lprd   mod, tos[r0:b]
    #
    # Memory Space
    lprd   us, 63(fp)[r0:d]
    lprd   dcr, 63(fp)[r0:d]
    lprd   bpc, 8191(fp)[r0:d]
    lprd   dsr, 8191(fp)[r0:d]
    lprd   car, 536870911(fp)[r0:d]
    lprd   fp, 536870911(fp)[r0:d]
    lprd   sp, 63(sp)[r0:d]
    lprd   sb, 63(sp)[r0:d]
    lprd   usp, 8191(sp)[r0:d]
    lprd   cfg, 8191(sp)[r0:d]
    lprd   psr, 536870911(sp)[r0:d]
    lprd   intbase, 536870911(sp)[r0:d]
    lprd   mod, 63(sb)[r0:d]
    lprd   us, 63(sb)[r0:d]
    lprd   dcr, 8191(sb)[r0:d]
    lprd   bpc, 8191(sb)[r0:d]
    lprd   dsr, 536870911(sb)[r0:d]
    lprd   car, 536870911(sb)[r0:d]
    lprd   fp, 63(pc)[r0:d]
    lprd   sp, 63(pc)[r0:d]
    lprd   sb, 8191(pc)[r0:d]
    lprd   usp, 8191(pc)[r0:d]
    lprd   cfg, 536870911(pc)[r0:d]
    lprd   psr, 536870911(pc)[r0:d]
#
end:
