.psize 0, 143   # suppress form feed, 143 columns
#
# Test case for FORMAT 1 instructions
#
# Instruction under test: FLAG
#
# Syntax:   FLAG
#
#           !     FLAG      !
#           +---------------+
#           !1 1 0 1 0 0 1 0!
#           !-+-+-+-+-+-+-+-!
#           7               0
#
# The FLAG instruction activates the Flag Trap (FLG) if the F flag in the PSR is
# set. The Flag Trap passes control to the Flag service procedure. The return
# address pushed on the Interrupt Stack is the address of the FLAG instruction
# instruction itself. If the F flag is not set, program execution continues with
# next sequential instruction.
#
    flag
#
end:
